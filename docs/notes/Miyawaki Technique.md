---  
title: Miyawaki Technique  
source: None  
date: 2021-03-17 23:09  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Environment]] 

---

# Miyawaki Technique
**flashcard**{: #flashcard}{: .hash}  

- An afforestation technique to create urban forest.
- Layered approach - takes 2-3 years for small level.
- First found out  by Akira Miyawaki in 1980s.