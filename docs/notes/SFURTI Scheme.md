---  
title: SFURTI Scheme  
source: None  
date: 2021-02-25 04:33  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Economics]] , [[GS3]] 

---

# SFURTI Scheme
**flashcard**{: #flashcard}{: .hash}  

- Scheme of Fund for *Regeneration* of Traditional Industries
- Clusterise the local artisans for increasing their income and better management.