---  
title: Bamiyan Buddha  
source: None  
date: 2021-03-18 23:05  
tags: status/seed  
aliases: []  
---  
Category:: [[Buddhist Architecture]]

---

# Bamiyan Buddha
**flashcard**{: #flashcard}{: .hash}  

- In Bamiyan Valley of Afganistan, near Kabul.
- Confluence of Gupta, Sassanian(Iranian) and Helenistic(Greek) artistic styles
- Dates back to 5th Century AD
- Was once the tallest standing Buddhas in the world.
- Two Statues
	- Salsal - "Light of Sun"
	- Shamama -"Queen Mother"
- Destroyed by Taliban.