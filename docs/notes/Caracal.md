---  
title: Caracal  
source: None  
date: 2021-03-11 22:05  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Caracal
**flashcard**{: #flashcard}{: .hash}  

- medium-sized wild cat
- Native to Africa, Middle-East, [[Central Asia]], South Asia
- [[National Board for Wildlife]] - includes it into CRITICALLY ENDANGERED.
	- IUCN - Least concern
	- Wildlife Protection Act, 1972 - Schedule I
![[Pasted image 20210311220601.png]]