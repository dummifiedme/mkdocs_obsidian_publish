---  
title: Tso Kar  
date: 2021-06-24 18:16  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Tso Kar
- **Location** - ==Ladakh, Leh District==
- **Major species** - black-neched crane, saker falcon, snow leopard
- Declared a Ramsar Site in 2020
- Hypersaline (name relates to that)
- Stop over for [[Central Asian Flyway]]