---  
date: 2021-12-15 22:32  
aliases: []  
---  
# Lost Wax Technique
[[Drawing - Lost Wax Technique_20211215 231600.excalidraw.svg| -med -right]]
- Wax figures covered in clay and allowed to dry
- When heated, the wax flows out of a tiny hole.
- Molten metal is poured in, takes shape of the hollow clay.
- When cooled, solidifies.
- The clay mould is broken off to obtain a bronze sculpture.
- **Example** — Dancing Girl 





- - -
**References**
[[📕 NCERT - Fine Arts XI]]