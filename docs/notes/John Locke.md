---  
---  
> “For men, being all the workmanship of one omnipotent, and infinitely wise maker, they are his property, whose workmanship they are, made to last during his, not one another’s pleasure. ”
	> — *John Locke* **quote**{: #quote}{: .hash}  



> ”The state of nature has a law of nature to govern it which obliges  everyone: and reason, which is that law, leaches all mankind, who will but consult it, that being all equal and independent, no one ought to harm another in his life, health, liberty or possessions”
	> — *John Locke* **quote**{: #quote}{: .hash}  
 
