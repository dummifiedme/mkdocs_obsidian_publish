---  
title: Indian initiatives to fight Climate Change  
date: 2021-06-11 12:17  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims 
# Indian initiatives to fight Climate Change
**flashcard**{: #flashcard}{: .hash}  

- [[National Clean Air Programme (NCAP)]]
- [[Jawaharlal Nehru National Solar Mission]]
- Bharat Stage VI directly from Bharat IV
- [[National Action Plan on Climate Change (NAPCC)]]
- [[UJALA Scheme]] <!--SR:!2021-08-22,3,250-->