---  
title: Representation of UTs in RS  
source: None  
date: 2021-03-10 07:41  
tags: status/mature  
aliases: []  
---  
Category:: 

---

# Representation of UTs in RS

## Elected by 
Indirectly elected by members of an electoral college specially constituted for the purpose.

## System of election for RS
[[single transferable vote]].



## UTs that have representation in RS
**flashcard**{: #flashcard}{: .hash}  

Three out of nine:
- Delhi
- Puducherry
- Jammu & Kashmir <!--SR:!2021-09-25,4,270-->