---  
Title: Hybrid System of Democracy  
Source: SR Polity Lecture 2  
Tags: None  
Date: 2020-12-01 05:10  
NotesAddress: A1d  
---  
# Hybrid System of Democracy
## Hybrid System of Democracy
> Also called as Semi Presidential form
> Examples -> FRANCE, RUSSIA, SRI LANKA, AFGANISTAN

- New countries prefer hybrid system of democracy
- France is known as laboratory of political experiments.
	- Experimented with multiple firms of government.
	- PM is useless in French system.
		- Can be removed by President
		- Ca be removed by no confidence motion.
- [[Co-Habitation]] is linked to hybrid system.

**TODO**{: #TODO}{: .hash}  
 
- Charles de Gaulle