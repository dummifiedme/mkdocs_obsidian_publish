---  
title: Constitutional Bodies  
source: None  
date: 2020-12-22  
aliases: ['Constitutional Bodies']  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Constitutional Bodies

Those bodies that are established by the constitution.

```dataview
list 
where contains(type-of-body, "Constitutional Body") or contains(type-of-body, "constitutional body") or type-of-body=[[Constitutional Bodies]] 
```