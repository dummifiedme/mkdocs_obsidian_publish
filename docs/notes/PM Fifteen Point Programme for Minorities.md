---  
title: PM Fifteen Point Programme for Minorities  
source: None  
date: 2021-05-21 23:45  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# PM Fifteen Point Programme for Minorities
Basically, some important schemes of the government are amended/made amenable to favour minorities in a more populous area. Almost all the major schemes are part of it.

## Objectives
- Enhance education
- enhance employment, economic activities
- living conditions
- prevention of communal disharmony/violence