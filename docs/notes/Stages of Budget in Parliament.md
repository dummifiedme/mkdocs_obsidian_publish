---  
title: Stages of Budget in Parliament  
source: None  
date: 2021-03-17 17:10  
tags: status/seed  
aliases: []  
---  
Category:: [[Budget]] , [[Parliament]] 

---

# Stages of Budget in Parliament
**flashcard**{: #flashcard}{: .hash}  

1. Presentation of Budget
2. General Discussion
3. Scrutiny by departmental committees
4. Voting on [[Demands for Grants]]
5. Passing of Appropriation bill
6. Passing of Finance bill