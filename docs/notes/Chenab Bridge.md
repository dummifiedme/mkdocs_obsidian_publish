---  
title: Chenab Bridge  
source: None  
date: 2021-03-20 00:37  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Chenab Bridge
**flashcard**{: #flashcard}{: .hash}  

- World's Highest  rail bridge over [[Chenab River]] in Jammu And Kashmir.
- 359 meters in height.
- Part of Udhampur-Srinagar-Baramulla Rail link project.
- Blast proof bridge - DRDO involved.
- ![[Chenab Bridge Image.png]]