---  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Sara Aakash

- - -
Director:: Basu Chatterjee
Watched:: 2021-11-03
- - -

## Synopsis

This movie marked the directorial debut of the legendary and one of my absolute favourite director, Basu Chatterjee. This story is all about a nascent period of a newly wedded couple — the difficulty in aproaching each other, inability to accept the fact that they are married, inability to understand each other and various social elements in the household that surrounds a new woman. 

While the guy is unable to make peace with the fact that he has to leave the once-dreamt-of life of a ‘neta’ or a revolutionary for a stable settled life with a partner, the girl is unable to understand how to approach the guy. The frustrations of his personal ideals not being met, makes the guy repulse her in every walk of the day. The girl doesn’t really say anything and keeps on walking on the thin line between a estranged husband and a foul-mouthed household kins. Only the sister of her husband keeps her in company that too was short-lived as she had to go away with her husband. 

The depiction of tensions prevailing in the air is heavily apt with an exceptional pice of cinematography, which was rightfully awarded at the Filmfare as well. The use of sudden-jerk and motions in the shoot captures the moods and expressions of the protagonist well.

Though the girl accepts everything and keeps mum, the relationship in a typical household especially the education factor is spotlighted all over the story. The girl being educated and keen on reading texts is regularly bullied over petty issues citing her education as a curse. The girl loses her calm when she was doubted on for harmlessly looking out of the window while working as waiting for ‘someone’. It torments her and she spends rest of the day and the night crying on the roof (apparently everyone goes to the roof when they aren’t calm). 

It is finally on that night, the guy asks her whats wrong, and the girl bursts out in questions asking what is her fault? WHat did she do wrong? Why is he behaving like this? The guy breaks too, and comforts her. All of this took more than six months, and they finally spoke. The movie ends with ladies in the house exclaiming “bhaiya-bhabhi raat bhar chhat par rahe!”. THE END