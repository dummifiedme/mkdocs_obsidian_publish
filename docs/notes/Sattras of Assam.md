---  
title: Sattras of Assam  
source: None  
date: 2021-03-10 21:50  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Sattras of Assam
**flashcard**{: #flashcard}{: .hash}  

Monastic institutions as part of 16th century Vaishnavite reformist movement.
- religious, social and cultural reforms.
- *Worship though art* - [[Sankardeva]]
	- Music - Borgeet
	- Dance - Xattriya
	- Theatre - bhaona.