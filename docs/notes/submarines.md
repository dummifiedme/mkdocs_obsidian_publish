---  
title: submarines  
source: None  
date: 2021-03-17 21:18  
tags: status/seed  
aliases: []  
---  
Category::  

---

# submarines

## Types of Submarines
- Conventional Submarine
- Nuclear Submarine

## Nuclear Submarine
- Costly
- Bigger
	- Stealth reduces.

## Conventional Submarine
- Using diesel or fueled engines.
- Requires oxygen - has to come out to surface
	- Reduces Stealth.