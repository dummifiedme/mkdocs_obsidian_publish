---  
title: Discussion - God and Hinduism - Pandit Mishra Ji  
date: 2021-10-07 19:49  
tags: status/seed personal/discussion  
aliases: []  
---  
# Discussion - God and Hinduism - Pandit Mishra Ji
- The discussion started with me being Atheist. 
- According to him, Hinduism isn’t even a philosophy but rather a lifestyle. 
- He goes into various narration’s mentioned in the texts, but almost every time strayed away from the point of what I asked. 
- Nevertheless, I got to learn a few things which needs pondering upon. 
	- Read about various ‘darshans’ of Indian philosophy (or ‘lifestyle’) Bharat Darshan a book by Dutta (and someone).
	- Try meditation with no thoughts for about 40 minutes. It might take a long time, but will try. 
	- There are many little stories which need to be explored.
	- I also got an idea to form a flow chart on the question and answers and counter questions. 
		- There is a serious need to consolidate my questions and gather different answers to them - either from people or from the readings or from both.
- Try to see the reason behind the narrative stories and texts. What do they actually want to convey, or are they mere historical narrations?
- According to him, the learning’s aren’t lost, the people are just not at par with the commitment and quality that is required out of them to be capable to interpret it.
	- But then, why even bother to preach?
	- Why ‘menial’ men like us are supposed to follow these elaborate rituals if we can’t even get what we are promised to?
- The meaning of life, he says, is just to get out of the rut of cycle of life.
	- According to him, the child when inside the womb, is surrounded by his own excerpts and void - all darkness - and is aware of his past life. He keeps on preaching the God or almighty, asking and promising Him to search for the meaning of life or a way to find Him. 
	- But as soon as he wakes up into the world, he realised he is reborn as a human! And that’s when the child cries!
	- The child smiles when he sleeps, and cries when is awake. Why do you think this happens? Asks pandit ji.