---  
title: Diplomacy  
date: 2020-12-18 05:08  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR   

---
**review**{: #review}{: .hash}  
 
# Diplomacy


### What is Diplomacy? **definition**{: #definition}{: .hash}  
 **QnA**{: #QnA}{: .hash}  

- Practice of negotiation between representatives of states or group, so as to influence the decisions and conduct of foreign governments through 
	- dialogue
	- negotiation
	- other non-violent means.
- Generally by professionals aka DIPLOMATS, and on TOPICAL (specific) matters.]

#### Etymology of 'Diplomacy'
- derived from the 18th century French term *diplomate*
	-  based on the ancient Greek *diplōma*, which roughly means “an object folded in two"

[[Relation between the foreign policy and diplomacy]]

### Origins of Diplomacy
- 17th century Europe -- Origins
- 1961 *Vienna Convention on Diplomatic Relations*-> formal, accepted by most of the sovereign states

### Related Terms
- [[Diplomacy - Diplomatic Immunity]]
- [[Espionage]]

###  Diplomatic resolution of problems
- Arbitration and mediation
- Conferences
	-  Examples:
		-  Congress of Vienna (1815) -> after Napoleon to decide the fate of Europe
		-  Congress of Berlin (1878) -> in the wake of Russo-Turkish War, condition of Balkans
- Negotiations

[[Diplomacy - Types]]