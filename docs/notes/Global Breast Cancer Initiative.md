---  
title: Global Breast Cancer Initiative  
source: None  
date: 2021-03-18 22:59  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Global Breast Cancer Initiative
**flashcard**{: #flashcard}{: .hash}  

- Initiative by [[World Health Organization]]
- To reduce the global [[breast cancer]] mortality by 2.5% by 2040.
	- Primary focus on low-income countries.