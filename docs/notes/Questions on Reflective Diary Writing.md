---  
title: Questions on Reflective Diary Writing  
source: None  
date: 2021-04-07 16:56  
---  
---

# Questions on Reflective Diary Writing

- What do you mean by Reflective Diary Writing?
	- What do you reflect upon?
- How did you came across this term, let alone practicing as an hobby?
- In what ways do you think it has helped you?
	- In General
	- In the journey for till this interview.
- What are the other ways to journalling that you know of? 
	- How is this different from those other forms?
	- How does it suit you better?
- How long you have been doing it?
- What has been the single most effective outcome of this hobby?
- Why have you listed it as an hobby - when it could have been just something that you do? 
	> Like if you really do it daily and you feel its essential for you, then its just like food. (Silly way to put it, I hope you got the gist)
- Do you think your hobbies are a bit unusual? 
	>  How would you take it if someone in the panel says you have unusual hobbies?
- Does your writing involve poetry as well or just the prose?