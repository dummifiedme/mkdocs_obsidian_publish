---  
title: Special Majority  
date: 2020-12-22 22:11  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# Special Majority
**flashcard**{: #flashcard}{: .hash}  

A majority of total membership of that house and a majority of 2/3rd of those members present and voting.