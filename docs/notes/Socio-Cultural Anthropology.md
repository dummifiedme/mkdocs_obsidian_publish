---  
---  
# Socio-Cultural Anthropology
- 2.1 Nature of Culture
- 2.2 Nature of Society
    - [[Concept of society]]
    - [[Society and Culture]]
    - [[Social institutions]]
    - [[Social Groups]]
    - [[Social Stratification]]