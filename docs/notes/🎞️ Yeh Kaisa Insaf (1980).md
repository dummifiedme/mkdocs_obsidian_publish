---  
cover: https://m.media-amazon.com/images/M/MV5BMWJlOGVlZDItYjM1NC00OGIwLTliZmQtYTc5MjkzNzJhMjg2XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_SX300.jpg  
imdbId: tt0262918  
alias: ['Yeh Kaisa Insaf', 'Yeh Kaisa Insaaf (1980)', 'Yeh Kaisa Insaaf']  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi
# 🎞️ Yeh Kaisa Insaf (1980)

- - -
> [!movie]
> ![poster|-right](https://m.media-amazon.com/images/M/MV5BMWJlOGVlZDItYjM1NC00OGIwLTliZmQtYTc5MjkzNzJhMjg2XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_SX300.jpg)
> 
> **Director**:: Dasari Narayana Rao
> **Genre**:: "Drama"
> **Language**:: Hindi
> **IMDB Rating**:: 4.4
> **Year**:: 1980
> **Cast**:: "Vinod Mehra", "Shabana Azmi", "Sarika Thakur"
> **Watched**:: "2021-10-28"
> **Rating**:: 6
- - -

## Synopsis
**Plot**:: Hoping to impress his National Bank co-worker Sudha, Shankar recounts his story, right from his birth, as the lowly son of a servant in a once-wealthy Nagpur-based family consisting of Madhu, her brother, Sunder, and her widowed moth

The movie talks about status of women in a male dominated society and how taking care of families changes definitions depending on the gender of a person. The movie raises the question “Ye kaisa insaaf?” on the fact that a females’ family and her responsibility towards them is often neglected and she has to comply only with the husband’s family. She is supposed to take care of her husband’s family while completely turning a blind eye towards her own family of whom there is no one to take care of.

### Storyline

The movie begins with Shyam, a khajanchee at a bank, wooing Sudha (Kiran, played by Sarika). Shortly afterwards, the movie goes into a flashback and Shyam narrates the whole story of his godmother “Choti malkin”(Shabana Azmi), her sacrifices and later estrangement with her own husband over the issue of “Taking care of her family”.

Even in this movie, “Choti Malkin” marries a humble guy on a promise that she can spend/send her income to her family for there upkeep. Her family basically had her mom, her brother and a servants’ son (Shyam). 

At first, everything goes smoothly, but soon her husband had to give a hefty dowry for his sister’s wedding who was still ungrateful and kept on repeating the same thing throughout the story, “Bhaiya aur bhabhi toh dono kamate hain!”. As soon as he realises that it’s getting difficult to manage expenses, he asks his wife to bring her family to the same house and let the house be on rent. Her family was treated really badly and were equated to housemaids. 

Later on her brother cleared his  class XII examinations and wished for becoming a doctor, something his sister had already promised. But this required a lot of money. On top of it, her mom got cancer and another hefty amount was required for her therapy. These things finally broke the relationship and he forces her to move to Bombay with him leaving her family here itself. She refuses and decides to give up her daughter and her husband for the sake of her family.

Coming back to the time, it soon was revealed that the girl(Sudha) was none other than “Choti Malkin’s” own daughter. Shyam and Sudha get married on a condition that Shyam will live with her parents. Then in order to teach her father a lesson, they stage a scenario similar to what her mother had faced. 

The family reunites. THE END.