---  
title: Certificate of Origin  
source: None  
date: 2020-12-23  
aliases: ['ROO - CO', 'ROO - Certificate of Origin', 'CO']  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Certificate of Origin
- home country issues it
- to be produced at landing port

![[SS - Certificates of Origin (CO).png]]