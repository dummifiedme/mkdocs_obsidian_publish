---  
title: Sub-Mission on Agro Forestry Scheme  
source: None  
date: 2021-03-10 22:30  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Sub-Mission on Agro Forestry Scheme
**flashcard**{: #flashcard}{: .hash}  

- A part of National Agro Forestry Polity in 2014.
	- Launched after World Agro Foresty Congress held in Delhi in 2014.
- Main purpose - To encourage farmers to grow multi-purpose trees together with agro crops.