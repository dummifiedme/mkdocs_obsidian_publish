---  
title: Centre for Science and Environment  
source: None  
date: 2021-03-07 23:28  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Centre for Science and Environment
**flashcard**{: #flashcard}{: .hash}  

- Based in New Delhi
- Estd in 1980
- Non-Profit Organisation
- Function - Works as a think tank on environment-development issues in India.