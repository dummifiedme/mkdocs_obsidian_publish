---  
title: Territorial Constituencies  
source: None  
date: 2021-05-23 23:48  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Territorial Constituencies
**flashcard**{: #flashcard}{: .hash}  

- Purpose of holding direct elections
- Two Provisions
	- Each state is allotted a number of seats in the LS such that:
		> The ratio between the number an its population is the same for all states. 
		> (Not applicable to the states with POPULATION< 6 Millions)
	- Each state is divided into territorial constituencies such that
		> Ratio between the population of each constituency and the number of seats allotted to it is the same throughout the state.
	- "Population" : population ascertained at the preceding census.