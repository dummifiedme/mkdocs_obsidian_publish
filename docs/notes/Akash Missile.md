---  
title: Akash Missile  
source: None  
date: 2021-02-25 13:34  
tags: status/seed  
aliases: []  
---  
Category:: [[Missiles]] , [[International Relations]]

---

# Akash Missile
**flashcard**{: #flashcard}{: .hash}  

Short-Medium range surface to air missile