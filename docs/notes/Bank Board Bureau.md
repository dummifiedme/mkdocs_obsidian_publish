---  
title: Bank Board Bureau  
source: None  
date: 2021-03-11 15:16  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Bank Board Bureau
**flashcard**{: #flashcard}{: .hash}  

- Part of Indradhanush's 7 pillars
- makes recommendations for appointment of whole-time irector as well as non-eectuive chairpersons of Public Sector Banks (PSBs) and state-owned financial institutions.