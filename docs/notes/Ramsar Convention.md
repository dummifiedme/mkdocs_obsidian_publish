---  
title: Ramsar Convention  
source: None  
date: 2021-05-28 17:50  
tags: status/seed  
aliases: ['Convention on Wetlands', 'Ramsar Sites']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Ramsar Convention
> Ramsar Convention on **Wetlands** of **International Importance** Especially as **==Waterfowl Habitat==**
- Signed in ==Ramsar, Iran (near Caspian Sea)== in ==1971==
	- Effective since ==1975==
- COP every ==three== years.
- **2331** Ramsar Sites in the world; ==46== [[Ramsar Sites in India]]
	- Listed in the [[Montreux Record]]
- Non-binding
- Three commitments by the partner nations
	- Wise use of all wetlands
	- Designate wetlands of International Importance.
	- Cooperate internationally on Transboundary, Shared Wetlands.

---
**Related**
- [[Wetland]]
- [[Bird Life International]]
- [[IUCN]]
- [[Wetland International]]
- [[World Wide Fund for Nature (WWF)]]
- [[International Waterfowl and Wetland Trust]]