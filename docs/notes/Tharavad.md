---  
---  
# Tharavad
##### What is a Tharavad?
A malayalam word for "ancestral home".
Basically a system of common housing practiced by [[Namboothris of Kerala]] of Kerala.

##### What is the system of Tharavad like? What is the chain of authority?
- Eldest living male member holds authority
- The property divides if all the eldest members of a line demand so.
[[../../Library/_media/8kyih9dz.bmp]]
>A typical _tharavad_ reproduced from [[https://en.wikipedia.org/wiki/K._M._Panikkar "K. M. Panikkar"]]'s article published in 1918. Capital and small letters represent females and males respectively. Supposing that the females A, B and C were dead and the oldest male member _karnavar_ being d, if the male members t, k and others demanded partition, the property would be divided into three 



parts.