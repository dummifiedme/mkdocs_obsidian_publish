---  
title: Daily Editorial Discussions - OnlyIAS  
date: 2020-12-23  
tags: MOC list  
---  
# 095 OnlyIAS - Daily Editorial Discussions

> merely a list of topics discussed each day

### 2021-01-18 | Monday
> Link: https://www.youtube.com/watch?v=M0M-pn4G17E

- [[Need for a Crop Diversification Policy]]
- WhatsApp Privacy Policy
	- Not important per say.
	- 'Privacy' as a concept is important.  
- Covid Vaccination in India
	- Not so important.
	- Enough heard already

### 2021-01-16 | Saturday 
- [[Inter-faith marriages]]

## 2021 February
- [[Editorial Discussion - 2021 February]]

## 2021 March
- [[Editorial Discussion - 2021 March]]
 
 ## 2021 June
 ### 2021-06-30 | Wednesday
- Recusal of Judges
- Same Sex Marriage rights
- Women in Patriarchal world

 ## 2021 July
 - [[Editorial Discussion - 2021 July]]