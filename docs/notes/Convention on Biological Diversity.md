---  
---  
# Convention on Biological Diversity
![[CBD Screenshot.png]]
Two Supplementary Protocols  **todo**{: #todo}{: .hash}  
/digdeeper ^4c813d
- [[Cartegena Protocol]]
- [[Nagoya Protocol]]

**Related**
- [[Biodiversity Act (2002)]] enacted to ratify this convention.