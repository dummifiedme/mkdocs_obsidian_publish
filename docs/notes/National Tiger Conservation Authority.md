---  
title: National Tiger Conservation Authority  
source: None  
date: 2021-05-27 16:05  
tags: status/seed  
aliases: ['NTCA']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# National Tiger Conservation Authority
- Established under ==[[Wildlife Protection Act]], 1972==
- ==Statutory== Body under ==[[Ministry of Environment, Forest and Climate Change|MoEFCC]]==
- **Aim**: to strengthen tiger conservation in India
- Launched "[[Project Tiger]]" in ==1973==
- Recommends the formation of [[Tiger Reserves]].

- Headed by :: Union Environment Minister