---  
title: Foreign Contribution (Regulation) Act  
source: None  
date: 2021-05-08 21:15  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Foreign Contribution (Regulation) Act
**flashcard**{: #flashcard}{: .hash}  

- FCRA 1976, amended in 2010
- to regulate foreign donations, to ensure they don't adversely effect internal security.
- applicable to all associations, groups and NGOs which intend to receive foreign donations.

### Who cannot receive the foreign donations? (FCRA)
**flashcard**{: #flashcard}{: .hash}  
 
Members of legislature and political parties, government officials, judges and media persons are prohibited from receiving foreign contributions.