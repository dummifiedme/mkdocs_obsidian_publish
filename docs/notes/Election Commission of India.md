---  
title: Election Commission of India  
source: None  
date: 2021-05-30 17:55  
aliases: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Election Commission of India
- Type of Body:: [[Constitutional Bodies]]
- Article 324
	- power of superintendence
	-  power of direction
	-  Power of control
    Of elections to 
    	- Parliament
    	- State Legislatures
    	- The office of President
    	- Office of VP
- PERMANENT and INDEPENDENT 
- Not responsible for elections to Panchayats and municipalities.
	- State Election Commissions.

## ECI - Composition
- Article 324 defines the composition
	- President decides the number of ECrs, appoints them, appoints regional commissioners on advice of EC and decides the tenure, salary and condition of service of them.
- All ECrs receive same salary, same conditions of service and same tenure.
	- The salary is equivalent to the salary of judge of Supreme Court.
	- 6 years term | 65 years
	- Can be removed before term, resign as well.

---

1. **By which amendment the voting age was reduced to 18 from 21 and when?**
**flashcard**{: #flashcard}{: .hash}  

	- [[CAA 61]] **CAA**{: #CAA}{: .hash}  

	- 1989

2. **When and why did the number of ECrs increased from a single CEC?**
**flashcard**{: #flashcard}{: .hash}  

	- It was increased from one CEC to a total of three (CEC + 2ECrs).
	- Due to increased load on the election commision with reduction of voting age from 21 to 18 years.
	- The year was *1989, 16th of October*.

---

## ECI - Independence
- CEC is provided with a secure tenure.
	- Cannot be removed except in the same manner as prescribed for [[Removal of SC Judge]]
	-