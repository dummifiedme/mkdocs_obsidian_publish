---  
title: Biotechnology Industry Research Assistance Council  
source: None  
date: 2021-03-11 21:59  
tags: status/seed  
aliases: ['BIRAC']  
---  
Category::  

---

# Biotechnology Industry Research Assistance Council
**flashcard**{: #flashcard}{: .hash}  

- not-for-profit Public Sector Enterprise
- Set up by [[Department of Biotechnology (DoBT)]]