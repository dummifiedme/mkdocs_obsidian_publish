---  
cover: https://m.media-amazon.com/images/M/MV5BMWRiZGQ1NDMtODQ2OS00MDlhLWJkZGYtM2ZmNjlhZThjOWRmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg  
imdbId: tt6467266  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie
# 🎞️ Sing 2 (2021)

- - -
![poster|-right](https://m.media-amazon.com/images/M/MV5BMWRiZGQ1NDMtODQ2OS00MDlhLWJkZGYtM2ZmNjlhZThjOWRmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg)

**Director**:: "Garth Jennings"
**Genre**:: "Animation", "Adventure", "Comedy"
**IMDB Rating**:: 7.6
**Year**:: 2021
**Cast**:: "Matthew McConaughey", "Reese Witherspoon", "Scarlett Johansson"
**Watched**:: "2022-01-09"
**Rating**:: 5
- - -

## Synopsis
**Plot**:: Buster Moon and his friends must persuade reclusive rock star Clay Calloway to join them for the opening of a new show.