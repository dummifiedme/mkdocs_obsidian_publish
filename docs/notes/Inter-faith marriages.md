---  
title: Inter-faith marriages  
source: None  
date: 2021-01-18 22:45  
tags: status/seed  
aliases: []  
---  
Category:: [[Burning Issues]]

---

# Inter-faith marriages
- Governed by ==[[Special Marriage Act, 1954]]==.
	- Required a compulsory 30-day notice to DM before an inter-faith marriage.
	- Published for public to raise objections , if any.
- A women sought protection under ==Right to Privacy== (following the judgement of ==[[Puttaswamy Case]]==) which was being curtailed upon by the "public notice".
- Court ruled to convert the notice to be ==optional and not-mandatory==,
- [[Love Jihad Laws]]