---  
---  
# 🎞️ Uphaar
- - -
**entertainment**{: #entertainment}{: .hash}  
/telefilm/doordarshan
Story by:: Arun Sadhoo
Watched:: 2021-10-28
- - -

The telefilm talks about appearances and the preconceived notions we hold of people coming across in life. We tend to equate all the apparently similar cases as the same, and it makes a lot of difference in our behaviour to a person or a situation. Sometimes it might be a good assessment, but what if it is not? It is good to be cautious about people or situations, but is it viable at the cost of misunderstanding a human being? 

## Storyline
The story starts with a suited senior officer scrutinising some reports, and he receives a call from someone claiming to be an old colleague. The protagonist apprehends it as yet another person trying to encash his success by duping him of money or services. He makes a small plan with a friend to have him called away at the time of the meeting with the person. During the meet as well, he is constantly thinking about what the person wants from him. All sort of selfish notions wander his thoughts, but the person doesn’t really ask anything, rather he keeps discussing and asking about his life, his children and his wife. Later, as planned, he gets a call and leaves the person (Mr Roy) waiting to never come back again.

Next morning, Mr Roy, visits protagonists house very early in the morning and waits for him in his lawn. Here too, the protagonist keeps trying to get rid of him assuming he might dupe him of something. His wife too becmomes a bit apprehensive, but upon meeting him, she  goes a bit soft. She lets him call overseas to his daughter, which is frowned upon by the protagonist as an unnecessary expense, albeit on the company account. 

Later the guy leaves, and when the friend asks him about Mr Roy, the protagonist laughs it out that it was a peasy transaction — just an overseas call and a breakfast —  on which the friend gives a laugh and comments “Saste mein chhoot gaye sir”. 

When the guy reaches home, he receives a package from Mr Roy. The package had a cheque of Rs 500 (a lot for that time!), a saree and perfume for his wife, a shirt for him, and a card for his daughter. 

This comes as a shock to him, he now goes into remorse and realises that all Mr Roy wanted was love and affection, and he went cheap even in giving out love. Before the curtain falls, he says to his wife, “Never forget these gifts (Uphaar) ever”. THE END.