---  
title: Issues in Anti-Defection Law  
source: M Puri  
date: 2021-05-30 17:49  
aliases: []  
tags: status/mature  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Issues in Anti-Defection Law

1. The speaker decision being final
2. [[Kohoto Hollahan Case]]
	- Speaker decision is subject to judicial review
3. Whip gagging or Whip culture.
	-  Implication:
		1. Legislator act according to diktats of the party high command as opposed to their conscience. It also compromises their ability to voice the interest of their voters.
		2. The PM and CoM are accountable to the Parliament. The Anti-Defection law reverses this position.
		3. The ruling party need to only convince leader of political party to get their bill through.  
4. The inclusion of coalition partner under the anti defection law. It has been suggested by the [[Administrative Reforms Commission II]] as instability in government is also a function of coalition partners leaving the formation.
5. Role of Election Commission
	- Ideally EC should be authorized to decide cases under the Schedule 10 much like its power under Article 103.
6. The law is silent on Anti-Party activities outside legislature. Such members can be expelled by the party, yet they would not attract disqualification and would be allowed to continue as unattached member.
	- They should formally be considered as members of parent political party and would attract anti-defection law only when they formally resign.


```dataview
task
from **note**{: #note}{: .hash}  
/upsc/GS2/polity 

``````