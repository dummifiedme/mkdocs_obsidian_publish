---  
---  
# Clan
##### What are Clans?
A unilineal kin group larger than a lineage.
When lineages grow, they split under a single clan.

##### Are the relationships or lines of descent clearly defined in a Clan?
No. 
The actual lineage cannot be established by a [[Genealogical Table]].
Descent is traced to a mythical ancestor, can be human, plant, animal or an inanimate object and called as *[[totem]]*

##### What is the nature of marriage in Clans?
**flashcard**{: #flashcard}{: .hash}  

Exogamous Marriages