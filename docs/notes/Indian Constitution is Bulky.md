---  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Indian Constitution is Bulky
## Factors Responsible
1. Incorporated the accumulated experience, the working of several constitution
2. It is more full of words to avoid ambiguity and excessive litigation.
3. One constitution for the entire country.
4. To incorporate the diversity of country
5. GoI Act 1935 was itself a bulky document.
6. It carries both justifiable and non-justifiable rights, also FD also adds to the bulk.
7. Union state relations are dealt with elaboration.
8. Most constitutions contain fundamental principles, Indian constitution carries the details of administration as well.
	1.  e.g Details of account audits, languages, civil services etc.