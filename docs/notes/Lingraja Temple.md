---  
title: Lingraja Temple  
source: None  
date: 2021-03-18 23:15  
tags: status/seed  
aliases: []  
---  
Category:: [[+ Temples]]

---

# Lingraja Temple
**flashcard**{: #flashcard}{: .hash}  

- Built by Somvansi Dynasty - from 9th to 12th century.
- syncretisation of Shaivism and Vaishnavism sects in Odisha.
- Presiding deity in the temple is known as Hari-Hara.	
	- Hari - Vishnu
	- Hara - Shiva
- Bindusara Lake nearby.
- In ODISHA.