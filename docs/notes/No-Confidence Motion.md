---  
title: No-Confidence Motion  
source: None  
date: 2021-03-10 22:23  
tags: status/seed  
aliases: []  
---  
Category:: [[Motions]] 

---

# No-Confidence Motion
**flashcard**{: #flashcard}{: .hash}  

- Rule 198 of [[Rules of Procedure and Conduct of Business of Lok Sabha]] specifies the procedure for moving a no-confidence motion.
	- Only in Lok Sabha, not in RS.
- Any member of house can move it.
	- No need for a reason
	- at least 50 member support.
	- report before 10 AM - written.
- After this, 10 days allotted to the government - prove confidence.
> Related : Article 75 [[75#^dafd18]]
![[SS Infographic - No-ConfidenceMotionInfoGraphics.png]]