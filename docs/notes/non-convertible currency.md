---  
date: 2022-01-11 13:17  
aliases: ['non-convertible currency']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics/terms 
# non-convertible currency
**Definition**:: A non-convertible currency is one that is used primarily for domestic transactions and is not openly traded in the forex (FX) market. This is usually the result of government restrictions, which prevent it from being exchanged for foreign currencies. A non-convertible currency is commonly known as a "blocked currency."


- - -
## References
- [b] Ref:: [Non-Convertible Currency Definition](https://www.investopedia.com/terms/n/nonconvertiblecurrency.asp)


[[Library/Assets/non-convertible currency_20220111 134741.excalidraw.md]]