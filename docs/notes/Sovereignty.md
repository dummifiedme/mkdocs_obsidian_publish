---  
---  
# Sovereignty
---
title: Sovereignty
source:
date: 2021-02-27 19:52
tags: status/mature note/vocab 
aliases: [ ]
---

**note**{: #note}{: .hash}  
/upsc/GS2/polity 

---

**Sovereignty**
**flashcard**{: #flashcard}{: .hash}  
/R
It means the *independent authority* of a state. It means that the state has the *authority to legislate on any subject* and that it is not subject to the control of any other State or external power.