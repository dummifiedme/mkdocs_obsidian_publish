---  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Biraj Bahu (1954)
- Mostly about a pati-vrata lady, Biraj. 
- effectively depicts
	-  the family problems in the rural areas
	- Rural philosophies and beliefs of mid-20th century India
- good story
- Hrishikesh Mukherjee was editor.

!!! note "Fun Fact"
	Bajre da Sitta song music found in this movie!,
