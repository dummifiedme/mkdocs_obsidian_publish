---  
---  
# Critique of Constituent Assembly and Constitution
## Critique of Constituent Assembly and Constitution

---

|             Source             |           Tags           | Source | 
|:------------------------------:|:------------------------:|:------ :|
| [[../MPuri GS2 Notes]] | **note**{: #note}{: .hash}  
/upsc/GS2/polity **mPuri**{: #mPuri}{: .hash}  
 **2020**{: #2020}{: .hash}  
-12-05 | #📝/1  | 

---

### 1. Constitution was unrepresentative
**Debunking the Criticism:**
- Why not a freshly elected representatives?
	- Congress originally demanded a directly elected constitutional assemble but dropped this demand as this would delay the creating of the assembly.
- The difference between voice and opinion representation:
	- Physical representation for all the sections was not feasible yet ==every shade of public opinion was represented==.
- The draft constitution was placed in public domain. Submissions were invited and several were considered and some also incorporated.
- The members also adopted the tool of public reason and emphasized larger public interest over sectional interest.
- The constitution ==reflects will of many== rather than the needs of the few.
--- 

### 2. Constitutional assembly was congress dominated
- Close to 82% of the members of assembly belong to congress
- **Debunking the Criticism**
	- Congress never represented a single coherent ideology but was a broad church incorporating in its fold people belonging to all shades of political ideology.

---

### 3. Constituent Assembly was Hindu dominated
**Debunking the Criticism**
- The right wing groups were critical of constitution on account of secular nature and that it did not incorporate elements of ancient Hindu polity.

---

### 4. Constitution was un-Indian and slavish imitation of the west
**Debunking the criticism**
- Fundamentals of secular democratic polity were more or less settled and thus the Indian constitution could not fabricate fresh ideals.
- A constitution prepared so late in the day can defer from the others in  details not in fundamentals.
- No nation has an inherent patent right over constitutional ideals.
- The fundamentals of constitution were not discovered overnight but were vigorously debated during the course of freedom struggle.
- The objective of the assembly was to prepare a workable constitution not an ideal one.
- The adaption was intelligent.
- > "Calling the Constitution un-Indian amounts to measuring it against an undefined yardstick" - Granville Austin
- That is a slavish imitation of west is a belief held by those who have set the British spread myth that the constitution was the culmination of a process that the British started in 1861.

---

### 5. Complex language and Elephantine
> Elephant also represent the constitution as a symbol (officially)
- Lawyers Paradise
- Complicated language
- Elephantine in Size

---

### 6. Constitution is typically un-Gandhian
- The congress was Gandhian in its approach towards the national movement and typically un-Gandhian w.r.t. post independence Indian polity and economy.
- Gandhian ideology has ==not== been completely sidelined. 
	- Provision with respect to untouchability. 
	- Affirmative Action
	- Prohibition
	- Cow Slaughter.
---

### 7. Not socialist (like soviets)
- Communist were opposed to parliamentary democracy they wanted soviet style state architecture and socialist were unhappy with recognition of private property and wanted  a categorical pronouncement^[special provision for nationalization of important sector] in favor of nationalization.