---  
title: Protected Areas under WPA  
date: 2021-06-29 21:12  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Protected Areas under WPA
(types)
**flashcard**{: #flashcard}{: .hash}  

Five types of protected areas under the Act:
- [[National Parks]]
- [[Wildlife Sanctuaries]]
- Conservation Reserves and Community Reserves
- [[Tiger Reserves]]
- [[Marine Protected Areas]]

[[Diagram - Relations - Protected Areas under WPA.excalidraw|600]]

---

- [B] **Related**: 

- [[Objectives of the protected areas under WPA]]
- Eco-Sensitive Zones are defined under :: [[Environment Protection Act (1986)]]