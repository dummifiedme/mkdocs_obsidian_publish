---  
---  
# Kinship
##### What is a Kinship?
Kinship is the relationship between persons by blood or marriage.
- [[Family of Orientation]]
- [[Family of Procreation]]

##### What are the types of kinship?
- [[Consanguineal Relationship]]
- [[Affinal Relationship]]