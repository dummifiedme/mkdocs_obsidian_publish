---  
title: Partition of Bengal Province  
source: None  
date: 2021-05-20 01:57  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Partition of Bengal Province
- Bengal province was partitioned in :: 1905 (formal date was 16th October).
<!--SR:!2021-10-01,30,270-->
- The Bengal Partition was proposed during the reign of Viceroy :: Lord Curzon
<!--SR:!2021-09-09,5,230-->
- The partition of Bangal was *annulled* in the year :: 1911
<!--SR:!2021-09-06,13,250-->
	- Orissa and Assam were carved out of the Bengal Province.
	- Capital was shifted to Delhi to :: appease the gruntled Muslims.
<!--SR:!2021-09-30,29,270-->

#### The Surat Split
[[Surat Split]]

- BG Tilak was in Burma Jail from :: 1908-1914. 
<!--SR:!2021-09-11,18,270-->
	- Tilak was charged with sedition for a piece written in his newspaper :: Kesari.
<!--SR:!2021-09-14,21,270-->

##### Government policies on the revolutionaries
- Carrot and Stick policy
- Rallying
- Basically, three pronged approach of repression-conciliation-suppression.

#### Morley-Minto Reforms of 1909
[[Indian Councils Acts#Indian Councils Act of 1909]]

- Simla Deputation :: a group of muslim elites. Later took over [[All India Muslim League]]
<!--SR:!2021-09-13,20,270-->
	- In October 1906, Simla Deputation met Lord Minto under the leadership of :: Agha Khan.
<!--SR:!2021-09-29,28,250-->