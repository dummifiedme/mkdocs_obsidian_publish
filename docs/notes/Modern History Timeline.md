---  
title: Modern History Timeline  
source: None  
date: 2021-03-19 14:46  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history
# Modern History Timeline

- 1498 - Vasco De Gama came to India
  - Calicut
- 1595 - Dutch arrival in India
  - Pulicat
- 1600 - Queen Elizabeth I signs Charter to English company to trade with the east.
- 1608 - British arrive in India
  - Surat
- 1616 - Danish arrive in India
  - Traquebar
- 1651 - First British factory in Hugli
- 1664 - French arrive in India
  - Pondicherry
- 1707 - Auranzeb dies.
  - Bahadur Shah I becomes new Mughal Ruler.
- 1713 - Farruksiyar becomes the Mughal Ruler
- 1717 - Murshid Quli Khan became the first Nawab of Bengal - till 1727.
- 1740 - Second strong Nawab of Bengal Alivardi Khan reigns power.
- 1756 - Third strong Nawab of Bengal Siraj-ud-Daula assumes power.
  - Captures Fort William, Calcutta from the British.
  - [[Robert Clive]] arrives and leads an army against Siraj-ud-Daula.
    - Robert Clive made a pact with the commander of Siraj-ud-Daula.
- 1757 - Battle of Plassey, Britain wins the battle - British rule starts.
  - Siraj-ud-Daula killed by Mir Jafar while trying to flee on a camel.
  - Mir Jafar was made the puppet Nawab
  - Robert Clive became the Governor of Bengal.
- 1758 -  Robert Clive starts the system of Residency.
  - Appointed [[Warren Hastings]] as resident in Bengal Court.
- 1760 - Mir Qasim was made the puppet Nawab when Mir Jafar objected.
- 1764 - Mir Jafar made the Nawab again upon the suicide of Mir Qasim.
  - Battle of buxar won by British, Shah Alam II lost.
- 1765 - Mir Jafar dies
  - Robert Clive introduces double system of governance.
    - Remained till 1772.
  - Shah Alam II gave Diwani to the Company - upon losing the Battle of Buxar.
- 1770 - The Great Bengal Famine.
- 1771 - Warren Hastings became Governor of Bengal - like Clive was in 1757.
- 1772 - [[Hastings' Judicial Plan]].
- 1773 - [[Regulating Act of 1773]]
  - Warren Hastings became the First Governor General of India
  - Supreme Court was established in Calcutta.

## Britain vs France in India

### Causes for British success and French failure
**flashcard**{: #flashcard}{: .hash}  

1. Control
   - British were a private company - instant decision and enthusiasm, self-confidence
   - French EIC was a state company - government policies and delays in decision making
2. Superior navy with British - helped cut French sea links and support
3. Centers of control
   - British - Madras, Bombay and Calcutta
   - French - Pondicherry only.
4. Ambition - French interested in territories -> short on funds.
5. Good Commanders on British side, French had only Dupleix
<!--SR:!2021-09-08,13,250-->

[[Danish East India Company]]
[[Reasons for British success over other Europeans in India]]