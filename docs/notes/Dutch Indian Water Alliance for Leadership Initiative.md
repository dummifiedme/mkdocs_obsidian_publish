---  
title: Dutch Indian Water Alliance for Leadership Initiative  
source: None  
date: 2021-03-07 22:55  
tags: status/seed  
aliases: []  
---  
Category:: [[India-Netherlands]] , [[Keywords GS3]]  

---

# Dutch Indian Water Alliance for Leadership Initiative
**flashcard**{: #flashcard}{: .hash}  

> DIWALI
- Consortium of experts from two countries (India-Netherlands) would explore sustainability of Dutch solutions to resolve issues related to water.