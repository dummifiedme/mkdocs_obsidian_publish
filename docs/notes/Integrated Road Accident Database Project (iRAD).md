---  
title: Integrated Road Accident Database Project (iRAD)  
source: None  
date: 2021-05-08 21:07  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Integrated Road Accident Database Project (iRAD)
**flashcard**{: #flashcard}{: .hash}  

- Central accident database management system.
- Developed by IIT Madras
- Implemented by [[National Informatics Centre]]
- Project supported by World Bank - Rs.  258 crore.
- Pilot basis in six states - Karnataka, MP, Maharashtra, Rajasthan, TN, UP
- <!--SR:!2021-08-17,1,230-->