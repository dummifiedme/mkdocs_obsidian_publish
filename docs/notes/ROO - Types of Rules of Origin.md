---  
title: ROO - Types of Rules of Origin  
source: [['Rules of Origin']]  
date: 2020-12-23 23:17  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# ROO - Types of Rules of Origin
1. Non-preferential Rules of Origin
    - anti-dumping
    - counter-vailing
    - quotas
    - "made in" labels
2.  Preferential Rules of Origin
    - reciprocal trade preferences
        - regional trade agreements
             - Includes issueing of [[Certificate of Origin]] (CO)
        - customs union
    - non-reciprocal trade preferences
        - in favour of developing countries or [[Least-Developed Countries) (LDCs]].
- Preferential rules are more restricive than non-preferential rules. **doubt**{: #doubt}{: .hash}  
