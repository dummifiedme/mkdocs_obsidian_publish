---  
title: RORO  
source: None  
date: 2021-05-08 19:48  
tags: status/seed  
aliases: []  
---  
Category::  

---

# RORO
**flashcard**{: #flashcard}{: .hash}  

- Roll-On-Roll-Off Service
- Railway service where road transport vehicle are loaded on trains as is.
- Various advantages
	- Helps reducing carbon footprint
	- drivers avoid long distance driving
	- Saves fuel
	- Reduces Congestion

## When and where was RORO train service first introduced in India?
**flashcard**{: #flashcard}{: .hash}  

Konkan Railways in 1999
> While RORO ferry first time between Ghogha in Bhavnagar and Dahej in Bharuch - [[Gulf of Cambay]].