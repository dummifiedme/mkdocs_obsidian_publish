---  
title: Nurturing Neighbourhoods Challenge  
source: None  
date: 2021-02-21 09:39  
tags: status/draft  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# Nurturing Neighbourhoods Challenge
**flashcard**{: #flashcard}{: .hash}  

- MoHUA - 25 cities - under Smart Cities Mission.
- Three-year Initiative.
- Collaboration with Bernard van Leer Foundation and World Resources Institute (WRI) India.
- Focus on early childhood development (0-5 yo) <!--SR:!2021-10-12,21,250-->