---  
title: PM Urja Ganga Project  
source: None  
date: 2021-02-25 15:40  
tags: status/seed  
aliases: []  
---  
Category::  

---

# PM Urja Ganga Project
**flashcard**{: #flashcard}{: .hash}  

- Natural Gas Pipeline
	- Cooking gas
	- CNG
- Launched in 2016 in Varanasi
- ![[Urja Ganga Map.png]]
- Implemented by GAIL