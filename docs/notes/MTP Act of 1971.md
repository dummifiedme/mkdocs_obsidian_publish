---  
title: MTP Act of 1971  
source: None  
date: 2021-02-13 00:37  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/social-issues/women 
# MTP Act of 1971
- Medical Termination of Pregnancy
- Framed in context of reducing the maternal mortality ratio due to unsafe abortions.
- Issues with the Act
	- Law is framed not to respect a woman's right over her own body.

## Issues with the amendment bill
- Continues the legacy of hetero-patriarchal population control.
- Still requires doctors to sign off
- Personal choice has no place in the bill
- The act or the bill require doctors and trained gynaecologists, but there is 75% shortage of them in the community health centers in rural areas.
	- Hence no safe option for termination of pregnancy even after permission.