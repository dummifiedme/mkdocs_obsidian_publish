---  
---  
(of a method of persuasion or coercion) characterized by both the offer of reward and the threat of punishment: *a carrot-and-stick approach.*