---  
title: Membership of the Parliament  
source: None  
date: 2021-03-10 18:57  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Membership of the Parliament
## [[Qualifications for the being a MP]]
## [[Disqualifications for a MP]]
## [[Vacating of the Seats in Parliament]]
## [[Oath or Affirmation by MPs]]
## [[Salaries and Allowances for MPs]]