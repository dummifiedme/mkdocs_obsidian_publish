---  
title: Hokera Wetlands  
date: 2021-06-24 18:12  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3   
# Hokera Wetlands
**flashcard**{: #flashcard}{: .hash}  

Near Srinagar, JK
- Ramsar Site since 2005
- Only reedbeds in Kashmir
- Pathway to water fowl species coming from Siberian region.