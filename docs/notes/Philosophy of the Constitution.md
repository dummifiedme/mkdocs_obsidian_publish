---  
---  
# Philosophy of the Constitution

ANKI Export: No
Paper: GS2
Status: Doing
Subject: Polity

# Philosophy of the Constitution

- Constitution as Means of Democratic Transformation
    - Constitution provides basic rules to continuously check the tendency of the states to harm the interest of atleast some individuals and groups.
        - Therefore prevent states from turning tyrannical.
    - Constitution provide peaceful, democratic means to bring about social transformations.
    - Constitutions embody and announce the first real exercise of political
    self-determination.
    - Nehru's views on the Constituent Assembly
        - Constituent Assembly represented a collective demand for full self
        determination.
            - Only elected representatives of Indian people had the right to
            frame India's Constitution — without external interference.
        - Constituent Assembly is not just a body of people or gathering of lawyers.

            > "Constituent Assembly is a nation on the move, throwing away the shell of it's past political and possibly social structure and fashioning for itself a new garment of its own making." - Nehru.

Constitution was designed to break the shackles of traditional social hierarchies and to usher in the new era of freedom and equality.

- Constitution exist not only to limit people in power but to empower those who traditionally have been deprived of it.

Constitutions can give vulnerable people the power to achieve collective good.
Why do we need to go back to the Constituent Assembly and its debates?
Since, it's part of a recent past, unlike American Constitution which was framed in 18th century.
A history of our Constitution is still very much the history of the present.
Moreover, there are high chances of forgetting the important underlying principles on which the Constitution was formed.
When the going gets good, the forgetting is harmless.
But then these fundamental blocks are under threat, neglect of those principles could be very harmful.
Therefore, to hold and grasp the value and notion behind the philosophy of the Constitution.
What is the the political philosophy of Indian Constitution? It is committed to:
 Freedom
 Equality
 Social Justice
 Respect for diversity and minority rights.  Secularism
 Universal Franchise
 Federalism
 Some form of national unity.
Philosophy of the Constitution 2

Comment about 'individual freedom' as a commitment of Indian Constitution.
Result of continuous intellectual and political activity of well over a century.
Raja Ram Mohun Roy → protested against freedom of press — as means to communicate people's need — hence important.
Rowlatt Act — national movement opposed it vehemently.
individual freedom at the core of all the bills, scheme, resolution of INC even since 40 years before the Constitution.
Therefore, Freedom of expression integral part of Indian Constitution. — (not surprising)
Strong liberal character, Fundamental Rights strongly reflect freedom. Comment about 'equality' as a commitment of Indian Constitution.
**todo**{: #todo}{: .hash}  
/open
Describe the idea of Classical Liberalism.
Classical Liberalism always privileges rights of the individuals over the demands of social justice and community values.
How is the idea of Indian Liberalism, enshrined in the Constitution, is different from the Classical Liberalism? **todo**{: #todo}{: .hash}  
/open
The Liberalism of Indian Constitution is different in two ways :  It was always linked to social justice.
But, at the base of all the things and ideas, there's a definitive emphasis on peace and democratic practices.
Classical Liberalism: Individual Rights > Social Justice OR Community Values.
Philosophy of the Constitution 3
💡
💡

Example: Provisions of reservations for SC, STs in legislature as well as in the public sector.

What are the two streams of Indian Liberalism according to K. M. Panikker?
 Began with Rammohan Roy
Emphasis on individual Rights, particularly rights of women.
 Thinkers like KC Sen, Justice Ranade, Swami Vivekanand. Introduced a spirit of social justice within orthodox Hinduism.
Vivekananda: such reordering of Hindu society couldn't have been possible without liberal principles.
In what ways does the Indian Constitution tackle the problem of diversity and how is it different from western solutions?
Western liberal constitutions resolve the problem of diversity and cultural variation by not recognising communities at all!
This method would have been unworkable in India.
In western world, the people form communities or identify themselves based on their linguistic roots.
Example: French, German etc.
While, India is land of multiple cultural communities.
It was important to ensure not one community dominates any other community.
So, Indian Constitution recognises community based rights.
One such right is right of religious communities to establish and run their own educational institutions.
Book: In Defence of Liberalism 1962
Philosophy of the Constitution 4
💡

How do most secular states treat religion?
Most secular states treat religion as only a private matter.
The mainstream western conception of Secularism is for the state and religion to be mutually exclusive in order to protect the values like individual freedom and citizenship rights.
State, hence, according to most western states, must not intervene in the domain of religion, and religion likewise must dissociate itself from influencing the policies or the functioning of the state.
Example: If a state promotes a particular religion, it would be difficult for people identifying with other religion to live freely.
Comment about the 'Secularism' as a commitment of the Indian Constitution.
Indian Constitution responded differently to the problem posed by religion vis-a-vis state and worked out an alternative conception of Secularism.
Comment about 'national unity' as a commitment of Indian Constitution.
Shows that, Indian Constitution doesn't see religion merely as a 'private' matter concerning the individual.
Philosophy of the Constitution 5
💡