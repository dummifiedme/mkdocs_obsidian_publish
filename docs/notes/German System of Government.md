---  
Date: 2020-12-01 06:04  
Title: German System of Government  
Parent: [['SR Polity Lecture 2']]  
NotesAddress: A1f  
---  
# German System of Government

### German System of Government
- Sub-type of Parliamentary System
- Also called 'Chancellor's Democracy'
	- Not of 'Westminister Type'
- Constitutional Head => Chancellor
- PM (Germany) >> PM (UK)
- Collective responsibility - strong cabinet doesn't exist.
- Polity decisions are taken by Chancellor
	- No need to be approved by the cabinet	
- Individual ministers enjoy autonomy
- Cabinet Principle apply only when there is a conflict between two departments or ministers