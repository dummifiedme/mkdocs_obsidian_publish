---  
title: Asola Bhatti Wildlife Sanctuary  
source: None  
date: 2021-03-17 22:53  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Asola Bhatti Wildlife Sanctuary
**flashcard**{: #flashcard}{: .hash}  

- In Delhi
- 200 species of resident and migratory birds.
- Both floral and faunal diversity
![[Inb![[Asola Bhatti Wildlife Mines 1.png]]