---  
title: Composition of Air  
source: None  
date: 2021-05-29 18:56  
aliases: []  
tags: status/seed environment  
---  
Category:: 

---

# Composition of Air
**flashcard**{: #flashcard}{: .hash}  

Nitrogen > Oxygen > Argon > Carbon Dioxide> Neon > Helium
![[PieChart - Composition of Air.png]]