---  
date: 2022-01-02 14:09  
aliases: ['Decentralisation in India']  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/governance #note/upsc/GS2/polity 
# Decentralization in India

**Definition**:: Transfer or dispersal of decision-making powers. Accompanied by delegation ⇾ individuals or units ⇾ even if located far.

- [[Timeline of Decentralisation in India]]