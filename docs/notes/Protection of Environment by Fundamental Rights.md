---  
title: Protection of Environment by Fundamental Rights  
source: Vision Test 2018 09 2436  
date: 2021-04-08 17:10  
tags: None  
aliases: []  
---  
Category:: [[🗃️ Environment]], [[Fundamental Rights]]

---

# Protection of Environment by Fundamental Rights
Articles that make a protection of environment a mandate by the Fundamental Rights:
- Article 21 of the constitution, ―no person shall be deprived of his life or personal liberty except according to procedure established by law‖. Right to environment, free of danger of disease and infection is inherent in it. Right to healthy environment is important attribute of right to live with human dignity. 
- The constitution of India under Article 19(1)(a) read with Article 21 of the constitution guarantees right to decent environment and right to live peacefully. 
- Article 19(1)(g) of the Indian constitution confers fundamental right on every citizen to practice any profession or to carry on any occupation, trade or business.This is subject to reasonable restrictions. A citizen cannot carry on business activity, if it is health hazards to the society or general public. Thus safeguards for environment protection are inherent in this.  
- Public Interest Litigation under Article 32 and 226 of the constitution of India resulted in a wave of environmental litigation. 
- At local and village level also, Panchayats have been empowered under the constitution to take measures such as soil conservation, water management, forestry and protection of the environment and promotion of ecological aspect.