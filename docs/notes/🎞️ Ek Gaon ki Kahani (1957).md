---  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Ek Gaon ki Kahani (1957)
---
Cast:: "Abhi Bhattacharya", "Nirupa Roy", "Mala Sinha", "Talat Mahmood"
Year:: 1957
Director:: Dalal Guha
Music:: Salil Choudhary
Watched:: 2021-11-20

---
A very good film about the social conditions and situations prevailing in the villages of the era. With a frequent mention of caste based division, the film highlights the negligence towards healthcare and family planning in the village families. There are subtle clues into the mindset of people of all classes - the rich, the poor and the destitute. 

There is much to learn from the movie about the benevolence from the way Abhi Bhattacharya's character navigates through the story. He is portrayed as someone who is ready to help and is through and through rational. He doesn't believe in caste and just believes in the human good. Talat's character is a doctor with a good heart, willing to work for even the untouchables. Both the characters bring out a collective notion of being good to humans at any cost.

Other characters performed well as well. Mala Sinha's father, the compounder was an amazing character and played the role flawlessly while Nirupa Roy and Lalita Pawar were classic in their role as a 'bahu-saas' duo with all the traditional spice.