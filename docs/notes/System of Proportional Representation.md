---  
---  
# System of Proportional Representation
- Tries to remove the defects of [[Inbox/UPSC/First-Past-The-Post System]]
- All section of the people get representation in proportion to their number.
- Smallest group get a representation.


 ![[3 Resources/Pasted image 20201125031054.png]] 
 

### Two kinds of proportional representation:
1.  {1| [[Single Transferable Vote System]]}
	- Used in elections to 
		- {2| Rajya Sabha}
		- {2| President} 
		- {2| Vice-President} 
2.  {1| [[0 Inbox/List System  of proportional Representation]]}

System of Proportional Representation
[[Demerits of System of Proportional Representation]]
 
 ---