---  
title: Ramsar Sites - WB  
date: 2021-06-24 18:18  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - WB
**flashcard**{: #flashcard}{: .hash}  

- Sundarban [[Wetland]] - *(Largest in India)* - 2019
- [[East Kolkata Wetlands]] - 2002