---  
---  
# Fundamental Rights prevailed over DPSP before CAA 25
---
title: Fundamental Rights prevailed over DPSP before CAA 25
date: 2021-05-30 16:49
tags: status/mature 
aliases: [ ]
---

**note**{: #note}{: .hash}  
/upsc/GS2/polity

---

Fundamental Rights prevailed over DPSP before [[CAA 25]] and that a law enacted to implement a DPSP wouldn't be invalid if it conflicted with a Fundamental Right.