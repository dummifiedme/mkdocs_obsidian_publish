---  
title: National Board for Wildlife  
source: None  
date: 2021-03-11 22:08  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment/bodies

---


# National Board for Wildlife
- Advisory body under ::  [[Wildlife Protection Act]], 1972
- Ex-offico chairman :: Prime Minister.

## Functions of National Board of Wildlife
**flashcard**{: #flashcard}{: .hash}  

- **Main Function** - to promote the conservation and development of wildlife and forests.
- **Advisory board** - offers advice to central government on the issues of wildlife conservation.
- **Apex body - reviews and approves** all the matters related to wildlife, projects related to National Parks and [[Wildlife Sanctuaries]] etc. <!--SR:!2021-10-11,20,250-->

- - -
- [ ] Check what kind of a body it is.