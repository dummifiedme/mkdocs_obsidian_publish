---  
---  
# Democratic Decentralisation
### Democratic Decentralisation
**Definition**:: It is when a meaningful authority devolved to the local units of governance that are accessible and accountable to the local citizenry, who enjoy full political rights and liberty. 
DEMOCRATIC COMPONENT - aka - use of elections.