---  
title: Global Food Security Index  
source: None  
date: 2021-05-08 19:46  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Global Food Security Index
**flashcard**{: #flashcard}{: .hash}  

- measures food security across countries
- published in 2012 - managed and updated annually by The Economist Intelligence Unit.
- 2019 Rank - Singapore rank 1, India rank 72.