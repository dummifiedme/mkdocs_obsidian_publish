---  
title: Arjun Main Battle Tank  
source: None  
date: 2021-02-17 00:51  
tags: status/seed  
aliases: []  
---  
Category:: [[International Relations]]

---

# Arjun Main Battle Tank
**flashcard**{: #flashcard}{: .hash}  

- Initiated by DRDO in 1972
	- CVRDE - Combat Vehicles Research and Development Establishment
- MK1 prototype of Arjun already manufactured.