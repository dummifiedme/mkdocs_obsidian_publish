---  
---  
# Swiss System of Government
---
Date: 2020-12-01 06:47
Title: Swiss System of Government
Parent: [[SR Polity Lecture 2]]
NotesAddress: A2a 
---


### Swiss System of Government
- Direct Democracy
- Three main institutions: 
	- **Referendum**
		- Any bill passed by the legislature is put to vote by the citizen.
	- **Initiative**
		- Initiation of bills by citizens themselves.
	- **Recall**
		- Citizens can call the elected representatives before the completion of the term if dissatisfied.
		- Not practical in India 	
			- Cost of election --> HIGH
			- might be for politics --> parties aapas mein.
				- INSTABILITY

What is Plebicite? **todo**{: #todo}{: .hash}  
/open