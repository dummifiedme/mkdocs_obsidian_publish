---  
title: Composition of Rajya Sabha  
source: None  
date: 2021-03-10 07:23  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Composition of Rajya Sabha

- Maximum strength is RS is fixed at 250.
	- {1| 238} are representatives of the states and UTs
	- {1| 12} are nominated.
	- Presently, has {1: 245} members.
		- {1| 229} from states
		- {1| 4} from UTs
		- {1| 12} nominated 

- ==Schedule 4== of the constitution deals with the allocation of the seats in the RS to the states and UTs. 

[[Representation of States in RS]]
[[Representation of UTs in RS]]
[[Nominated Members in RS]]