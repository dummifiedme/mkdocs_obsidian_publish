---  
title: Digital Intelligence Unit  
source: None  
date: 2021-02-25 16:05  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS2]]

---

# Digital Intelligence Unit
**flashcard**{: #flashcard}{: .hash}  

- Set up by Ministry of Communications
- to deal with unsolicited commercial communication and financial frauds related complaints.