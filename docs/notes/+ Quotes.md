---  
---  
**note**{: #note}{: .hash}  
/quotes
# + Quotes

- [ ]  "Jayema Saṁ Yudhi Spr̥dhaḥ", which is taken from Rigveda and means "I defeat those who fight against me".


```dataview
TABLE WITHOUT ID ("[[" + file.name + "|💭]]") as Link, Quote
where Quote
```