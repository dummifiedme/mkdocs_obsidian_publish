---  
title: nitrifying bacteria  
source: None  
date: 2021-05-29 20:15  
aliases: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# nitrifying bacteria
**flashcard**{: #flashcard}{: .hash}  

Converts ammonium to nitrates.
eg.
- Early stage - Nitrosomonas --> to Nitrites(NO2-)
- Later - Nitrobacter --> Nitrates (NO3-)