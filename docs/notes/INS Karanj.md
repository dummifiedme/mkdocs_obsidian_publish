---  
title: INS Karanj  
source: None  
date: 2021-03-17 22:44  
tags: status/seed  
aliases: []  
---  
Category::  

---

# INS Karanj
**flashcard**{: #flashcard}{: .hash}  

- Diesel powered Submarine
- Commissioned in 2021
- Kalvari Class - [[Scorpene Class Submarine]].
- [[Air Independent Propulsion]] system to be fitted
	- Not first.