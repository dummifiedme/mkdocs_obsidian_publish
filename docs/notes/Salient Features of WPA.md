---  
title: Salient Features of WPA  
source: None  
date: 2021-05-27 17:48  
tags: status/seed  
aliases: ['Salient Features of Wildlife Protection Act']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Salient Features of WPA
**flashcard**{: #flashcard}{: .hash}  

- Protection of *listed species* of animal, birds and plants
	- Prohibition on hunting of endangered species
	- Prohibition on trade of scheduled animals.
- The Act created *[[Schedules of Wildlife Protection Act]]* which gave varying degrees of protection to classes of flora and fauna.
	- For the **first time**, a comprehensive list of the endangered wildlife of the country was prepared.
- Licenses for sale, transfer and possession of some wildlife species.
- Establishment of [[Protected Areas under WPA]] in the country.
	- Wildlife Sanctuaries
	- National Parks
	- Tiger Reserves etc
- The Act provides for the formation of wildlife advisory boards, wildlife wardens, specifies their powers and duties, etc.
	- Established [[Central Zoo Authority]]
	- Established [[National Board for Wildlife]]
	- Established [[National Tiger Conservation Authority]]
- It helped India become a party to [[CITES]]

---
**Note**:

> A list of endangered wildlife of the country was prepared for the first time as part of ==WPA Act, 1972==.

> Protected areas for wildlife are declared under the ==WPA Act of 1972==.