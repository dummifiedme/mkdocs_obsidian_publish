---  
---  
# 🎞️ Sampark (1979)
---

**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
Watched:: 2021-10-30
Cast:: "Girish Karnad", "Rameshwari", "Kulbhushan Kharbanda"

---

Couldn't learn anything new from this story. The basic message to me was to be sure of the notions we conceive of others, especially the close ones, as things aren't as people or the world puts it to you. You must know what the dynamics are in a relationship. You are one who should be able to see past the things people are discussing about your own household! 

## Storyline
Another typical drama staged in a small village with a wealthy lender Mishir Ji against a righteous taxi driver and his small family. The story revolves around the problems and hardships the driver faces after loosing both his hands and subsequent defamation plots by the evil lender against his wife claiming of an illicit relationship between the driver's adopted brother and his wife. Towards the end, the story ends with a very common judicial enquiry in a village panchayat where both sides place arguments and the jury aka the lender, decides to create another "agnipareeksha" for the lady in question, but this time a dramatic way of proving innocence by putting in the hands into a container with boiling oil. She agrees to do so on one condition that someone from the village itself would put in their hands too, afterall all other ladies in the village are supposedly "clean" of all sins. The jury considers it as an act against the rule of law and times, and refuses to entertain. Later on, the man in question -- the driver's brother -- comes in with the truth about lender's own daughter-in-law who eloped with a lover. this creates a commotion at the scene and every argument against the driver reverses onto the jury itself. They both are asked to put their hands into the oil, but the jury flips it over and leaves in shame. THE END.