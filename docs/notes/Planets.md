---  
title: Planets  
source: None  
date: 2021-02-12 21:59  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/geography 
# Planets

According to International Astronomical Union in 2006, conditions for being a planet are 
**flashcard**{: #flashcard}{: .hash}  

1. Must orbit the star
2. must have sufficeint mass for its self-gravity to overcome rigid body forces so that it assumes hydrostatic equilibrium (spherical shape)
3. Must be big enough that it's gravity clears away any other objects of similar size near its orbit around the sun. <!--SR:!2022-03-05,60,230-->

## Dwarf Planets
- Points 1 and 2 of conditions for a planet are applicable to dwarf but not the third.