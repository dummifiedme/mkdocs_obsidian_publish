---  
title: Securities Appellate Tribunal  
source: None  
date: 2021-02-21 10:12  
tags: status/seed  
aliases: []  
---  
Category:: [[SEBI]]  , [[🗃️ Economics]]

---

# Securities Appellate Tribunal
**flashcard**{: #flashcard}{: .hash}  

- Statutory, SEBI Act 1992
- HQ - Mumbai
- SEBI, PFRDA, IRDAI
- Civil Court equivalent
- SC after appellate

## Composition - SAT
**flashcard**{: #flashcard}{: .hash}  

- Chairman + 8 members
- Committees are formed whenever necessary