---  
title: Amendments in the Preamble  
date: 2021-05-30 17:01  
aliases: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Amendments in the Preamble
**flashcard**{: #flashcard}{: .hash}  

- CAA 42 added the terms 'Socialist' and 'Secular' in the original charaterization of India as 'Soverign Democratic Republic', and
- The words ‘Unity of the nation’ have been changed to ‘Unity and *integrity* of the nation’.