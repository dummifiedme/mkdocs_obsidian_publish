---  
title: Schedules of Constitution  
source: None  
date: 2021-05-30 14:50  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# + Schedules of Constitution
- [[Knowledge/Academics/UPSC CSE/First Schedule]]
- [[Knowledge/Academics/UPSC CSE/Second Schedule]]
- [[Knowledge/Academics/UPSC CSE/Third Schedule]]
- [[Knowledge/Academics/UPSC CSE/Fourth Schedule]]
- [[Knowledge/Academics/UPSC CSE/Fifth Schedule]]
- [[Knowledge/Academics/UPSC CSE/Sixth Schedule]]
- [[Knowledge/Academics/UPSC CSE/Seventh Schedule]]
- [[Knowledge/Academics/UPSC CSE/Eighth Schedule]]
- [[Knowledge/Academics/UPSC CSE/Ninth Schedule]]
- [[Knowledge/Academics/UPSC CSE/Tenth Schedule]]
- [[Knowledge/Academics/UPSC CSE/Eleventh Schedule]]
- [[Knowledge/Academics/UPSC CSE/Twelfth Schedule]]

> [[Differences in 5th and 6th Schedules]]