---  
cover: https://m.media-amazon.com/images/M/MV5BZDZmMTA0NGUtZTg3ZS00MzhjLWFjMDgtMjdkMjAzZDJlOWIyXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SX300.jpg  
imdbId: tt0061974  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Milan (1967)

- - -
![poster|-right](https://m.media-amazon.com/images/M/MV5BZDZmMTA0NGUtZTg3ZS00MzhjLWFjMDgtMjdkMjAzZDJlOWIyXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SX300.jpg)

**Director**:: "Adurthi Subba Rao"
**Genre**:: "Drama", "Musical", "Romance"
**IMDB Rating**:: 6.8
**Year**:: 1967
**Cast**:: "Sunil Dutt", "Nutan Samarth", "Jamuna"
**Watched**:: "2022-01-09"
**Rating**:: 8
- - -

## Synopsis
**Plot**:: On the banks of the holy River Ganga, Gopi, a ferryman conducts an unconsummated romance with Radha, a girl from the upper classes. At the same time, he rejects the advances of Gauri, a girl from his own village, who is attracting the