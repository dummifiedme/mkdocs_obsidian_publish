---  
---  
# (SN40) Anth101 Cultivation
(SN40) Anth101 Cultivation

Saturday, 5 June 2021

1:12 PM

 

- Cultivation refers to the plantation of plants and animals for consumtions.

- All humans were foragers until \~15KYA.

- Subsistence = how people sustain themselves, how do they get food for themselves.

    - Five main types

        -   Foraging

        -   Cultivation

            -   Horticulture, Agriculture, Pastoralism.

        -   Industrialism

- Cultivation

    - Horticulture

        -   Small scale cultivation of plant foods that does not make intensive use of complex systems.

        -   Farming for oneself.

        -   Use simple tools - hoes, digging sticks

        -   Land management techniques

            -   Leave lands fallow for a few years.

            -   Slash-and-burn technique.

        -   Gender Roles? - Women are primary produces in \~50% of the societies.

            -   Same jobs when done by different genders can have different prestige/status attached to it.

            -   Men 50% were mostly prestigious in Status.

            -   While Women 50%, on such prestige attached to agriculture.

        -   SHIFT from foragers? - Last Glacial Maximum reducing.

            -   Glaciers retreating - climate more warmer and wetter.

            -   Broad spectrum Revolution - Since the diet became a lot more varied.

            -   Middle East had wide variety of terrain. Best time for being nomadic or hunter gatherer. But with Middle East, suddenly, everything was around. -&gt; Some settled— First real villages in all of human history in the “hilly flanks”.

                -   First ones were Natufian Culture (12500 - 10500 BP)

                    -   Collected wild cereal plants and hunted gazelles.

                    -   Lived in year round villages.

                    -   At Ain Mallaha - burials with dogs.

                    -   At Booker Tachtit - burials with al lot of different animal parts - referred as ‘Shaman’ Burial.

            -   But, drier climate after 11000BP. (arid)

                -   Natufian villagers entered around permanent water sources

                -   More people surviving on constant supply of food.

                    -   Led to movement from Hilly Flanks to Steppe (grassland/plain)

        -   

| WILD Plants            | DOMESTICATED Plants    |
|------------------------|------------------------|
| Seeds are smaller      | Seeds are larger       |
| Axis of plant shatters | Axis is not shattering |
| Hard husks on seeds    | Husks easily removed   |

- Genetic mutation in wild plants produced phenotypic variation.

- “Domestication” was not the end goal but a ”happy accident”.

- 

| Wild Animals                                             | Domesticated Animals                             |
|----------------------------------------------------------|--------------------------------------------------|
| Smaller than domesticated                                | Larger than wild animals                         |
| Maybe herd animals                                       | Herd animals                                     |
| Lack temperament for symbiotic relationship with humans. | Able to have symbiotic relationship with humans. |

 

- Agriculture

    - Cultivation of plant foods hat does make intensive use of comple systems

    - Labor, land, capital, machinery

 

- Pastoralism

    - Domesticating animals.

    - Use of animals as primary and secondary resources

        -   Primary: meat, blood.

            -   Blood can be utilised without killing the animal - has rich nutrients (As part of diet).

        -   Secondary: milk, wool, transport.

    - Pastoralist generally be using secondary resources more often.

    - Nomadism

        -   Entire group moves with animals throughout year.

    - Transhumance

        -   Part of group moves, while others stay in villages.