---  
title: Rules of Origin  
source: None  
date: 2020-12-19 22:03  
aliases: ['ROO']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics **review**{: #review}{: .hash}  
/upsc/mains 
# Rules of Origin
## What are the Rules of Origin?
Criteria prescribed to determine the national origin of an imported product in a country. **definition**{: #definition}{: .hash}  



## What are the purposes of Rules of Origin?
- to implement measures and instruments of commercial policy such as anti-dumping suites and safeguard measures.
- to determine if qualified for [[most-favoured nation) (MFN]] treatment -- aka preferential.
- trade statistics
- application of marketing and labelling requirements etc- government procurement.

## Types of Rules of Origin
[[ROO - Types of Rules of Origin]]

## Criteria for determining the origin of good
[[ROO- Criteria for determining the origin of goods]]

## Significance
[[ROO - Significance]]

## Related Rules
[[CAROTAR 2020]]

## Concerns
[[ROO - Related Concerns]]