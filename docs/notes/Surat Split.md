---  
title: Surat Split  
source: None  
date: 2021-05-20 14:43  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/modern
# Surat Split

- Surat Split in [[Indian National Congress]] happened in :: 1907. <!--SR:!2021-09-15,20,250-->

- The earlier(2) sessions of [[Indian National Congress]] before the split were in ==Benaras in 1905 and Calcutta in 1906==
 
<!--SR:!2021-08-27,1,190-->