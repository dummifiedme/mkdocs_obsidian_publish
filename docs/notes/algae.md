---  
title: algae  
source: None  
date: 2021-05-30 12:10  
aliases: []  
tags: status/seed note/upsc/GS3/scitech/bio note/upsc/GS3/environment/species  
---  
Category | [[🗃️ Science and Tech]] [[🗃️ Environment]]

---

# algae

## Uses of Algae
**flashcard**{: #flashcard}{: .hash}  

- *commercially* cultivated for Pharmaceuticals, Nutraceuticals, Cosmetics and Aquaculture purpose.
- Carbon Dioxide *Fixation*
- *Bio-fuel & Oil extraction*: bioethanol, biodiesel, biobutanol
- *Purification* of wastewater
- *Food Supplements* -- China, Japan, Ireland, Chile, NZ, Columbia etc