---  
---  
**review**{: #review}{: .hash}  
/upsc/mains
- [[Centre and State Relations during COVID-19#Issues]]
- Definition of ‘lethal’ or ‘infectious’ or ‘contagious’ diseases are not included in any legislation. 
	- No elaborations on the rules and procedure either.
- no specific provision of or sequestering required for availability of drugs.
- Management of health crisis quickly became an issue of law and order.
	- [[Ministry of Home Affairs]] released the guidelines rather than the [[Ministry of Health and Family Welfare]]
	- The language used in The policies/guidelines had words like “curfew”, “fines” and “Lockdowns”
- Pro-active consolidated policy approach was missing
	- ad-hoc and reactive rule making
- [[Epidemic Diseases Act (1897)]] contains no provisions for sequestering the dissemination of drugs/vaccines and the quarantine measures.- [[Indian Penal Code]] provisions had to be employed.