---  
title: Seaweed Mission  
source: None  
date: 2021-02-13 22:08  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Seaweed Mission
**flashcard**{: #flashcard}{: .hash}  

- Commercial farming of seaweeds
- Further processing and value addition as well.
- If seaweed cultivation is done in 5% of the Exclusive Economic Zone (EEZ)
	- Set up a new seaweed industry
	- employment, national GDP
	- sequester a lot of CO2

## Uses of Seaweed
**flashcard**{: #flashcard}{: .hash}  

- A type of algae
- Binding agent
	- Fruit Jelly
- Cosmetic Products