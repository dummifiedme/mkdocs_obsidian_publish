---  
title: Features of Basic Structure  
date: 2021-07-18 21:57  
alias: ['Principles of Basic Structure']  
tags: status/seed  
sr-due: 2022-02-27  
sr-interval: 60  
sr-ease: 270  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity  **review**{: #review}{: .hash}  
/upsc 
# Features of Basic Structure

1.  **Supremacy of the Constitution**
2.  Sovereign, democratic and republican nature of the Indian polity
3.  Secular character of the Constitution
4.  Separation of powers between the legislature, the executive and the judiciary
5.  Federal character of the Constitution
6.  Unity and integrity of the nation
7.  Welfare state (socio-economic justice)
8.  Judicial review
9.  Freedom and dignity of the individual
10. Parliamentary system
11. **Rule of law**
12. **Harmony and balance between Fundamental Rights and Directive Principles**
13. Principle of equality
14. Free and fair elections
15. Independence of Judiciary
16. **Limited power of Parliament to amend the Constitution**.
17. Effective access to justice
18. Principles (or essence) underlying fundamental rights
19. Powers of the Supreme Court under Articles [[32]], [[136]], [[141]] and [[142]]
20. Powers of the High Courts under Articles [[226]] and [[227]].