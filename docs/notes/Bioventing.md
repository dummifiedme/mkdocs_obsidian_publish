---  
title: Bioventing  
source: None  
date: 2021-05-30 12:52  
aliases: []  
tags: status/seed environment  
---  
Category:: 

---

# Bioventing
**flashcard**{: #flashcard}{: .hash}  

The supply of air and nutrients through wells to contaminated soil to stimulate the growth of indigenous bacteria.
- It is used for hydrocarbon contamination and can be used where it is deep under the surface.