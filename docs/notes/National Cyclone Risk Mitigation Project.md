---  
title: National Cyclone Risk Mitigation Project  
source: None  
date: 2021-03-20 00:26  
tags: status/seed  
aliases: []  
---  
Category::  

---

# National Cyclone Risk Mitigation Project
**flashcard**{: #flashcard}{: .hash}  

- Ministry of Home Affairs with support by World Bank
- Undertake suitable structural and non-structural measures to mitigate the effects of cyclones in the coastal states and UTs.
	- Structural - infrastructure, shelters etc.
	- Non-Structural - 
- Implemented by [[National Disaster Management Authority) (NDMA]] in association with  National Institute of Disaster Management (NIDM.