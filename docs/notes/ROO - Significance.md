---  
title: ROO - Significance  
source: None  
date: 2020-12-23  
aliases: ['significance of ROO', 'significance of rules of origin']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# ROO - Significance
1.  Addressing trade distorting practices
    - correcting "unfair trade"
        -   anti-dumping duty
    - protecting local industry
        -   checking unforeseen increase in imports
2.  Ensuring effectiveness of Trade Agreements
    - check re-routing of exports to save customs
3.  Transparency in custom procedures
    - clear rules for business across countries
4.  Implementing environmental or sanitary measures
    - prevents import of contaminated or nuclear waste etc
5.  Administering "buy national" policies
    - adjusting [[Balance of Payments (BOP)]]
6.  Ensuring national security or political policy.
    - control strategic weapons or specific products on which sanctions are applied.