---  
title: Insurance  
source: None  
date: 2021-05-06 11:34  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Insurance
- Insurance - Meaning and Significance
	- Insurance is a debt insurance or legal contract against the eventualities of death or damage.
	- Two parties in the contract 
		- Insured or client
		- Insurer or underwriter
	- Basically insurance companies invest clients' premium in various public private sector projects, therefore investment.


- Insurance Principles
	- *Uberrima fides* :: Good faith, hide nithing
	- *Indemnity* :: Only "REAL" loss, not imaginary
	- *Subrogation* :: Insurer can recover from negligent 3rd party
	- *Causa Proxima* :: Direct loss link
	- *Insurable interest* :: If "risk-x" not happen, client remains in the same position,  "risk-x" happens, client in bad position.

- History of Insurance
- Types of Insurance
	- Life Insurance
	- General Insurance
		- Health Insurance
	- Catastrophe Insurance
	- Re-Insurance
- Insurance Regulator - IRDAI
- Challenges to the insurance sector