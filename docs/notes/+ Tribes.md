---  
date: 2022-01-12 13:40  
aliases: ['+ Tribes MOC', 'Tribes List']  
---  
# + Tribes
```dataview
table without id choice(aka[0], ("[["+ file.name + "]] (" +   aka + ")"), file.link) as Tribe, Location, Anthropologist, Description 
from **note**{: #note}{: .hash}  
/upsc/anthropology/tribes
sort file.name ASC
``````