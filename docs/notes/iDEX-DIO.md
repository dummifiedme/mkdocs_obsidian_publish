---  
title: iDEX-DIO  
date: 2021-07-01 12:46  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims   
# iDEX-DIO
**flashcard**{: #flashcard}{: .hash}  

- Innovations for Defence Excellence-[[Defence Innovation Organisation]]
- Funded by Defence Ministry for next five years - ₹498 crore
- Problem is posted, idea is floated when selected, given funding of ₹1.5 crore for prototyping
- part of Make in India

**Related**
- Defence Industrial Corridors
- Artificial Intelligence in Defence
	- N Chandrasekaran Task Force