---  
title: Abiotic Factor- Water  
source: None  
date: 2021-04-17 07:06  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Abiotic Factor- Water
- life on earth originated in water
	- unsustainable without water
- the productivity and distribution of plants is also heavily dependent on water.
- Quality of water - pH, chemical composition - also matters to the aquatic life.
- Some organisms are tolerant of a wide range of salinities - ==*euryhaline*==.
- Some organisms are restricted to a narrow range of salinities - ==*stenohaline*==.