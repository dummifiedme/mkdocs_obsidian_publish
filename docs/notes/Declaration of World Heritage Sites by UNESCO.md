---  
title: Declaration of World Heritage Sites by UNESCO  
source: None  
date: 2021-03-10 22:56  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Declaration of [[World Heritage Sites]] by UNESCO
> Dholavira: A Harrappan Site is nominated to be a part of World Heritage Sites.


## Steps taken by India to protect heritage sites
**flashcard**{: #flashcard}{: .hash}  

- [[Adopt Heritage Project]] **todo**{: #todo}{: .hash}  

- [[HRIDAY Scheme]] **todo**{: #todo}{: .hash}  
