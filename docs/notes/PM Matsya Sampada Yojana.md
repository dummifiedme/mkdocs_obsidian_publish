---  
title: PM Matsya Sampada Yojana  
source: None  
date: 2021-05-08 20:54  
tags: status/seed note/upsc/GS3/economics  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/agriculture 
# PM Matsya Sampada Yojana
**flashcard**{: #flashcard}{: .hash}  

- Department of Fisheries, [[Ministry of Fisheries, Animal Husbandry and Dairying]].
- Flagship programme - focused and sustainable development of the fisheries sector. <!--SR:!2021-08-18,3,250-->