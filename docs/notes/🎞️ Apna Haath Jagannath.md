---  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Apna Haath Jagannath
- - -
Year::
Watched:: 2021-11-15
Cast:: "Kishore Kumar", "Leela Chitnis"
- - -

- Contains a funny dissent song about "Permit" Raj, sung by Kishore Kumar
	- permit pe "mar mit"
	- "permit bina iss jahan mein do din bhi na jiya jaaaye"
	- "permit ke liye permit"
	- "zeher ke liye permit"