---  
title: Equalization Levy  
date: 2021-05-29 21:12  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Equalization Levy
**flashcard**{: #flashcard}{: .hash}  

- A tax from those non-resident companies who provide *ad services to the Indian entities*.
- Since 2016.
- Currently amounts to 6%.
- To be paid by Indian payer only. 
	- Deduct the tax amount and then pay the international company.
	- The deducted amount is submitted as tax.
	![[InfoGraph - Equilisation Levi.png]]
- - -
Related: [[Digital Service Tax|Equalisation Levy 2.0]]
	 <!--SR:!2021-08-19,3,250-->