---  
---  
# Other Backward Classes
**TODO**{: #TODO}{: .hash}  
 
**doing**{: #doing}{: .hash}  
 

## Who are OBCs?
A: ![[Pasted image 20201126044836.png]]
- classify castes which are educationally or 
socially disadvantaged. 
- OBCs are a vastly heterogeneous group.

---

The article {340} of the Indian Constitution lays down conditions for the appointment of a {Commission to 
investigate the conditions of backward classes}. 

---

## Background of OBC reservations
1980 - Mandal Commission
1990 - Announced 27%
1992 - Implemented - only in govt. jobs
2006 - Implemented - in educational institutions

## Related Cases
- {[[Indira Sawhney Case]]} 
	- Introduced '{Creamy Layer}'
	- Reason? : {To percolate the benefits to the actual needy}.
	- Additional: {combined reservations not greater than 50 percent}.

## Idea of Sub-Category in OBC
| Year | Name of Committee                                  | Sub-Category                    |
| ---- | -------------------------------------------------- | ------------------------------- |
| {1955} | {First Backward Class Committee}                     | {Backward and Extremely Backward} |
| {1979} | {Mandal Commission + dissent note by member LK Naik} | {Intermediate and Depressed}      |
| {2015} | {NCBC}                                               | {Extremely BC, More BC, BC}       |


NCBC in 2015 proposed sub-categorizations in OBC 
1. Extremely BC 
	- {-> Nomadic, aboriginal tribes, semi-nomadic tribes 
	- -> Carrying out their traditional occupations}
2. More Backward Classes 
	- -> {vocational groups carrying on with their traditional occupations}
3. Backward Classes
	- -> {comparatively more forward}

---

## Need for Sub-Categorization of OBC
- {Benefits reached only limited sections}
- {Benefits reaches more towards economically stronger sub-sections}
	
	
### STATS: OBC castes benefiting from reservations
According to NCBC in 2015:
- {Rohini Commission} highlighted that -- 1900/2633 central list OBCs not benefited.
	- Half of 1900 -> {no benefits at all}
	- Other half of 1900 -> {<3% share in OBC quota}
	- 25% of benefits from the OBC reservations -> availed by {only 10 sub-castes}.


### Recommendations by NCBC w/r to OBC reservations and sub-categorization
- {Fixed quota of 8-10% (within 27%) for almost 1900 least benefited castes
	- 1900 castes = 2-3% to total seats
	- Wont affect other groups
	- But may create substantial opportunities for them.}
- {Sub-categorizing to be based on relative benefits among the OBC
	- not on SOCIAL BACKWARDNESS}