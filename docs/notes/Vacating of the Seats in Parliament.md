---  
title: Vacating of the Seats in Parliament  
source: None  
date: 2021-03-10 19:17  
tags: status/seed  
aliases: []  
---  
Category:: [[Parliament]] 

---

# Vacating of the Seats in Parliament
(Reasons/Cases/Modes)
**flashcard**{: #flashcard}{: .hash}  

- [[Double Membership in Parliament]]
- [[Anti-Defection Law#Grounds of Defection]]
- Resignation
	- When it gets accepted
	- Chairman/speaker may not accept it if they don't find it genuine -> [[Anti-Defection Law]]
- Absence
	- if absent from all the meetings in the period of 60 days.
- Other Cases
	- election void
		-  as per [[Representation of People Act, 1951]], decided by high court not the parliament. -- appeal to SC.
	- expelled
	- elected for President/Vice-President
	- appointed as a governor of a state