---  
title: GVK Commettee - Recommendations  
date: 2021-06-23 18:48  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# GVK Commettee - Recommendations

- District level body -- Zila Parishad -- PIVOTAL IMPORTANCE.
- Panchayati Raj institutions at district and lower levels -- assigned an important role w.r.t {planning, implementation and monitoring of rural development programs}.
- Create post -> District Development Commissioner
	- {CEO} of Zila Parishad
	- in-charge of all the development departments at the district levels.
- Regular elections to Panchayati Raj.

---