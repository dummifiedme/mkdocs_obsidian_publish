---  
title: Adjournment Motion  
source: None  
date: 2021-03-17 17:31  
tags: status/seed  
aliases: []  
---  
Category:: [[Motions]] 

---

# Adjournment Motion
**flashcard**{: #flashcard}{: .hash}  

Introduced to draw attention of the house to a definite matter of urgent public importance and needs the support of at least 50 members. 
- A minimum of 2.5 hours for discussion
- Only in LS, not in RS - since an element of censure against *government*.
- Restrictions
	- Only one topic, multiple topics can’t be discussed.
	- No privilege discussion.
	- Those topics in court, can’t be raised.