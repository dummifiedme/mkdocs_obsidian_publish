---  
title: Black-necked Crane  
source: None  
date: 2021-03-07 21:06  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Black-necked Crane
**flashcard**{: #flashcard}{: .hash}  

- Maybe harmed if Tawang Dam is constructed.
	- Buddhist monks protesting against the dam.
		- They believe this breed to be embodiment of the sixth Dalai Lama - Tsangyang Gyatso.
- ![[Pasted image 20210307210749.png]]
- Habitat - Tibetan Plateau, Sichuan China, Ladakh India.
- WLA - Schedule 1
- IUCN Red List - Near Threatened