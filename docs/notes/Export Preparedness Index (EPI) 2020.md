---  
title: Export Preparedness Index (EPI) 2020  
source: None  
date: 2020-12-31  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics/reports **review**{: #review}{: .hash}  
/upsc/mains 
# Export Preparedness Index (EPI) 2020
- Released by [[NITI Aayog]] + Indian Institute of Competitiveness 
- to identify the core areas crucial for export promotion at sub-national level.

### Primary Goals of EPI 
- Bring favorable export promotion policies
- Ease regulatory framework
- create necessary infrastructure for exports
- Help identifying strategic recommendations

### Roles of EPI
- A framework for continual assessment of export readiness of Indian states and UTs.
	- Ranking of Indian states and UTs based on their index scores
	- examine the preparedness for export and performance
	- identification of challenges and opportunities 
	- Enhance effectiveness of policies

### The four pillars of EPI
- policy (20%)
- Business Ecosystem (40%)
- Export Ecosystem (20%)
- Export Performance(20%)

![[Pasted image 20201231212810.png]]

### Findings of the EPI report
- India scored an average score of 39
- Least performing pillar -> EXPORT Ecosystem Pillar
- Highest performing pillar -> Policy and Business Ecosystem
- Highest performers based on geographical classification -  Coastal States
	-  Top 3 Coastal States -> Gujrat, Maharashtra, Tamil Nadu
	-  Top 3 Landlocked states -> Rajasthan, Telangana, Haryana
	-  Top 3 Himalayan States -> Uttarakhand, Tripura, HP
	-  Top 3 UTs/City States -> Delhi, Goa, Chandigarh 

[[India's Trends in Global Market]]