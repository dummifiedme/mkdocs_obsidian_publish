---  
title: Central Zoo Authority  
source: None  
date: 2021-05-27 15:51  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Environment]]

---

# Central Zoo Authority
**flashcard**{: #flashcard}{: .hash}  

Established under [[Wildlife Protection Act]] in 1992
- Oversight of zoos in India.
- under [[Ministry of Environment, Forest and Climate Change]]