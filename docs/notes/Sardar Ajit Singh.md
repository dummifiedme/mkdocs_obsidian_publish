---  
title: Sardar Ajit Singh  
source: None  
date: 2021-03-07 21:13  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Sardar Ajit Singh
**flashcard**{: #flashcard}{: .hash}  

- Uncle of Bhagat SIngh
- 1907 - [[Pagri Sambhal Movement]] - Exiled to Mandalay in Burma.
	- Along with Lala Lajpat Rai
- Started Bharat Mata Book Agency as a part of Bharat Mata Society.