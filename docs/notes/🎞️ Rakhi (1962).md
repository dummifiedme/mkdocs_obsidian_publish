---  
cover: N/A  
imdbId: tt0156922  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Rakhi (1962)

- - -
![poster|-right](N/A)

**Director**:: "A. Bhimsingh"
**Genre**:: "Drama", "Family"
**IMDB Rating**:: 6.6
**Year**:: 1962
**Cast**:: "Ashok Kumar", "Waheeda Rehman", "Pradeep Kumar", "Raj Mehra"
**Watched**:: "2021-11-15"
**Rating**:: 6
- - -

## Synopsis
**Plot**:: Orphaned at a very young age, Raju and Radha are very close to each other. Radha takes care of the house, sells milk, paints dolls, etc. while Raju works in a mill. When the mill workers'...

This movie basically revolves around the eternal love between a brother (Ashok Kumar) and sister(Waheeda Rehmaan) despite the occasional hampers due to the legendary societal negative actress Lalita Pawar. The schemes and plots of Lalita Pawar, who plays the role of sister to Waheeda's husband, to disintegrate the bond for ensuring a better life of the couple, herself and her son.

The story had its overdramatized moments and predictable storyline, but was fairly realistic in most of the parts.