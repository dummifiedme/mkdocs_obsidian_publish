---  
title: National Urban Digital Mission  
source: None  
date: 2021-03-07 21:18  
tags: status/seed  
aliases: []  
---  
Category::  , [[Keywords GS3]]

---

# National Urban Digital Mission
**flashcard**{: #flashcard}{: .hash}  

- Under MoHUA in partnership with MEITy.
- TO build the shared digital infrastructure.
- Focus on three pillars
	- People
	- Process
	- Platform
- [[India Urban Data Exchange]] (IUDX) is a part of this mission.