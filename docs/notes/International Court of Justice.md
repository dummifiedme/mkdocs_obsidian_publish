---  
title: International Court of Justice  
source: None  
date: 2021-02-10 21:09  
tags: status/mature  
aliases: ['ICJ']  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR/bodies
# International Court of Justice
- Established in 1946
- HQ :: Hague, Netherlands <!--SR:!2021-11-10,1,230-->
- Official Court of UN
	- Hence, UN Funded
- Contentious between parties and advisory opinions
	- Not for individuals.

---

- [B] **Related**: [[ICJ vs ICC]]