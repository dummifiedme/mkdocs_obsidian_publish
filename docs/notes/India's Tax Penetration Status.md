---  
title: India's Tax Penetration Status  
source: None  
date: 2020-12-31  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics/facts 
# India's Tax Penetration Status
- F:: Income Tax return = ~5% of the population = 57.8 million individuals
- F:: Income tax payers =~1.15% of the population = 15 million individuals
- F:: India's [[Tax-to-GDP Ratio]]= 17% in FY 2020
	- Direct Tax-to-GDP = ~6%
	- Indirect Tax-to-GDP= ~11%