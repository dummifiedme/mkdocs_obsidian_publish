---  
title: + Women  
source: None  
date: 2021-05-20 22:34  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/social-issues
# + Women

- [[Impact of COVID Pandemic on Women]]

## Important Acts/Bills 
- Medical termination of pregnancy amendment bill 2020 **todo**{: #todo}{: .hash}  
/digdeeper ^0ee4a5
- Surrogacy regulation bill 2019  **todo**{: #todo}{: .hash}  
/digdeeper ^8242c6
- Assisted Reproductive technology regulation bill 2020  **todo**{: #todo}{: .hash}  
/digdeeper ^2fa2e3