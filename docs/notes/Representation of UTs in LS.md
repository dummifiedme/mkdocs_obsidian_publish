---  
title: Representation of UTs in LS  
source: None  
date: 2021-03-10 17:26  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 

---



# Representation of UTs in LS
- Constitution prescribed parliament to make a law for election into the LS from UTs.
	- Parliament enacted the ==Union Territories (Direct Election to the House of the People) Act, 1965==.
	- Members of LS from the UTs are also chosen by direct election. <!--SR:!2021-09-24,3,250-->