---  
title: Angular Cheilitis at the lip corner  
date: 2021-10-15 12:11  
tags: status/seed personal/health-care  
aliases: []  
---  
# Angular Cheilitis at the lip corner
There is no natural remedy to cure it in a jiffy. We can only relieve the pain and keep it moisturized.
- Caused mostly due to Fungi or Bacteria.
	- Frequent hands around the lip region.
	- Excessive saliva production. 
		- If the saliva is accumulated around a region for a longer time, it will end up causing it. (Most probable for me)

## Natural Remedies
Idea is to keep it moisturized so that it is not painful due to stretching while dryness.
- Ice
- Cold cucumber
- Honey
- Vaseline
- Lip Balm which aren't scented.