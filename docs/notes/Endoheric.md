---  
title: Endoheric  
source: None  
date: 2021-05-28 16:16  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Endoheric
**flashcard**{: #flashcard}{: .hash}  

A closed basin which stores water but allows no outflows.
- Water flows in, doesn't flow out.
![[Endoheric.png]]