---  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity/cases 
# Kohoto Hollahan Case
**Year**::
**Verdict**:: *Defection* only if the voted against whip on a major policy/confidence/no-confidence
In Kohoto Hollahan judgement, the court held that the defiance of party whip shall amount to defection only if it's on a vote of confidence or no-confidence or on a matter which is a major policy issue which the party went to polls.