---  
title: Singorgarh Fort  
source: None  
date: 2021-03-10 21:57  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Singorgarh Fort
**flashcard**{: #flashcard}{: .hash}  

- Gond ruler Sangram Shah took it from [[Chandel]] kings in 16th century,
- Resident fort of Gondwana rulers.
- Rani Durgawati - fought against Mughals -won one, lost another.
![[Pasted image 20210310220003.png]]