---  
date: 2022-01-03 22:44  
aliases: ['Statutory Body']  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/governance #note/upsc/GS2/polity 
# Statutory Bodies

```dataview
table without id file.link as Body, [default(established-in, ""), default(HQ, "")] as "Estd-HQ", [formed-under, Ministry] as "Under", Aim

where contains(type-of-body, "Statutory Body") or contains(type-of-body, "statutory body") or type-of-body=[[Statutory Bodies]] or contains(type-of-body, "Statutory Bodies")

sort Estd-HQ 
```


```dataview
table list
from [[Statutory Bodies]] 
where list

``````


- - -
## References
- [b]