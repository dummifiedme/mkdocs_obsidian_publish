---  
---  
# Yako of Eastern Nigeria
- property divided into both Patrilineal possessions and Matrilineal possessions.
	- Patrilineage owns perpetual productive resources like lands
	- Matrilineage own consumable property like livestock.
- Matrilineal line is legally weaker but is stronger in religious matters.
> A Yako might inherit grazing land from father's side while inheriting some ritual privileges from mother's side.