---  
title: Ramsar Sites - UP  
date: 2021-06-24 18:17  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - UP
**flashcard**{: #flashcard}{: .hash}  

- Upper Ganga River
- Samaspur Bird Sanctuary
- Parvati Aranga Bird Sanctuary
- Saman Bird Sanctuary
- Sandi Bird Sanctuary
- Nawabganj Bird Sanctuary
- Sarsai Nawar Jheel
- [[Sur Sarovar]]