---  
title: EASE Banking Reforms Index  
source: None  
date: 2021-05-08 20:30  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# EASE Banking Reforms Index
**flashcard**{: #flashcard}{: .hash}  

- For clean and smart banking
- Prepared by Indian Banking Association (IBA) commissioned by Finance Ministry <!--SR:!2021-08-17,1,210-->