---  
title: Danish East India Company  
source: None  
date: 2021-05-09 18:44  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Danish East India Company
(Denmark)

- Established in ==1616==
- First Factory in India 1620 in ==Trandquebar near Tanjore== 
- Fought against [[British EIC]] - Battle of ==Bidar== in ==1759==
- Sold all the factories to Britain - ==1845==.
<!--SR:!2021-09-30,26,250!2021-09-10,6,230!2021-09-15,22,270!2021-09-29,25,250!2021-09-05,12,250-->