---  
title: Principles of Data Privacy  
source: None  
date: 2021-02-13 01:36  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS2]] , [[Keywords GS3]]

---

# Principles of Data Privacy
(Two Principles)
**flashcard**{: #flashcard}{: .hash}  

- Necessity
	- Is it really required for the functioning of the app?
- Proportionality
	- Is the collection of data proportionate to the extent to which the right to privacy is being infringed?