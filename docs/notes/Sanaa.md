---  
title: Sanaa  
source: None  
date: 2021-03-10 22:00  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Sanaa
**flashcard**{: #flashcard}{: .hash}  

- Used to be capital of Yemen
	- Currently, capital of Yemen is Aden.
- Occupied by [[Houthis]] rebels.
- Old city of Sana'a is a [[UNESCO World Heritage Site]].