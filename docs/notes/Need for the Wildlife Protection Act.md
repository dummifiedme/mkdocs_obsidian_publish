---  
title: Need for the Wildlife Protection Act  
source: None  
date: 2021-05-27 17:47  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Need for the Wildlife Protection Act
**flashcard**{: #flashcard}{: .hash}  

1.  India is a treasure-trove of varied flora and fauna. 
	- Rapid decline in numbers
		- *Edward Pritchard Gee* - Tiger population: 40000 (1900s) -> 1827(in 1972)
2.  A drastic decrease in the flora and fauna
	-  causes ecological imbalance - affects many aspects of climate, the ecosystem.
3. There were only five national parks in India prior to the enactment of this Act.
4. Wild Birds and Anomal Protection Act, 1935 had become completely outdated.
	1. Punishment << benefits from poaching.