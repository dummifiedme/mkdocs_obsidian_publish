---  
title: Danuvius  
source: None  
date: 2021-05-30 13:51  
aliases: []  
tags: status/seed species/primate  
---  
Category:: 

---

# Danuvius
**flashcard**{: #flashcard}{: .hash}  

- Danuvius guggenmosi = species of extinct ape
	- able to walk upright on two legs and
	- also uses all four limbs while clambering through trees.
	 > BEST OF BOTH :D
- Oldest bipedal and close to human ape; *11.6 BYO*.
	- Now, it is from *Germany* (European!!)
	- *Earlier, oldest was from Kenya*. (6 BYO) --> Orrorin tugenensis