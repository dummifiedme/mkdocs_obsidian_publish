---  
title: States Startup Ranking  
source: None  
date: 2021-05-08 21:01  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# States Startup Ranking
(Conducted by?)
**flashcard**{: #flashcard}{: .hash}  

- Conducted by the [[Department for Promotion of Industry and Internal Trade (DPIIT)]]
- Best performer - Andaman & Nicobar Islands, Gujrat <!--SR:!2021-08-18,3,250-->