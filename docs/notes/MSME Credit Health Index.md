---  
title: MSME Credit Health Index  
source: None  
date: 2021-03-10 22:33  
tags: status/seed  
aliases: []  
---  
Category::  

---

# MSME Credit Health Index
**flashcard**{: #flashcard}{: .hash}  

To provide a measure of growth and strength of the MSME Sector in India
- two parameters - growth and strength.