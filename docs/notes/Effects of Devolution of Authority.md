---  
---  
# Effects of Devolution of Authority
## Effects of Devolution of Authority
1. Delegation by privatisation -- Establishment and empowerment of local resource user groups  -> improves the ways of management and use of natural resource.
2. Collaboration b/w public agencies and local people 
	- creates synergistic outcomes -> citizen and civil servants cooperate private goods 
		- Examples:	**TODO**{: #TODO}{: .hash}  
 **todo**{: #todo}{: .hash}  
/digDeeper
			- Joint forest management (IFAD, 2001)
			- fisheries co-management
			- participatory watershed management
3. Democratisation and empowerment of local administrative bodies --> enhances participation in decision-making fora.