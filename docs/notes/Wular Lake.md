---  
title: Wular Lake  
date: 2021-06-11 10:17  
alias: []  
tags: status/seed  
locations: None  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Wular Lake
- Map:: [Wular Lake][](geo:34.35477416538757,74.54051971435548) tag:lakes
Largest Freshwater Lake in India
- In Bandipore, Jammu and Kashmir
	![[Wular Lake Map.png]]
- Ramsar site since 1990
- floating vegetation - water chestnut (trees)

> Largest Freshwater Lake in India :: Wular Lake