---  
title: Benzene Pollution  
source: None  
date: 2021-03-20 01:07  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Benzene Pollution
**flashcard**{: #flashcard}{: .hash}  

- Colourless Sweet flammable liquid.
- Both natural and man-made activities.
	- Natural - Volcanoes and forest fires
	- Also a part of gasoline, crude oil and cigarette smoke.
- Used as industrial chemical and is a major part of gasoline.
	- Used to make plastics, synthetic fibers, rubber lubricants, dyes.
- Can affect humans if in contact