---  
title: Main committees related to Panchayati Raj  
source: None  
date: 2021-05-29 06:57  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity

---
**review**{: #review}{: .hash}  

# Main committees related to Panchayati Raj
**todo**{: #todo}{: .hash}  
/flashcard 
1. [[Balwant Rai Mehta Committee (1957)]]
2. [[Inbox/Old Vault UPSC/Ashok Mehta Committee]](1976)
3. [[GVK Rao Committee]] (1985-86)
	-  More emphasis on Panchayati Raj institutions wrt planning and development
4.  [[Hanumantha Rao Committee]] (1984)
	-  District level planning
	-  District Collector or Minister as head of the planning bodies in districts.
	-  Differed from most of the committees -- as others emphasized the local bodies over Collector.
5. [[LM Singhvi Committee]] (1986)