---  
title: Bar Kochba Revolt  
source: None  
date: 2021-03-18 22:55  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Bar Kochba Revolt
**flashcard**{: #flashcard}{: .hash}  

Armed Jewish uprising against Rome during the reign of Emperor Hadrian
(132AD to 136AD)