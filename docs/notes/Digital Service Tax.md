---  
title: Digital Service Tax  
date: 2021-02-12 23:22  
tags: status/draft  
aliases: ['Equalisation Levy 2.0']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Digital Service Tax
**flashcard**{: #flashcard}{: .hash}  

DST is a levy on the overall revenues earned by the suppliers of specific digital services.


## Disadvantages of Digital Service Tax
**flashcard**{: #flashcard}{: .hash}  

- Taxing the revenues instead of the firm's profit
	- Loss to the genuine companies
- India-US trade ties will be hurt
	- Amazon, FB, Twitter, Apple etc are mostly US based.
- May harm start-ups
- Risk of 'double taxation'
- Transfer of burden to the product consumers --> increased costs.
- Compliance issues on various trade agreements. 


Related: [[Equalization Levy]]