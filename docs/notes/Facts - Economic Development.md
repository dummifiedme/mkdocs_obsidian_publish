---  
---  
### Start Ups
- 10% increase in registration of new forms in a district leads to an increase of 1.8% in the GDP of that district.

### GDP
**2019**: India is among the top 5 economies of the world in terms of GDP at current US$ trillion i.e. USA (21 Tn$), China ($14 Tn), Japan ($5Tn), Germany ($3.9Tn), India ($2.9Tn)