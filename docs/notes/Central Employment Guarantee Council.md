---  
title: Central Employment Guarantee Council  
source: None  
date: 2021-03-07 21:17  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Central Employment Guarantee Council
**flashcard**{: #flashcard}{: .hash}  

- Founded in 2006, under section 10 of MNREGA
- To monitor and advise on the implementation of MNREGA.