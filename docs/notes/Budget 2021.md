---  
title: Budget 2021  
source: None  
date: 2021-02-10 00:37  
tags: status/seed  
aliases: []  
---  
Category::  , [[GS3]]

---

# Budget 2021

## Part 1 - Health and Well being
> https://www.youtube.com/watch?v=zI_l0M3nhws

- Spending Increase by 137% wrt to 2019-20
- ![[GoI spending on Health.png]]
- Rs 35000 cr for Covid Vaccines
	- Funds via cess on Diesel and Petrol.
		- Price neutral though.
- Pneumococcal Vaccines 
	- Serum Institute Pune developed indigenous in India
	- For Pneumonia
	- Hence ->  Atma Nirbhar
- PM AtmaNirbhar Swasth Bharat Yojana
	- ₹64k crore over 6 years.
	- Rural and Urban health and wellness centers
	- 4 regional National Institutes for virology
	- 15 health emergency operation Centers and 2 mobile hospitals.
	- Integrated public health labs 
		- In district level labs instead of sending it to big cities.
		- Self-Dependence, De-centralization.
	- Critical care blocks in many districts
	- Strengthen NCDC - National Center for Disease Control
		- 8 branches currently
		- HQ - Delhi
	- Bidding for - Regional Research Platform for [[World Health Organization]] South East Asia Region
		- for reputation
	- Build 9 bio-safety level 3 labs in the country.
		- Total 4 levels with level 4 as maximum level.
- Mission Poshan 2.0 to be launched
	- ICDS
	- Anganwadi Services
	- Poshan Abhiyan
	- Scheme for Adolescent Girls
	- National Creche Scheme
	- POSHAN 1.0 - Started in 2018 - Jhunjhunu, Rajasthan
- Urban Swach Bharat Mission 2.0
	- Town and Cities (ofcourse)
	- 5 years - 2021 - 2026 - ₹1.41L crore
	- Areas:
		- Urban Transportation - buses and metro
			- PPP
			- reduce personal vehicles
			- reduce pollution 
			- generate employment
		- Waste water treatment
		- Segregation of garbage
		- reduction in single use plastic
		- reduction in air pollution
		- providing clean water
		- proper sanitation
	- Linked with Swasth Bharat.
- 2k crore funds for tackling pollution
- Vehicle scrappage policy
	- ![[Flow Chart - Vehicle Scrappage Policy.png]]
	- AtmaNirbhar -> scrap parts

> Government mainly uses 4 types of policies to discourage or adopt a commodity or problem/solution
> - increase taxes on problematic commodities
> - subsidy for alternatives
> - bans certain products
> - introduces permits
> ![[Diagram - Government, Permits and Environment.png]]


## Part 2 -
- [ ] **todo**{: #todo}{: .hash}  
 Budget 2021 Part 2