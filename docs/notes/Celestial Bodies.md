---  
title: Celestial Bodies  
source: None  
date: 2021-02-12 21:56  
tags: status/seed note/upsc/GS3/scitech/space  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/geography/physical value/B
# Celestial Bodies
**flashcard**{: #flashcard}{: .hash}  

![[Celestial objects.png]]
- Asteroids
- Meteoroid
- Meteorites
- Meteor
- Comet
- [[Planets]]
So basically,
$Planet > Asteroid > Meteoroid \rightarrow \frac{Meteor}{(light streak)} \rightarrow \frac{Meteorite} {(reaches Earth)}$