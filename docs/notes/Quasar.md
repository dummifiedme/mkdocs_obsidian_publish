---  
title: Quasar  
source: None  
date: 2021-03-17 21:24  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Quasar
**flashcard**{: #flashcard}{: .hash}  

- A very luminous objects in the faraway galaxies that emit jets at radio frequencies.
- Eat a lot of energy and they emit the energy in the form of a beam extending to light-years.
- Short for - "Quasi-Stellar Radio Source"
- Found in galaxies with super-massive black holes.
![[Quasars.png]]