---  
---  
# Transport and Marketing Assistance Scheme
- introduced for export of *specified agriculture products*
- to *mitigate the disadvantage of higher cost of transportation*