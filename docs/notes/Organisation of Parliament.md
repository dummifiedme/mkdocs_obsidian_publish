---  
title: Organisation of Parliament  
source: None  
date: 2021-03-10 07:03  
tags: status/mature  
aliases: []  
---  
Category:: 

---

# Organisation of Parliament

## Parts of Parliament according to Constitution
**flashcard**{: #flashcard}{: .hash}  

Three Parts
- President
- Council of States (Rajya Sabha)
- House of the People (Lok Sabha) <!--SR:!2021-09-25,4,270-->

## Reasons for President being an integral part of Parliament
**flashcard**{: #flashcard}{: .hash}  

Though president isn't a member of any proceedings and doesn't sit in Parliament to attend its meetings, he is an integral part - 
- All the bills have to be signed by him before becoming a law.
- he summons and pro-rogues both the Houses, 
- dissolves the Lok Sabha
- addresses both the houses
- issues ordinances (LEGISLATION!)
- so on. <!--SR:!2021-09-24,3,250-->

## Difference between Presidential and Parliamentary form of government in terms of legislation
**flashcard**{: #flashcard}{: .hash}  

Presidential form lies on the **principle of division of powers** of executive and legislature while the parliamentary form emphasises on **interdependence** of them. <!--SR:!2021-09-24,3,250-->