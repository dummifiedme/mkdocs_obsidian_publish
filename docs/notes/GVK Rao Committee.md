---  
title: GVK Rao Committee  
date: 2021-06-23 19:12  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# GVK Rao Committee
 GVK Rao Committee

- Appointed by planning commission in 1985

- The developmental process was gradually {bureaucratized and divorced} from Panchayati Raj.

- >  'Grass without Roots' since the democratization weakened the Panchayati Raj institutions.


- Differed from Dantwala Committee Report (1978 - Block Level) and the Hanumantha Rao Committee Report (1984 - District Level) in terms of leading role appointed to Panchayati Raj in local planning and development.

- [[GVK Commettee - Recommendations]]