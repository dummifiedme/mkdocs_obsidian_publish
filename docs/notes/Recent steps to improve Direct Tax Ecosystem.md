---  
title: Recent steps to improve Direct Tax Ecosystem  
source: None  
date: 2020-12-31  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Recent steps to improve Direct Tax Ecosystem

- Personal Income Tax
	- Concessional rates to individuals/co-operatives if not availing certain exemptions
	- Finance Act, 2020
- Abolition of [[Devolution Distribution Tax]]
	- boosts attractiveness of the share/equity markets.
- [[Vivad se Vishwas]]
	- dispute redressal forum
	- for pending tax disputes
	- brings down the mounting litigation costs.
- Faceless e-Assessment System
	- privacy
	- reduces corruption 
	- team based assessments
- [[Document Identification Number (DIN)]]
	- auto-generated number for every communication of the department.
	- efficiency and transparency
- Simplification of compliance norms for [[start-ups]] 
	- Hassle free taxation environment
		- simplification of tax assessment procedures.
		- exemptions from Angel-Tax
		- constitution of dedicated star-ups cell etc
- Raising monetary limit for filing of appeal
	- Raised the limit for appeals at various levels,
	
![[SS - Vision - Measures to improve direct tax collection.png]]