---  
title: NAVDEX and IDEX  
source: None  
date: 2021-02-25 05:14  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[Keywords GS2]] , 
---

# NAVDEX and IDEX
**flashcard**{: #flashcard}{: .hash}  

- India-UAE Naval Exhibition (not an exercise).
- Recently Indian Naval ship - [[INS Pralaya]] goes to Abu Dhabi for this exhibition.
- Biennially takes place.
- Only exhibition of this sort in whole of [[MENA region]].