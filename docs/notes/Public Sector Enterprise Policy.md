---  
title: Public Sector Enterprise Policy  
source: None  
date: 2021-02-25 05:09  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# Public Sector Enterprise Policy

## Strategic Sectors where public holding will remain
**flashcard**{: #flashcard}{: .hash}  

- Atomic Energy
- Space
- Defence
- transport and telecommunication
- power
- petroleum
- coal, other minerals
- Banking, Insurance and Financial services

## Fate of non-core sectors
Either close them or privatize them.