---  
title: Presiding Officers of Parliament  
source: None  
date: 2021-03-11 15:58  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Presiding Officers of Parliament
| Lok Sabha               | Rajya Sabha     |
| ----------------------- | --------------- |
| Speaker                 | Chairman        |
| Deputy Speaker          | Deputy Chairman |
| A panel of chairpersons | A panel of vice-chairpersons                |

[[Speaker of LS]]