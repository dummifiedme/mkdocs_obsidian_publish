---  
title: Human Capital Index  
source: None  
date: 2021-05-08 21:20  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Human Capital Index
**flashcard**{: #flashcard}{: .hash}  

- Launched in 2018, as part of Human Capital Project(HCP) - global effort - [[World Bank]]
- Measures the capital loss due to lack of education and health.
	- *Basically in children - the future human capital*
- Ranges between 0 and 1.
- 174 countries (2020)
	- Topped by Singapore
	- Hong Kong
	- Japan 
	- Korea
	- Canada
	- *India at 116th position (2020)* - decreased score wrt 2018. <!--SR:!2021-08-18,2,230-->