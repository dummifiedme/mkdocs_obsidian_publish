---  
title: Bhatti Mines  
source: None  
date: 2021-03-17 22:52  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Bhatti Mines
**flashcard**{: #flashcard}{: .hash}  

- Used for sand mining.
- Now abandoned and hollow.
- Now a [[Asola Bhatti Wildlife Sanctuary]]
![[Asola Bhatti Wildlife Mines 1.png]]