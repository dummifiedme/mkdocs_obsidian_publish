---  
title: Pilibhit Tiger Reserve  
source: None  
date: 2021-03-20 00:29  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Pilibhit Tiger Reserve
**flashcard**{: #flashcard}{: .hash}  

- Situated in Pilibhit District, Lakhimpur Kheri District and Bahraich District of UP.
- Notified as Tiger Reserve in 2014
- Forms a part of Terai Arc Landscape.
- At the boundary of India-Nepal on the north and Sharda & Khakra Rivers in south.