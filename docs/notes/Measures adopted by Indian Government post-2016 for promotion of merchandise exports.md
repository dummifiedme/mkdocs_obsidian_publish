---  
title: Measures adopted by Indian Government post 2016 for promotion of merchandise exports  
source: None  
tags: status/seed  
date: 2020-12-31 21:54  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Measures adopted by Indian Government post-2016 for promotion of merchandise exports
- mid-term review of Foreign Trade Policy 2015-2020 -> in 2017
	- incentive-rate revisions for MSME/labour-intensive sectors
- "Logistics Division" established
	- a part of [[Department of Commerce]]
	- to organize the integrated development of logistics sector
- [[Trade Infrastructure for Export Scheme (TIES)]] launched in April 2017 
	- to address the existing export infrastructure gaps
- other sector specific policies such as Agriculture Export Policy to target export contribution at micro-level.
- [[Transport and Marketing Assistance Scheme]]
	- introduced for export of specified agriculture products
	- to mitigate the disadvantage of higher cost of transportation