---  
title: Exercise Plan  
date: 2021-06-27 21:59  
tags: personal/health  
---  
# Exercise Plan
!!! note 
	- Pick any **three** from [[Exercise Plan#General Body Exercises]]
	- Pick **two each** from all sections in [[Exercise Plan#ACL Strength Training]].

## Warm Up Exercises
- Side Bends
- Forward-Backward Bends
- 
## General Body Exercises
- Push-ups
- Squats
- Plank
- Scissors
- ABS

## ACL Strength Training
> Focus is to *remove all kinds of pain* in the knee and to bring more *balance*. Ideally, there shouldn't be much pain while doing these exercises and general motion. But currently( as on 2021-06-27), there is.

**Quads and Hamstrings**
- Squats
- Resistance-band Squats
- Wide-stance Squats
- Belgian Squats
- Wall Squats
- Lunges
- Walking Lunges

**Calves**
- Toe-Raises
- Calf-Stretches (Wall)

**Sitting Exercises**
- Hamstring Curls
- Sitting Leg Raises
- Static Exercises

**Hips**
- Front Leg Raises
- Side Leg Raises