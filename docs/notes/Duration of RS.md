---  
title: Duration of RS  
source: None  
date: 2021-03-10 18:46  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Duration of RS
- RS first constituted in 1952.
- Permanent body
	- not subject to dissolution.
- One-third of the RS members retire every second year.
	- Seats are filled up by fresh elections and nominations at the beginning of every third year.

## Term of office for a RS member
**flashcard**{: #flashcard}{: .hash}  

- No fixed term of office of members as per constitution.
	- [[Representation of People Act, 1951]] fixes it at 6 years.
> First batch had a lottery on who will retire.