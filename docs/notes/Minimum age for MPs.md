---  
title: Minimum age for MPs  
source: None  
date: 2021-03-10 19:04  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Minimum age for MPs
**flashcard**{: #flashcard}{: .hash}  

- Must not be less than 
	- 30 years of age - RS
	- 25 years of age - LS