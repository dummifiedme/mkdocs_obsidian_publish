---  
source: None  
date: 2020-12-14 02:12  
card-deck: UPSC  
file-tags: IR  
---  
## Indian outreach during COVID-19


# Indian outreach during COVID-19

## To Indian Ocean Nations

- ==MISSION SAGAR== - Assistance to Indian Ocean Region Nations
	- ==INS Kesari== for Maldives, Mauritius, Seychelles, Madagascar and Comoros.
	- SAGAR => ==Security and Growth for All in the Region==
		-  2015 by Indian PM
		- for collective action and cooperation 
			- PEACE and SECURITY
	![[Pasted image 20201213213434.png]]