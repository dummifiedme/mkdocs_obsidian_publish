---  
title: Biome  
source: None  
date: 2021-05-30 13:22  
aliases: []  
tags: status/seed environment  
---  
Category:: 

---

# Biome
**flashcard**{: #flashcard}{: .hash}  

A large naturally occurring community of flora and fauna occupying a major habitat, e.g. forest or tundra.
> A biome is an area of the planet that can be classified according to the plants and animals that live in it.