---  
title: System of elections to Lok Sabha  
source: None  
date: 2021-03-10 17:55  
tags: status/seed  
aliases: []  
---  
Category:: 

---

# System of elections to Lok Sabha
For holding the election for LS, states are divided into [[Territorial Constituencies]].

After every census, a readjustment is to be made in 
- allocation of the seats to the state
- division of each state into constituencies based.

Parliament is empowered to determine the authority and the manner in which readjustment is to be made.
- Accordingly, Parliament has enacted ==Delimitation Commission Acts in 1952, 1962, 1972 and 2002==.
- ==42nd Amendment Act of 1976	 ([[CAA 42]])== froze the allotment of seats at the 1971 level till 2000.
	- Further it was extended till 2026 by ==CAA 84 of 2001==.
		- Also, changed to population census of 1991 
- ==CAA 87 of 2003==, changed the census figures to 2001.
	- though wouldn't change the number of seats allotted to each state.

[[Reservation of Seats for SC and STs in LS]]
[[Knowledge/Academics/UPSC CSE/First-Past-The-Post System]]