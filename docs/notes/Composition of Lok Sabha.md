---  
title: Composition of Lok Sabha  
source: None  
date: 2021-03-10 07:48  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Composition of Lok Sabha
- Maximum strength 552
	- {1| 530} members from states
	- {1| 20} members from UTs.
	- {1| 2} are nominated by president from the Anglo-Indian community.

[[Representation of States in LS]]
[[Representation of UTs in LS]]
[[Nominated Members in LS]]