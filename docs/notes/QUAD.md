---  
title: QUAD  
source: None  
date: 2021-03-10 22:40  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR 
# QUAD
**flashcard**{: #flashcard}{: .hash}  

- An informal strategic forum among the like minded democracies across Indo-Pacific Region.
- USA, India, Japan, Australia
	- Floated around by Japan - Shinzo Abe
	- Australia wasn't interest at first - Chinese influence.
		- Later joined QUAD 2.0
- To counter Chinese growth in global supply chain.

## QUAD and India
- A good counter for increasing Chinese footprint  in the South-East Asian countries.
- Since India is not present in the Pacific Ocean, joining QUAD might unsettle it's India-China relations.
- Also, there is no border dispute with China for other members of QUAD. (Japan has small dispute of 2 islands), so that might not even be raised there in the forum.
- India-China Galwan Valley clash seems to be a warning by China.
- We cannot go full offensive against China, since there is a lot of dependencies on it globally
	- US Companies have more than $1 Trillion equities in China.
	- More than 75% of the US companies will continue to invest more there in future.
	- India just can't move out of there.