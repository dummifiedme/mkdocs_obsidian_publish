---  
title: Editorial Discussion - 2021 February  
source: None  
date: 2021-03-10 22:38  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Editorial Discussion - 2021 February

### 2021-02-09 | Tuesday
- PM-JAY

### 2021-02-10 | Wednesday
- [[Nirmala's Disinvestment Policy]]
- [[Trans-fatty acids]]
- Dams and Damage
- State-of-the-apps
	- [[Principles of Data Privacy]]


### 2021-02-11 | Thursday
- Cybersecurity and disinformation
	- Cybersecurity
		- attacks to compromise confidentiality, integrity or availability.
	- Disinformation
		- exploits cognitive biases and logical fallacies
		- [[Cognitive Hacking]]
- [[MTP Act of 1971]]
	- Framed in context of reducing the maternal mortality ratio due to unsafe abortions.

### 2021-02-12 | Friday

> https://www.youtube.com/watch?v=bkZhgpkLlYY

- [[Digital Service Tax]]
- Biden's policy can bring changes to global scenario.
	- US-Iran
		- Nuclear problems
		- Iran's support to Shia 
		- Sanctions
	- US-MiddleEast
		- Revolves around the US-Iran relations
	- US-Russia
		- Called Putin
- Cryptocurrencies and it's state in India

	
### 2021-02-13 | Saturday
- [[India-China Standoff 2020-21]]
- Self-regulation of OTT

### 2021-02-16 | Tuesday
- Biden on [[Palestine Issue]]