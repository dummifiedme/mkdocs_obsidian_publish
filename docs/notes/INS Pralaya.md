---  
title: INS Pralaya  
source: None  
date: 2021-02-25 05:19  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR/defence/navy  
# INS Pralaya
**flashcard**{: #flashcard}{: .hash}  

- Commissioned in 2002
- Prabal Class Missile Vessels
- Built by Goa Shipyard Limited. <!--SR:!2021-11-10,1,230-->