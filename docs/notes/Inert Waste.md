---  
title: Inert Waste  
source: None  
date: 2021-03-17 22:51  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Inert Waste
**flashcard**{: #flashcard}{: .hash}  

Neither biologically not chemically active waste.