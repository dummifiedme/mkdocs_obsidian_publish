---  
title: Merchandise Exports from India Scheme (MEIS)  
source: None  
date: 2021-05-08 20:19  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Merchandise Exports from India Scheme (MEIS)
**flashcard**{: #flashcard}{: .hash}  

- To enhance the export of notified goods manufactured in a country
- Introduced through [[Foreign Trade Policy(FTP)]] - 2015.
- IMplemented by [[Ministry of Commerce and Industry]].
- Incentives are provided aka rewards. Also to the SEZ units. <!--SR:!2021-08-18,2,230-->