---  
title: Relation between the foreign policy and diplomacy  
date: 2020-12-18 05:18  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR   
# Relation between the foreign policy and diplomacy
- Foreign policy refers to a broader set of goals and strategies of a state vis-a-vis the rest of the world.
- Diplomacy is a result of various manifestations (agreements, treaties, alliances etc) of a foreign policy.