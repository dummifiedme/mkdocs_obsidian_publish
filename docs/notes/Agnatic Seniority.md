---  
title: Agnatic Seniority  
source: None  
date: 2021-02-13 12:34  
tags: status/mature  
aliases: ['agnatic']  
---  
# Agnatic Seniority
Category:: [[🗃️ Anthropology]] 

---

##### Agnatic Seniority
- When the monarch's younger brother is preferred over his children as next to the throne.
- Patriarchial System, women are excluded.
	![[Diagram -Agnatic Seniority.png]]
- [[audio - Agnatic Seniority.webm]]