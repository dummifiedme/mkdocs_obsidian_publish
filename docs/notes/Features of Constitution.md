---  
title: Features of Constitution  
date: 2021-07-19 20:21  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   

---
**review**{: #review}{: .hash}  
 
# Features of Constitution
### Features of Constitution
1. Borrowed Constitution
	- [[Knowledge/Academics/UPSC CSE/Borrowed Features of  Constitution]]
2. Bulky Constitution
	1. [[Indian Constitution is Bulky]]
3. Flexibility of Constitution
	1. [[Constitution as a living document]]
4. Despite its exhaustiveness, scope for growth of convention
5. Enacted and a Written Constitution
	1. [[SR Polity Lecture 1#Types of Constitution]]
6. Our constitution is via media (==middle path==), both extremes of parliamentary supremacy and judicial supremacy.

7. Our constitution guarantees ==social and economic justice along with political justice==.

8. Federal constitution with ==centrist bias==.

9. Westminster System of government with an ==elected head of state==.

10. We combine justifiable rights with non-justifiable rights.
11. ==Universal adult franchise== extended to all the citizen in one clean stroke of secular polity.

12. Independent Judiciary