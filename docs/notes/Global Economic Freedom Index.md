---  
title: Global Economic Freedom Index  
source: None  
date: 2021-05-08 21:03  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Global Economic Freedom Index
**flashcard**{: #flashcard}{: .hash}  

- Report by Fraser Institute in Canada
	- Co-published by Centre for Civil Society.
- 'Economic Freedom' = ability of individual to make their own economic decisions.
- Topped by - Hong Kong
- India ranked 105 (in 2020), 78(in 2019).
	- Indian rank in economic freedom fell. <!--SR:!2021-08-17,1,210-->