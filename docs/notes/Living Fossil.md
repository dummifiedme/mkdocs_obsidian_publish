---  
title: Living Fossil  
source: None  
date: 2021-02-25 15:23  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Science and Tech]]

---

# Living Fossil
**flashcard**{: #flashcard}{: .hash}  

- When a species thought to be extinct but is found alive again.
- And the structure isn't changed much between the fossil and the living.