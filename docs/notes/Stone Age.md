---  
title: Stone Age  
source: None  
date: 2021-02-08 21:03  
tags: status/seed note/upsc/anthropology  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/pre
# Stone Age

- Humans emerged in ==middle Pleistocene==.
	- Currently, we are in ==Holocene== Epoch. (See [[Geological Time-Scale]])

- Evolution of early humans studied according to the nature of ==stone tools used by them==.

- According to the ==nature of stone tools used==, the human evolution is divided into three periods.
	- Paleolithic Period
	- Mesolithic Period
	- Neolithic Period

## Timeline
![[Timeline -Stone Age classifications.jpeg]]
[[Pre-History Timeline#Stone Age|no-h]]