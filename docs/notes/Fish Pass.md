---  
title: Fish Pass  
source: None  
date: 2021-02-17 00:33  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Fish Pass
- Passage for Fishes in Dam areas.
	- Stepped passage.
- Example : at Farrakka Barrage 
	- [[Hilsa Fishes]] used to come from sea water to Prayagraj. But now restricted at Farrakka due to Dams.