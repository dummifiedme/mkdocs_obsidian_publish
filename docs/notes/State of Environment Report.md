---  
title: State of Environment Report  
source: None  
date: 2021-03-07 23:24  
tags: status/seed  
aliases: []  
---  
Category::  

---

# State of Environment Report
- released by [[Centre for Science and Environment]].

## State of Environment Report 2021
- Ranked on the basis of achieving Sustainable Development Goals
	- Best States
		- Kerala, 
		- HP, 
		- Andhra Pradesh, 
		- TN, 
		- Telangana.
	- Worst Performer
		- Bihar
		- Jharkhand
		- Arunachal Pradesh
		- Meghalaya
		- UP
- More polluted between 2009 and 2019