---  
title: Stand-Up India Scheme  
source: None  
date: 2021-03-17 22:17  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Stand-Up India Scheme
**flashcard**{: #flashcard}{: .hash}  

- Started in 2016 by Department of Financial Services, Ministry of Finance.
- Facilitates Bank loans for setting up new enterprises.
- SC/ST Women - greenfield project - 18+
> More than 81% of the accounts under this scheme are to women entrepreneurs.