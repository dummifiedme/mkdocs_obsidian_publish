---  
date: 2022-01-05 12:58  
aliases: []  
---  
**note**{: #note}{: .hash}  
/general/knowledge/tech/linux 
# Jellyfin Server on Linux
- Install `jellyfin-bin` from AUR.
- Run two commands
	- `sudo systemctl enable jellyfin.service`
	- `sudo systemctl start jellyfin.service`
- To check status: `sudo systemctl status jellyfin.service`
- Open browser and go to https://localhost:8096
	- Set up the profile
	- If HDD not accessible:
		- `sudo chmod -R a+rx /run/media/` for read and write permissions



- - -
## References
- [b] [How to set up the Jellyfin media server on Linux](https://www.addictivetips.com/ubuntu-linux-tips/jellyfin-media-server-linux/)
- [b] [Jellyfin can't detect external drives and files in home directory - **4**{: #4}{: .hash}  
 by Hienn - AUR - Manjaro Linux Forum](https://forum.manjaro.org/t/jellyfin-cant-detect-external-drives-and-files-in-home-directory/90994/4)