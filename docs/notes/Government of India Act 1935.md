---  
---  
# Government of India Act 1935
---
Date: 2020-12-02 17:09
Title: Government of India Act 1935
Parent: [[SR Polity Lecture 6]]
---

### Government of India Act 1935

Q: What is the impact of Goi Act 1935 on the present consitution?

### Background
- Dissatisfaction with Dyarchy.
- Non - Cooperation Movement.
- Muddieman Committee 1924
- Simon Commission 1927
- Protest against Simon Commission.
- Nehru Report 1928.
  - Demanded dominion status
- Congress Session 1929 at the banks of River Ravi in Lahore.
  - Nehru put forward the thought of Poorna Swaraj
- Civil Disobedience Movement 1930 by Gandhi.
- First Round Table Conference -> not participated by INC
- Gandhi-Irwin Pact 1931
  - Gandhi attended the second round table conference in 1931= fruitless. 
- Ramsey MacDonald Award = Communal Award 1932
 - Aimed at reducing Hindus into minority by giving separate electorate to the depressed classes.
- Poona Pact 1932
- Third Round Table Conference 1932
  -  no participation by INC
- on the basis of Third Round Table Conference, British Government produced a White paper. 
  - It was considered by the joint committee of British Parliament led by Lord Linlithgow. 
  - It also had participation by princely states.
  - it approved the introduction of federal form of government but with some conditions.
    - federation will some under consideration, only when approved by 50 percent of the princely states.
    - hence, federation never came into existence because states never approved.
      - princely states were not willing to surrender their fiscal autonomy.
      - smaller states were fearful of lack of adequate representation of them.
      - paramountcy issue was not resolved.

### Features of GoI Act 1935
1. Federation => to be comprised of
   1. Princely states
   2. Chief governor territories
2. For the first time provinces were recognized as separate entities having their own executive and legislative powers in their own field. 
3. The Constitution had provided three lists
   1. Federal List 
   2. Provincial List
   3. Concurrent List
4. Residuary powers with  the center --> viceroy. 
5. Since federation couldn't come into existence, ==Provincial autonomy was introduced==. 

### Form of Government at Provincial government 
1. At the Provincial level, popular (elected by people) government was introduced. 
2. The principle of collective responsibility was introduced. 
3. Governor was to act on the aid and advice. 
4. Members of legislature could ask questions, Supplimentary questions and could bring no confidence motion. 

> NOTE: The balancing of responsible government. 
> - Responsible government but governor had powers of discretion. 

#### Discretionary powers of Governor 
1. Power to summon legislature. 
2. Power to reserve the bills. 
3. Power to give assent to the bills. 
4. Take over the administration and run the administration of the provinces. 


Governors were also given special powers
1. With respect to the administration of tribal areas
2. Safeguarding the rights of the minorities. 
3. Protecting the privileges of civil servants. 

> Viceroy continued to be the ultimate power. 

### Changes at the Union level
1. Introduction of the dyarchy at the union level. 
2. Division of the subjects into transfered and reserved
3. However in practice dyarchy also never came into existence. 

### Other changes by GoI Act 1935
1. It abolished council of India. 
2. It established RBI to control currency and credit. 
3. Administration of Myanmar was separated. 
4. New provinces - Orissa and Sindh - were created. 
5. Federal Court was established in Delhi in 1937 => later became Supreme Court. 
6. I troduction of Provincial and Joint Public Service Commissions . 

### Assessment of GoI 1935
> JL Nehru - 
> "We are provided with a car, all brakes, no engine, unwanted, undemocratic and anti-national." 

> Churchill - 
> " A gigantic quilt of jumbled crochet work, a monstrous monument of shame built by pygmies." 

> Lord Linlithgow - 
> "Afterall we are farming the Constitution as we thought as it was the best way to hold India to Empire." 

> Tomilson - 
> "Reforms were dictated by the needs to attract Indians to collaborate with Raj. It was completely imperialist in design."


### Influence of GoI Act 1935 on present consitution

1. Government of India Act 1935 was primarily based on the 'white paper' released by British Government in 1933.
   - White paper was prepared by committee at British Parliament which also had representation of princely state. 
   - it was primarily based on the report of Simon Commission. 
   - it hardly represented the voice of INC, the only mass party in India. 
2. There was a stern opposition to Simon Commision as well as GoI Act 1935
   - The strongest opposition was from Nehru who compared it to a car with no engines but all brakes. 
      - thus, Pandit Nehru questioned the viability of the act. 
   - Rightly considered as imperialist in design and as mentioned by Lord Linlithgow, the aim of the act was to hold India to the Raj. 
    - thus the two parties were having contradictory aims
      - Congress was for swaraj, while British government wanted to hold the Raj by incorporating the loyalists. British always followed the policy of 'carrot and stick' . 
      - Constitutional reforms have been to pacify Indians, they have always been like a half baked cakes. 
      - response of the British has always been too little, too late. 
3. It is true that the act can be treated as a major progress, considering it as the first written Constitution for India, the introduction of collective responsibility and Provincial autonomy. However they continued the balancing act through the discretionary powers of the Governor and ultimate discretionary powers of the Union. Hence, so called responsibility to the legislature was actually responsibility to the Governor. Hence it was rightly called as car without engine. It was hardly suitable for meeting the Indian aspirations. Yet, Congress decided to participate in the elections, Congress was sure that it will be able to form the government in most of the provinces, it will legitimize the Congress as a real representatives of the Indians. 
 
 
 
###  Influence of GoI Act 1935.... 
What type of Constitution of India will be has been subjected to a lot of debates and discussions with multiple formulae and perspectives. Cabinet Mission Plan proposed weak federation. Center to have powers only on three subjects - defence, communications and currency. However, with the partition of India and the breakdown of law and order situation, alom with the secessionist demands from various regions like Nagas and Mizos, war with Pakistan, constituent Assembly ultimately had to go for an extremely centralised federation. The GoI Act 1935, which was the object of nationalist attacks, because the basic reference to determine the future Constitution. 

The present center and state relations, division of powers, emergency provisions, discretionary powers of Governor, special responsibility of Governor wrt tribal areas have been drawn from the Act. Thus the basic design of Indian federation, which is often criticized as quasi federal is hardly Indian in origin. 
Even the directive principles of state policy are drawn from the instrument of instruction in the Act. Hence, it is an irony that the situation compelled us to adopt majority of the provisions of the Act. 

Contrary to the myth that Indian Constitution was framed int he atmosphere of optimism, it was actually framed int eh atmosphere of fear and trepidation.