---  
title: Mandarin Duck  
source: None  
date: 2021-02-17 00:56  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Mandarin Duck
**flashcard**{: #flashcard}{: .hash}  

- South East Asian Bird
- Spotted in India - [[Maguri-Motapung Beel]], Tinsukia Assam.
	- Very rarely visit India
- IUCN - Least Concerned.