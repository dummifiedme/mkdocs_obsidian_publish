---  
title: High Impact Community Development Projects  
source: None  
date: 2020-12-14 02:12  
tags: status/seed note/upsc/GS2/IR  
aliases: ['HICDPs']  
---  
Category:: [[International Relations]]

---

# High Impact Community Development Projects
**flashcard**{: #flashcard}{: .hash}  

> High Impact Community Development Projects
- Projects with high level of community impact and participation
![[HICDPs InfoBox -Vision.png]]

**HICDPs** are projectes in the areas of ::: 
	- livelihood and income generation
	- health
	- education
	- gender - child empowerment
	- sports
	- sustainable development

- India has signed **HICDPs** with ::: 
	- Maldives
	- Afganistan
	- Bhutan etc