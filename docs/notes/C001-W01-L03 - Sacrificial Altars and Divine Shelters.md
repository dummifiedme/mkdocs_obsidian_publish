---  
title: C001-W01-L03 - Sacrificial Altars and Divine Shelters  
source: https://www.youtube.com/watch?v=opyi4PFWQQs  
date: 2021-01-16 22:40  
aliases: []  
tags: culture  
---  
# C001-W01-L03 - Sacrificial Altars and Divine Shelters
Lecture Code: C001-W01-L03
Video Source : https://www.youtube.com/watch?v=opyi4PFWQQs

- Vedic practices had sacrificial rituals. So the upcoming and emerging divine shelters needed to be reconciled these aspects as well.
- Hence, shelter combined with sacrificial alters.
- Early shelters
	- Tree - vertical shelters.
	- Hut like designs
- Either stone worships or sacrifices.
	- ![[Pasted image 20210116224719.png]]
		- Shiva linga and the yonis -> male and female reproductive parts were worshiped. 
	- From Elora ![[Pasted image 20210116224906.png]]
	- From Brihadeshwara 
		![[Pasted image 20210116224951.png]]
- The vertical axis will manifest in all directions and fill the "space"
	- Central vertical axis = Linga
	- ![[Pasted image 20210116225221.png]] 

## Temples
### Nagara Temples 
The temples share the same idea of following a vertical axis and the body just acts like a "sheath" around the axis.
![[Pasted image 20210116225559.png]]

> So, the axis actually holds some kind of divinity.


---

**What are tirthas?** ::: 
thresholds of crossings

---

- Shulva Sutras
	- The sacrificial manuals
	- Contained the guidelines to make the sacrificial altars.
	 ![[Pasted image 20210116225917.png]]
	 
### The age of great temples
![[Pasted image 20210116230149.png]]

All of these had a "vertical axis" and "altars" at top.

All the later temples had all the characteristics as the 540AD Elephanta cave-temple.
- Had two axes 
	- ritual axis - east to west - to linga
	- north to south axis - to a grand idol of trimurti.
![[Pasted image 20210116230545.png]]

> Simple cave in Barabar to a complex Elephanta Caves to temples

![[Pasted image 20210116230740.png]]

Manifestation of gods along all the directions to fill the whole universe.

### Two type of temples
1. Latina Temples
2. Kutina Temples

#### Latina Temples
- Towards the north of Karnataka
- ![[Pasted image 20210116230916.png]]
- Utara-vedi is a altar.
- The the space between the altars is filled by the body. Multi-Stored palace cloaking the *vertical axis*(divine) of the temple.