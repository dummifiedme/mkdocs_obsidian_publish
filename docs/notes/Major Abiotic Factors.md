---  
title: Major Abiotic Factors  
source: None  
date: 2021-04-17 07:00  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Major Abiotic Factors
- [[Abiotic Factor - Temperature]]
- [[Abiotic Factor - Water]]
- [[Abiotic Factor - Light]]
- [[Abiotic Factor - Soil]]