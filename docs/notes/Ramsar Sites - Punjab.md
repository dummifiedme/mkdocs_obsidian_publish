---  
title: Ramsar Sites - Punjab  
date: 2021-06-24 18:17  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - Punjab
(6x)
**flashcard**{: #flashcard}{: .hash}  

- Harike Wetlands
- Keshopur-Miani Community Reserve
- Ropar [[Wetland]]
- Kanji [[Wetland]]
- Nangal Wildlife Sanctuary
- Beas Conservation Reserve
![[Map - Ramsar - JK,Leh,PB,HP 1.png]]