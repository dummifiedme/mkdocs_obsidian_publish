---  
date: 2021-12-25 23:16  
aliases: ['Civil Services and Democracy']  
status: 🟢  
---  
# Civil Services and Democracy
The relationship between Civil Services and Democracy is *Paradoxical* and *Complementary* at the same time.

## Paradoxical

|Civil Services                                   | Democratic Institutions         |
|--------------------------------------------- | ------------------------------- |
|Legal and indifferent                            | responsible                     |
|Hierarchical , Authoritarian                     | Participation, consensus making |
|Conservative Outlook                             | Accommodating Outlook           |
|demands principles of consistency and regularity | demands Principle of Change     |

## Complementary
- Necessary for providing effective and responsive governance                              
- Balanced - responsiveness of democratic institutions and impartiality of the bureaucracy
- Civil Services minimize arbitrary actions, governments tempted to engage in for votes.  
- Democratic process is important for legitimacy on governing process. 
- Emphasis on efficiency and use of market for public service delivery → values of the civil services important in democracy.                                                                                     

- - -
## References
- [b] Ref:: [[VVAM-216 (Governance) Role of Civil Services in a Democracy (pg.33).pdf#page=9]]