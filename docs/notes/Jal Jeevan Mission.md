---  
title: Jal Jeevan Mission  
source: None  
date: 2021-02-25 16:25  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# Jal Jeevan Mission
**flashcard**{: #flashcard}{: .hash}  

- Universal coverage of water supply ad sewage treatments 
- Centrally Sponsored
![[PeyJalSarvekshanCities.png]]