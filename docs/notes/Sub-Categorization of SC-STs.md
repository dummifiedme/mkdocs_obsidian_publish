---  
---  
# Sub-Categorization of SC-STs

---

Q: Can there be a Creamy Layer in case of SC/STs? Why or Why not? 
A: Concept of Creamy Layer in SC/STs was upheld by SC in 2018 in [[Jarnail Singh vs Lachhmi Narain Gupta]].

---

Q: What are the reasons behind Scheduled Castes remaining without any sub-category?
A: 
- SInce, its not based on educational, economic or other backwardness but those who suffered untouchability.
- No Preferential Treatment
> In EV Chinnaiah Case in 2005
> "all SCs can and must collectively enjoy the benefits of reservation regardless of inter-se inequality"  

![[3 Resources/Pasted image 20201126061903.png]]
![[3 Resources/Pasted image 20201126061913.png]]