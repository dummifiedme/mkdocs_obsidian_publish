---  
title: Right - Left - Center  
date: 2020-12-02 16:50  
alias: []  
tags: status/seed  
sr-due: 2022-01-25  
sr-interval: 17  
sr-ease: 250  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# Right - Left - Center
- Right, Center and Left originated in French Revolution.
    - People sitting right, left or center of the king.

| Right        | Center          | Left                |
| ------------ | --------------- | ------------------- |
| Conservative | [[Liberals]]        | Socialist/Communist |
| Tilak        | Dadabhai Naroji | Nehru               |
|              | JL Nehru        |                     |
|              | BR Ambedkar     |                     |

- Gandhism is an ideology in itself.