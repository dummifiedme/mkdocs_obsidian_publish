---  
title: Financial Inclusion  
source: None  
date: 2021-05-06 11:42  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Financial Inclusion
- Bank accounts for everyone
- Investments other than Bank
	- Department of Post
	- Chit Funds and Prize Chits
- Credit - Loans
	- Credit Guarantee
	- Refinance
	- MSME
	- NBFC - MUDRA
	- Stand Up India Scheme
	- SHG
	- Kisan Credit Card
	- Interest Subvention
	- Paisa Portal
- Customer Protection