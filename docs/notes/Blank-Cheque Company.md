---  
title: Blank-Cheque Company  
source: None  
date: 2021-03-07 23:01  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Blank-Cheque Company
**flashcard**{: #flashcard}{: .hash}  

A company formed solely for acquiring another company.
- The money is raised from the Public.
> the money is kept in an [[escrow account]] while the transaction.