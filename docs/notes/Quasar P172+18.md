---  
title: Quasar P172+18  
source: None  
date: 2021-03-17 21:24  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Quasar P172+18
'Radio Loud' [[Quasar]]
**flashcard**{: #flashcard}{: .hash}  

- Radio loud - has beam
- Radio Quiet - no beam - less radio emission. (Mostly these are found)