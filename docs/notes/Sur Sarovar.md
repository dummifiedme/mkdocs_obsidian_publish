---  
title: Sur Sarovar  
date: 2021-06-24 18:17  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Sur Sarovar
- **Location** - ==Agra, UP==
- **Major species** - Sarus Crane, Greater Protted eagle, wallago catfish
- Declared a Ramsar Site in 2020
- Created initially as a water supply storage