---  
title: Guru Nanak Dev  
source: None  
date: 2021-02-25 14:51  
tags: status/mature  
aliases: ['Baba Nanak']  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/persons  
# Guru Nanak Dev
**flashcard**{: #flashcard}{: .hash}  

- Born: April 15, 1469 at Rai Bhoi ki Talwandi, near Lahore
- founder of Sikhism
- Started writing the Guru Granth Sahib.
	- Added 900 hymms
- Community based meditation - God is one. <!--SR:!2021-09-22,1,230-->