---  
---  
# Essentiality vs Right to Freedom of Religion
‘[[Ratilal Gandhi vs the State of Bombay]]’ (1954) acknowledged that “every person has a fundamental right to entertain such religious beliefs as may be approved by his judgment or conscience”. 
> However, the Essentiality test **impinges** on this autonomy.