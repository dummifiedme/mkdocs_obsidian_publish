---  
---  
# Classical Languages
| Source                                     | Tags                       | Stage       |          |
| ------------------------------------------ | -------------------------- | ----------- | -------- |
| :------------------------------:           | :----------**mPuri**{: #mPuri}{: .hash}  
 ------------: | :------ :   |          |
| [[../1 Projects/UPSC/GS2/MPuri GS2 Notes]] | **artCulture**{: #artCulture}{: .hash}  
 **m**{: #m}{: .hash}  
             | **2020**{: #2020}{: .hash}  
-12-07 | #📝/1 |
|                                            |                            |             |          |

---
### Classical Languages
#### Criteria
1. High Antiquity -> Sahitya Academy recommendation
2. Original literacy tradition
3. A large body literary text
4. The classical literature is distinct from the modern literature.

#### CLassical Language Status benefits
1. Awards presented annually by the centre.
2. A Center for Excellence in language would be established.
3. UGC would seek to setup a chair on these languages in central universities.

#### Classical Languages - LIST
- Tamil
- Telugu
- Kannada
- Malayalam
- Sanskrit
- Odia