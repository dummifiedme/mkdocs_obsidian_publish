---  
title: Sangay Volcano  
source: None  
date: 2021-03-20 00:58  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Sangay Volcano
**flashcard**{: #flashcard}{: .hash}  

- In Equador
- Sangay National Park around it.
- It forms a part of Andes Mountains
![[Volcanos in Equador Map.png]]