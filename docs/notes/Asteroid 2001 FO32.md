---  
title: Asteroid 2001 FO32  
source: None  
date: 2021-03-20 01:12  
tags: status/seed  
aliases: []  
---  
Category:: [[Celestial Bodies]]

---

# Asteroid 2001 FO32
- Discovered in 2001
- Speed greater than other asteroids.
- no threat of collision
- passing by - 2M kms away.