---  
---  
# Demerits of System of Proportional Representation
---
TARGET DECK
UPSC::General Studies 2

FILE TAG
GS2 Polity::Parliament Elections

---

## Demerits : System of Proportional Representation
A: The following demerits:
1. Highly expensive.
2. No scope for organizing by-elections.
3. Eliminates intimate contacts between voters and representatives.
4. Promotes minority thinking and group interests.
5. Increases the significance of party system + decreases that of voter.