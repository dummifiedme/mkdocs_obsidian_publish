---  
title: East Kolkata Wetlands  
date: 2021-06-11 10:17  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# East Kolkata Wetlands
**flashcard**{: #flashcard}{: .hash}  

Renowned as a model of a *multiple use [[Wetland]]*
- The outflow form the city is utilized in fish ponds
- Generates 150 tons of vegetables daily
- Produces 10500 tons of fish every year - livelyhood of 50000 people
- Ramsar site since 2002