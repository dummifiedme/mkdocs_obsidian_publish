---  
title: International Mother Language Day  
date: 2021-02-25 15:14  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/languages
**note**{: #note}{: .hash}  
/upsc/prelims/days


# International Mother Language Day
**flashcard**{: #flashcard}{: .hash}  

- **Date**:: 21st February
- **Description**:: Started by USA in 1999. <!--SR:!2022-01-07,3,250-->