---  
title: Disinvestment vs Strategic Disinvestment  
source: None  
date: 2021-02-13 00:50  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Disinvestment vs Strategic Disinvestment
| Disinvestment                                             | Strategic Disinvestment                       |
| --------------------------------------------------------- | --------------------------------------------------------- |
| When the share sold doesn't effect the managerial control | When the management is also given off to private                                                      |                                                         |
|                                                           | For increasing efficiency      |