---  
title: 📀 Vision - Post-Independence History of India  
MaterialType: 📀  
MaterialImportance: None  
Status: 🔴  
LecturesDone: 1  
LecturesNotes: 0  
---  
# 📀 Vision - Post-Independence History of India
Category:: [[🗃️ History - Post-Independence India]]
!!! note "📀 Vision - Post-Independence History of India"
	Publisher:: VisionIAS
	LectureBy:: *MAM*
	Year:: 2019
	Lectures:: 6
	Duration:: 3 hours
---

```dataviewjs
function pageProgress(dv){
	let lectures = dv.current().Lectures
	let dlectures = dv.current().LecturesDone
	let nlectures = dv.current().LecturesNotes

const rprogress = "<progress value=\""+ dlectures +"\" max=\"" + lectures + "\">reading</progress> (" + dlectures + "/" + lectures + " lectures)";
	const nprogress = "<progress value=\""+ nlectures +"\" max=\"" + lectures + "\">reading</progress>(" + nlectures + "/" + lectures + " lectures)";
	dv.paragraph(dv.current().Status + dv.current().title + "\n**Watching** " + rprogress + "\n**Notes** " + nprogress);}
	
pageProgress(dv)

```


## Topics and Notes

Two components in the lecture - story and the notes. The dictated notes to be written as is, while the story is to be jotted down as and when necessary. Preferably in a section separated by `---`.

---

Entire Post-independence History is divided into four eras - 
- Nehruvian Era (1947-64)
	- Integration of Princely States
	- Tribal Consolidation
	- Official Language issue
	- LInguistic reorganisation of states
	- Foreign Policy of Nehru
- Shastri Era (1964-66)
	- Issues confronting Indian polity and governance
	- 1965 Indo-Pakistan War
	- Evolution of Slogan - "Jai Jawan, Jai Kisan"
- Indira Gandhi Era
	- 1966-69
		- 1967 Elections
		- 1969 Congress Split
	- 1969-73
		- 1971 Bangladesh Liberation War
	- 1973-77
		- Gujrat Unrest
		- Bihar Unrest
		- JP Movement
		- Emergency
	- 1980-84
		- Assam Crisis
		- Kashmir Crisis
		- Punjab Crisis - 'Operation Blue Star'
- Rajiv Gandhi Era
	- Sri Lanka Civil War
	- Various developments
	- Bofors Scam
- Post 1990
	- Mandal Commission
	- Babri Masjid Demolition
	- Pokhran Test
	- Kargil War
- Miscellaneous topics
	- Land Reforms
	- Social Impact of Green Revolution
	- Social Movements