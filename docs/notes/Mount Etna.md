---  
title: Mount Etna  
source: None  
date: 2021-03-11 22:09  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Mount Etna
**flashcard**{: #flashcard}{: .hash}  

- In Italy.
- Highest peak in Italy
- Volcanically active
- Due to subduction 
![[Pasted image 20210311220942.png]]