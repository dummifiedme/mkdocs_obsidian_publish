---  
---  
# How to - GS4
**plan**{: #plan}{: .hash}  
/how-to

- The main source — **Syllabus**.
	- Grab the 30 terms mentioned in the syllabus and they are to be used everywhere.
	- Try to use all of them in some part or another of the paper.
- **Theory** — not much. Just mention in the answers.
- **PYQ TREND**
	- Check for the trend of questions.
	- Don't incline too much on the pattern. It keeps changing.
	- Practice all the PYQ since 2013 (Since it was initiated)
		- Questions are often repeated.
- **Sources**
	- Pick one source/book.
	- Make sure to cover content.
- Three types of questions
	- Repeated
	- Syllabus
	- Applied

## Answer Writing
- Write 10-12-15 points in other GS, but not in GS4. 
- But use hub-spoke model to write less important points. Mention only the best 4-5 points in detail/in bullets.
	- Because the space is limited.
- Use *diagrams* as and when possible.
### Structure
1. Intro
2. Keywords & Quotes
4. Answer Body
	1. Quality
	2. Quantity
5. Theory
6. Example
	1. Professional Life
	2. ~~School, Family~~
	3. Work Experience — create a fake job :D
	4. Social Issues
	5. Government Policies
	6. Area of Interests ⭐️
	7. Sports
		1. IMPORTANT
		2. Relate with Current
	8. Sugar Coated LIES
	9. Diagrams
		1. Block 
		2. Venn
		3. Hub-Spoke
	10. Conclusion
		1. VERY IMPORTANT.

### Case Studies
*More IMPORTANT*

#### Format
1. Actors Involved
2. Dilemma
3. Answer Body
4. Theory
5. Diagram
6. Conclusion — A MUST. Keep a few iterations of the following statement:
	> The above mentioned steps address all the actors and the dilemma hence it is a holistic solution