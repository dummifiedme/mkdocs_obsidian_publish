---  
---  
# Affinal Relationship
When kins are related by marriage, it is called ==Affinal== Kins. 
- Relatives formed through marriage.

**Examples of Affinal Kins** ::: 
Husband and Wife and their relatives.