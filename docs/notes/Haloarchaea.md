---  
title: Haloarchaea  
source: None  
date: 2021-05-28 16:19  
tags: status/seed  
aliases: ['halophilic archaea']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Haloarchaea
**flashcard**{: #flashcard}{: .hash}  

Haloarchaea or halophilic archaea is a bacteria culture that produces pink pigment and is found in water saturated with salt.