---  
title: Cognitive Hacking  
source: None  
date: 2021-02-13 00:19  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# Cognitive Hacking
**flashcard**{: #flashcard}{: .hash}  

A cyberattack that seeks to manipulate the perception of the people by exploiting their psychological vulnerabilities.