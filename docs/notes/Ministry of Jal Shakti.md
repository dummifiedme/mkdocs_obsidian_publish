---  
---  
2019-May: Government formed *Jal Shakti Mantralaya* by merging following ministries:

1. Ministry of Water Resources, River Development and Ganga Rejuvenation

2. Ministry of Drinking Water and Sanitation

Now Ministry**1**{: #1}{: .hash}  
 and Ministry**2**{: #2}{: .hash}  
 have been made ‘Departments’ under the Jal Shakti Ministry.