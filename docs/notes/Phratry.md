---  
aliases: ['phratries']  
tags: ANT-P1  
---  
# Phratry


##### What is Phratry?
**flashcard**{: #flashcard}{: .hash}  

A group of clans.
Unilineal [[Descent]] group larger than a [[Knowledge/Vocabulary/Clan]].
> Derived from a Greek word "*phrater*" meaning "brother"








Are the relationships in a phratry are clearly defined? ::: 
**todo**{: #todo}{: .hash}  
/flashcard 

##### What is the nature of marriage in a Phratry?
**flashcard**{: #flashcard}{: .hash}  

May or may not be an exogamous marriage.
There can be a intra-phratry marriage unline clans, lineages or families.