---  
title: Sant Ravidasji  
source: None  
date: 2021-02-25 14:30  
tags: status/seed  
aliases: []  
sr-due: 2022-01-16  
sr-interval: 8  
sr-ease: 250  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/saints 
# Sant Ravidasji
 **flashcard**{: #flashcard}{: .hash}  

- Span:: 15th century
- Region:: Kashi(Benaras) UP
- Sect::  [[Nirgun Sampradaya]] - sant parampara
- Saint, Philosopher, Poet, Social reformer, follower of God.
- Harijan Caste.
- Equality and against discrimination.
- Values expressed in Constitution as well - Ambedkar.


- [B] **Related**: [[Map - Bhakti Saints]]