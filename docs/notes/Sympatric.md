---  
title: Sympatric  
source: None  
date: 2021-05-20 19:01  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Sympatric
**flashcard**{: #flashcard}{: .hash}  

In Biology, two related species or populations are considered **sympatric** when they exist in the same geographic area and thus frequently encounter one another.
![[https://media.nationalgeographic.org/assets/photos/135/969/0a3aac3a-e676-4b49-9c50-67083cab7f6f.jpg]]