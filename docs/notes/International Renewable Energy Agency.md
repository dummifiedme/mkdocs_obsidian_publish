---  
title: International Renewable Energy Agency  
source: None  
date: 2021-03-18 22:53  
tags: status/seed  
aliases: []  
---  
Category::  

---

# International Renewable Energy Agency
**flashcard**{: #flashcard}{: .hash}  

(IRENA)
- Founded in 2009
- India is the 77th foundation partner.
- HQ - UK