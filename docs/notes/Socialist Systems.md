---  
title: Socialist Systems  
date: 2021-06-29 21:12  
alias: ['Socialism']  
tags: status/seed  
sr-due: 2022-02-27  
sr-interval: 60  
sr-ease: 250  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# Socialist Systems
- Stark opposition of Capitalism
	- Since Capitalism leads to inequality.
- Socialism is essentially an economic doctrine.
- Hence, based on *Social Ownership* and against inequality
- It aims to promote social and economic equality and freedom from necessities.
- One party state
	-  even if multiple parties -->  common socialist aims.

## Types of Socialism and their relationships
- [[Capitalism vs Socialism]]
- [[Relation between Socialism and Communism]]
- [[Fabian Socialism]]
- [[Fabian Socialism vs Communism]]