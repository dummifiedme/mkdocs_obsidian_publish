---  
title: AsterX  
source: None  
date: 2021-03-18 23:09  
tags: status/seed  
aliases: []  
---  
Category::  

---

# AsterX
**flashcard**{: #flashcard}{: .hash}  

First space military exercise by France.
- USA and German space agencies participated as well.
- Named "AsterX" in the memory of 1969 first space flight of France.