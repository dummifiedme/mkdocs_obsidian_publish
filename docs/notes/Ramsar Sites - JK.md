---  
title: Ramsar Sites - JK  
date: 2021-06-24 18:16  
alias: []  
tags: status/mature  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - JK
**flashcard**{: #flashcard}{: .hash}  

- [[Wular Lake]] - Large
- [[Hokera Wetlands]] - Small
- Surinsar Mansar Lakes - Very Small
![[Map - Ramsar - JK,Leh,PB,HP 1.png]]