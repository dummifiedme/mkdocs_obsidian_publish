---  
---  
**personal**{: #personal}{: .hash}  
/lifestyle/study 
# Inability to study consistently
@ 2021-11-21 12:14
It's been more than 2 years since I left my job. And I am unable to work a system out for studies. I am not consistent with the studies. There are so many things wrong with me, so many things to fix that would eventually lead to a better study pattern.

The most important thing for me is to remove the [[Need for instant gratification or dopamine]]. This is the most significant factor for me. As stated in a quote by Abraham Lincoln, I read today, "Discipline is choosing between the things you want **now** and the things you want the **most**", I need to focus more towards the final aim rather than constantly feeding myself with all the reasons for justifying my inability to study. 

I need to clear this EXAM. PERIOD. Let's get it over with!