---  
title: Index of Economic Freedom 2021  
source: None  
date: 2021-03-10 22:51  
tags: status/seed  
aliases: []  
---  
Category:: [[+ General Studies 3]]# Index of Economic Freedom 2021
**flashcard**{: #flashcard}{: .hash}  

- Annual index since 1995
- Released by a think tank - The Heritage Foundation and the Wall Street Journal.
- Measures the degree of economic freedom of an individual in a country.
- 12 indicators in 4 categories.
	- Rule of Law
	- Government Size
	- Regulatory Efficiency
	- Open Markets
- India - 121st - "Mostly UNFREE!"
![[Index of EconomicFreedom2021.png]]