---  
title: National Commission for SCs  
source: None  
date: 2021-05-30 18:05  
aliases: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# National Commission for SCs



## Composition of NCSC
**flashcard**{: #flashcard}{: .hash}  

- Chairperson
- Vice-Chairperson
- Three other members
> Appointed by President by [[warrant under his hand and seal]].

## Functions of NCSC
- *Monitoring and investigating* all the issues concerning the safeguards provided for the SCs under the constitution.
- *Enquiring into the complaints* - in cases of deprivation of rights.
- *Advising central and state governments* in planning of socio-economic development of SCs.
- *Regular Reporting to the President*
- *Recommending steps* to further the socio-economic development.
- Performs similar functions for Anglo-Indian communities -earlier OBC as well.