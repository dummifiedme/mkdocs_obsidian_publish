---  
title: President's Rule  
date: 2021-02-12 20:39  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# President's Rule
**flashcard**{: #flashcard}{: .hash}  

- Article 356
## Approvals for President's Rule
**flashcard**{: #flashcard}{: .hash}  

- Approval by both the Houses of Parliament
	- Simple Majority
- Valid for 6 months.
- Extension for 3 years with approval every 6 months.

## Consequences of President's Rule
- Council of Ministers of State are removed
**todo**{: #todo}{: .hash}  
/open 

## Cases related to President's Rule
**flashcard**{: #flashcard}{: .hash}  

- [ ] [[SR Bommai Case]] (1994)