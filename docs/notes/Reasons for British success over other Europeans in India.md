---  
title: Reasons for British success over other Europeans in India  
source: None  
date: 2021-05-09 18:43  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Reasons for British success over other Europeans in India
1. Structure of the companies
	- British company was an amalgamation of many home companies; largely a shareholding and help meetings; decisions according to votes.
	- French or Portuguese EIC was largely a government holding - hence lesser shareholder meetings and it ran mostly as a state department.
2. Naval Superiority
	- Defeated navies of French, Portuguese and Spanish Armada.
	- They understood the need of technologically improved fleet.
3. Industrial Revolution
	- Started in England - early 18th century.
	- Jenny - steam engine operated power loom.
	- Late in other countries.
4. Military Skill and Discipline
	- Good at strategies
	- Disciplined and well trained.
5. Stable government
	- Only the Glorious Revolution of 1688 in Britain
	- While in France
		- Violent French Revolution - 1789.
		- Napoleonic Wars till 1815.
6. Less on Religion, more on commerce -compared to Spain, Portugal and the Dutch.
7. Use of Debt Instruments
	- by Bank of England to raise money.