---  
---  
# Readjustment to Territorial Constituency

- After every census, a readjustment is to be made in 
	1.  allocation of seats in LS to the states.
	2.  division of each state into territorial constituencies.

- Parliament is empowered to determine the authority and the manner in which readjustment is to be made.
	* Delimitation Commission Acts 
		* 1952
		* 1962
		* 1972 - froze till 2000 at 1971 census level
		* 2002 - froze till 2026 at 1991 census level - {84th} CAA 
		* 2003 - change in 2002 amendment at 2001 census levels - {87th} CAA