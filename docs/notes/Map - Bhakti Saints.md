---  
title: Map - Bhakti Saints  
source: None  
date: 2021-05-09 08:18  
tags: status/seed  
aliases: []  
sr-due: 2022-01-19  
sr-interval: 8  
sr-ease: 250  
---  
Category:: [[+ Maps]] 

**note**{: #note}{: .hash}  
/upsc/map #note/upsc/GS1/history/culture #note/upsc/GS1/history/medieval 
# Map - Bhakti Saints

Image:: ![[Bhakti-cult-Medieval-History.png]]

- [[Sant Ravidasji]] aka Raidas (in the map)