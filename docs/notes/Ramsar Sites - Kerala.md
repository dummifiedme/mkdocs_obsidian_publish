---  
title: Ramsar Sites - Kerala  
date: 2021-06-11 10:17  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - Kerala
**flashcard**{: #flashcard}{: .hash}  

- [[Vembanad Lake]] *(Very Large)*
- Sasthamkotta Lake *(Very Small)*
- Ashtamudi [[Wetland]] *(Large)*