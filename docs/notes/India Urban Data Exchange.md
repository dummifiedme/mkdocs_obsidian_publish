---  
title: India Urban Data Exchange  
source: None  
date: 2021-03-07 21:20  
tags: status/seed  
aliases: ['IUDX']  
---  
Category::  

---

# India Urban Data Exchange
**flashcard**{: #flashcard}{: .hash}  

- Partnership between Smart Cities Mission and the Indian Institute of Science.
- To consolidate the data silos.