---  
title: cryptobiosis  
date: 2021-06-27 20:54  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# cryptobiosis
**flashcard**{: #flashcard}{: .hash}  

Metabolic activities to a reversible standstill.
- Rose of Jericho
- [[Tardigrade]]