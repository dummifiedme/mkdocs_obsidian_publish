---  
title: Pagri Sambhal Movement  
source: None  
date: 2021-03-07 21:10  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Pagri Sambhal Movement
**flashcard**{: #flashcard}{: .hash}  

- 1907 started by [[Sardar Ajit Singh]]
- 23rd February as Pari Sambhal Diwas
	- by Samukta Kisan Morcha
- 1879 - British tried to settle people in the areas around modern day Faislabad - later it flourished - they tried to get hold of it back - Ajit SIngh started this movement against British law to grab the lands.
	- British had to terminate the law.