---  
title: Accelerating Growth of New India's Innovations  
source: None  
date: 2021-03-07 23:08  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Accelerating Growth of New India's Innovations
**flashcard**{: #flashcard}{: .hash}  

- **Nodal agency** - Office of the Principal scientific Advisor to the GoI.
- Purpose 
	- Commercialize Indian tech innovations.
	- supports by connecting owners of innovative and new solutions with the market.
- Implementation - executed at [[Invest India]].