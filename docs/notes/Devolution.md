---  
title: Devolution  
date: 2021-06-29 21:12  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# Devolution
**flashcard**{: #flashcard}{: .hash}  

**Definition**:: in which sub - national units of government are either created or strengthened in terms of political, administrative and fiscal power