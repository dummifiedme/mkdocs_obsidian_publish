---  
title: Kanwar Taal  
date: 2021-06-11 10:17  
alias: ['Kabartal Wetland']  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Kanwar Taal
or Kabar Taal Lake
- **Location** - ==Begusarai, Bihar <br>![[KabarTal Map.png]]==
- **Major species** - red-headed vulture, white-rumped vulture, Indian Vulture, Baer's Pochard
- Declared a Ramsar Site in 2020
- Stop over for [[Central Asian Flyway]]