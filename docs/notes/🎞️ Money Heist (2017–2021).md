---  
cover: https://m.media-amazon.com/images/M/MV5BNDJkYzY3MzMtMGFhYi00MmQ4LWJkNTgtZGNiZWZmMTMxNzdlXkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_SX300.jpg  
imdbId: tt6468322  
---  
**entertainment**{: #entertainment}{: .hash}  
/show/spanish
# 🎞️ Money Heist (2017–2021)

- - -
> [!movie]
> ![poster|-right](https://m.media-amazon.com/images/M/MV5BNDJkYzY3MzMtMGFhYi00MmQ4LWJkNTgtZGNiZWZmMTMxNzdlXkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_SX300.jpg)
> 
> **Director**::  
> **Genre**:: "Action", "Crime", "Drama"
> **Language**:: Spanish, Russian, Serbian, English
> **IMDB Rating**:: 8.2
> **Year**:: 2017–2021
**Seasons**:: 5
> **Cast**:: "Úrsula Corberó", "Álvaro Morte", "Itziar Ituño"
> **Watched**:: "2022-01-10"
> **Rating**:: 5
- - -

## Synopsis
**Plot**:: An unusual group of robbers attempt to carry out the most perfect robbery in Spanish history - stealing 2.4 billion euros from the Royal Mint of Spain.