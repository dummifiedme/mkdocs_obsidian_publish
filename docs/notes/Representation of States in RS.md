---  
title: Representation of States in RS  
source: None  
date: 2021-03-10 07:34  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Representation of States in RS

## Elected by
Elected members of the state legislative assemblies.

## System of election for RS
**flashcard**{: #flashcard}{: .hash}  

[[single transferable vote]].

## Basis for the allotment of seats to states in RS
**flashcard**{: #flashcard}{: .hash}  

- Population
> Hence, varies from state to state - UP has 31 seats while Tripura has only 1.

### RS Election: Difference from USA
**flashcard**{: #flashcard}{: .hash}  

- All states have equal representation (2 seats) in Senate irrespective of their population.
- 50 states, 100 members in Senate, 2 per state.