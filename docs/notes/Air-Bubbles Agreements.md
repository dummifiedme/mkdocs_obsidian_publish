---  
---  
# Air-Bubbles Agreements
---
source: Vision Monthly Magazine August 2020
date: 2020-12-14 02:12
---

### Air-Bubbles Agreements
**flashcard**{: #flashcard}{: .hash}  

- aka transport bubbles
- aka Air Travel Agreements
- COVID 19 temporary arrangement between two countries.
- India signed with many countries Canada, USA, France, UAE, UK, Germany etc.
- RECIPROCAL in Nature.
![[CutOut - AirBubble Agreement.png]]