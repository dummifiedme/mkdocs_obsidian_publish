---  
title: Joint Sitting of two houses  
source: None  
date: 2021-03-11 16:17  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Joint Sitting of two houses
(Yes? | When | By Whom)
**flashcard**{: #flashcard}{: .hash}  

Summoned *by President* when there is a deadlock between the two houses on a bill