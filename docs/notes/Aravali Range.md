---  
---  
![[Map - Aravali Range.png]]
- Older than the collision with Eurasian plate.
- Proofs of mining of copper as early as 5th century BC.