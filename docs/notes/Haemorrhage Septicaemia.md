---  
title: Haemorrhage Septicaemia  
source: None  
date: 2021-02-17 00:49  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Haemorrhage Septicaemia
**flashcard**{: #flashcard}{: .hash}  

- bacterial disease
- affects cattle
- high mortality rate
- Killed 6 elephants in [[Karlapat Wildlife Sanctury]]