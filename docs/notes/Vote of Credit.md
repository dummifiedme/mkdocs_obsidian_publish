---  
title: Vote of Credit  
source: None  
date: 2021-03-17 17:36  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Vote of Credit
**flashcard**{: #flashcard}{: .hash}  

When a demand is raised by the government for a service for which the demand is not definite.
- "Blank Cheque"