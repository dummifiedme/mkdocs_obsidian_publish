---  
status: 🔴  
---  
l**note**{: #note}{: .hash}  
/upsc/syllabus/topics
# GS2-12 Vulnerable Section - Welfare Schemes, Laws, Protection 

- [[Vulnerable Sections]]


- Bonded Labour or Forced Labour
- Trafficking of person
- Domestic Help
- Children
- Youth
- Women
- Mental Health
- People with Disability
- SC-ST
- Old Age
- Muslims
- Homosexual and Transgendered People
- Manual Scavenging