---  
title: Qualifications for the being a MP  
source: None  
date: 2021-03-10 18:58  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Qualifications for the being a MP
[x4]
**flashcard**{: #flashcard}{: .hash}  

- Citizen of India
- make and subscribe an [[Oath or Affirmation by MPs]] (before a person authorized by [[Election Commission of India]]).
- [[Minimum age for MPs]] - Must not be less than 
	- 30 years of age - RS
	- 25 years of age - LS
- Must posses other [[Qualifications prescribed by the parliament for MPs]].