---  
---  
# Single Transferable Vote System
- Preference voting.
- Originated in Britain.
- In India used in Election to the RS from states and two UTs.
- Better suits the proportional representation system.

## VoteSheet
![[3 Resources/Pasted image 20201125030745.png]]

## Pro/Cons
![[3 Resources/Pasted image 20201125030804.png]]