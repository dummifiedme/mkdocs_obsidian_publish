---  
title: Role, Powers and Functions of Speaker  
source: None  
date: 2021-03-11 16:07  
tags: status/seed  
aliases: []  
---  
Category |

---

# Role, Powers and Functions of Speaker
- Guardian of powers and privileges of members, house as a whole and the committees.
- Principal spokesperson of the house.

## Sources of powers of Speaker of LS
**flashcard**{: #flashcard}{: .hash}  

1. Constitution of India
2. [[Rules of Procedure and Conduct of Business of Lok Sabha]]
3. [[Parliamentary Conventions]] 



 
## Powers and duties of Speaker of LS
(x10)
1. maintain order and decorum of the house
2. final interpreter of the constitution, [[Rules of Procedure and Conduct of Business of Lok Sabha]] , parliamentary precedents - IN THE HOUSE.
3. adjourns when [[Quorum for House]] doesn't meet.
4. doesn't vote in the first instance - votes in case of a tie - tie-breaker vote
5. presides over [[Joint Sitting of two houses]].
- [ ] **todo**{: #todo}{: .hash}  
  Powers of Speaker