---  
title: Brookesia nana  
source: None  
date: 2021-02-11 17:20  
tags: status/seed  
aliases: []  
---  
Category::  [[GS3]] , [[+ Current Prelims]] , [[🗃️ Environment]]

---

# Brookesia nana
**flashcard**{: #flashcard}{: .hash}  

- Chameleon discovered in Madagascar by scientists from Germany and Madagascar.
- World's smallest adult reptile.
	- Earlier it was Brookesia Micra