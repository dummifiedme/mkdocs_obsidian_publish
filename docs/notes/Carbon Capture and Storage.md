---  
title: Carbon Capture and Storage  
source: None  
date: 2021-05-30 12:09  
aliases: ['CCS', 'Carbon Capture, Utilisation & Storage', 'CCUS']  
tags: status/seed environment science  
---  
Category:: 

---

# Carbon Capture and Storage
**flashcard**{: #flashcard}{: .hash}  

![[Image - CCUS Flow.png]]
![[Image-CCU Components.png]]
**Related**
- ACT - Accelerating CCUS Technologies
	- An initiative by [[Department of Science and Technology]]