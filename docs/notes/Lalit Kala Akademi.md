---  
title: Lalit Kala Akademi  
source: None  
date: 2021-03-11 15:22  
tags: status/seed  
aliases: []  
sr-due: 2022-01-17  
sr-interval: 10  
sr-ease: 230  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/bodies
# Lalit Kala Akademi 
- **Type of body**:: [[Statutory Bodies]]
- **Established in**:: 1954
- *Statutory authority* in 1957
- **HQ**:: New Delhi
- **Aim**:: In pursuance of the dream for a cultural and national identity.