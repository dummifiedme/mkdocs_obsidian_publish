---  
title: Diphtheria  
source: None  
date: 2021-03-17 22:48  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# Diphtheria
**flashcard**{: #flashcard}{: .hash}  

- Infectious disease causted by bacterium Corynebacterium Diphtheriae.
- Sore Throat, low fever, swollen glands
- Mostly affects children.
- Vaccine -  Bacterial Toxoid.
> Getting resistant to treatment and anti-biotics