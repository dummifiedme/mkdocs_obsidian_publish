---  
title: Carbon Footprint  
source: None  
date: 2021-02-25 14:18  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Environment]] , [[Keywords GS3]]

---

# Carbon Footprint
**flashcard**{: #flashcard}{: .hash}  

Amount of greenhouse gases especially $CO_2$ released in to the atmosphere by a particular human activity.