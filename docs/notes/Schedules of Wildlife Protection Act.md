---  
title: Schedules of Wildlife Protection Act  
source: None  
date: 2021-05-27 15:56  
tags: status/seed  
aliases: ['Schedules of WPA 1972', 'Schedules of WPA']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment

---
**review**{: #review}{: .hash}  

# Schedules of Wildlife Protection Act
---

> ==Schedule I and Schedule II (Part II)== get ==absolute protection and maximum penalties==.

## Schedule I of WPA
**flashcard**{: #flashcard}{: .hash}  

- Endangered Species
- Rigorous protection + Harshest Penalties
	- ❌ No Hunting (except when threat to human life)
	- ❌ Trade prohibited


**Examples**
**flashcard**{: #flashcard}{: .hash}  

- Tiger
- Blackbuck
- Himalayan Brown Bear
- Brow-Antlered Deer
- Blue Whale
- Common Dolphin
- Cheetah
- Clouded Leopard
- Hornbills

## Schedule II of WPA
**flashcard**{: #flashcard}{: .hash}  

- High protection
	- ❌ Trade prohibited
	- ❌ No Hunting (except when threat to human life)


**Examples**
**flashcard**{: #flashcard}{: .hash}  

- Kohinoor(insect)
- Assamese Macaque
- Bengal Hanuman Langur
- Large Indian Civet
- Indian Fox
- Larger Kashmir Flying Squirrel
- Kashmir Fox

## Schedule III and IV of WPA
**flashcard**{: #flashcard}{: .hash}  

- Not Endangered
- Includes protected species, but less penalties

**Examples**
**flashcard**{: #flashcard}{: .hash}  

- Hyena
- Himalayan Rat
- Porcupine
- Flying Fox
- Malabar Tree Toad

## Schedule V of WPA
**flashcard**{: #flashcard}{: .hash}  

- animals that can be hunted
- Examples: mice, rat, common crow, fruit bats etc.

## Schedule VI of WPA
**flashcard**{: #flashcard}{: .hash}  

- Plants forbidden from cultivation
- Examples: pitecher plant, blue vanda, red vanda, kuth etc.