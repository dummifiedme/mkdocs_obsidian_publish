---  
title: Five Star Villages Scheme  
source: None  
date: 2021-05-08 20:57  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Five Star Villages Scheme
**flashcard**{: #flashcard}{: .hash}  

- Department of Posts, [[Ministry of Telecommunications]]
- Universal coverage - flagship postal schemes in rural areas of country
- pilot basis in Maharashtra, nationwide afterwards
- basically promotion of various Indian Posts Schemes. <!--SR:!2021-08-17,1,210-->