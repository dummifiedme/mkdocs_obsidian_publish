---  
cover: https://m.media-amazon.com/images/M/MV5BNTc2MzM2N2YtZDdiOS00M2I2LWFjOGItMDM3OTA3YjUwNjAxXkEyXkFqcGdeQXVyNzA5NjUyNjM@._V1_SX300.jpg  
imdbId: tt0212671  
---  
**entertainment**{: #entertainment}{: .hash}  
/show
# 🎞️ Malcolm in the Middle (2000–2006)

- - -
![poster|-right](https://m.media-amazon.com/images/M/MV5BNTc2MzM2N2YtZDdiOS00M2I2LWFjOGItMDM3OTA3YjUwNjAxXkEyXkFqcGdeQXVyNzA5NjUyNjM@._V1_SX300.jpg)

**Director**::  
**Genre**:: "Comedy", "Family"
**IMDB Rating**:: 8.0
**Year**:: 2000–2006
**Seasons**:: 7
**Cast**:: "Frankie Muniz", "Bryan Cranston", "Justin Berfield"
**Watched**:: "2021-12-21 - 2021-12-31"
**Rating**:: 9
- - -

## Synopsis
**Plot**:: A gifted young teen tries to survive life with his dimwitted, dysfunctional family.