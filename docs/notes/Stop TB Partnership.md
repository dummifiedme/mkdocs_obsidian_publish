---  
title: Stop TB Partnership  
source: None  
date: 2021-03-20 00:32  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Stop TB Partnership
**flashcard**{: #flashcard}{: .hash}  

- Established in 2000.
- Aim is to eliminate [[TB]] across the world.
- [[Amsterdam Declaration 2000#^4c3caa]]