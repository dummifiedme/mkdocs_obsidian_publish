---  
title: National Parks  
source: None  
date: 2021-05-27 16:45  
tags: status/seed  
aliases: ['National Park']  
sr-due: 2022-01-28  
sr-interval: 20  
sr-ease: 230  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# National Parks
“National Parks are the areas that are set by the government to ==conserve the natural environment==.”
- More restrictions than [[Wildlife Sanctuaries]].
- **Main Objective** - Protect natural environment of the area and biodiversity conservation.
- *Ecosystem Approach*
> National Park matches the [[IUCN]] Category 2.


## Features of National Parks
**flashcard**{: #flashcard}{: .hash}  

- *No* human activities are allowed
	- ❎ Grazing of livestock 
	- ❎ Private ownership rights 
	- ❎ Destruction, removal or exploitation - any wildlife
- Landscape, fauna and flora are present in their natural state
- *Cannot be downgraded* to [[Wildlife Sanctuaries]]. <!--SR:!2022-01-24,16,250-->

## Establishment of National Parks
Under :: [[Wildlife Protection Act]]
- Declared by the  **State Government** by a Notification.
- Boundaries can be altered by a Resolution of the State Legislature.
	- Nature of the Boundaries :: **fixed and defined**. <!--SR:!2022-01-25,17,230-->

---
[[+ National Parks]]