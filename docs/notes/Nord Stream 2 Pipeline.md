---  
title: Nord Stream 2 Pipeline  
source: None  
date: 2021-02-13 22:16  
tags: status/mature  
aliases: []  
---  
**upsc**{: #upsc}{: .hash}  
/prelims 
# Nord Stream 2 Pipeline
**flashcard**{: #flashcard}{: .hash}  

![[Nord 2 Stream Pipeline Map.png]]
- 1200km pipeline from Russia to Germany via Baltic Sea.
- Controversy?
	- Criticism from the US since Russian influence increasing in the Europe.