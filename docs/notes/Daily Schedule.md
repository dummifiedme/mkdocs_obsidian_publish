---  
---  
# Daily Schedule
## Daily Schedule
| Time          | Activity                                     |
| ------------- | -------------------------------------------- |
| 05:00 - 05:30 | Wake Up, Washroom, Brush, Face Wash          |
| 05:45 - 07:45 | *Slot 1* - For fresh topics                  |
| 08:00 - 09:00 | Pranayam, Exercise Routine                   |
| 09:00 - 10:00 | Bath, Breakfast                              |
| 10:15 - 12:15 | *Slot 2* - For fresh topic too               |
| 12:30 - 12:55 | ANKI 1                                       |
| 13:00 - 15:00 | *Slot 3* - Revision, Note Taking, Flashcards |
| 15:00 - 16:00 | Lunch, Rest                                  |
| 16:15 - 18:15 | *Slot 4* - PT                                |
| 18:30 - 19:30 | *Slot 5* - Monthly Magazine                  |
| 19:30 - 21:30 | Show/Entertainment, Dinner                   |
| 21:30 - 05:00 | Sleep                                        |



```mermaid
pie

"Studies" : 9
"Daily Essentials" : 1
"Rest/Sleep" : 8
"Flashcards" : 0.5
"Pranayam/Exercise" : 1
"Eating" : 1.5
"Breaks" : 1
"Entertainment" : 1.5

```