---  
title: Nankana Sahib  
source: None  
date: 2021-02-25 14:45  
tags: status/draft  
aliases: []  
sr-due: 2022-01-18  
sr-interval: 10  
sr-ease: 230  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/monuments #note/upsc/GS1/history/modern/places 
# Nankana Sahib
**flashcard**{: #flashcard}{: .hash}  

**Region**:: Punjab - Pakistan
**Description**:: Birth place of [[Guru Nanak Dev]] <!--SR:!2022-01-18,10,219-->



## Massacre
**flashcard**{: #flashcard}{: .hash}  
 
- 21st February 1921
- Jatha (Unarmed Sikh Leaders) entered Nankana Sahib, they were massacred by armed mahant behind closed doors. <!--SR:!2021-09-22,1,230-->
**note**{: #note}{: .hash}  
/upsc/GS1/history/modern/event 

### Saka Nankana Sahib
**flashcard**{: #flashcard}{: .hash}  

Cetenary of Sri [[Nankana Sahib]] massacre - 21st February 1921 <!--SR:!2022-01-12,12,230-->