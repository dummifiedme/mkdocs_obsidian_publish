---  
cover: https://m.media-amazon.com/images/M/MV5BMTExMzU0ODcxNDheQTJeQWpwZ15BbWU4MDE1OTI4MzAy._V1_SX300.jpg  
imdbId: tt2543164  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/English
# 🎞️ Arrival (2016)

- - -
![poster|-right](https://m.media-amazon.com/images/M/MV5BMTExMzU0ODcxNDheQTJeQWpwZ15BbWU4MDE1OTI4MzAy._V1_SX300.jpg)
**Director**:: "Denis Villeneuve"
**Genre**:: "Drama", "Sci-Fi"
**Language**:: English, Russian, Mandarin
**IMDB Rating**:: 7.9
**Year**:: 2016
**Cast**:: "Amy Adams", "Jeremy Renner", "Forest Whitaker"
**Watched**:: "2022-01-09"
**Rating**:: 9

- - -

## Synopsis
**Plot**:: A linguist works with the military to communicate with alien lifeforms after twelve mysterious spacecraft appear around the world.