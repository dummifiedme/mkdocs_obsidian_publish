---  
title: India-China Standoff  
source: None  
date: 2021-02-13 12:30  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[Keywords GS2]] , 
---

# India-China Standoff 2020-21

## 2020-21
![[Pangong Tso northbank fingers.png]]
- India has a post between 2nd and 3rd fingers on the north bank of Pangong Tso - Dhan Singh Post.
	- Dhan Singh was a valiant soldier who sacrificed his life in the Indo-China War of 1962.
- Prior to April 2020, Indian army patrolled the region till 8th finger. 
- In April 2020, Chinese army made a 'permanent' infrastructure near the 4th finger.
	- Though, they had a road system since a while back from 4th to 8th fingers.
	- With the new infra, they blocked the India patrol from 4th to 8th fingers.
	- And, had a strong army presence.
- India tried to negotiate and raise concern, but it fell on deaf ears since we did not have any leverage for being taken seriously.
- In August 2020, India captures some important peaks and passes on the southern bank of the lake where no one ever had any army presence.
	- This became an important move and China was now ready for negotiation.
- We also have other conflict points as well:
	- Gogra Post at PP17A and Hot Springs area near PP15.
	- PP14 in Galwan Valley[^1]
		- There was a violent clash here as well.
		- 20 soldiers were martyred from the Indian side, while the Chinese casualties are not yet clear. (40-50 as per Indian estimates)
	- Depsang Plains - close to Daulat Beg Oldie base near Karakoram pass.
- As of February 2021, there is an agreement on "synchronized and organized disengagement".
	- remove the forward deployment in a "phased", "coordinated" and "verified" manner.


[^1]: **Galvan Valley** is located in the (range) :: Karakoram Range