---  
---  
# moral desert
**flashcard**{: #flashcard}{: .hash}  

Moral desert is simply what we deserve.
- Relates to the word “*desert*” with the meaning — ’*deserved reward or punishment*’