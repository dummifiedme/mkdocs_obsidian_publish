---  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Arzoo
- - -
Watched: 2021-11-02
Cast:: "Rajendra Kumar", "Sadhna", "Mehmood"
- - -

## Comments
- In a scene, Mehmood takes Rajendra Kumar to visit a temple in Kashmir and on the way he introduces a location. There is a profound irony in the way he introduces:
	- He first points to the lotuses in the lake at a distance and then refers to a location by it as a place where Nehru used to stay on visits. 
	- The irony being in lotus bring the symbol of BJP, staunch opposition of Nehru’s Congress.