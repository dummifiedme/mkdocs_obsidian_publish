---  
title: Puttaswamy Case  
source: None  
date: 2021-01-18 22:40  
tags: None  
aliases: ['Puttaswamy Case (2017)']  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity/cases 
# Puttaswamy Case
**flashcard**{: #flashcard}{: .hash}  

**Year**:: 2017
**Verdict**:: Landmark judgement of [[Right to Privacy]] becoming one of the [[Fundamental Rights]] in India.