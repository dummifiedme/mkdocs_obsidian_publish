---  
title: Paleolithic Period  
source: None  
date: 2021-02-08 20:43  
tags: status/seed note/upsc/GS1/history/ancient  note/upsc/anthropology  
aliases: []  
---  
# Paleolithic Period

The period is sub-divided into - Lower, Middle and Upper Paleolithic

### Lower Paleolithic
- Characteristic stone tools :: Hand Axes, Cleavers, Choppers <!--SR:!2022-01-05,1,230-->
 <!--SR:!2021-11-05,1,230-->
- Major Activity of the people :: Hunting and Gathering <!--SR:!2022-01-07,3,250-->

### Middle Paleolithic

### Upper Paleolithic