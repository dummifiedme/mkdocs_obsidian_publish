---  
title: National Bamboo Mission  
source: None  
date: 2021-05-08 20:47  
tags: status/seed note/upsc/GS3/economics  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/agriculture

---


# National Bamboo Mission
**flashcard**{: #flashcard}{: .hash}  

- Launched in 2018-19
- For holistic development of the complete value chain of the bamboo sector
- Hub and spoke model <!--SR:!2021-08-17,2,230-->


==Indian Forest Act 1927== was amended in 2017 to remove bamboo for the category of trees. <!--SR:!2021-08-17,2,230-->