---  
title: COTPA Act  
source: None  
date: 2021-05-20 23:49  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# COTPA Act
**flashcard**{: #flashcard}{: .hash}  

Cigarettes and Other Tobacco Products Act, 2003.
- Trade and Commerce
- Prohibitions