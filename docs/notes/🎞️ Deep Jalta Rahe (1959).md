---  
cover: N/A  
imdbId: tt0213599  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/hindi 
# 🎞️ Deep Jalta Rahe (1959)

- - -
![poster|-right](N/A)

**Director**:: "Datta Dharmadhikari"
**Genre**:: "N/A"
**IMDB Rating**:: N/A
**Year**:: 1959
**Cast**:: "Ravi Tandon"
**Watched**:: "2021-11-15"
**Rating**:: 8
- - -

## Synopsis
**Plot**:: A decent film with a social message. Story of an adopted son of a wealthy man, who for the sake of reputation his godfather and his family leaves all his acquired wealth.