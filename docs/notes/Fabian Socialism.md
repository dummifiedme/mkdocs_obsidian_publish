---  
title: Fabian Socialism  
date: 2021-06-29 21:12  
alias: ['Nehruvian Socialism']  
tags: status/seed  
sr-due: 2022-01-25  
sr-interval: 21  
sr-ease: 270  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# Fabian Socialism
**flashcard**{: #flashcard}{: .hash}  

A state run socialism.
- Nehruvian Socialism
- Socialism through legislative means.
- British Socialism - since it started in Britain.
Related: [[Fabian Socialism vs Communism]]
<!--SR:!2022-03-05,60,250-->