---  
---  
# Checklist - Anthropology Topics
```

- # Anthropology Syllabus

- ## Paper - I

- **501.1 MEANING AND SCOPE OF ANTHROPOLOGY**
- ---
- **501.2 RELATIONSHIP WITH OTHER DISCIPLINES**
- ---
- **501.3 MAIN BRANCHES OF ANTHROPOLOGY**
	- Socio-cultural Anthropology
	- Physical and Biological Anthropology 
	- Archaeological Anthropology
	- Linguistic Anthropology
- ---
- **501.4 HUMAN EVOLUTION & EMERGENCE OF MAN**
	- Theories of Organic Evolution
	- Modern Synthetic Theory
	- Biological and Cultural factors in Human Evolution
	- Basic Concepts And Terms In Evolutionary Biology
- ---
- **501.5 PRIMATES & EVOLUTION**
	 - Classification and Taxonomy
	- Tertiary and Quaternary Fossil Primates
	- Classification Of Order Primates
	- Characteristics of Primates
	- Comparative Anatomy Of Pongidae And Hominidae
	- Comparative Anatomy of Man and Apes
	- Primate Locomotion
	- Skeletal changes due to bipedalism and implications
- ---
- **501.6 PHYLOGENETIC STATUS, CHARACTERISTICS AND DISTRIBUTION OF HUMAN FOSSIL ANCESTORS**
	- Plio-Pliestocene Hominids in South and East Africa -- Australopithecines
	- Homo Habilis
	- Homo Erectus
	- Neanderthal Man
	- Rhodesian Man
	- Homo Sapiens
	- Cro-Magnon man
	- Grimaldi Man
	- Chancelade man

- ---
- **501.7 THE BIOLOGICAL BASIS OF LIFE**
	   - The Cell, DNA structure and replication, Protein Synthesis, Gene,
		- Mutation, Chromosomes, and Cell Division.

- ---

- **501.8 PRINCIPLES OF PREHISTORIC ARCHAEOLOGY**
	- Chronology: Relative and Absolute Dating
	- Cultural Evolution – Broad Outlines
- ---
- **502.1 THE NATURE OF CULTURE**
	- The concept and characteristics of culture and civilization
	- Ethnocentrism and Cultural Relativism
	- Culture and civilization
- ---
- **502.2 THE NATURE OF SOCIETY**
	- Concept of society
	- Social Institution
	- Social Groups
	- Status & Role
	- Social Stratification
	- Forms of Social Stratification

- ---
- **502.3 MARRIAGE**
	- Definition and characteristics
	- Regulation of marriage
	- Types and Forms of Marriage
	- Ways of acquiring mates in Tribal societies:
	- Marriage Payments (Types of bride price and dowry)

- ---
- **502.4 FAMILY**
	- Definition of Family
	- Household and domestic group
	- Basic structure and Functions of a family
	- Types of Family
	- Impact of urbanization, industrialization and feminist movement on feminist movement on family


- ---
- **502.5 KINSHIP**
	- Principles of Descent – Types and functions
	- Descent, Filiation and Complementary Filiation
	- Forms of Descent Groups
	- Kinship Terminology
	- Alliance and Descent

- ---
- **503.0 ECONOMIC ORGANIZATION**
	- Meaning and scope of Economic Anthropology 
	- Various types & sub-types of economies 
	- Characteristics of a Tribal economy 
	- Formalists and Substantivists debate in Economic anthropology 
	- Modes of exchange 
- ---
- **504.0 POLITICAL ORGANIZATION AND SOCIAL CONTROL**
	- Types of political organizations
	- Power, Authority and Legitimacy
	- Social Control
	- Social Control in Small-Scale Societies
	- Resolution of Conflict
	- Law & Justice in simple societies
- ---
- **505.0 RELIGION**
	- Anthropological approaches to study of religion
	- Forms of Religion in Tribal & Peasant Societies
	- Myths and Rituals
	- Religious Practitioners
	- Magic

- ---
- **506.0 ANTHROPOLOGICAL THEORIES of Cultural Evolution**
	- Evolutionary School
	- Classical Evolutionism
	- Neo-Evolutionism
	- Cultural ecology
	- Leslie White – Cultural Materialism
	- Diffusionism
	- British school of Diffusion – G.E.Smith, W.J.Perry and WHR Rivers
	- German school of Diffusion – Frederick Ratzel, Frietz Graebner and Father William Schmidt
	- American school of Diffusion – Franz Boas; A.L.Kroeber, Clark Wissler
	- Historical Particularism – Franz Boas
	- Structuralism – Claude Levi Strauss
	- Edmund Leach
	- Functionalism – Bio-cultural functionalism by Malinowski & Structural functionalism by Radcliffe-Brown
	- Malinowski’s theory of functionalism
	- Structural Functionalism – Radcliffe Brown
	- Culture and Personality – Ruth Benedict, Margaret Mead, Linton and Cora-Dubois
	- Configurational Approach – Ruth Benedict & Margaret Mead
	- Basic personality structure approach
	- Modal Personality approach – Cora Dubois
	- Symbolism – Clifford Geertz, Victor Turner & David Schneider
	- Cultural Materialism– Harris
	- Cognitive Anthropology
	- Post Modernism – Lyotard, Baudrillard 
- ---
- **507.0 CULTURE, LANGUAGE AND COMMUNICATION**
	- Nature, origin and characteristics of language
	- Verbal and Nonverbal communication
	- Language and Culture

- ---
- **508.0 RESEARCH METHODS IN ANTHROPOLOGY**
	- Approaches of Anthropological Research:
	- Method, Methodology and Technique:
	- Field work tradition in Anthropology:
	- Objectivity, Subjectivity, and Inter-subjectivity
	- Basic techniques of data collection
	- Interview
	- Observation
	- Questionnaire
	- Schedule
	- Case study
	- Genealogical methods
	- Participatory Rural appraisals (PRA)

- ---
- **509.1 HUMAN GENETICS**
	- Methods of Studying Human Genetics
	- Biochemical method
- ---
- **509.2 MENDELIAN GENETICS IN MAN FAMILY STUDY**
	- Single factor multifactor and polygenic inheritance in man:
	- Lethal Gene action
	- Semi-lethal genes
- ---
- **509.3 CONCEPT OF GENETIC POLYMORPHISM AND SELECTION**
	- Genetic Polymorphism
	- Mendelian population
	- Hardy-Weinberg Equilibrium
	- Deviations From Hardy-weinberg Law Or Factors Affecting Gene Frequencies
	- Consanguineous And Nonconsanguineous Matings


- ---
- **509.4 CHROMOSOME AND CHROMOSOMAL ABERRATIONS IN MAN, METHODOLOGY**
	- Numerical chromosome abnormalities
	- Abnormalities of sex chromosomes
	- Genetic imprints in Human diseases
	- Genetic Screening
	- Genetic counselling
	- DNA Fingerprinting
	- Gene mapping and Genome study


- ---
- **509.5 RACE AND RACISM**
	- Biological Basis of Morphological variation of Non-metric and metric characters
	- Serological and Genetic criteria . .
	- Racial Criteria, Racial traits in relation to Heredity and Environment
	- Causes Of Biological Diversity
	- Formation of Races
	- Racism

- ---
- **509.6 AGE, SEX AND POPULATION VARIATION AS – GENETIC MARKER**
	- Genetic markers
	- Variations at physiological level and blood level
	- Human Blood Group Systems

- ---
- **509.7 CONCEPTS AND METHODS OF ECOLOGICAL ANTHROPOLOGY**

	- Human Adaptability
	- Acclimatization and Extreme Cold
	- Acclimatization to Desert Habitats
	- Adjustments to High Altitudes


- ---

- **509.8 EPIDEMIOLOGICAL ANTHROPOLOGY**
	- Infectious and Non-infectious diseases
	- Infectious diseases
	- Non-Infectious Diseases
	- Ecology of Malnutrition
	- Effects of Nutritional Stress

- ---
- **510.0 CONCEPT OF HUMAN GROWTH AND DEVELOPMENT**
	 - Stages of Human Growth
	- Factors affecting Growth and Development
	- Genetic factors
	- Environmental Factors
	- Nutrition
	- Socio-Economic Status
	- Endocrine factors
	- Ageing & Senescence
	- Theories of Aging
	- Human Physique and Somatotypes
	- Methods in the Assessment of Physique
	- Methodologies for Growth studies
	- Cross-sectional method
	- Longitudinal Method
	- Mixed-Longitudinal study

- ---

- **511.0 SOCIAL DEMOGRAPHY**
	- 11.1 Relevance of Merarche
	- Relevance of Menopause
	- Other Bio-Events to Fertility
	- 11.2 Theories of Population Growth
	- 11.3 Biological and socio-ecological factors influencing Fecundity, Fertility, Natality and Mortality


- ---

- **512.0 APPLICATIONS OF ANTHROPOLOGY**
	- Anthropology of Sports
	- Nutritional anthropology
	- Design of Defence and other equipments
	- Forensic anthropology
	- Fingerprints in personal identification
	- DNA fingerprinting
	- Personal Identification
	- Applied Human genetics
	- Paternity diagnosis
	- Eugenics & Euthenics
	- Application of DNA technology
	- DNA technology in medicine
	- Serogenetics and cytogenetics in reproductive biology

- ---


- ---



- ## PAPER – II – INDIAN ANTHROPOLOGY

- **521.1 EVOLUTION OF INDIAN CULTURE AND CIVILIZATION**

    - Paleolithic Culture
    - Lower Paleolithic in India
    - Middle Paleolithic
    - Upper Paleolithic
    - Mesolithic
    - Neolithic
    - Pre-Harappan Sites from the Subcontinent
    - Chalcolithic (Copper stone)
    - Ahar (@Banas) Culture
    - Kayatha Culture
    - Malwa Culture
    - Jorwe Culture
    - Ochre colored pottery (OCP) Culture
    - Painted Gray Ware (PGW) Culture
    - Iron age Culture in India
    - Indus Valley Civilisation
    - Vedic Age
    - Later Vedic Age
    - Post Vedic Age – Epic Age
    - Contribution of Tribal Cultures to Indian Civilisation
- ---
- **521.2 PALEO – ANTHROPOLOGICAL EVIDENCES FROM INDIA**

    - > with special reference to Siwaliks and
    - Narmada basin (Ramapithecus, Sivapithecus & Narmada Man).
    - Narmada Man
    - Paleoanthropological Evidence from India with special references to Siwaliks
    - Ramapithecus
    - Sivapithecus
- ---
- **521.3 ETHNO-ARCHAEOLOGY IN INDIA**

    - Concept of Ethno-Archaelogy
- ---
- **522.0 DEMOGRAPHIC PROFILE OF INDIA**

    - Ethnic elements in Indian Population
    - Linguistic elements in Indian population
    - Indian Population – Factors influencing Structure and Growth
    - Size and Growth rate of population in India
    - Factors for high population growth in India
    - Population Control in India
- ---
- **523.1 STRUCTURE AND NATURE OF TRADITIONAL INDIAN SOCIAL SYSTEM**

    - Varna
    - Purushartha
    - Ashrams
    - Karma and Rebirth
    - Rina
    - The Hindu Joint Family
- ---
- **523.2 CASTE SYSTEM IN INDIA**

    - Theories of origin of caste
    - Principal features of caste system in India
    - Varna vs. Caste – The difference
    - Is caste system unique to India?
    - Dominant caste
    - Jajmani system
    - Caste mobility and Future of caste system
- ---
- **523.3 SACRED COMPLEX & NATURE MAN SPIRIT COMPLEX**

    - Sacred complex
    - Nature-Man-Spirit complex
- ---
- **523.4 IMPACT OF RELIGION ON INDIAN SOCIETY**

    - Impact of Buddhism on Indian society
    - Impact of Jainism
    - Impact of Islam
    - Impact of CHRISTIANITY 
- ---
- **524.0 EMERGENCE AND GROWTH OF ANTHROPOLOGY IN INDIA**

    - Contribution of Anthropologists and Scholar-Administrators
    - Sarat Chandra Roy (SC Roy) – Father of Indian Ethnography
    - Nirmal Kumar Bose
    - D N Majumdar
    - S C Dube
    - MN Srinivas
    - L P Vidyarthi
    - Irawati Karve (1905-1970)
    - CHRISTOPH VON-FUHRER HAIMENDORF
    - Verrier Elwin
    - Dewan Bahadur L.K. Ananthakrishna Aiyar
    - Hasmukh Dhirajlal Sankalia
- ---
- **525.1 INDIAN VILLAGE**

    - Why village studies are needed?
    - Types of village studies
    - Traditional settlements in Indian Villages
    - Socio-cultural aspects of an Indian village
    - Agrarian social structure and social organization of Agriculture
    - Agrarian Reforms and its Impact on the Villages
    - Post-Independence Transformation of Rural Society
    - Impact of globalization on Indian villages
- ---
- **525.2 LINGUISTIC AND RELIGIOUS MINORITIES]**
	- > Socio-economic status

    - Religious minorities
    - Muslims
    - Christians
    - Sikhs
    - Linguistic minorities
    - Status of minority languages
    - Constitutional provisions
- ---
- **525.3 INDIGENOUS AND EXOGENOUS PROCESSES OF SOCIO-CULTURAL CHANGE IN INDIAN SOCIETY]**

    - Sanskritisation
    - Westernization
    - Modernisation
    - Tribe-caste continuum
    - Hindu Mode of Tribal Absorption
    - Folk-Urban Continuum
    - Great and Little tradition
    - Universalization and Parochialization
    - Panchayati Raj and social change
    - Media and Social change
- ---
- **526.1 TRIBAL SITUATION IN INDIA**

    - Biogenetic variability of tribes in India
    - Socio-Economic Characteristics of Tribal population
    - Economic Classification of the Tribes
    - Tribal Religion
    - Kinship, Family and Socio-economic characteristics of tribes based on region
        - North-Eastern Zone
        - Central Zone:
        - Southern Zone:
        - Small and Isolated zone:
- ---
- **526.2 PROBLEMS OF TRIBAL COMMUNITIES**

    - Land Alienation
    - Poverty and Indebtedness
    - Low literacy and poor educational facilities
    - Identity-Crisis
    - Unemployment
    - Displacement and Rehabilitation
    - Health, Nutrition and Hygiene
    - Other Problems
    - Summary of XaXa report
    - Key issues faced by Tribals and causes
    - Unemployment Rate
    - Education
    - Health
    - Nutrition
- ---
- **526.3 DEVELOPMENTAL POLICIES AND TRIBAL DISPLACEMENT AND PROBLEMS OF REHABILITATION**

    - THE FACTORS AFFECTING TRIBAL DEVELOPMENT
    - Displacement and Rehabilitation
    - Impact of Industrialization on Tribe
    - Impact of Urbanization on Tribals
    - Impact of Sancturies and National Parks on Tribal Population.
    - Forest Policy and Tribes ....
    - Forest and Tribes:
    - Pre-Colonia Period:
    - Forest Policies Under the Rule of British Crown:
    - Post-Colonial Fores Policies
- ---
- **527.1 PROBLEMS OF EXPLOITATION AND DEPRIVATION OF SC, ST and OBC**

    - Scheduled Tribe
    - Other Backward Class
    - Problems of the OBCs and empowerment tasks-
    - Scheduled caste
    - Socio-Economic Problems and Disabilities:
    - Constitutional Safeguards
    - Background of Reservation
    - Constitutional Safeguards of the SC/ST
    - Denotified Tribes
    - Realities in the implementation of constitutional welfare programmes
- ---
- **527.2 IMPACT OF DEMOCRATIC INSTITUTIONS, DEVELOPMENT PROGRAMS AND WELFARE**

    - MEASURES ON TRIBES
    - Democratic institutions
    - The Tribal Development Programmes And Welfare Schemes
    - The Governmental Ministries and Departments for Tribal Welfare
    - The Major Schemes and Plans of Change
    - Other Welfare Programmes for Tribal Development.
    - Post-Independence Transformation of Tribal’s Society
- ---
- **527.3 EMERGENCE OF ETHNICITY AND TRIBAL MOVEMENTS**

    - Concept of ethnicity
    - Ethnic Unrest in India -Major Factors
    - Nature of Tribal movements
    - Some Important Revolts
    - Pseudo - Tribalism
- ---
- **528.1 IMPACT OF HINDUISM, CHRISTIANITY, ISLAM AND OTHER RELIGIONS ON TRIBAL SOCIETIES**

    - Hinduism
    - Buddhism
    - Islam
    - Christianity
- ---
- **528.2 TRIBE & NATION STATE**

    - > Comparative study of tribal communities in India & other countries.
    - Colonization and tribes
    - The tribes today and modern nation states
        - South America
        - Africa
        - South Asia and South East Asia
        - North America
- ---
- **529.1 HISTORY OF ADMINISTRATION OF TRIBAL AREAS**

    - The tribal ‘Panchsheel’ - as enunciated by him are as follows:
    - Approaches to Tribal welfare and Development
    - Role of NGO in Tribal Development
    - Particularly Vulnerable Tribal Groups (PVTG)
- ---
- **529.2 ROLE OF ANTHROPOLOGY IN TRIBAL AND RURAL DEVELOPMENT**

    - Involvement Of Anthropologists
    - Relevance And Contribution Of Anthropology To Development
- ---
- **529.3 CONTRIBUTION OF ANTHROPOLOGY TO UNDERSTANDING OF REGIONALISM, COMMUNALISM AND ETHNOPOLITICAL MOVEMENTS**
    - Regionalism:
    - Communalism
```