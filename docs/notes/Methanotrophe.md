---  
title: Methanotrophe  
source: None  
date: 2021-05-30 12:54  
aliases: []  
tags: status/seed science/biology  
---  
Category:: 

---

# Methanotrophe
**flashcard**{: #flashcard}{: .hash}  

Methanotrophs are prokaryotes that metabolize methane as their only source of carbon and energy. 
$$ CH_4 \rightarrow CO_2 $$