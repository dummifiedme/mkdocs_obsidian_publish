---  
title: Nominated Members in RS  
date: 2021-03-10 07:45  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Nominated Members in RS
**flashcard**{: #flashcard}{: .hash}  

- 12 members
- from people who have special knowledge or practical experience in 
	- art
	- literature
	- science
	- social service


## Rationale behind the nominations in RS
**flashcard**{: #flashcard}{: .hash}  

to provide eminent persons a place in the RS without going through the process of election.
> American Senate has ==no== nominated members.