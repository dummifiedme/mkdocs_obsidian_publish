---  
title: King Krishnadevaraya  
source: None  
date: 2021-03-07 23:11  
tags: status/seed  
aliases: []  
---  
Category::  

---

# King Krishnadevaraya
**flashcard**{: #flashcard}{: .hash}  

> Inscription found with information f death of Krishnadevaraya. 
> It's a first time when there is a inscription found mentioning him. 
- [[Vijayanagara Empire]]
- died on October 17, 1529.
	- Reigned from 1509 - 1529
- Belongs to Tuluva Dynasty.