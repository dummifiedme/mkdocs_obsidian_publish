---  
title: Double Membership in Parliament  
source: None  
date: 2021-03-10 19:19  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Double Membership in Parliament
**flashcard**{: #flashcard}{: .hash}  

AS per [[Representation of People Act, 1951]]
- if elected in both the houses, within 10 days intimate the house he wants to serve - else, RS seat becomes vacant.
- If a sitting member in one house is elected into second house - first one becomes vacant.
- If elected for two seats in the same house, must choose one - else, both become vacant.

## Double membership - state and center
**flashcard**{: #flashcard}{: .hash}  

Choose within 14 days - else lose the parliament seat.