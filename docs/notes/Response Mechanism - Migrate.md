---  
title: Response Mechanism - Migrate  
source: None  
date: 2021-04-17 08:18  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Response Mechanism - Migrate
- Organism can move away temporarily from the stressful habitat to a more hospitable area and return when stressful period is over.
- Migratory birds
	- Keoladeo National Park, Bharatpur (Rajasthan)
		- from Siberia