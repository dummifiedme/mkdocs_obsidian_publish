---  
title: Response Mechanism - Suspend  
source: None  
date: 2021-04-17 08:43  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Response Mechanism - Suspend
- In bacteria, fungi and lower plants - thick walled spores - germinate when they find the suitable environment
- In higher plants, seeds and some other vegetative reproductive structures serve as means to tide over periods of tress helping in dispersal - germinate when they find the environment suitable - favorable moisture and temperature.
	- They do so by reducing their metabolic activities and going into a state of '*dormancy*'
- Some animals, might just escape in time if they can't migrate
	- Hibernation
		- bears
	- aestivation
		- snails and fishes to avoid summer related problems like [[dessication]]
		- eg Rose of Jericho!
	- diapause
		- zooplankton species in lakes and ponds