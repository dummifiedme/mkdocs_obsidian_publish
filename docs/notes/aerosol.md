---  
title: aerosol  
source: None  
date: 2021-05-29 20:33  
aliases: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# aerosol
**flashcard**{: #flashcard}{: .hash}  

Suspension of fine solid particles or liquid droplets in air or other gas.
> Aero-Solution
- mostly found in the lower atmosphere
	- sometimes reaches into the statosphere as well. e.g. volcanic eruptions.
- [[Sources of Aerosol]]
- [[Effects of Aerosol on the environment]]