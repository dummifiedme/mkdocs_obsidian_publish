---  
title: + UPSC  
date: 2020-12-18 04:50  
tags: note/MOC  
---  
Category:: [[../_Index]]


# + UPSC

## Plans
- [[Sources for UPSC Prelims and Mains]]
- [[~]]

## Part A - Preliminary Examination
Paper 1 - [[🔨 Project Prelims 2021]]
Paper 2 - [[+ Prelims CSAT]]

## Part B - Mains Examination
- Paper 1 - [[+ Essay]]
- Paper 2 - [[Keywords GS1]]
- Paper 3 - [[Keywords GS2]]
- Paper 4 - [[Keywords GS3]]
- Paper 5 - [[Syllabus GS4]]
- Paper 6 - [[🗃️ Anthropology#Paper 1]]
- Paper 7 - [[🗃️ Anthropology#Paper 2]]

## Part C - Interview
> Add later
[[🗃️ Interview]]

## Other
[[+ Facts and Stats]]]]
- [[+ Current Affairs]]
- [[091 OnlyIAS Prelims Booster]]
- [[095 OnlyIAS - Daily Editorial Discussions]]
- [[🗃️ Interview]]
- [[OnlyIAS - Revision Series]]

## Tests
[[+ UPSC Tests]]]]