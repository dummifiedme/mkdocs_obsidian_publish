---  
title: Abiotic Factor - Soil  
source: None  
date: 2021-04-17 07:40  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Abiotic Factor - Soil
- nature and properties of soil in different places vary - dependent on
	- the climate
	- the weathering process
	- type - transported or sedimentary.
- Characteristics of Soil
	- soil composition
	- grain size
	- aggregation
	- percolation
	- water holding capacity