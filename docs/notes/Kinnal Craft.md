---  
title: Kinnal Craft  
date: 2021-02-12 20:44  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/craft 

---
**review**{: #review}{: .hash}  
/upsc/prelims
# Kinnal Craft
**flashcard**{: #flashcard}{: .hash}  

- Traditional Wooden Craft
- aka Kinhal Craft
- Kinnal town, Koppal District of Karnataka.
- 15-16th Century, patronage of Vijayanagara Empire
- Hampi Chariot carvings <!--SR:!2022-01-05,1,230-->

## Significance of Kinnal Craft
- Expensive and traditional
- '*Lajawara Method*'
	- hand beating of tin sheet and mixed with gold to be used as paint.