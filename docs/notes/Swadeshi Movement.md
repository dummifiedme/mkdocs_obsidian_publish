---  
title: Swadeshi Movement  
source: None  
date: 2021-05-20 14:32  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Swadeshi Movement

- Swadesi Movement was started in response to ::  [[Partition of Bengal Province]] proposal - 1903.
<!--SR:!2021-09-17,24,270-->

- Time span of Swadeshi Movement is from :: 1903 to roughly around 1908.
<!--SR:!2021-09-16,23,270-->

- It was mostly under Moderates' control from 1903-1905, later went on to be under Extremists from 1905-1908.