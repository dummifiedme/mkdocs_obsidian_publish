---  
title: InvIT Model  
source: None  
date: 2021-05-08 20:28  
tags: status/seed  
aliases: ['Infrastructure Investment Trust']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# InvIT Model
**flashcard**{: #flashcard}{: .hash}  

- Infrastructure Investment Trust
- Like a mutual fund - enables direct investment of small amounts of money - individual or institutional investors - get dividend.
- Regulated by [[SEBI]] InvIT Regulations, 2014. <!--SR:!2021-08-18,3,250-->