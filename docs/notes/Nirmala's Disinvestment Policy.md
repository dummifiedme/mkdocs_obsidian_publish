---  
title: Nirmala's Disinvestment Policy  
source: OnlyIAS Editorial Discussion  
date: 2021-02-13 00:45  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Economics]], [[Keywords GS3]]

---

# Nirmala's Disinvestment Policy
- [[Disinvestment vs Strategic Disinvestment]]
- As part of - [[Atma Nirbhar Bharat]] package, the government stated that it will only stay in the strategic sectors
	- Atomic Energy, Space and Defence
	- Transport and Telecom
	- Power, Petroleum, Coal and other minerals
	- Banking and financial services.