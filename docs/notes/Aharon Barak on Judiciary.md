---  
title: Aharon Barak on Judiciary  
source: None  
date: 2021-05-15 23:44  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity/judiciary general

---
**review**{: #review}{: .hash}  
 
# Aharon Barak on Judiciary
*Aharon Barak* writes in *The Judge in a Democracy* that judicial policy and judicial philosophy are fundamental to the judiciary since they guide it in the most difficult hours. The strength of the judges is in understanding their limitations. More than they have answers to difficult legal problems, they have questions regarding the path they should take. All human beings err; for the judges, there is nothing more proper and practical than good judicial philosophy.