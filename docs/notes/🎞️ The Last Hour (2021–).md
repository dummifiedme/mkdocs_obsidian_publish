---  
cover: https://m.media-amazon.com/images/M/MV5BNDNmNTNjOTAtNmI4Mi00MTY5LWI4ODgtYmI2NGVhNzM5MDczXkEyXkFqcGdeQXVyMTIxMDk2NDE4._V1_SX300.jpg  
imdbId: tt9805612  
---  
**entertainment**{: #entertainment}{: .hash}  
/show/hindi
# 🎞️ The Last Hour (2021–)

- - -
> [!movie]
> ![poster|-right](https://m.media-amazon.com/images/M/MV5BNDNmNTNjOTAtNmI4Mi00MTY5LWI4ODgtYmI2NGVhNzM5MDczXkEyXkFqcGdeQXVyMTIxMDk2NDE4._V1_SX300.jpg)
> 
> **Director**::  
> **Genre**:: "Crime", "Drama", "Mystery"
> **Language**:: Hindi
> **IMDB Rating**:: 7.5
> **Year**:: 2021–
> **Seasons**:: 1
> **Cast**:: "Sanjay Kapoor", "Karma Takapa", "Shaylee Krishen"
> **Watched**:: "2022-01-05"
> **Rating**:: 6
- - -

## Synopsis
**Plot**:: A mysterious young shaman, on the run protecting a secret gift, joins hands with a seasoned city cop to hunt down a dangerous figure from his dark past. But when he falls for the cop's young daughter, he is torn between duty and love