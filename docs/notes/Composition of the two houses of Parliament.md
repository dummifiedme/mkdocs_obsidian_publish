---  
title: Composition of the two houses of Parliament  
source: None  
date: 2021-03-10 07:21  
tags: status/seed  
aliases: []  
---  
Category:: [[Parliament]] 

---

# Composition of the two houses of Parliament
- [[Inbox/Old Vault UPSC/GS2/Composition of Rajya Sabha]]
- [[Inbox/Old Vault UPSC/GS2/Composition of Lok Sabha]]