---  
title: Sankardeva  
source: None  
date: 2021-03-10 21:55  
tags: status/seed  
aliases: ['Shankardeva', 'Srimana Shankardeva']  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture/saints 
# Sankardeva
**flashcard**{: #flashcard}{: .hash}  

- Region:: Assam
- Span::
- Sect::Bhakti saint - *eka-sharana-naam-dharma*.
- rejects focus on vedic ritualism
- focus on devotion - bhakti
- 4 components
	- Dev (God)
	- Naam (prayers)
	- Bhakta (devotees)
	- Guru (teachers)