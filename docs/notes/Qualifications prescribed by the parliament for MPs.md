---  
title: Qualifications prescribed by the parliament for MPs  
source: None  
date: 2021-03-10 19:05  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Qualifications prescribed by the parliament for MPs
**flashcard**{: #flashcard}{: .hash}  

Added in the [[Representation of People Act, 1951]].
1. must be registered as an elector for a parliamentary constituency.
	- same for both RS and LS.
	- Prior to 2003, it was mandatory to be from the same constituency in case of RS. - valid as per SC in 2006.
2. Must be SC/ST from any state or UT to contest a [[Reservation of Seats for SC and STs in LS]].