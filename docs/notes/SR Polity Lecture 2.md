---  
Source: Subhra Ranjan  
Lecture: 2  
Date: 2020-11-27  
---  
# SR Polity Lecture 2
---

## Democratic Systems
1. Direct Democracy (Switzerland)
	1. [[Swiss System of Government]]
2. Indirect Democracy
	1. [[Parliamentary System) (UK]]
		1. [[German System of Government]] (Germany)
	2. [[Presidential System) (USA]]
	3. [[Hybrid System of Democracy]] (aka Semi Presidential, France)

---

### Democracy vs Republic 
[[Democracy vs Republic]]

---

### Presidential vs Parliamentary System
[[Presidential vs Parliamentary System]]

---

### Rights of Nominal Head
>- Right to be {informed}
>- Right to {warn} (as elder)
>- Right to {encourage}
>> {Bagehot} wrote these rights wrt QUEEN of UK

--- 
 
 [[Cabinet Form of Government]]
 
---

> - There can't be a President's Rule at UNION.
>     - There shall always be a council of ministers.

---

### Reasons for adopting Parliamentary System
**flashcard**{: #flashcard}{: .hash}  

1. Familiarity with the system
2. More Democratic because the power is not concentrated in the hands of one person, there is a cabinet or council of ministers.
3. It is more accountable on day-to-day basis.
4. More representative, thus more suitable for India's diversity.

---

### Drawbacks of Parliamentary system
**flashcard**{: #flashcard}{: .hash}  

1. Party Politics --> Federalism distorts the Parliamentary system. **todo**{: #todo}{: .hash}  
/open

---

###  Reasons for a probable shift to Presidential System
**flashcard**{: #flashcard}{: .hash}  

- India had faced an era of coalition politics in the absence of healthy coalition^["to come together"] culture, there were many problems:
	- Policy Paralysis
	- Defections
	- Instability
	- Corruption or Horse-trading

---

### Should there be a Presidential System in India?
1. The reasons which were in the mind of constituent assembly, at the time of formation of the government are still relevant.
2. Institutions will not make a difference, until and unless political culture changes.
	1. As suggested by {Ambedkar}, the constitution will work or not, will depend upon the persons holding these institutions.
3. Presidential System has it's own challenges. 
	1. Problem of "grid lock".
4. USA is the only country where democratic nature of the system could be retained, while all other third-world countries transformed into Dictatorship
COUNCLUSION: Institutions will not make the difference, person holding these institutions do not want to change.

### How to better the parliamentary system?
**flashcard**{: #flashcard}{: .hash}  

The main reason for dissatisfaction with parliamentary system are defections, corruption and political instability. Hence, we can experiment with following systems.
1. Introduction of system with constructive vote of no-confidence as prevalent in Germany.
2. **TODO**{: #TODO}{: .hash}  
 

### Arguments against Presidential System in India
[[Arguments against Presidential System in India]]

### Political Parties
- Essential for [[Parliamentary System]]
- Isn'r required for [[Presidential System]]

### Role of Independent Candidates
- Not able to form government --> hence ineffective at spreading the ideology.
- Law Commission report --> suggested to remove independent candidate.

### What is a Hybrid System of Government?
[[Hybrid System of Democracy]]

### What is Co-Habitation?
[[Co-Habitation]]

### German System of Government
[[German System of Government]]

### Swiss System of Government
[[Swiss System of Government]]