---  
title: Vedanthangal Bird Sanctuary  
source: None  
date: 2021-05-27 16:38  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Vedanthangal Bird Sanctuary
[[https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.esrd6Oi0uw3-N_zyYxo9KgHaDk%26pid%3DApi&f=1]]
- In ==Tamil Nadu==
- ==Oldest== bird sanctuary in India.