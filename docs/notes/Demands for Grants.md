---  
title: Demands for Grants  
source: None  
date: 2021-03-17 17:12  
tags: status/seed  
aliases: []  
---  
Category:: [[Parliament]] [[Budget]]

---

# Demands for Grants
A request/grant of funds for the budget. (Before Appropriation Bill)

## Two important characteristics of Demand for Grants
**flashcard**{: #flashcard}{: .hash}  

- RS can't vote on it.
- Only the votable part is voted upon - charged on part is not votable (but can  be discussed)