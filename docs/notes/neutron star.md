---  
title: neutron star  
date: 2021-06-28 19:22  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims   
# neutron star
**flashcard**{: #flashcard}{: .hash}  

A star formed when the core of a massive star undergoes gravitational collape at the end of it's life.