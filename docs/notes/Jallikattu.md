---  
title: Jallikattu  
date: 2021-06-11 12:00  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims 
# Jallikattu
**flashcard**{: #flashcard}{: .hash}  

- Silver/Gold coins tied to bull horns.
- Tame and take those coins to win the sport.
- Around the time of Pongal 
- Positives:
	- Better and virile bull identified and preserved
- Negatives:
	- Violent - people die
	- Cruelty against animal
- Case : Being heard by a special commission.