---  
title: Reservation of Seats for SC and STs in LS  
source: None  
date: 2021-03-10 18:28  
tags: status/mature  
aliases: []  
---  
Category:: 

---

# Reservation of Seats for SC and STs in LS
**flashcard**{: #flashcard}{: .hash}  

- BASIS - Population ratios
- [[CAA 95]] extends the reservation till 2020.
- though reserved, they are voted upon by the people of the constituency.
- Not debarred from contesting elections from a non-reserved seat. <!--SR:!2021-09-24,3,250-->