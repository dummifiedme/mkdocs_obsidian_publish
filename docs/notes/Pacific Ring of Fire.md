---  
title: Pacific Ring of Fire  
source: None  
date: 2021-03-18 23:03  
tags: status/seed  
aliases: ['RIng of Fire']  
---  
Category::  

---

# Pacific Ring of Fire
A region with boundaries of many tectonic plates leading to a large number of frequently erupting active volcanoes and earthquakes.