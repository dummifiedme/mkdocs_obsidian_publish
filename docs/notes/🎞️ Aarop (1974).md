---  
cover: https://m.media-amazon.com/images/M/MV5BNjE4MWZiZDMtNzA3Yy00NTFiLTliOGQtZGRkZGUzZDMxYzM4XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_SX300.jpg  
imdbId: tt0154108  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie/Hindi
# 🎞️ Aarop (1974)

- - -
> [!movie]
> ![poster|right](https://m.media-amazon.com/images/M/MV5BNjE4MWZiZDMtNzA3Yy00NTFiLTliOGQtZGRkZGUzZDMxYzM4XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_SX300.jpg)
**Director**:: "Atma Ram"
**Genre**:: "Drama"
**Language**:: Hindi
**IMDB Rating**:: 8.6
**Year**:: 1974
**Cast**:: "Vinod Khanna", "Saira Banu", "Vinod Mehra"
**Watched**:: "2021-10-29"
**Rating**:: 5
- - -

## Synopsis
**Plot**:: The story of three friends, Subhash (Vinod Khanna), the fiery editor of a weekly newspaper called Mashal, Aruna (Saira), a school teacher who helps to run the paper, and Ravi (Vinod Mehra), a lawyer and a post, in the small hill town

---
Typical right vs wrong drama taking place in a small town. The righteous section, consisting of Subhash(Vinod Khanna), Ravi(Vinod Mehra) and Aruna(Saira Banu), and being part of a fiery newspaper journal "Mashaal" are constantly pointing out the negative, vile and corrupted actions of Singh Saab. They write against the opening of bars and dance parlors among other evils gripping the poor peoples' lives around the town.

There is nothing of note in the movie and it can be just classified as a typical drama involving a love triangle, friendship and good-bad narrative.