---  
date: 2021-12-23 13:40  
aliases: []  
---  
# Direct Tax

- **Definition**:: A type of tax where incidence and impact of taxation fall on the same entity. The burden of tax cannot be shifted by the tax payer to someone else.
- **Examples**:: "Income Tax", "Property Tax", "Inheritance Tax", "Gift Tax"

- **Direct taxes are considered progressive.**
	  [[Drawing - Progressive Taxation.excalidraw.svg|300-right]]
	- Government can impose lower taxes to low-income earners and relatively higher taxes to high-income earners.
	- possible to impose higher taxation rates for the rich and lower for the poor.
	- e.g. personal income tax in India has a higher tax rate for higher income.




- - -
## References
- [[2021VMT04 1490 - GS3 Economy]]