---  
---  
# + Newspaper Bits
## 2022
### January
#### 2022-01-12
##### Trating planet well can aid progress
- [!]  The twin challenges of poverty alleviation and environmental sa  feguarding that former Prime Mi  nister Indira Gandhi ﬁ rst articulat  ed in her lecture during the Stockholm conference on the hu  man environment in 1972 still re  main unattended. Fifty years have passed. There is little change in the scenario. In fact, the situation is much more complex now.
- Gender Develop  ment Index, Gender Inequality In  dex, and Multidimensional Pover  ty Index

- Both global and local evi  dence indicate that biodiversity loss, climate change, land system/ land use change, disruption of biogeochemical cycles, and scarci  ty of freshwater availability are a threat and increase the vulnerabil  ity of society
- When planetary pressure is adjust  ed, the world average of HDI in 2019 came down from 0.737 to 0.683.
- The average per cap  ita global CO2 emission (produc  tion) is 4.6 tonnes and the per capita material footprint is 12.3 tonnes.In the case of India, the PHDI is 0.626 against an HDI of 0.645 with an average per capita CO2 emis  sion (production) and material footprints of 2.0 tonnes and 4.6 tonnes, respectively. India gained in global rankings by eight points (131st rank under HDI and 123rd rank under PHDI), and its per cap  ita carbon emission (production) and material footprint are well below global percapita averages.
- The Chipko movement (1973) in Uttarakhand and the Silent Valley movement (the late 1970s) in Kerala are two of the most well_known modern_day people’s movements for environ_ mental protection in India that in_ spired several other environmen_ tal movements during the last ﬁ_ve decades.
- [b] standalone environ_ mental safeguarding actions are not suﬃ_cient to navigate the An_ thropocene
- the central chal_ lenge is to nest human development including social and economic systems into the ecosys_ tem, and biosphere building on a systematic approach to nature_ based solutions that put people at the core.

##### Malayalam film industry in rot
- Hema Committee report
- Women harassment and abuse


##### Why tackling air pollution is a win, win
- China reduced air pollution levels - invested hugely, increased gdp even more.
- reduced coal, steel etc
- As we move into 2022, the Commission for Air Quality Management (CAQM) in Delhi NCR and Adjoining Regions has to ﬁnd its balance between the China and California models to battle Delhi’s severe air pollution crisis.
- Cali_ fornia Air Resources Board (CARB) set a precedent in identifying auto_ mobiles and industry as point sources of pollution back in the early 1950s.

#### 2022-01-13
- Rajmannar Committee
- National Education Alliance for Technology