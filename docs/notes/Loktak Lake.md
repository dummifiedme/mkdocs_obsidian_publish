---  
title: Loktak Lake  
date: 2021-06-11 10:17  
alias: []  
tags: status/mature  
locations: None  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Loktak Lake
- **Location**::  Manipur
- Kaibul Lenju National Park --> Sangai (Dancing Deer; Critically Endangered)
- Lok (stream) + tak (end)
- famous for [[Phumdis]]
- Last natural refuge for Sangai (Endangered)
- Ramsar site since 1990
- Loktak Day is celebrated every 15th October at the periphery of Loktak Lake.

**Map**:: [Loktak Lake](geo:24.561487844674105,93.80762100219727) tag:lakes