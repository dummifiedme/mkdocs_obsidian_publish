---  
title: Abiotic Factor- Temperature  
source: None  
date: 2021-04-17 07:01  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Abiotic Factor- Temperature
- most important ecologically relevant environmental factor
- it effects the kinetics of enzymes and through it the metabolic activity.
- A few organisms can thrive in a wide range of temperatures - ==*eurythermal*== 
- A vast majority of organisms are restricted to a narrow range of temperatures - ==*stenothermal*==
- The level of tolerance o different species determine to a large extent their geographical distribution.