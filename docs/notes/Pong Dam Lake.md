---  
title: Pong Dam Lake  
date: 2021-06-24 18:16  
alias: ['Pong Dam']  
tags: status/mature  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Pong Dam Lake
**flashcard**{: #flashcard}{: .hash}  

- Pong Dam [[Wildlife Sanctuaries]]
- [[Ramsar Sites - HP|Ramsar Site]] since 2002
- 220 species of birds 
	- Bar Headed Geese
	- Eurasian Coot
- Concern:
	- marginally less migration to the lake.

- Pong Dam is on :: River Beas.