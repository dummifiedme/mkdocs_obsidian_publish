---  
title: Grants  
source: None  
date: 2021-03-17 17:27  
tags: status/seed  
aliases: []  
---  
Category:: [[Parliament]] 

---

# Grants
## List of Grants
- [[Budget]]
- Supplementary Grant
- Additional Grant
- Excess Grant
- [[Vote of Credit]]
- Exceptional Grant
- Token Grant