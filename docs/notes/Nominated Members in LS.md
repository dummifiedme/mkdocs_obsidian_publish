---  
title: Nominated Members in LS  
source: None  
date: 2021-03-10 17:29  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Nominated Members in LS
**flashcard**{: #flashcard}{: .hash}  

President could nominate 2 members form the Anglo-Indian Community. (Till 2020)

- Originally, the nomination of Anglo-Indians into the LS was to operate till 1960, but was extended till 2020 by the 95th Amendment Act, 2009[[CAA 95]]. 

- Now, the nomination for Anglo-Indians is removed by [[CAA 104]]