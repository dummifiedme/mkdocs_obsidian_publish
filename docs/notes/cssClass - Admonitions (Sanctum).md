---  
---  
**note**{: #note}{: .hash}  
/general/knowledge/tech/config/obsidian
# cssClass - Admonitions (Sanctum)

- [ ] Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
- [⭐] Lorem ipsum dolor sit amet, consectetur adipiscing elit.   
- [a] Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
- [❤] Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
- [s] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [S] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [-] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [>] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [<] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [l] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [B] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [X] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [n] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [p] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [c] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [W] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [b] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [I] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [!] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [?] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [i] Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
- [1] Lorem ipsum dolor sit amet, consectetur adipiscing elit.