---  
title: Maguri-Motapung Beel  
date: 2021-02-17 00:58  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Maguri-Motapung Beel
**flashcard**{: #flashcard}{: .hash}  

![[Map - Maguri-Motapung Beel.png]]
- Important Bird Area by Bombay Birds Society.
- In Tinsukia, Assam