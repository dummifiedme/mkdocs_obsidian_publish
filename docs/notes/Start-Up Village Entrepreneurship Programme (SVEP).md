---  
title: Start-Up Village Entrepreneurship Programme (SVEP)  
source: None  
date: 2021-05-08 20:24  
tags: status/seed  
aliases: ['SVEP']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Start-Up Village Entrepreneurship Programme (SVEP)
**flashcard**{: #flashcard}{: .hash}  

- Sub-Scheme under [[Deendayal Antyodaya Yojana-National Rural Livelihoods Mission]]
- Flagship rural livelihood mission of [[Ministry of Rural Development]]
- Objective - helping rural households - including women - to setup enterprises
- Both individual and group enterprises
- <!--SR:!2021-08-17,1,210-->