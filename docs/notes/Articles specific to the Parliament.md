---  
title: Articles specific to the Parliament  
source: ['$B - Laxmikant - Indian Polity']  
date: 2021-03-09 18:05  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# Articles specific to the Parliament
**flashcard**{: #flashcard}{: .hash}  

> Articles 79 to 122 in Part V 
- Deals with the organisation, composition, duration, officers, procedures, privilege ,powers and so on of the Parliament.