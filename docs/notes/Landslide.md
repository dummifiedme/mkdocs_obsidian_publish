---  
---  
- As per [[Geological Survey of India]], around 12.6% of area of India is prone to landslide hazards.

## Landslide Prone Areas in India
![[Map - Landslide prone areas in India.jpeg]]