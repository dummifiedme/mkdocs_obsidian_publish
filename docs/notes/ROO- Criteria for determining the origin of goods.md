---  
title: ROO - Criteria for determining the origin of goods  
source: None  
date: 2020-12-23 22:01  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# ROO- Criteria for determining the origin of goods
1. Wholly Obtained criterion
2. Substantial/Sufficient transformation criterion
	1. Value Content Method
	2. Change in Tariff Classification Method
	3. Process Rule Method