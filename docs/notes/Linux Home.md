---  
---  
# Linux Home

- [[Linux - Fan Control]]
- [[Touchpad Gestures in Linux]]
- [[Adding bookmarks to a pdf using GhostScript]]
- [[Jellyfin Server on Linux]]
```ad-info
title: List of Linux Notes

 ```dataview
list choice(contains(file.tags, "#grab"), file.tags, "")
from **note**{: #note}{: .hash}  
/general/knowledge/tech/linux

``````




## Common Commands

### Installing Apps
- [[pamac]]
- [[pacman]]

### Graphics
- `nvidia-settings` ─ NVIDIA settings GUI to control temps.
- `prime-run` ─ to run a program with GPU. Only for proprietary drivers.
- `glxgears` ─ shows the fps for animated gears
- `glxinfo | grep renderer` ─ shows the graphics drivers in use

## Issues
- **HDD on Manjaro making noise** ─ faster/slower rotations
	- was due to a module called `tlp` which is used for conserving battery. Has various parameters, including one for reducing the spin rate for HDD. 
		- It is terrible to reduce or increase the spin rate. Never do that! 
		- Removed `tlp` altogether.