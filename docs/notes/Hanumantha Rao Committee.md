---  
---  
# Hanumantha Rao Committee
- District level planning
	-  District Collector or Minister as head of the planning bodies in districts.
	-  Differed from 
		-  Balwantrai Mehta Committee
[[Inbox/Old Vault UPSC/Ashok Mehta Committee]]  [[Inbox/Old Vault UPSC/Ashok Mehta Committee]]
		-  GVK Rao Committee

			These committees recommended reduction in the developmental role of the District Collector and assigned major role to Panchayati Raj in the development administration.