---  
title: Global Innovation Index  
source: None  
date: 2021-05-08 20:14  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Global Innovation Index
**flashcard**{: #flashcard}{: .hash}  

- Annual
- Ranks 131 countries
- Released jointly by [[WIPO|World Intellectual Property Organisation]], Cornell University and INSEAD Business School
- Theme for 2020 - "Who will Finance Innovation?"
- India under 50th rank = Rank 48. First time.