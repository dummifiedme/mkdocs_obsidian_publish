---  
title: Editorial Discussion - 2021 July  
date: 2021-07-02 07:01  
alias: []  
tags: MOC  
---  
Category:: [[095 OnlyIAS - Daily Editorial Discussions|All Editorials]]

## Editorial Discussion - 2021 July

### 2021-07-01 | Thursday
- [[Censor Board]]
- [[Reimagining Examinations]]
- Small doses of fund sanction to industries, and various sectors like health.
	- No [[Direct Benefit Transfer|DBT]] as of now. 

### 2021-07-02 | Friday
- Delhi's [[lame duck]] assembly
	- due to the new amendment act to GNCTD Act.
		- Delhi's legislative assembly or the committees cannot make rules for or control the day-to-day executive.
		- the original act had a lot more control
- [[Police Reforms]]
- [[Smart City Mission (SCM)]]

### 2021-07-03 | Saturday
ShankarIAS [The Hindu Daily News Analysis || 3rd July 2021 || UPSC Current Affairs || Prelims 2021 & Mains - YouTube](https://www.youtube.com/watch?v=Iw4HqrSU178)
- Various prehistoric periods.
	- Neolithic
	- Chalcolithic
	- ![[Map - Neolithic and Chalcolithic Cultures.png]]
	- **India's important Chalcolithic Cultures**
		- Ochre-coloured pottery culture
			- Indo-Gangetic divide and upper Ganga-Yamiuna Divide
		- Ahar Culture or Banas Culture
			- Mewar Rajasthan
		- Kayatha and Malwa Cultures
			- Malwa region of Rajasthan
		- Malwa and Jorwe Cultures
			- Western Maharashtra.
			- Ahmadnagar District og Gijarat.
		- Narhan Culture and Variants
			- northern vindhyas and the middle and lower ganga vallye
			- Narhan Village near Gorakhpur!!
			- White pottery~~~~
- - [[Mobile Bridging System]]

- Vaccine Discrimination by EU
	- Covishield is not being given certificated for travel in EU.
	- EU Green Passport


### 2021-07-04 | Sunday
[ShankarIAS - YouTube](https://youtu.be/LsH7qP5lU5g)

- China goes malaria-free
[[Keywords GS2]]	- Certified by WHO
	- 70-year effort
	- Malaria
		- Vector - Mosquito
		- P. falciparum and P. Vivax pose greatest threat
		- **2019**: 229 million cases of malaria worldwide **facts**{: #facts}{: .hash}  
 
		- most cases and deaths occur in sub-saharan Africa
	- Malaria-free countries
		- Australia (1981), and others.
		- China first in western-pacific region.
		- [[India is not yet a malaria-free country]]
	- China's journey to malaria elimination
		![[China to Malaria-free journey.png]]
- Afghan Issue
	- US Invasion of Afghanistan
	- Motives of the US to withdraw forces from Afghanistan
	- Role of Pakistan in Afghanistan politics
	- India-Afghanistan
- Petrol Price Hike
- EU Green Passport
- [[2300yo step-well found near Erode]]
- Subhiksja Keralam Project
- OPEC and OPEC+
- Krishna River


### 2021-07-05 | Monday
[5 July, Editorial Discussion, Current Affairs The Hindu | Sumit Rewri | Rural power, Education - YouTube](https://www.youtube.com/watch?v=t4yliL2cWoQ)
- Dairy Sector
	- ![[Screenshot - Significance of Dairy Sector.png]]
	- 1970s - Operation Flood
	- 1991 - LPG reforms - partially opened for private
	- 1999 - completely open for private companies.
	- Private players have transformed the Dairy Sector
		- Green feed distribution services
		- Sexed Semen Technology - controlling the bulls birth rate right from the sperm (chromosome identification) - BAIF, MH.
		
### 2021-07-06 | Tuesday

### 2021-07-07 | Wednesday
[7 July, Editorial Discussion, Current Affairs The Hindu | Sumit Rewri | India-Turkey, Dowry - YouTube](https://www.youtube.com/watch?v=QqmcOPxpr7E)
- Afghan Issue 
	- The Turkish role and India-Turkey relations
- Dowry Issue 



### 2021-07-08 | Thursday
[8 July, Editorial Discussion, Current Affairs The Hindu | Sumit Rewri | Population control, Afghan - YouTube](https://youtu.be/OvSxRZA7lTw)
- Population Control
	![[Stages of Human Population Pyramids.png]]
	- National Population Policy(NPP) 2000
		- Family planning is free of targets and would remain voluntary.
	- Minister of Health and Family Welfare, Anupriya Patel in 2017 - Family planning programme in India is target free and voluntary in nature. 
	- What are the methods that could be employed for controlling population and their viability? (11 points) **Questions**{: #Questions}{: .hash}  
 
	- Awareness >> Policy.
- Terrorism
	- UAPA - 'draconian'
		- earlier TADA
		- 'Terrorism' is not clearly defined universally in India.
			- [[United Nations General Assembly]] in its 50 years of existence, couldn't define it well.
	

### 2021-07-09 | Friday
- Farmer Producer Organisation
- Tracking fugitives