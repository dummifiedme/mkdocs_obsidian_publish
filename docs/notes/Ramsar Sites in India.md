---  
title: Ramsar Sites in India  
date: 2021-06-11 10:17  
alias: []  
tags: note/MOC  
---  
# Ramsar Sites in India

## Ramsar Sites in India
- India has **46 sites** in Ramsar List
![[Ramsar-Sites-in-India.jpeg]]

## State-wise Ramsar Sites

### North
- [[Ramsar Sites - Ladakh]]
- [[Ramsar Sites - JK]]
- [[Ramsar Sites - HP]]
- [[Ramsar Sites - Punjab]]
- [[Ramsar Sites - Uttarakhand]]
- [[Ramsar Sites - UP]]
- [[Ramsar Sites - Haryana]]

### East
- [[Ramsar Sites - Bihar]]
- [[Ramsar Sites - WB]]
- [[Ramsar Sites - Odisha]]

### North-East
- [[Ramsar Sites - Assam]]
- [[Ramsar Sites - Tripura]]
- [[Ramsar Sites - Manipur]]

### West and Central
- [[Ramsar Sites - Rajasthan]]
- [[Ramsar Sites - Gujrat]]
- [[Ramsar Sites - MP]]
- [[Ramsar Sites - Maharashtra]]

### South
- [[Ramsar Sites - Andhra Pradesh]] 
- [[Ramsar Sites - TN]]
- [[Ramsar Sites - Kerala]]



## Year-wise Ramsar Sites

**2020**

1. [[Tso Kar]]
2. [[Sur Sarovar]]
3.[[Lonar Lake]]
4. [[Asan Barrage]]
5. [[Kanwar Taal]] or Kabar Taal Lake

**2019**

1. Parvati Aranga Bird Sanctuary
2. Saman Bird Sanctuary
3. Samaspur Bird Sanctuary
4. Beas Conservation Reserve
5. Keshopur-Miani Community Reserve
6. Nangal Wildlife Sanctuary
7. Sandi Bird Sanctuary
8. Nawabganj Bird Sanctuary
9. Sarsai Nawar Jheel
10. Nandur Madhmeshwar
11. Sundarban Wetla1.

**2012**

1. Nalsarovar Bird Sanctuary

**2005** and older

1. Chandra Taal
2. [[Hokera Wetlands]]
3. Renuka Lake
4. Rudrasagar Lake
5. Surinsar-Mansar Lake
6. Upper Ganga River - Brighat to Narora Stretch
7. Ashtamudi [[Wetland]]
8. Bhitarkanika Mangroves
9. Bhoj Wetlands
10. Deepor Beel
11. [[East Kolkata Wetlands]]
12. Kolleru Lake
13. Point Calimere Wildlife and Bird Sanctuary
14. [[Pong Dam Lake]]
15. Sasthamkotta Lake
16. Tsomoriri
17. [[Vembanad Lake]]
18. Kanji [[Wetland]]
19. Ropar [[Wetland]]
20. Harike [[Wetland]]
21. [[Loktak Lake]]
22. Sambhar Lake
23. [[Wular Lake]]
24. [[Chilika Lake]]
25. Keoladeo National Park