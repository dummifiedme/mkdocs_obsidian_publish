---  
title: Integrated Guided Missile Development Program  
source: None  
date: 2021-02-25 13:27  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[Missiles]] , 
---

# Integrated Guided Missile Development Program
**flashcard**{: #flashcard}{: .hash}  

- Conceived by Dr. APJ Abdul Kalam
- Approved by GoI in 1983, completed in March 2012.
- Self-sufficiency in defence equipment.

## Missiles developed under this IGMDP 
**flashcard**{: #flashcard}{: .hash}  

1. [[Prithvi Missile]]
2. [[Agni Missile]]
3. [[Trishul Missile]]
4. [[Nag Missile]]
5. [[Akash Missile]]
> Pnemonic: ***PATNA***