---  
title: Dandi March  
source: None  
date: 2021-03-20 00:54  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ History - Modern India]]

---

# Dandi March
**flashcard**{: #flashcard}{: .hash}  

- Date: 12th March to 5th April 1930 - 24-day march
- From Sabarmati with 79 followers to Dandi in Arabian Sea.
	- 241 Miles.
- A tax resistance campaign against the British salt monopoly.
	- 1882 Salt Act gave the monopoly to British.
	- Though it was freely available on Indian coasts.
- Marked a start of Civil Disobedience Movement.