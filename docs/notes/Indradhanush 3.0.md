---  
title: Indradhanush 3.0  
source: None  
date: 2021-03-07 21:14  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Indradhanush 3.0
**flashcard**{: #flashcard}{: .hash}  

- to increase immunization coverage across the country.
- IMI 3.0 - Intensified Mission Indradhanush 3.0
	- Started with 7 vaccines - hence Indradhanush
		- though now 12 vaccines
			![[Pasted image 20210307211609.png]]
	- Indradhanush started in 1985, IMI in 2017.