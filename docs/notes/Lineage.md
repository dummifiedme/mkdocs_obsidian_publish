---  
---  
# Lineage
- a descent group, made up of different families.
- includes ancestors of 5-6 generations
- may or maynot share common residence
- exogamous unit
- relationships between the members are clearly defined. 
	- ancestors are not mythological or legendary figure

---

## Questions
##### What are Lineages?
A descent group made up of families. 
- includes ancestors from 5-6 generations

##### What kind of marriage do lineages prefer/have?
Exogamous Marriages

##### How clear are the definitions of relationships in a lineage?
There are clear and established linkages, without any mythical or legendary figurine (ancestor).

##### Examples of Lineages
- Patrilineage
- Matrilineage



- [[Tharavad]]