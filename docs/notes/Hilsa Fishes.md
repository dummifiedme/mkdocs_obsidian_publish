---  
title: Hilsa Fishes  
source: None  
date: 2021-02-17 00:35  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment 
# Hilsa Fishes
**flashcard**{: #flashcard}{: .hash}  

A fish that goes from sea water to Padma river to Prayagraj
![[HilsaFish Image.png]]