---  
title: Giant Leatherback Turtle  
source: None  
date: 2021-02-25 16:38  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Environment]] , [[Keywords GS3]]

---

# Giant Leatherback Turtle
**flashcard**{: #flashcard}{: .hash}  

> Tourism and port development is a dangerous to their breeding grounds - Andaman and Nicobar Islands.
- Largest of the seven species of sea turtles.
- Nesting in selected locations
	- Indonesia
	- Andaman and Nicobar Islands
- COnservation Status
	- IUCN: Vunerable
	- India's Wildlife Protection Act, 1972: Schedule I.