---  
title: Representation of People Act, 1951  
source: None  
date: 2021-03-10 18:49  
tags: status/seed  
aliases: ['RPA 1951']  
---  
Category:: [[Government Acts]] , [[Keywords GS2]]

---

# Representation of People Act, 1951
- Fixes the term of office for the RS member at 6 years.
- [ ] **todo**{: #todo}{: .hash}  
 read and complete soon.