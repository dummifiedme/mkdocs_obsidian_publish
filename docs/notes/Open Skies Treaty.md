---  
title: Open Skies Treaty  
date: 2021-06-11 12:13  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims 
# Open Skies Treaty
**flashcard**{: #flashcard}{: .hash}  

- After Cold War -- between USA and Russia.
	- Allowed spying via skies - imaging.
- USA went out alleging violation on part of Russia.
- 34 states signed. India is **not** a member.