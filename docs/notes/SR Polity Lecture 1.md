---  
---  
# SR Polity Lecture 1
---
Source: Shubhra Ranjan
Lecture Number: 1
Date: 2020-11-27
---

## Introduction to GS-2 (Polity)

- Three parts of Polity
    *   Constitution
        *   Max-questions
    *   Social Justice and Governance
        *   Governance has more questions
    *   International Relations

- Nature of Polity is very dynamic

- Very important than other subjects -- score is not more
    *   Since quality is missing

- Go for Standard information
    *   SC judgments
    *   COmmittees report
    *   aka authentic info
    *   not in a superficial manner although its general studies.

- Important to understand concepts -> basic concepts

- *Political Theory Book for Basic .Questions --> IDEA BASED*
### Constitution

- Polity --> Understanding: Political System --> Base: Constitution
   - though its dynamic

Q: What is the purpose of the constitution?
A: To lay down the framework of the political system.

**Q: Constitution as a power map. Explain.**
A: Constitution is a power mapping defining the powers of different branches of the government.

**Q: Is there a country without a constitution?**
A: No, no country can exist without a constitution. Written/ Unwritten.


> [[Legus Legum]] - Laws of the Laws
- most fundamental law according to which a nation state is governed
- It gave us framework of our polity. 
    - Trinity of the state - Legislature, executive and judiciary
    - Constitution provides norms that given the relationship within the trinity. 
    - Nature of the relationship between state and its citizens. 



### Types of Constitution
1.  Written and Unwritten Constitutions
    1.  Written constitutions are distinguished on the basis of the fact that they are products of the constituent assembly.
    2.  British Constitution = Unwritten --> Has some parts written too.
        *   Unwritten because there was no constituent assembly.
        > *   No constitution is entirely written or unwritten.
        > *   Even Indian constitution has some unwritten parts.
    3.  Indian constitution is written.
2.  Rigid and Flexible Constitutions
    1.  Basis: Amendments.
    2.  British = Flexible --> Amendments with Simple majority. Relatively Easy.
    3.  USA = Rigid --> Amendments with Special Majority. Difficult.
    4.  India = not too rigid, not too flexible. --> Simple majority as well as special majority. Depends on the subject.
---

1. [[Knowledge/Academics/UPSC CSE/Preamble]]
2. [[Constitution as a living document]]
3. [[Significance of the Constitution]]


5.  [[Types of Majority]]
6.  [[Constitutionalism]]
7.  [[Democracy]]
8.  [[Right - Left - Center]]
    1.  [[Liberals]]
9.  [[Liberty and Freedom]]
10.  [[Constitution as a living document]]
11. [[Judicial Supremacy]]
12. [['Procedure Established by Law']]
13. [[Due Process of Law]]

***

Q: Arguments wrt to 'Laws limit freedom'
A: 
> *   Extremes are usually bad.
> *   Men, not Man.
> *  One's freedom ends where other's freedom start.