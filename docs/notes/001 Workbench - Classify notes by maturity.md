---  
cssclasses: dvcounter  
---  
# 001 Workbench - Classify notes by maturity
---

```tracker
searchType: fileMeta, tag
searchTarget: cDate, **status**{: #status}{: .hash}  
/seed,status/draft, status/mature, #status/list 
datasetName: null, Seed, herb, Tree, dynamic
xDataset: 0
pie:
    title: Notes by Maturity
    data: '{{sum(dataset(1))}},{{sum(dataset(2))}}, {{sum(dataset(3))}}, {{sum(dataset(4))}}'
    
    label: Seed,Herb,Tree,Dynamic
    ratioInnerRadius: 0.3
```

- - -
## Seed 🌱 
```dataview
table date AS "DATECREATEDON", Category
from #**status**{: #status}{: .hash}  
/seed AND -"Library" AND -**note**{: #note}{: .hash}  
/upsc/anthropology AND -#note/upsc/GS4 
sort Category, date

```



## Herb 🌿 
```dataview
table date AS "DATECREATEDON", Category
from **status**{: #status}{: .hash}  
/draft AND -"Library" AND -**note**{: #note}{: .hash}  
/upsc/anthropology AND -#note/upsc/GS4 
sort Category, date
```


## Tree 🌲
```dataview
table date AS "DATECREATEDON", Category
from **status**{: #status}{: .hash}  
/mature AND -"Library" AND -**note**{: #note}{: .hash}  
/upsc/anthropology AND -#note/upsc/GS4 
sort Category, date

```