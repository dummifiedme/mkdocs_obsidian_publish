---  
title: International Commission on Large Dams  
source: None  
date: 2021-03-07 22:59  
tags: status/seed  
aliases: ['ICOLD']  
---  
Category::  

---

# International Commission on Large Dams
**flashcard**{: #flashcard}{: .hash}  

- Founded in 1928
- non-intergovernmental.
	- 10000 members from across the nations - from national commissions.
- HQ - Paris, France
- Discuss and present research based on Large Dams.