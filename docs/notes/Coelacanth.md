---  
title: Coelacanth  
source: None  
date: 2021-02-25 15:21  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Science and Tech]]

---

# Coelacanth
**flashcard**{: #flashcard}{: .hash}  

- Giant Fish regarded as an iconic example of a "[[Living Fossil]]".
- Very less changes in the structure of the living fish found and the old fossil.
- Deep sea, elusive creatures - 2300 feet below the surface.
![[Pasted image 20210225152631.png]]