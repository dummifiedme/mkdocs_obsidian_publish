---  
title: Quorum for House  
date: 2021-06-29 21:12  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# Quorum for House
**flashcard**{: #flashcard}{: .hash}  

One-tenth of the total strength of the house.
 -   For any official business, quorum is necessary.
 -   Voting can proceed.