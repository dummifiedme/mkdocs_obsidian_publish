---  
title: National Coal Index  
source: None  
date: 2021-02-13 22:04  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# National Coal Index
**flashcard**{: #flashcard}{: .hash}  

- Released by *Ministry of Coal*
- *Price index* which reflects the change in the price level of coal on a particular month relative to the fixed base year.
- Base Year - FY 2017-18
- Prices from all channel of sales.
- Separate sets of *five sub-indices*
	- three for non-coking coal
	- two for coking coal