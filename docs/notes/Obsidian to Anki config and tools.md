---  
title: Obsidian to Anki config and tools  
source: None  
date: 2020-12-18 07:49  
tags: None  
---  
**note**{: #note}{: .hash}  
/general/knowledge/tech/config 
# Obsidian to Anki config and tools

- For Paragraph style with header to question and tags+ indicator at the end

```

### This is question
This is answer Paragraph
- can have bullets too
- asahdshduahsd **tag1**{: #tag1}{: .hash}  
 **tag2**{: #tag2}{: .hash}  

**flashcard**{: #flashcard}{: .hash}  
(optional)
	**flashcard**{: #flashcard}{: .hash}  

```


	- The regex: `^#{2,}(.+)\n*((?:\n(?:^[^\n#].{0,2}$|^[^\n#].{3}(?<!<!--).*))+\s)#flashcard`
	- 2 or more `#` in question line

- For Paragraph style with header to question and tags+ indicator affter ### line

	```
	This is question
	**tag1**{: #tag1}{: .hash}  
 **tag2**{: #tag2}{: .hash}  

   **flashcard**{: #flashcard}{: .hash}  

	This is answer Paragraph
	- can have bullets too
	- asahdshduahsd 
	```

	- The regex: `((?:[^\n][\n]?)+\s)#flashcard((?:\n(?:^.{1,3}$|^.{4}(?<!<!--).*))*)`
	- irrespective of ### 

### From Github
#### Bullet

content
- Question1
    - Answer

- Question2
    - Answer

Solved! Use the regex below ( Basic card )

```

^\- ((?:.+\n)*?)\n*[\t ]+((\- )?.+(?:\n(?:^.{4}(?<!<!--)(?<!\- ..).*))*)(?:\n^$)?
```


Usage:

```

- Single Line

- Question1
    - Answer
- Question2
    - list root
    - Answer part 1
    - Answer part 2
        - Nested Answer

- Question3
    - Answer

It will stop scan answer when there is a empty line
```


Result  
[[https://user-images.githubusercontent.com/18050469/103931142-c6413200-515a-11eb-9a03-c87a69d26948.png]]

your original md change to

I often use `*` instead of `-` for unorder list, except todo box `- [ ]` (which will be converted to flashcard too)