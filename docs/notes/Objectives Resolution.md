---  
title: Objectives Resolution  
source: None  
date: 2021-05-28 18:58  
tags: status/seed note/upsc/GS2/polity  
aliases: []  
sr-due: 2022-01-22  
sr-interval: 12  
sr-ease: 230  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/modern 

---


# Objectives Resolution

> **Quote**:: The Objectives Resolution was "something more than a resolution. It is a *declaration*, a firm *resolve*, a *pledge*, an *undertaking* and for all of us a *dedication*".<cite>[[Jawahar Lal Nehru]]</cite>

- **Who moved the ‘Objectives Resolution’ in the Constituent Assembly and when?** :: JL Nehru on December 13, 1946. <!--SR:!2022-01-23,13,230-->

- **What was ‘Objectives Resolution’?**
**flashcard**{: #flashcard}{: .hash}  

It was an historic resolution moved by Nehru which laid down the *fundamentals and philosophy* of the constitutional structure. <!--SR:!2022-01-13,9,250-->


## Features of Objectives Resolution
1. *Character of the country* was put forward
        1. Independent
        2. Sovereign
        3. Republic
2. **Territories**: British India + Territories that (then) formed the Indian States + other territories as willing to join.
    1. Boundaries of the states determined by ::  [[Constituent Assembly]] <!--SR:!2022-01-20,10,239-->
        - Basically reorganizing powers
    2. Power and Authority to India, parts of it and its government— *derived from the PEOPLE*.
3. **Guaranteed and Secured** to all people —
        1. *Justice* — Social, Political, Economic
        2. *Equality* — of status of opportunity, before the law.
        3. *Freedom* — thought, expression, belief, faith, worship, vocation, association
4. **Safeguards** for Minorities, backward and tribal areas, depressed and other backward classes.
5. **Maintain integrity** of the territory — land, sea, air — according to Justice and laws of CIVILISED NATIONS
6. Ancient land attains *rightful and honoured place in the world*. And, make full and willing contribution — *world peace and welfare of mankind*.

> Points 4 and 5:  Expressed clearly in the [[Knowledge/Academics/UPSC CSE/Preamble]] [[Knowledge/Academics/UPSC CSE/Preamble|Preamble of the Constitution]]
> Point 6:  Important statement — even while getting our own freedom, we were thinking of world at large.