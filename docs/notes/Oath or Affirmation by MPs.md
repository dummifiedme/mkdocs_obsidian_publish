---  
title: Oath or Affirmation by MPs  
source: None  
date: 2021-03-10 19:02  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Oath or Affirmation by MPs
(x3)
**flashcard**{: #flashcard}{: .hash}  

1. To bear true faith and allegiance to the Constitution of India
	2. To uphold the sovereignty and integrity of India
	3. To faithfully discharge the duty upn which he is about to enter.

## Consequences of not taking an oath as a MP
- Cannot vote
- 500 Rs penalty daily if sits and votes in a house.