---  
title: India gives a line of credit of $100M to procure Indian defence equipment  
source: None  
date: 2021-02-25 05:07  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[India-Maldives]] ,  , [[Keywords GS2]]
---

# India gives a line of credit of $100M to procure Indian defence equipment
**flashcard**{: #flashcard}{: .hash}  

- Maldives wanted only $10M but India insisted.
- Maldives accepted since the interests was only on the amount taken (say $10M) - soft loans.
- Win-Win for both the parties.
	- India - Sale of defence equipments.