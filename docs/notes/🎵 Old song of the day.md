---  
title: 🎵 Old song of the day  
date: 2021-09-29 18:55  
tags: status/seed personal/projects/songs  
aliases: []  
---  
# 🎵 Old song of the day

## 2021

### September
- Jab Deep Jale Aana
- Mehelon ka Raja mila
- Kai baar yun bhi dekha hai
- Meri Jaan, Mujhe Jaan na kaho
- Pal Bhar mein ye kya ho gya?
- 2021-09-29
	- **Song**: Dil ka Bhanwar Kare Pukaar
- 2021-09-30
	- **Song**: O Re Manjhi, Mere Sajan hain uss paar.

### October 
- 2021-10-01
	- **Song**: Ajnabee Kaun Ho Tum
- 2021-10-02
	- **Song**: Sabarmati Ke Sant Tune Kar diya Kamaal
		- Kavi Pradeep
		- Jagriti 1954
			- Best Movie in Filmfare 1956
- 2021-10-03
	- **Song**: Zindagi Zindagi Mere Ghar Aana
- 2021-10-04
	- **Song**: Wo shaam kuch ajeeb thi
- 2021-10-05 (17)
	- **Song**: Do Naina Ek Kahani
- 2021-10-06
	- **Song**: 
- 2021-10-07
	- **Song**: Phir Chidi Raat
- 2021-10-08 (20)
	- **Song**: Kabhi Kuch Pal Jeevan Mein
- 2021-10-09
	- **Song**: Kaun Disa Mein
- 2021-10-10
	- **Song**: Diye Jalte hain 
- 2021-10-11
	- **Song**: Ek pyaar ka Nagma
- 2021-10-12
	- **Song**: Jaane Kya Baat Hai
- 2021-10-13 (25)
	- **Song**: Teri Duniya Se Hoke Majboor Chala
		- When Kishore Kumar died, this song was played across India on Radio.
		- 13th October 1987 was the day!