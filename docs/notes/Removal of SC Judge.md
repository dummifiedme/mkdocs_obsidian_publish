---  
title: Removal of SC Judge  
source: None  
date: 2020-12-22  
aliases: ['removal of SC Judges', 'impeachment of SC judges']  
tags: note/upsc/GS2/polity/judiciary  
---  
# Removal of SC Judge


[[Removal of SC Judge Audio.webm]]
- by an order of President
- only after an address by parliament to the president about the removal
	- [[Special Majority]] of each house of the Parliament

##  GROUNDS of REMOVAL
1. Proven Misbehaviour
2. Proven Incapacity

---

1. **Which act regulates the procedure for removal of judges of the Supreme Court?** **QnA**{: #QnA}{: .hash}  

	- Judges Enquiry Act (1968)  

2. **What process is employed for the removal of judges of SC?** **QnA**{: #QnA}{: .hash}  
 
	- Impeachment Process 

---

## Process of Removal
1. Removal Motion - Signed by 100 members (LS) or 50 members (RS) ==to be given to Speaker/Chairman==
2. Speaker/Chairman ==may accept or refuse== the motion
3. IF ADMITTED
	- a three member committee constituted by speaker chairman to constitute the charges	
	- Committee =>
		-  CJI or a judge of SC 
		- CJ of HC
		- a distinguished jurist.
4. IF FOUND GUILTY 
	- House can take up the consideration of the ==motion==.
5. IF the motion passes with a special majority in each house, an address to the president for removal of the judge.
6. FINALLY, President passes an order.

## Impeachment of a SC judge in history of independent India
- No judge of SC has yet been impeached in India.
- The first case of Impeachment was of Justive V. Ramaswami of SC (1991-1993)
	- Wasn't impeached.
	- Found guilty by the committee
	- Motion failed in Lok Sabha 
		- Congress party abstained from Voting