---  
title: Need for a Crop Diversification Policy  
source: None  
date: 2021-01-18 22:26  
tags: status/seed  
aliases: []  
---  
Category::  [[🗃️ Agriculture]] [[GS3]] 

---

# Need for a Crop Diversification Policy
### Suggestions and respective reasons
- Focus on quality of water and soil.
- Focus on crops other than the Rice and Wheat. Give other crops their due diligence. 
	- MSP are defined for 24 crops but procurement only for Rice and Wheat. Other farmers are stressed, the farming is disadvantageous.
	- Haryana is a water scarce state and still one of the leading producer of water intensive Rice.
	- If there is no subsidy, the cost of Rice would not be viable and export problems will rise. Rice is a competitive crop.
	- The water being used is basically being exported as of now. 🔻
	- Over procurement by FCI -> forced to procure and sell cheaply 😥
		- In a lot of debt.
- Livestock and Fisheries are growing at 3-5 times than the cereals and sugarcane. So, there is a need to focus on them as well.
	- The contribution of these sections are more than that of agriculture to the GDP.
- Improvement in the infrastructure and transports.
- Diversification is also important to the dimension of malnutrition -- 'hidden hunger'.
	- Cash equivalent funds to the farmer than to procure the grains. --> FCI would be de-stressed.

		
### Conclusion 
> What needs to be cared for the most?
- Produce enough food, feed and fiber for its population
- Protect environment while production.
- The losses should be minimized
	- As opposed to the losses while transport and in storage.


### Implementation 
- R&D in agriculture
	- Invest in the research and labs
	- hold them accountable for the funds


---
[[095 OnlyIAS - Daily Editorial Discussions]]