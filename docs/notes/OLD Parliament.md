---  
---  
# OLD Parliament

**status**{: #status}{: .hash}  
/dump
***


- Articles {1:79 to 122::range of articles} in Part {1:V} of the Constitution deal with the organisation, composition, duration, officers, procedures, privileges, powers and so on of the Parliament.


- wrt to Parliament form of government, constitution makers took up British Model over American Model.
    *   American Model: President not a constituent part of congress
    *   British Model: ""*Crown - in - Parliament*"" approach.

***

### Three Parts of Paliament

1.  {President}
2.  {Council of States -> Rajya Sabha}
3.  {House of People -> Lok Sabha}



***

### Parliament and President


- President is { not a member of any :: membership} house.




- Doesn't sit in any of the house or attend any of its sessions.

- Integral part of the Parliament
    *   Since, {no bill can pass without his assent}.

- Performs certain functions (x4)
    *   {3: summons and pro-rogues} both the Houses
    *   {3: Dissolves the Lok Sabha}
    *   {3:Addresses both the houses}
    *   {3: issues ordinances when Parliament not in session }.



***

### Composition of Two Houses
[[Knowledge/Academics/UPSC CSE/Composition of Rajya Sabha]]a]]
[[Knowledge/Academics/UPSC CSE/Composition of Lok Sabha]]a]]

---

### System of Elections to Lok Sabha

- [[Territorial Constituencies]]


- [[Readjustment to Territorial Constituency]]


- [[Reservation of Seats for SCs and STs in LS]]


- **System for Elections: **
    *   Rajya Sabha
        *   {1:Proportional representation}
    *   Lok Sabha
        -  = {1:System of territorial representation}
        -  = {1:[[Inbox/UPSC/First-Past-The-Post System]]}



---

### Duration of Two Houses
#### Duration of Rajya Sabha

- Rajya Sabha was first constituted in {1952}

- Continuing chamber --> not subjected to dissolution
- One third of its member retire every third year
	-	Retiring members are eligible for re-election and re-nomination.
- Constitution has not fixed the term for RS
	- Left it to parliament to make laws in this matter
	- Parliament , hence, enacted the Representation of the People Act (1951)
		- Provided that the term of RS shall be of 6 years.
		- First batch to be decided by Lottery on who should retire --> President empowered to curtail their tenure.

#### Duration of Lok Sabha
- Not a continuing chamber
- Term of 5 years from the date of its first meeting after general elections
- President is authorised to dissolve LS any time even before the completion of five years
	- CANNOT be challenged in the court of LAW.
- Term of LS can be extended in EMERGENCY
	- Cannot continue beyond 6 MONTHS after the emergency is lifted.

---
### Membership of Parliament

#### QUALIFICATIONS for MP 
**As per the constitution:**
1. {Citizen of India}
2. {Make and subscribe to OATH or AFFIRMATION before a person authorized by ELECTION COMMISSION}
	> OATH
		> - {To bear true faith and allegiance to the CONSTITUTION of India}.
		> - {To uphold SOVEREIGNITY and INTEGRITY of India}.
3. {1| AGE Conditions:}
	1. {1| Not less than 30 years for RAJYA SABHA}. 
	2. {1| Not less than 25 years for LOK SABHA}.
4. {Other qualifications as prescribed by the Parliament}.

#### Additional Qualifications for MP (RPA 1951)
1. registered as elector for a parliamentary constituency
	1. For both RS, LS.
	2. Same state rules for RS was quashed in 2003. 
2. SC and ST seats to be contested by only SCs and STs. 
	1. Though, SC/STs can contest non-SC/ST seats as well.

#### Disqualifications for MP as per Constitution
{These **five** conditions:
1. Office of Profit under Union or State
2. Unsound mind and stands so declared by a court.
3. undischarged insolvent
4. not a citizen of India or voluntarily acquired the citizenship/allegiance of a foreign state.
5. disqualified under any law by Parliament. }

#### Disqualification as per RPA 1951 
{
1. Not guilty of certain election frauds
2. Not convicted for offence (of more than two or more years)
	1. Preventive detention isn't included.
3. Must have lodged election expenses within time.
4. No interest in govt contracts, works or services
5. not a director or manager or any other Office of Profit in which govt has atleast 25% share.
6. not dismissed for corruption 
7. not convicted for promoting enmity
8. not punished for preaching/practising social crimes such as untouchability, dowry and sati.
}

#### Disqualification on the grounds of defection
---