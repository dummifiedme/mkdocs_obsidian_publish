---  
title: Dead Sea Scroll  
source: None  
date: 2021-03-18 22:55  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Dead Sea Scroll
- Related to [[Bar Kochba Revolt]]
- Jewish and Hebrew religious literature.
- Recently found near Dead Sea, stashed in a cave.
	- Cave - Qumran Caves - "Cave of Horror"