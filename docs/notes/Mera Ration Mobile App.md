---  
title: Mera Ration Mobile App  
source: None  
date: 2021-03-20 01:10  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Mera Ration Mobile App
**flashcard**{: #flashcard}{: .hash}  

- Facilitated by Ministry of Consumer Affairs.
- Developed by Department of Food & Public Distribution and National Informatics Centre (NIC)
- To help identify the nearest Fair Price Shops.