---  
title: Helina and Dhruvastra  
source: None  
date: 2021-02-25 05:21  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[Missiles]] , 
---

# Helina and Dhruvastra
**flashcard**{: #flashcard}{: .hash}  

- Anti-tank guided missile systems
- [[Nag Missile]] when launched by Helicopters -> HeliNa
	- IAF variant  of 'Helina' - Dhruvastra.