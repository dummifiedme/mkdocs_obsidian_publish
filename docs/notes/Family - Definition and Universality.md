---  
title: Definition and Universality  
date: 2020-12-19 01:57  
---  
# Family - Definition and Universality

## Definitional Dilemma 
- Research by **Jan Trost (1990)** -> difficulty and diversity with which people identify those who should or shouldn't be labelled as family.
    - For some in her sample, family consisted if only closest family members (nuclear family)
    - While for others, it extended to other kins, friends and even pets.
- Defining the family does not end with the determining of family membership, family definitions are also linked to ideological differences.

- **Examples of definition of family not being limited to structure but also oriented to both ideology and process. (Researcher?)** 
	-   **Researcher**: John Scanzoni and colleagues, 1980s.
		- Tried expanding the traditional definition of the family (= two parents + child/children)
			> All other family forms or sequencing tend to be labeled  as deviant(as in research on minorities) or as alternatives (when occurring among whites)
		- Shows bias based on one's own conditioning while defining the family.
	-   **Researcher**: Kathleen Gough
		-   Defines the ideology and process
		-   States:
			> Our assumptions, values, feelings and histories shape the scholarship we propose, the findings we generate and the conclusions we draw. Our insights about family processes and structures are affected by our membership in particular families, by the lives of those we study, and by what we care about knowing and explaining.
		- Basically, the scholars personal bias kicks in while defining or understanding any concept (in this case, the family, it's structure and its definition).

Q: **What are the factors on which a definition of family might depend? **
	1.  Theory
	2.  History
	3.  Culture
	4.  Situation

## Definition

Q:**What is a universal definition?** (General)
    [[Universal Definition]]

Q:**Is it possible to arrive at a universal definition *for family*?**
- Most of the scholars argue it's either not possible ( Settles 1987 ) or only possible to discuss in relation to categories of definitions (Trost 1990)
- Most experts (Fine 1993) in the field have concluded that — there is no single correct definition of what a family is.

Q: **What are the importance of *variety* in family forms?**
 Different family patterns are:
1.  Interesting products of human inventiveness
2.  Solutions to different sorts of problems which people must cope with.

Q: **What is the definition of Family and by whom?** (which encompasses most of the societies)
Definition:: *William Newton Stephens* — The family in general is a group based on marriage and marriage contract, including the recognition of rights and duties of parenthood, common residence for husband, wife and children and reciprocal economic obligations between husband and the wife. ![[Definition of Family - William Newton Stephens.png]]