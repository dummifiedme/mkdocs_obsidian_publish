---  
---  
# Democracy
---
Date: 2020-12-02 16:48
Title: Democracy
Parent: [[SR Polity Lecture 1]]
---

## Democracy

> Democracy is a modern concept
>
> *   Earlier, there was a theory of 'Divine Rights of King' -- transformed into --> Natural rights of man.
> *   Now, King is public servant and sovereignty lies with the people.


- Above concepts are based on the liberal idea of democracy
    *   India is a liberal democracy.
        *   Not a socialist, but welfare state.
	 > FR makes India Liberal, while DPSP makes it Socialist

> India is a socialist country, but in theoretical terms it isn't a socialist per say, rather liberal.