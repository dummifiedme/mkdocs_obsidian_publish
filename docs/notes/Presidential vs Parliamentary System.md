---  
Source: Subhra Ranjan  
Lecture: 2  
Date: 2020-11-27  
---  
# Presidential vs Parliamentary System
> Based on the **relationship between executive and legislature.**


|                     Presidential System                     |                     Parliamentary System                     |
|:-----------------------------------------------------------:|:------------------------------------------------------------:|
| **Separation of powers** between legislation and executives | **Fusion of power** i.e. executive is a part of legislation. |
|                 Fixed term -aka- STABILITY                  |                     relatively unstable                      |
| Single executive <- head of state  +    head of government  |         Plural executive: Nominal (Prez) + Real (PM)         |
|                       Not responsible                       |                         Responsible                          |
|                                                             |                       nature of executive = plural executive, because of cabinet.                                       |