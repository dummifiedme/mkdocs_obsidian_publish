---  
title: Tree Cities of the World  
source: None  
date: 2021-02-21 11:31  
tags: status/mature  
aliases: []  
---  
Category:: [[🗃️ Environment]] 

---

# Tree Cities of the World
**flashcard**{: #flashcard}{: .hash}  

- Arbor Day Foundation and the [[Food and Agriculture Organisation (FAO)]]
- Five Core Standards listed as 1,2,3,4 and 5.
- Hyderabad won; only city in India to have won.
	- For commitment to growing an maintaining urban forestry - Harita Haram programme.
- USA > Canada > UK. <!--SR:!2021-09-24,3,250-->