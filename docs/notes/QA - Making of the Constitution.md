---  
---  
# QA - Making of the Constitution
> From [[📕 Laxmikant - Indian Polity]]

## Demand for Constituent Assembly

- Who put forward the idea of Constituent Assembly for India? :::
    MN Roy in 1934

- When did INC officially demand the Constituent Assembly? ::: 
    1935

- What did JL Nehru declare in 1938 regarding the framing of Constitution? (on behalf of INC) ::: 
    > ' The Constitution of free India must be frame, without outside interference, by a Constituent Assembly elected on the basis of adult franchise.' - Nehru (1938)

- When was the demand for Constituent Assembly finally accepted by the British? ::: 
    In 'August Offer' of 1940.

- What was Cripps mission? ::: 
    In 1942, Sir Stafford Cripps came to India, with draft proposal of Indian Constitution.
    - Rejected by Muslim League — demanded two autonomous states with two separate Constituent Assemblies.

- What was Cabinet mission? ::: 
    - After the failure and rejection of Cripps Mission, another mission was sent to form the Constituent Assembly. 
    - Cabinet Mission rejected the idea of 2 Assemblies (Muslim League demands on Cripps mission) , but offered some form of scheme which satisfied Muslim League. 

## Composition of Constituent Assembly

- When was the Constituent Assembly constituted? :::
    November 1946
    Under the scheme formulated by Cabinet Mission Plan. 

- Was there any communal representation in Constituent Assembly? ::: 
    Yes.
    Seats allocated in British provinces had three divisions in proportions to their populations:
    1. Muslims
    2. Sikhs
    3. General (other than Muslims and Sikhs)

- What were the voting methods employed for electing the Constituent Assembly? ::: 
    1. Representatives of each communities → elected by members of that community only.
    2. By the method of proportional representation by means of single transferable vote. 

>  Constituent Assembly was to be a partly  elected  and partly  nominated  body. Members were to be  indirectly  elected by members of  Provincial Legislative Assemblies  → themselves elected on  limited  franchise.


- What was the distribution(breakup) of seats in the Constituent Assembly scheme by Cabinet Mission? ::: 
	- ![[3 Resources/Pasted image 20201205190626.png]]

## Working of Constituent Assembly

- The Constituent Assembly held its first meeting on  December 9, 1946 . 

- The  Muslim League  boycotted the meeting (constituent assembly) and insisted on a  separate state of Pakistan . The meeting was, thus, attended by only 211 members.

- {Dr. Sachchidananda Sinha} , the oldest member, was elected as the temporary President of the Constituent Assembly, following the  French  practice.

- Who was the first elected president of the Constituent Assembly? ::: 
    Dr. Rajendra Prasad.

- How many vice-presidents did Constituent Assembly have? Name them. ::: 



 
    Two.
    1. HC Mukherjee 
    2. VT Krishnamachari

- Did representatives of the princely states join Constituent Assembly? ::: 
   	-  Initially stayed away, but later joined before Independence Act.
   	-  6 representatives were present on April 28, 1947.
   
- Did representatives of the Muslim League join Constituent Assembly? ::: 

    Members of Muslim league from  the Indian Dominion joined the Assembly.

### Objectives Resolution

- Who moved the ‘Objectives Resolution’ in the Constituent Assembly and when? ::: 
    JL Nehru on December 13, 1946.

- What was ‘Objectives Resolution’? ::: 
    It was an historic resolution moved by Nehru which laid down the fundamentals and philosophy of the constitutional structure.

- List some important features of Objectives Resolution. **later**{: #later}{: .hash}  

    1. Character of the country was put forward
        1. Independent
        2. Sovereign
        3. Republic
    2. Territories: British India + Territories that (then) formed the Indian States + other territories as willing to join.
    3. Boundaries of the states — determined by → Constituent Assembly
        - Basically reorganizing powers
    4. Power and Authority to India, parts of it and its government— derived from  → the PEOPLE.
    5. Guaranteed and Secured to all people —
        1. Justice — Social, Political, Economic, Political
        2. Equality — of status of opportunity, before the law.
        3. Freedom — thought, expression, belief, faith, worship, vocation, association
    6. Safeguards for Minorities, backward and tribal areas, depressed and other backward classes.
    7. Maintain integrity of the territory — land, sea, air — according to Justice and laws of CIVILISED NATIONS
    8. Ancient land attains rightful and honoured place in the world. And, make full and willing contribution — world peace and welfare of mankind.

     Points 4 and 5:  Expressed clearly in the [[NotesVault/1 Projects/UPSC/GS2/Preamble]]
     Point 8:  Important statement — even while getting own freedom, we were thinking of world at large.

- When was the 'Objectives Resolution' adopted in Constituent Assembly? ::: 
    January 22, 1947.

### Changes by the Independence Act

- When was Mountbatten Plan accepted? ::: 
    June 3, 1947.
    aka Partition of India.

- What were the three changes to Constituent Assembly by Independence Act of 1947?  :::
    1. Assembly = Fully sovereign body =  ENACT ORDINARY LAWS
        - Can alter or abrogate any British Law in relation to India.
    2. Assembly = legislative body. = FRAME NEW CONSTITUTION
    3. Muslim League members (from Pakistan areas) withdrew from the assembly.


- What were the two main roles of Constituent Assembly after the Independence Act of 1947?  ::: 
    1. Legislative Role — Frame new constitution.
    2. Executive Role — Enact ordinary laws.

- What were the changes in number of seats Constituent Assembly after Independence Act of 1947? 
    - Number of seats : 389 → 299.
        1.  Indian Provinces:  296 → 229.
        2.  Princely States:  93 → 73.

- What were the other functions of Constituent Assembly?  (Other than executive legislative function) ::: 
    1. It ratified the India’s membership of the Commonwealth in May 1949.
    2. It adopted the national flag on July 22, 1947.
    3. It adopted the national anthem on January 24, 1950.
    4. It adopted the national song on January 24, 1950.
    5. It elected Dr. Rajendra Prasad as the first President of India on January 24, 1950.

- When was India ratified as a member into Commonwealth? 
    May 1949.

- When was national flag adopted? ::: 
    July 22, 1947.

- When was the national anthem adopted? ::: 
    January 24, 1950.

- When was the national song adopted? ::: 
    January 24,1950.

- When was the first President of India elected? ::: 
    January 24, 1950

- How many session did Constituent Assembly have and for how long? 
    - 11 sessions.
    - Two Years, 11 Months, 18 Days.


- When did Constituent Assembly have its last session? 
    January 24, 1950.
    But it did not end, and continued as the provisional parliament of India from January 26, 1950 till formation of new Parliament after the first general election in 1951-52.