---  
title: Arka Shubha  
source: None  
date: 2021-02-12 20:31  
tags: status/mature life/short  
aliases: []  
---  
Category:: 

---

# Arka Shubha
**flashcard**{: #flashcard}{: .hash}  

- A new variety of Marigold (Gende ka Phool)
- Indian Institute of Horticulture IIHR
- Significance:
	- Carotene can be extracted even if spoiled
		- Used in pharmaceutical sector
		- Currently imported from China. <!--SR:!2021-09-24,3,250-->