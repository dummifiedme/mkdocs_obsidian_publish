---  
title: Determinants of Population  
source: None  
date: 2021-05-30 11:52  
aliases: []  
tags: status/seed Environment Science  
---  
Category:: 

---

# Determinants of Population
**flashcard**{: #flashcard}{: .hash}  

- Natality Rate
- Mortality Rate
- Biotic potential of specie
- Fertility Rate