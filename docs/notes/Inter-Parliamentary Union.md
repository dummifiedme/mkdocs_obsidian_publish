---  
title: Inter-Parliamentary Union  
source: None  
date: 2021-03-18 22:35  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Inter-Parliamentary Union
**flashcard**{: #flashcard}{: .hash}  

- Founded in 1889
- HQ - Geneva, Switzerland
- Aim - Democracy for everyone
- Members - 179 national parliaments + 13 regional parliamentary assemblies
	- India is a member.	
- Works closely with UN and its organisations.