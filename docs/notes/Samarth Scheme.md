---  
title: Samarth Scheme  
source: None  
date: 2021-02-25 16:40  
tags: status/seed  
aliases: []  
---  
Category:: [[+ Government Schemes]]

---

# Samarth Scheme
**flashcard**{: #flashcard}{: .hash}  

- demand driven, placement oriented  and National Skills Qualification Framework compliant.
- Supplement efforts of the industry in creating jobs in the *organised textile and elated sectors.*
	- *Excludes Spinning and Weaving*
- Implemented through industry, government institutions and reputed training institutions/NGOs/Societies in textile sector.
- Monetary component present as well.