---  
title: PM Kisan Sampada Yojana  
source: None  
date: 2021-05-08 19:55  
tags: status/seed  
aliases: []  
---  
Category::  

---

# PM Kisan Sampada Yojana
**flashcard**{: #flashcard}{: .hash}  

- 27 projects under development of integrated cold storage chains and value addition infra in India.
- Central Sector Scheme
- Objectives
	- Create modern infra
	- create effective [[Backwards and Forward Linkages]]
	- create supply chain infra for perishables