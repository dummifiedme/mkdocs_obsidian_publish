---  
title: Abiotic Factor - Light  
source: None  
date: 2021-04-17 07:16  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Abiotic Factor - Light
- important for living organisms - especially autotrophs.
- spectral quality of solar radiation is also important
- intensity and duration is important as well - photoperiod