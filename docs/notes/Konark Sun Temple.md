---  
title: Konark Sun Temple  
source: None  
date: 2021-03-17 22:13  
tags: status/seed  
aliases: []  
---  
Category:: [[GS1-01 Art and Culture, Ancient and Medieval History]] 

---

# Konark Sun Temple
**flashcard**{: #flashcard}{: .hash}  

- Ganga Dynasty - Narsimhadeva I
- Sun Chariot - Pulled by Seven Horses
- Also known as Black Pagoda - in Odisha
- World Heritage Site
- Chandrabhaga Mela - Hindu Festival - mid-February.