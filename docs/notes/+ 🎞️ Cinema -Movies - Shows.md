---  
title: 🎞 Cinema -Movies - Shows  
date: 2021-08-28 09:24  
tags: status/list  
aliases: []  
cssclass: cards  
---  
**entertainment**{: #entertainment}{: .hash}  
 
# + 🎞️ Cinema -Movies - Shows
## Movies
### Hindi Movies
- [[🎞️ Musafir (1957)]] 2021-08
- [[🎞️ Nartaki]] 2021-08-27
- [[🎞️ Oonche Log]] 2021-08-27
- [[🎞️ Pati Patni]] 2021-08-28
- [[🎞️ Didi]] 2021-08-27
- Anuradha (1960)
- [[🎞️ Maa ka Aanchal]] 2021-08-28
- Nau Bahar
	- > Aankh hai to jahan hai. — girl’s father.
- [[🎞️ Naya Kanoon (1959)]] 2021-08-30
- [[🎞️ Biraj Bahu (1954)]] 2021-08-30
- Parakh (1960) 2021-08-31
- Naukari (1954) 2021-08-31
- Insaan Jaag Utha (1959) 2021-08-31
- Suhagan (1964) 2021-08-31
- [[🎞️ Sujata (1960)]] 2021-09-02
- [[🎞️ Rajnigandha (1974)]] 2021-09-21
- [[🎞️ Yeh Kaisa Insaaf (1980)]] 2021-10-28
- [[🎞️ Aarop (1974)]] 2021-10-29
- [[🎞️ Sampark (1979)]] 2021-10-30
- Aap Aise to Nahi The (1980s) 2021-10-31
- Zindagi 2021-11-02
- [[🎞️ Arzoo]] 2021-11-02
- [[🎞️ Ek Duje Ke Liye]] 2021-11-03
- [[🎞️ Sara Aakash]] 2021-11-03
- [[🎞️ Apna Haath Jagannath]]
- [[🎞️ Ek Gaon ki Kahani (1957)]]
-  [[🎞️ Safed Jhooth (1977)]]
- [[🎞️ Rakhi (1962)nnnn]]
- [[🎞️ Deep Jalta Rahe (1959)]]
- [[🎞️ Laga Chunari Mein Daag]] 2021-12-17
- [[🎞️ 420 IPC]] 2021-12-18
- [[🎞️ Bunty aur Babli 2]] 2021-12-18
- [[🎞️ Dont Look Up (2021)]]
- [[🎞️ Arrival (2016)]]
- [[🎞️ Sing 2 (2021)]]

## Shows
- Bosch 2021-06-20 -> 2021-06-30
- [[🎞️ Money Heist]] 2021-09-13 -> 2021-09-16
- [[🎞️ Scrubs]] 2021-09-17 -> 2021-09-
- [[🎞️ Time]] 2021-11-28
- [[🎞️ Only Murders in the Building]] 2021-11-29
- [[🎞️ Arya]] 2021-12-12
- [[🎞️ Aranyak]] 2021-12-14
- [[🎞️ Special Ops 1.5]] 2021-12-13
- [[🎞️ Quarry]] 2021-12-16
- [[🎞️ Sneaky Pete]] 2021-12-19
- [[🎞️ Condor]] 2021-12-18
- [[🎞️ McMafia]] 2021-12-17
- [[🎞️ Escape at Dannemora]] 2021-12-19
- [[🎞️ The Wheel of Time]] 2021-12-27
- [[🎞️ The Witcher]]
- [[🎞️ Tehran]] 2021-12-20
- [[🎞️ The Spy (2019)]] 2021-12-20
- [[🎞️ Malcolm in the MIddle]] 2021-12-21 → 2021-12-31
- [[🎞️ The Last Hour]] 2022-01-02


## Telefilm
- [[🎞️ Uphaar]] 2021-10-28

- - -

## Table - Movies

```dataview
table without id ("![50]("+ cover + ")") as Cover, file.link as Film, "by " + Director as Director, Genre, Watched, imdb-rating
from **entertainment**{: #entertainment}{: .hash}  
/movie 
where contains(file.name, "🎞️")
sort watched
```


## Table - Shows
```dataview
table without id ("![|50]("+ cover + ")") as Cover, file.link as Show, Watched, Genre, Rating
from **entertainment**{: #entertainment}{: .hash}  
/show 
where contains(file.name, "🎞️")
sort Rating
```