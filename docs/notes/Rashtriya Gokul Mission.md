---  
title: Rashtriya Gokul Mission  
source: None  
date: 2021-05-08 20:51  
tags: status/seed note/upsc/GS3/economics  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/agriculture
# Rashtriya Gokul Mission
**flashcard**{: #flashcard}{: .hash}  

- Conservation and development of indigenous breeds - artificial and natural inseminition.
- Department of Animal Husbandry and Dairying, [[Ministry of Fisheries, Animal Husbandry and Dairying]] 
- Under National Programme for Bovine Breeding and Dairy Development. <!--SR:!2021-08-19,3,250-->