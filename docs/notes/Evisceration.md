---  
title: Evisceration  
source: None  
date: 2021-06-22 13:01  
aliases: []  
---  
##### Evisceration
**flashcard**{: #flashcard}{: .hash}  
/R
lit. Removal of viscera and leaving the shell as is. (in eyes or organs)
eng. Clear out something and just leave a structural shell there.