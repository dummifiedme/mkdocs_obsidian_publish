---  
title: INS Viraat  
source: None  
date: 2021-02-12 21:17  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR/defence/navy
# INS Viraat
**flashcard**{: #flashcard}{: .hash}  

- Decommissioned **aircraft carrier**
- Originally commissioned by British Navy as HMS Hermes in 1959
	- [[Falkland Islands]] war in 1982
- India bought in 1986
- Operation Jupiter in 1989
- Guiness Book of World Record to serve for the longest time. <!--SR:!2021-11-10,1,230-->

**What is the motto of the INS Viraat? :::** 
Sanskrit Phrase - "Jalamev Yashya, Balamev Tasya" which means 'who controls the sea is the powerful'