---  
title: Gregarious Bamboo Flowering  
source: None  
date: 2021-03-20 00:43  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Gregarious Bamboo Flowering
**flashcard**{: #flashcard}{: .hash}  

In [[Wayanad Wildlife Sanctuary]].
- Is a treat to Nilgiri Biosphere Reserve.
- It flowers once in a lifetime.
- Impact of flowering
	- adversely affect migration espl by Elephants, wild gaurs.
	- worsen the man-animal conflicts.
	- threat to wildlife.