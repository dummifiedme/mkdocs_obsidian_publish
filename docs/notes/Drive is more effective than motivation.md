---  
---  
**Source**: YT video by James Scholz , [[$YT -The power of believing that you can improve  Carol Dweck - YouTube]]

There is a difference between Drive and Motivation. Drive doesn't change with situation. Motivation is volatile. With challenges, motivation wean away. We need to **focus on** DRIVE.