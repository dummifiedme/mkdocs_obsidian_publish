---  
title: Impact of COVID Pandemic on Women  
source: The Hindu  
date: 2021-05-20 22:36  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/social-issues
# Impact of COVID Pandemic on Women
> It is getting from bad to worse for women workers

 ## A widening gap

- Even prior to 2020, the gender employment gap was large. 
	- Only 18% of working-age women were employed as compared to 75% of men. 
	- Reasons include a lack of good jobs, restrictive social norms, and the burden of household work. 

> ‘State of Working India 2021: One Year of Covid-19’  shows that the pandemic has worsened the situation.

- The nationwide lockdown hit women much harder than men. Data from the Centre for Monitoring Indian Economy Pvt. Ltd. show that 61% of male workers were unaffected during the lockdown while only 19% of women experienced this kind of security. 
- Even by the end of the year, 47% of employed women who had lost jobs during the lockdown, had not returned to work. The equivalent number for men was only 7%.


- Poorer options at entry too.
- Disproportionate exit from the jobs too.
- Men were able to regain their jobs more easily.

## Growing domestic work
- Schools closed; children at home = increased care.
- Along with Paid Work, the number of hours go up.
- Generally without relief.

## The course to take
- MNREGA expansion targeted to women employment guarantee
- coordinated efforts of the states - facilitate employment
	- Aanganwadi Centers
	- Opening of schools
	- Self-Help Groups - PPE production
- adequate public investment in Social Infrastructure - National Employment Policy.


## Conclusion
The time is right to imagine a bold universal basic services programme that not only fills existing vacancies in the social sector but also expands public investments in health, education, child and elderly care, and so on, to be prepared for future shocks. This can help bring women into the workforce not only by directly creating employment for them but also by alleviating some of their domestic work burdens, while also overcoming nutritional and educational deficits that we are likely to be confronted with as we emerge from this crisis.

---
Sources:
- [[https://epaper.thehindu.com/Home/ShareArticle?OrgId=GOK8J8SCH.1&imageview=0]]