---  
title: BRICS  
source: None  
date: 2021-03-07 22:52  
tags: status/seed  
aliases: []  
---  
Category::  

---

# BRICS
**flashcard**{: #flashcard}{: .hash}  

- Acronym for an association of five major emerging national economies.
- Started in 2009 in Russia. 
	- South Africa joined in 2010.
- Chairmanship rotates annually among the members.

## Significance of BRICS
**facts**{: #facts}{: .hash}  

**flashcard**{: #flashcard}{: .hash}  

- 42% of world's population
- 23% of global GPD
- 17% of world trade.