---  
title: Montreux Record  
date: 2021-05-28 17:57  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Montreux Record
**flashcard**{: #flashcard}{: .hash}  

List of Ramsar Sites of International Importance that are undergoing changes or likely to change.
- Maintained as part of [[Ramsar Convention]]
- A total of 48 sites listed
	- 2 are from India
		- Keoladeo National Park
		- [[Loktak Lake]]
		- [[Chilika Lake]] - added in 1993, removed in 2002.

> The Montreux Record was established on recommendations by the ==**Conference of the Contracting Parties (1990)==.**