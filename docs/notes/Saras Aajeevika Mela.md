---  
title: Saras Aajeevika Mela  
source: None  
date: 2021-03-11 22:00  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Saras Aajeevika Mela
**flashcard**{: #flashcard}{: .hash}  

- transform rural India in general and the lives of women especially.
- Initiative by [[Deendayal Antyodaya Yojana-National Rural Livelihoods Mission]] (DAY-NRLM), Ministry of Rural Development (MoRD)