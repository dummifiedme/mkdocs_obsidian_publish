---  
title: Ramsar Sites - Gujrat  
date: 2021-06-11 10:17  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - Gujrat
**flashcard**{: #flashcard}{: .hash}  

- Nalsarovar Bird Sanctuary 
- [[Thol Lake]]
- Wadhawana