---  
title: Shahtoot Dam  
source: None  
date: 2021-02-12 21:01  
tags: status/seed  
aliases: []  
---  
Category:: [[India-Agranistan]]

---

# Shahtoot Dam
**flashcard**{: #flashcard}{: .hash}  

- Lalandar Shatoot Dam in Afghanistan
- For Kabul province
- Second major dam in collaboration with India
	- First one - India-Afghanistan Friendship Dam (Salma Dam) 
		- Inaugurated in June 2016