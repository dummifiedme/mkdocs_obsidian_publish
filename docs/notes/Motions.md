---  
title: Motions  
source: None  
date: 2021-03-17 17:30  
tags: status/seed  
aliases: []  
---  
Category:: [[Parliament]] 

---

# Motions
[[No-Confidence Motion]]
[[Adjournment Motion]]
[[Motion of Thanks]]