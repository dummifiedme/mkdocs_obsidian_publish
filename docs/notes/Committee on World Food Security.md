---  
title: Committee on World Food Security  
source: None  
date: 2021-02-25 16:46  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS3]]

---

# Committee on World Food Security
**flashcard**{: #flashcard}{: .hash}  

- Established in 1974 as intergovernmental body
- Permanent secretariat - Italy, Rome - same as FAO HQ.
- Eradicated hunger and malnutrition, food security.