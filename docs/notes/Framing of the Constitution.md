---  
---  
# Framing of the Constitution
## Framing of the Constitution

---

|             Source             |           Tags           | Stage |
|:------------------------------:|:------------------------:|:------ :|
| [[../MPuri GS2 Notes]] | **note**{: #note}{: .hash}  
/upsc/GS2/polity **mPuri**{: #mPuri}{: .hash}  
 **2020**{: #2020}{: .hash}  
-12-05 | #📝/1  |             

---
Q&A from Laxmikant - [[QA - Making of the Constitution]]

> "Constitutional Autochthony^[greek; from the people of the land]" 
> - A phrase by K.C. Wheare
> - A desire for constitution that has arisen form the land itself. 

- The demand for constitutional assembly was regularly voiced post 1929, the year of Poorna Swaraj. 
- The demand was also highlighted in the congress party's manifesto on the eve of 1937 elections.
- The constitutional assembly was created under Cabinet Mission Plan 1946.

## Background
1. Gandhi promised that swaraj will. Not be a free gift of British. The Constitution will have to be framed by the Indians. 
2. Nehru Report was the first attempt by the Indians to frame the Constitution of future India
   - it included the scheme of FR, Social and economic rights, special rights for minorities, parliamentary form of government and dominion status. 
3. In 1934, congress working committee passed the resolution that Constitution will be framed by the elected Constituent Assembly. 
4. British accepted the demand in principle in August Offer 1942.
5. In 1942, the first formal acceptance of the right of Indians to frame the Constitution was mentioned by Cripps Mission. 
6. India's Constituent Assembly was ultimately based on the Cabinet Mission Plan 1946
   - It was to be indirectly elected. 
   - Elected by the members of state legislative Assemblies on the basis of proportional representation by single transferable vote system. 
   - it is to be noted that the members of Provincial Assemblies were not elected on the mandate of the formation of the Constitution. 
   - Assemblies also had nominated members from the princely states. 
7. The assembly was ultimately constituted in November 1946
8. First meeting took place on 9th December 1946
9. Dr. Rajendra Prasad was elected president on 11th December 1946
10. Pandit Nehru moved the Objectives Resolution on 13th December 1946, which was accepted on 22nd January 1947.
   - The resolution was basis of the Preamble.
11. The constituon was adopted on 26th November 1949. 
   - It is also the date of enactment. 
     - Note: Enactment is when from a bill it became law. The date of enactment and commencement of the Constitution is same.
12. Article 394 - Commencement of the Constitution 
   - It mentions some of the articles that came into force immediately but the entire Constitution was commenced since 26th January 1950.

### Representativeness of the Indian Constitution. 
- Constitutional Representation
   - Non Elected members
      - indirectly elected 
      - nominated
      - co-opted members --> Ambedkar... Lost elections, but kept in the assembly. 
   - Elected members
- > Churchill called it an assembly of Brahmans and one party assembly. 

### Defending the Constituent Assembly w.r.t representation
- It is to be noted that the situations in which India got independence were not conducive for direct elections. There was no point in postponing the formation of the assembly. It is true that assembly was dominated by Congress, but it is also true that 'Congress was India and India was Congress'. 
- To ensure that the Constitution remains a consensus document, Congress had co-opted the members of the Republican Party , Hindu Mahasabha etc. Every provision was adopted after deliberation and most of the provisions were adopted by consensus. In 1952, when first general election took place, most of the members of the Constituent Assembly got reelected. 
- Supreme court in Kesavanand Bharti Case has held that there is no relevance of debate on the factual correctness of the term 'We the People of India' mentioned in the Preamble. The matter is already settled that the constituent represented the will of the people. 
- The biggest testimony of the representativeness of the Constitution is the survival of India as a nation and Granville Austin rightly calls Indian Constitution as 'the cornerstone of a nation'. According to Pratap Bhanu Mehta, Indian Constitution is not an ordinary Constitution but a revolutionary document through which an attempt has been made to transform a highly traditional society into a modern vibrant nation, the so called silent revolution.

### What was the method adopted by the Constituent Assembly for acceptance of the provisions of the Constitution? 
1. Majority of the provisions have been adopted by consensus, though for some provisions there has been voting. 
2. Provisions were adopted after exhaustive debates. 

### Constituent Assembly Committees
[[Constituent Assembly Committees]]

### Critique of Constituent Assembly and Constitution
[[Knowledge/Academics/UPSC CSE/Critique of Constituent Assembly and Constitution]]

### Is Indian Constitution a bag of borrowings? 
- It is said that Indian Constitution is a bag of borrowing for the reasons that we have taken various provisions from different Constitution of the world. The biggest influence is of the Constitution of Britain, the parliamentary form of government. Some features were taken from the USA - the Republic nature of India, the fundamental rights. The federal nature of Indian Polity is taken from Canada besides the Government of India Act 1935. The concurrent list and finance commission from Australia. Fundamental duties from Russia. 
- Though Indian Constitution has borrowed from multiple sources, each provision was adopted after thorough discussions and with consensus in most of the situations. 
- There is nothing wrong in borrowing wisdom from the others of we think that it will serve the will of the people. Indian culture has always been accommodative, pluralistic and receptive. We can refer to Gandhi who is known as father of nation that despite rooted in Indian tradition and culture, Gandhi held that one should keep the windows of house open and door closed. Let the winds around the world come to the house but it should not blow off my feet. **todo**{: #todo}{: .hash}  
/digDeeper **quote**{: #quote}{: .hash}  



---

### Mains Based Questions
Q: To what extent it is appropriate to suggest that the present Constitution represent the aspirations of Indians?

Q: To what extent we can consider the Constitution as true representation of Indians?