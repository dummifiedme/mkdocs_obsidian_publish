---  
title: Puducherry  
source: None  
date: 2021-02-25 16:21  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Puducherry
**flashcard**{: #flashcard}{: .hash}  

- Four regions once under French
	- Puducherry
	- Yanam
	- Karaikal
	- Mahe
	![[Pasted image 20210225162222.png]]
- French before leaving had signed a treaty to not alter the French culture existing in the areas.