---  
title: Singapore Convention on Mediation  
source: None  
date: 2021-05-08 21:13  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics
# Singapore Convention on Mediation
**flashcard**{: #flashcard}{: .hash}  

aka UN Convention on International Settlement Agreements.
- Adopted in December 2018.
- 53 signatories including India, China, USA.
- Mediated settlement for businesses seeking enforcement. <!--SR:!2021-08-17,1,210-->