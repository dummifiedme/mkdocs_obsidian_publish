---  
title: Nag Missile  
source: None  
date: 2021-02-25 05:24  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[Missiles]] , 
---

# Nag Missile
**flashcard**{: #flashcard}{: .hash}  

- third-generation, fire and forget, anti-tank guided missile
- A part of [[Integrated Guided Missile Development Program]]