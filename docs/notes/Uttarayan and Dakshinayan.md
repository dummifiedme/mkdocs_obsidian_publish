---  
title: Uttarayan and Dakshinayan  
date: 2021-02-12 22:12  
tags: status/draft  
aliases: ['Uttarayan', 'Dakshinayan', 'Solstice']  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/geography/physical 
# Uttarayan and Dakshinayan
- 'ayan' is :: Solstice <!--SR:!2022-01-11,7,210-->

## Uttarayan 
**flashcard**{: #flashcard}{: .hash}  

- Sun appears to be moving north for 6 months
- Northward migration begins after December 22 <!--SR:!2022-03-05,60,250-->

## Dakshinayan 
**flashcard**{: #flashcard}{: .hash}  

- Sun appears to be moving south for 6 months
- Southward migration begins after June 21 <!--SR:!2022-01-10,6,210-->