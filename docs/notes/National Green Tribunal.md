---  
title: National Green Tribunal  
date: 2021-09-23 01:12  
alias: []  
tags: status/seed  
sr-due: 2022-01-25  
sr-interval: 12  
sr-ease: 150  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment/bodies 
# National Green Tribunal
- **Type of Body** :: [[Statutory Bodies]]  <!--SR:!2022-01-07,3,130-->
- **Established in**:: 2010
- **HQ**:: Delhi with regional offices in Chennai, Kolkata, Bhopal, Pune
- [[Principles of Natural Justice]] (not via civil or criminal code)
- *Has powers of Civil Court*

## Composition
- Chairperson - retired SC Judge
- Members - 10 experts and 10 Judicial members