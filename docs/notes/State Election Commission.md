---  
title: State Election Commission  
source: None  
date: 2021-03-20 00:50  
tags: status/seed  
aliases: []  
---  
Category::  

---

# State Election Commission
**flashcard**{: #flashcard}{: .hash}  

- Constituted under the provisions of Article [[243Z]]- Election of panchayats and municipalities.
- Composition - Single member commission - [[#State Election Commissioner]]
	- Appointed by Governor of the state.
	- Same powers as [[Election Commission of India]]

## State Election Commissioner
- appointed → by governor
- conditions of service → by governor
- tenure of the office → by governor
- shall not be removed, except as in the manner of removal of a HC judge.
- Conditions of the services may not be varied as per his disadvantage.