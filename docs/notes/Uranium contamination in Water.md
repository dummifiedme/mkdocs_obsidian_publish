---  
title: Uranium contamination in Water  
source: None  
date: 2021-05-21 23:21  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Uranium contamination in Water

## Facts and Figures
- BARC did the a nationwide survey - *83.6 percent* of all the samples had Uranium.
- 8/12 parameters exceeded the acceptable limits.
	- The acceptable limits for **drinking water** is set by *BIS* - **0.03 mg/l**.
	- **Groundwater** acceptable limits are set by *Atomic Energy Regulatory Board* for radiological safety.
- No surface water exceeded the prescribed limits - there is a upward trend of uranium with depth.

## Effects on Humans
- might effect various bones and organs - mostly kidneys.

## Factors
- Aquifer Rocks
- Water-Rock Interaction
	- Oxidation enhances solubility
- Human activities of drilling or boring
- Other interaction of uranium with chemicals.

## The condition in the country
Western India is most affected - Punjab, Rajasthan, Gujarat, Haryana.
![[Map-Uranium contamination.png]]