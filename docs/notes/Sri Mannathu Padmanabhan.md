---  
title: Sri Mannathu Padmanabhan  
source: None  
date: 2021-03-07 23:18  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Sri Mannathu Padmanabhan
**flashcard**{: #flashcard}{: .hash}  

- Teaching 1893
- 1905 - Law in Magistrate Courts
- Against untouchability, helped Nayars community.
- Unselfish social service.
- Awards
	- "BHARAT KESARI" in 1959 by President.
	- [[Padma Bhushan]] in 1966