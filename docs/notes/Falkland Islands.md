---  
title: Falkland Islands  
date: 2021-07-18 21:57  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/geography  
# Falkland Islands
**flashcard**{: #flashcard}{: .hash}  

- Argentina Coast
- Disputed between Argentina and UK
<!--SR:!2021-09-25,30,290-->