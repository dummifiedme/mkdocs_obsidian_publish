---  
aliases: []  
status: 🟡  
---  
**source**{: #source}{: .hash}  
/upsc/test/mains 
# 2021VMT04 1490 - GS3 Economy
**Marks**::
**Paper**:: GS3
**Subject**:: Economics
**PDF**::

## Progress
**Attempted**.. <progress value=07 max=20>Attempted</progress>
**Notes**...... <progress value=2 max=20>Attempted</progress>

### Checklist
- [ ] Test
- [ ] Solution
- [ ] Notes
- [ ] Marks and Analysis Section

## Remarks or Suggestions
- S:: 'Way Forwards' in the 'Conclusions' are missing!
- S:: 


## Questions
1. What do you understand by direct and indirect taxes? Giving examples, explain why direct taxes are considered progressive while indirect taxes are regarded as regressive.
	- [[Direct Tax]] - progressive
	- [[Indirect Tax]] - regressive
2. What do you understand by Gross Domestic Product or GDP? How is it calculated? Also differentiate it from Gross National Product or GNP. 
	- [[Gross Domestic Product]] 
	- [[Gross National Product]]
3. Explain the concept of Gender Budgeting. What is the significance of this concept in India? Has the government taken any steps to adopt this concept in practice? 
	- [[Gender Budgeting]]
4. In the context of the national currencies, bring out the difference between Depreciation and Devaluation. Also explain the factors that affect the value of Indian Rupee.
	- [[Depreciation of currency]]
	- [[Devaluation of currency]]
	- [[Depreciation vs Devaluation of currency]]
5. Discuss the reasons behind unclear land titles in India and the challenges faced due to it. How will the digitisation of land records help overcome these challenges?
6. Discuss the limitations of using GDP as a measure of well-being of a country.
7. What role does fiscal policy plays in an economy? Highlight the tools used by the government to control fiscal deficit.
8. Distinguish between Revenue and Capital accounts of the Budget. Discuss the significance of increasing capital expenditure for an economy.
9. Bring out the major factors influencing inflation in India. 
10. Distinguishing between revenue deficit, fiscal deficit and primary deficit, highlight the implications of fiscal deficit on the economy. 
11. Providing a brief background on the economic crisis facing India in 1991, enumerate the key measures that were taken to mitigate the crisis. 
12. Tracing the course of land reforms in India, highlight the pressing concerns in present times.
13. Explain whether the following are included in the calculation of GDP and why: 
	(a) Payment of pensions to retired government employees 
	(b) Income from the sale of an old car 
	(c) Interest on national debt 
	(d) Food grains produced by farmers for their own consumption 
	(e) Services provided by housewives
14. Highlight the various measures of money supply used by the RBI in India. 
15. Differentiating between FII (Foreign Institutional Investment) and FDI (Foreign Direct Investment), highlight the role of FDI in the economic development of India. FII 
16. What do you understand by Financial Inclusion? Discuss the role of niche banks in achieving Financial Inclusion in India.
17. The government budget in India includes the threefold function of allocation, redistribution and stabilization in the economy. Discuss. 
18. Explain the concept of GDP deflator? How is it different to other inflation indices such as CPI and WPI? 
19. What do you understand by Balance of Payments? Highlight the factors, which contribute to a negative Balance of Payments in an economy like India. 
20. Give an account of the various measures employed by the RBI to control credit in the economy.