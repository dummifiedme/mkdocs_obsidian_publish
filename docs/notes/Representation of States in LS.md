---  
title: Representation of States in LS  
source: None  
date: 2021-03-10 17:22  
tags: status/mature  
aliases: []  
---  
Category::  

---

# Representation of States in LS
**flashcard**{: #flashcard}{: .hash}  

- directly elected by people from the territorial constituencies.
- via Universal Adult Suffrage
	- above 18 years of age 
- [[First-Past-The-Post System]]<!--SR:!2021-09-24,3,250-->

> Earlier minimum age for voting was 21 years, reduced to 18 by :: [[61st Amendment Act]], 1988
<!--SR:!2021-11-14,5,230-->