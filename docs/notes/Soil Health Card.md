---  
title: Soil Health Card  
source: None  
date: 2021-02-17 01:01  
tags: status/seed note/upsc/GS3/economics/schemes  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/agriculture 
# Soil Health Card
**flashcard**{: #flashcard}{: .hash}  

- Started in 2015
- also focus on enabling employment generation after skill evaluation.
- Objectives
	- Improve soil quality
	- Employment generation for youth
	- update information
	- soil testing facilities.
- Once every 2 years.

## Testing Parameters
- [[Macro nutrients in Soil]]
- [[Secondary Nutrients in Soil]]
- [[Micro Nutrients in Soil]]
- [[Physical Parameters of Soil]]