---  
title: Indian Cyber Crime Coordination Center  
source: None  
date: 2021-03-17 23:09  
tags: status/seed  
aliases: ['I4C']  
---  
Category::  

---

# Indian Cyber Crime Coordination Center
**flashcard**{: #flashcard}{: .hash}  

- Established in 2018
- HQ - Delhi
- Under Ministry of Home Affairs