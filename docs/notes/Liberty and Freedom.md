---  
Date: 2020-12-02 16:57  
Title: Liberty and Freedom  
Parent: [['SR Polity Lecture 1']]  
sr-due: 2022-01-29  
sr-interval: 19  
sr-ease: 250  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Liberty and Freedom

**Q: Is there any difference between 'Liberty' and 'Freedom'?**
A: For General Studies, there is no difference. 😉 → Use it interchangeably.

- For Political Studies --> various scholars gave different explanations.
    - Still *'Freedom' is widely used (amongst Socialists)* =>  since, 'liberty' got associated with  'Liberals' and hence, 'Capitalism'.
	- Some Marxist thinkers such as Karl Marx preferred 'freedom', due to association of 'liberal' with 'capitalism'
	    - *Freedom from necessities* is requirement.
      

## Difference between the freedom and liberty in Articles 19 and 21.

| [[Article 21]]                                                 | Article 19                                                     |
| -------------------------------------------------------------- | -------------------------------------------------------------- |
| Right to  Life and Personal Liberty                            | Right to Freedom                                               |
| Even 'freedom' could be used, its just a choice.               | BUT, the 'scope' of the 'freedom' is different.                |
| After [[Meneka Gandhi case]], 'liberty' and 'freedom' became same. | After Meneka Gandhi case, 'liberty' and 'freedom' became same. |

---

****Question**{: #Question}{: .hash}  
**:: Compare 'Right to Freedom' given in Article 19 with 'Right to Liberty' given in Article 21.