---  
title: Need for Direct Tax Reforms  
source: None  
date: 2020-12-31  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/economics 
# Need for Direct Tax Reforms
- Rationalization and simplification of income tax structure.
		- Rate structure has broadly remained same in last 20 years
		- Need to rethink the *incentives on savings*
- Simplify corporate tax rate structure and phase out exemptions. (Are there a lot of exemptions? Look into the exemptions #todo/digDeeper )		- Not equitable vertically (small firms end up paying more tazes)
		- loss of revenue due to large number of exemptions.
	- Widen Tax Base
		- will help cover the loss of revenue due to lower tax rates and simplified tax structure.
	- Reducing tax litigation
		- low success rate of appeals (~30%)
		- need to provide alternate ways of dispute redressal.
	- Need of technology infusion
		- improves efficiency of tax collection
		- aids the taxpayer
	- Better sync with global economy
		- differential treatment of foreign and domestic companies in the country could be phased out.