---  
title: Grid-connected Rooftop Solar Scheme  
date: 2021-06-11 12:06  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims 
# Grid-connected Rooftop Solar Scheme
**flashcard**{: #flashcard}{: .hash}  

- [[Ministry of New and Renewable Energy]]
- Indigenously manufactured PV modules and cells to be used.
- Subsidy scheme basically -> only for Residential use.
- Power ceilings on the amount of subsidies.