---  
title: United Nations Environment Programme  
date: 2021-06-29 21:12  
alias: ['UNEP']  
tags: status/draft  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment **bodies**{: #bodies}{: .hash}  

# United Nations Environment Programme
**flashcard**{: #flashcard}{: .hash}  

- Global environmental agenda
- Founded in *1972* at UN Conference on Human Environment.
- HQ - *Nairobi, Kenya*
- Governing body is UN Environment Assembly.
- Membership - 193 countries
	- India is a member
- Reports
	- [[Emissions Gap Report]]
	- [[Global Environment Outlook]]

- Governing body of the UNEP is ==UN Environment Assembly==.

## Awards by UNEP
- [[Champions of the Earth]]
- [[SEED Awards]]
- [[Sasakawa Prize]]

## Operated by UNEP
- [[Bonn Convention]]
- [[Montreal Protocol]]
- [[Minamata Convention on Mercury]]

## Secretariats hosted  by UNEP
- [[Convention on Biological Diversity]]
- [[Convention on International Trade in Endangered Species of Wild Fauna and Flora]]
- [[Minamata Convention on Mercury]]
- [[Basel Convention]]
- [[Rotterdam Convention]]
- [[Stockholm Convention]]
- [[Vienna Convention for the Protection of Ozone Layer]]
- [[Montreal Protocol]]
- [[Convention on Migratory Species]]
- [[Carpathian Convention]]
- [[Bamako Convention]]
- [[Tehran Convention]]

## Related Organisations
- Intergovernmental Panel on Climate Change formed by ::  [[United Nations Environment Programme|UNEP]] + [[World Meteorological Organisation]]
- REDD/REDD+ by :: [[United Nations Environment Programme|UNEP]] + [[Food and Agriculture Organisation (FAO)]] + [[UNDP]]
	- Bali/Warsaw
-