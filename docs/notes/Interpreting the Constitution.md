---  
---  
# Interpreting the Constitution
### Interpreting the Constitution
---

Stage: #📝/1 | Tags: **note**{: #note}{: .hash}  
/upsc/GS2/polity | Source: **mPuri**{: #mPuri}{: .hash}  
 

---

1. [[Article 366]]  - Contains definitions of commonly used terms throughout the constitution or laws.
2. The tools evolved by the apex court
	- Unless the context requires otherwise, commonly held meaning of English term shall be used to interpret the constitution
3. The Doctrine of liberal interpretation
	-  The constitution should be so interpreted as to give relevance and meaning to every word, phrase, every sentence.
4. The doctrine of progressive interpretation
	-  The provisions must be so interpreted as to adapt them to contemporary realities.
	-  All parts of the constitution must be read together.
5. Doctrine of prospective over interpretation
	- A fresh interpretation of constitution shall become operational from that moment prospective and will not have retrospective review. **But**{: #But}{: .hash}  
❗ 
		- (Kesavanand Bharti case was used as the point from which all the ninth schedule articles can be put under judicial review?!)