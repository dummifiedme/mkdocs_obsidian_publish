---  
title: CAROTAR 2020  
source: None  
date: 2020-12-23  
aliases: ['Customs (Administration of Rules of Origin under Trade Agreements) Rules']  
tags: Economics  
---  
Category:: 

---

# CAROTAR 2020
**flashcard**{: #flashcard}{: .hash}  

 - set the guidelines for [[Rules of Origin]]
- applicable on import of goods into India where importer makes claim of preferential rate of duty in terms of a trade agreement (TA)
- aims to *supplement the operational certification procedures. -- related to implementation of ROO.*
- linked with [[Certificate of Origin]]