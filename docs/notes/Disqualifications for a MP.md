---  
title: Disqualifications for a MP  
source: None  
date: 2021-03-10 19:09  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Disqualifications for a MP
**flashcard**{: #flashcard}{: .hash}  

1. holds any office of profit under Union or State Government.
2. Unsound mind and declared by a court
3. undischarged solvent.
4. not a citizen of India -- acquired a citizenship/allegiance of any other foreign state
5. [[Disqualifications for MP as per Parliament]].

## [[Anti-Defection Law]]