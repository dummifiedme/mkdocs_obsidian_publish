---  
---  
# POSH Act
| Source  | Stage    | Tags |
| ------- | -------- | ---- |
| **current**{: #current}{: .hash}  
 | #📝/1 | **women**{: #women}{: .hash}  
 **acts**{: #acts}{: .hash}  
 **social_issues**{: #social_issues}{: .hash}  
     |

---

### POSH ACT 2013
- Enacted to fill the legislative void partially addressed in '{Vishaka Case}' by SC.

- VISHAKHA CASE 
	- SC issued a guidelines called as '{Vishakha Guidelines}' which becomes the core for the {POSH Act}.

### Suggestions/Reforms
1. From News Article: 
	- The majority of members of ICC (Internal Complaints Committee) shouldn't be from the same departments as the accused (or the person under scrutiny).
		- Especially in cases against HIGH RANKING officials.
	![[3 Resources/Pasted image 20201206105152.png]]