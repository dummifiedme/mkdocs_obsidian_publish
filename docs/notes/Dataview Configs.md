---  
---  
**note**{: #note}{: .hash}  
/general/knowledge/tech/config/obsidian 
# Dataview Configs

## Sources
Will list out files with the source sorted by page numbers and the page number is linked (clickable)
```dvquery
table without id ("[[" + meta(Ref).path+ "#" + meta(Ref).subpath + "|"+ meta(Ref).subpath + "]]") as Page, file.link as Topic 
from [[2021MM October.pdf]]
where !contains(file.name, "📰")
sort Page
```