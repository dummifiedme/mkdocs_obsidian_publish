---  
title: SANKALP  
source: None  
date: 2021-02-17 00:55  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Economics]] , [[Keywords GS3]]

---

# SANKALP
**flashcard**{: #flashcard}{: .hash}  

- Skill development program for youth
- PPP model.
- Training of the Trainer (ToT)