---  
---  
# GS2-15 Important aspects of governance, transparency and accountability, e-governance applications, models, successes, limitations, and potential; citizens charters, transparency & accountability and institutional and other measures
**upsc**{: #upsc}{: .hash}  
/syllabus/topics 

## Governance
- [[E-Governance]]
- [[Citizen's Charter]]
- [[Social Accountability]]

## Local Governance
- [[Urban Local Governance]]
- [[Urban Local Bodies Reforms]]
- [[People's Plan Campaign]]
- [[Aspirational Districts Programme]]

## Transparency and Accountability
- [[Whistle-Blowing]]
- [[Right to Information]]
- [[Integrity Pact]]
- [[Television Rating in India]]