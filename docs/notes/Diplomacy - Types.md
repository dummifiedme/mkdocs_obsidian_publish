---  
title: Diplomacy - Types  
date: 2020-12-18 05:18  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR   
# Diplomacy - Types


- Appeasement
- Counterinsurgency diplomacy
- Debt-trap diplomacy
- Economic diplomacy
- Gunboat diplomacy
- Hostage diplomacy
- Humanitarian diplomacy
- Migration diplomacy
- Nuclear diplomacy
- Paradiplomacy
	-  International relations by Regional Governments (Sub-National Units)
	-  Globalization is making a shift.
- Peer to Peer Diplomacy
- Preventive diplomacy
- Public diplomacy
- Quiet diplomacy
- Science diplomacy
- Soft power
	-  "Hearts and minds Diplomacy"
- [[City Diplomacy]]