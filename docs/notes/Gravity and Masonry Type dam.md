---  
title: Gravity and Masonry Type dam  
source: None  
date: 2021-04-03 16:31  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Gravity and Masonry Type dam
**flashcard**{: #flashcard}{: .hash}  

Holds water in lieu of the weight of the material used.