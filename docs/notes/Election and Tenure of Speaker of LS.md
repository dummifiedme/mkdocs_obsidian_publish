---  
title: Election and Tenure of Speaker of LS  
source: None  
date: 2021-03-11 16:01  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Election and Tenure of Speaker of LS
**flashcard**{: #flashcard}{: .hash}  

- Elected as soon as a new sabha is formed from amongst the members of LS
- Date of election is fixed by the President
- Tenure - remains in office during the life of LS - though remains even after the general elections till new speaker is not elected.

**Power and Role of the Speaker**
(when resolution is being passed for **removal**)
**flashcard**{: #flashcard}{: .hash}  

- can take part in the proceedings - not as a speaker, but as a member of the house.
- can vote in the first instance but not when there is a tie (speaker powers)

## Speaker of LS - Vacant Seat
(3 cases)
**flashcard**{: #flashcard}{: .hash}  

- ceases to be a member of LS
- resigns - written - to Deputy Speaker
- removed by a resolution passed by a majority of the house.