---  
---  
- According to India State of Forest Report 2019, over 30,000 incidents of forest fires were reported in 2019. 
	- About *half* of India’s forests are prone to fires. 43% were prone to occasional fires and 5% to frequent fires, and 1% were at high or very high risk.

## Forest Fire Prone Areas in India
![[Map - Forest Fire prone areas in India1.jpeg]]