---  
title: Transparent Taxation - Honouring the Honest  
source: None  
date: 2020-12-31  
aliases: []  
---  
Category:: [[GS3]] , [[🗃️ Economics]]

---

# Transparent Taxation - Honouring the Honest
**flashcard**{: #flashcard}{: .hash}  

A platform to strengthen the efforts of reforming and simplifying the India's tax system. -> Launched by PM
- easing the tax compliance
- rewarding the honest tax payers
- Main features
  - Faceless Assessment
    - eliminate direct contact between taxpayer and IT officer
  - Faceless Appeal
    - random allotment of appeals to any officer in the country.
  - Taxpayer Charter
    - outline of rights ans responsibilities of both tax payer and officers.
- Ensures no direct contact in matters of scrutiny, notice, survey or assessment.
	- privacy
	- reduces corruption
- BOOST the confidence of taxpayer
- [[Need for Direct Tax Reforms]]
- [[Recent steps to improve Direct Tax Ecosystem]]
- [[India's Tax Penetration Status]]