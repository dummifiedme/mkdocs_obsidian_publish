---  
title: Ramsar Sites - Odisha  
date: 2021-06-24 18:18  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Ramsar Sites - Odisha
**flashcard**{: #flashcard}{: .hash}  

- [[Chilika Lake]]
	- [[Nalbana Island]] is the core Ramsar Site.
- Bhitarkanika Mangroves
![[Map - Chilika and Bhitarkanika.png]]