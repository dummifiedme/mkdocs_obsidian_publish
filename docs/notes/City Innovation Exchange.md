---  
title: City Innovation Exchange  
source: None  
date: 2021-03-07 23:05  
tags: status/seed  
aliases: []  
---  
Category::  

---

# City Innovation Exchange
**flashcard**{: #flashcard}{: .hash}  

- Initiative of MoHUA.
- Connect innovators with the problems faced by the cities.
- A part of [[Atal Innovation Mission), AGNIi ([[Accelerating Growth of New India's Innovations]]]]