---  
title: Pastuerella Multoceda  
source: None  
date: 2021-03-11 22:10  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Pastuerella Multoceda
**flashcard**{: #flashcard}{: .hash}  

- Common bacteria - found in respiratory tract of herbivores.
- causes diarrhoea - zoonotic infection in humans though animals.
- effects mostly all animals.
 ![[Pasted image 20210311221156.png]]