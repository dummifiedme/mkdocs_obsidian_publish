---  
title: National Safety Council  
source: None  
date: 2021-02-10 21:26  
tags: status/seed  
aliases: []  
---  
Category:: [[Keywords GS2]]

---

# National Safety Council
- Non-profit, self-financing apex body at the national level
- Objective -> To generate, develop and sustain a voluntary movement on ==Safety, Health and Environment== (SHE) at the national Level
- Autonomous Body
- Set up by ==[[Ministry of Labour and Employment]]== in 1965
![[National Safety Council Logo.png]]