---  
title: Srisalem Hydroelectric Power Plant  
source: None  
date: 2021-04-03 16:30  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Geography]]

---

# Srisalem Hydroelectric Power Plant
- Andhra Pradesh - Kurnool and Mahabubnagar Districts
- [[Gravity Type Dam]] and [[Masonry Type Dam]]