---  
title: + Wildlife Sanctuaries  
date: 2021-05-27 16:37  
tags: None  
alias: ['List of Wildlife Sanctuaries']  
---  
Category:: [[../../MOC]]

---

# + Wildlife Sanctuaries
- [[Asola Bhatti Wildlife Sanctuary]]
- [[Wayanad Wildlife Sanctuary]]
- [[Karlapat Wildlife Sanctury]]
- Indian Wild Ass Sanctuary
- [[Vedanthangal Bird Sanctuary]]
- [[Dandeli Wildlife Sanctuary]]