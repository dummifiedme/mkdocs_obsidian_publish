---  
title: Lonar Lake  
source: None  
date: 2021-05-28 16:14  
tags: status/seed  
aliases: []  
locations: None  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Lonar Lake
- **Location**::  Buldhana, MH
- Map:: [Lonar Lake](geo:19.97649985,76.50797393996032) tag:lakes
- **Major species**::  grey wolf, common pochard
- Declared a Ramsar Site in 2020
- [[Endoheric]]
- **Crater Lake** - Formed due to Meteorite impact 
- Turns pink due to the presence of '[[Haloarchaea]]' microbes