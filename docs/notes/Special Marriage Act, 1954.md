---  
title: Special Marriage Act, 1954  
source: None  
date: 2021-01-18 22:36  
tags: status/seed  
aliases: []  
---  
Category:: [[Government Acts]] , [[Keywords GS2]]

---

# Special Marriage Act, 1954
**flashcard**{: #flashcard}{: .hash}  

- Related to the inter-faith marriages.
	- Required a compulsory 30-day notice to DM before an inter-faith marriage.
	- Published for public to raise objections , if any.

---
- [ ] **todo**{: #todo}{: .hash}  
 Read more