---  
cover: https://m.media-amazon.com/images/M/MV5BM2ZmYjUxZDYtMmZlMi00YjlkLTkwNTItMjcxZDRlOTI3ZTNmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg  
imdbId: tt5952634  
---  
**entertainment**{: #entertainment}{: .hash}  
/show
# 🎞️ The Spy (2019)

- - -
![poster|-right](https://m.media-amazon.com/images/M/MV5BM2ZmYjUxZDYtMmZlMi00YjlkLTkwNTItMjcxZDRlOTI3ZTNmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX300.jpg)

**Director**::  
**Genre**:: "Drama", "History"
**IMDB Rating**:: 7.9
**Year**:: 2019
**Seasons**:: 1
**Cast**:: "Sacha Baron Cohen", "Hadar Ratzon Rotem", "Yael Eitan"
**Watched**:: "2021-12-20"
**Rating**:: 9
- - -

## Synopsis
**Plot**:: Based on the life of Israeli spy Eli Cohen.