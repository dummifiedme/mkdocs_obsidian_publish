---  
title: Kuruba Community  
source: None  
date: 2021-02-11 17:29  
tags: status/draft  
aliases: ['Kuruba Gowda', 'Kuruma', 'Kurumbar']  
locations: None  
---  
**note**{: #note}{: .hash}  
/upsc/anthropology/tribes  #note/upsc/GS1/history/culture/tribes
# Kuruba Community
- aka::  Kuruba Gowda, Kuruma, Kurumbar
- Hindu Caste native to 
	- (location:: "[Karnataka](geo:14.5203896,75.7223521)") 
	- (location:: "[Telangana](geo:17.8495919,79.1151663)")  
	- (location:: "[Maharashtra](geo:18.9068356,75.6741579)")
- Shephards, Millitiamen from hills, armed vessals.