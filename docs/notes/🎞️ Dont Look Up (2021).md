---  
cover: https://m.media-amazon.com/images/M/MV5BNjZjNDE1NTYtYTgwZS00M2VmLWEyODktM2FlNjhiYTk3OGU2XkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_SX300.jpg  
imdbId: tt11286314  
---  
**entertainment**{: #entertainment}{: .hash}  
/movie
# 🎞️ Dont Look Up (2021)

- - -
![poster|-right](https://m.media-amazon.com/images/M/MV5BNjZjNDE1NTYtYTgwZS00M2VmLWEyODktM2FlNjhiYTk3OGU2XkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_SX300.jpg)

**Director**:: "Adam McKay"
**Genre**:: "Comedy", "Drama", "Sci-Fi"
**IMDB Rating**:: 7.3
**Year**:: 2021
**Cast**:: "Leonardo DiCaprio", "Jennifer Lawrence", "Meryl Streep"
**Watched**:: "2022-01-09"
**Rating**:: 9
- - -

## Synopsis
**Plot**:: Two low-level astronomers must go on a giant media tour to warn mankind of an approaching comet that will destroy planet Earth.