---  
---  
# All Tasks

## Tasks

| [[#Overdue]] | [[#Due today]] | [[#Done today]] | [[#Upcoming Week]] | [[#No due date]] |

### Overdue
```dataview
task
from "" and -"Library"
where !completed and due and due<=date(today)
```



### Due today
```dataview
task
from "" and -"Library"
where (due=date(today) or completed=date(today)) or (due<date(today) and !completed and due)
```


---
### Upcoming Week
```dataview
task
from "" and -"Library"
where (due and (due>=date(today) and due<date(eow))) and !completed 
```


### No due date