---  
title: Disqualifications for MP as per Parliament  
source: None  
date: 2021-03-10 19:12  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Disqualifications for MP as per Parliament
**flashcard**{: #flashcard}{: .hash}  

1. He must not have been found guilty of *certain election offenses or corrupt practices* in the elections.
2. He must not have been convicted for any offense resulting in *imprisonment for two or more years*. But, the detention of a person under a preventive detention law is not a disqualification.
3. He must not have failed to lodge an account of his *election expenses* within the time.
4. He must *not have any interest in government contracts, works or services.*
5. He must *not be a director or managing agent nor hold an office of profit in a corporation in which the government has at least 25 per cent share*.
6. He must not have been *dismissed from government service for corruption or disloyalty to the State*.
7. He must not have been *convicted for promoting enmity* between different groups or for the offence of bribery.
8. He must not have been punished for preaching and *practising social crimes such as untouchability, dowry and sati*.