---  
title: Asan Barrage  
date: 2021-06-24 18:17  
alias: ['Asan Conservation Reserve']  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment   
# Asan Barrage
- **Location** - ==Uttarakhand==
- **Major species** - red-headed vulture, white-rumped vulture, Baer's Pochard
- Declared a Ramsar Site in 2020
- At the confluence of ==Asan and Yamuna Rivers==