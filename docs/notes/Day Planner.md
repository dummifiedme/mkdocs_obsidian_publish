---  
aliases: []  
cssclass: status  
---  
**personal**{: #personal}{: .hash}  
/planning
# Day Planner

## 2022-01-02
- [x] [[Governance]]
- [x] [[Citizen's Charter|Citizen Charter]]

## 2021-12-24 Fri
- **07:30 — 09:30** : [[@Devender Kumar|Devender]]’s Visit
- **10:30 — 12:00** : [[VVAM-216 (Governance) Role of Civil Services in a Democracy (pg.33).pdf]] Page 14-24
- **12:30 — 14:00** : Answer Writing [[2021VMT04 1490 - GS3 Economy]] (4 Questions)
- **14:00 — 14:30** : Break
- **14:30 — 16:00** : [[ANT2-09.2 ROLE OF ANTHROPOLOGY IN TRIBAL AND RURAL DEVELOPMENT]] Notes
- **16:00 — 16:30** : Break
- **16:30 — 18:30** : [[📰 2021MM October]] International Relations
- **18:30 — 20:30**: Gym and Walk
- **21:00 — 23:00**: Revise

## 2021-12-23 Thu
- **07:30 — 09:30** : [[VVAM-216 (Governance) Role of Civil Services in a Democracy (pg.33).pdf]] Page 14-16
- **09:30 — 10:00** : Bath, Breakfast
- **10:00 — 12:00** : Answer Writing [[2021VMT04 1490 - GS3 Economy]] (4 Questions)
- **12:00 — 12:30** : Break
- **12:30 — 14:30** : [[ANT2-09.2 ROLE OF ANTHROPOLOGY IN TRIBAL AND RURAL DEVELOPMENT]] Notes
- **14:30 — 23:00** : [[@Devender Kumar|Devender]] Visit