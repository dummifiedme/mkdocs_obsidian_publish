---  
title: Nalbana Island  
source: None  
date: 2021-05-28 17:07  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# Nalbana Island
**flashcard**{: #flashcard}{: .hash}  

An island in [[Chilika Lake]] - literally means "weed covered island"
- Main region of protection under Ramsar.