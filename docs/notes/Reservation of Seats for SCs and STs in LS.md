---  
title: Reservation of Seats for SCs and STs in LS  
date: 2021-06-23 18:48  
alias: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Reservation of Seats for SCs and STs in LS

- Abandoned the system of communal representation
	- Still provided for SC and ST reservations in LS
		- Basis: population ratios.