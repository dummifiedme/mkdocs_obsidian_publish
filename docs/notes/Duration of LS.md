---  
title: Duration of LS  
source: None  
date: 2021-03-10 18:52  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Duration of LS
- Not a continuing chamber
- Normal term is 5 years from the date of its first meeting after the general elections.
	- President is authorized to dissolve it any time though -> even before completion; can't be challenged in court.^[Maybe this is why every government when comes to power bids to change the President? (One of the many reasons). Also, why doesn't the president just dissolve the newly elected government before stepping out of office? :D]

## Extension to the term of LS
**flashcard**{: #flashcard}{: .hash}  

- In case of [[National Emergency]].
- for one year at a time
	- can't continue beyond 6 months of the end of emergency.