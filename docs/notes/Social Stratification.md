---  
title: Social Stratification  
source: None  
date: 2021-01-19 22:10  
tags: status/seed  
aliases: []  
---  
Category::  , [[🗃️ Anthropology]] , [[ANT-P1]]

---

# Social Stratification
- Social Stratification is the layering of a society in various strata based on socio-cultural, political or economic differences.
- The Societies can be egalitarian - possible.
	- Mostly in good gathering and horticultural societies.
- Stratification emerged with permanent communities
	- political systems, agriculture etc, enhanced it.
	- Ethnicity and racism in the modern societies make them stratified.
- There are three kinds of advantages
	- Economics Resources (Wealth)
	- Power
	- Prestige
- Three kind of societies, based on degree of equality or inequalities (degree of stratification):
	- [[Egalitarian Societies]]
	- [[Rank Societies]]
	- [[Class Societies) (Caste Societies]]