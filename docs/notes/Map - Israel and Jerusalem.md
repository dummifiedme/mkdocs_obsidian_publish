---  
---  
**note**{: #note}{: .hash}  
/upsc/map
# Map - Israel and Jerusalem

## Note the regions
- West Bank
- Gaza Strip
- Golan Heights
- Jerusalem
- Relative locations of the countries

![[Map - Israel and Jerusalem.jpg]]


## Related 
[[Abraham Accord]]