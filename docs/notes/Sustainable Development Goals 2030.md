---  
title: Sustainable Development Goals 2030  
date: 2021-01-21 18:03  
aliases: ['SDG 30', 'SDG 2030']  
---  
**note**{: #note}{: .hash}  
/upsc/GS3 
# Sustainable Development Goals 2030

![[E-WEB-Goal-01.png|200]]![[E-WEB-Goal-02.png|200]]![[E-WEB-Goal-03.png|200]]![[E-WEB-Goal-04.png|200]]![[E-WEB-Goal-05.png|200]]![[E-WEB-Goal-06.png|200]]![[E-WEB-Goal-07.png|200]]![[E-WEB-Goal-08.png|200]]![[E-WEB-Goal-09.png|200]]![[E-WEB-Goal-10.png|200]]![[E-WEB-Goal-11.png|200]]![[E-WEB-Goal-12.png|200]]![[E-WEB-Goal-13.png|200]]![[E-WEB-Goal-14.png|200]]![[E-WEB-Goal-15.png|200]]![[E-WEB-Goal-16.png|200]]![[E-WEB-Goal-17.png|200]]

[[SDG Goal 01]]
[[SDG Goal 02]]
[[SDG Goal 03]]
[[SDG Goal 04]]
[[SDG Goal 05]]
[[SDG Goal 06]]
[[SDG Goal 07]]
[[SDG Goal 08]]
[[SDG Goal 09]]
[[SDG Goal 10]]
[[SDG Goal 11]]
[[SDG Goal 12]]
[[SDG Goal 13]]
[[SDG Goal 14]]
[[SDG Goal 15]]
[[SDG Goal 16]]
[[SDG Goal 17]]