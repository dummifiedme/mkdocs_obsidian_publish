---  
---  
# 🗃️ Agriculture
---
title:  🗃️ Agriculture
Due Date::
Status: 🔴
Priority: ❗❗❗
stage: 🔨/🗃️
---

**project**{: #project}{: .hash}  

Master Project:: [[🔨 Project GS3]]

## Sources
 - Selected Chapters of [[📕 Fundamentals of Indian Geography Class XI - NCERT]] 
- Pillar Agriculture of [[📕 Mrunal - Economics Notes]]
- [[📓 PMFIAS - Agriculture Notes]]

![[45F78722-2C20-47AA-977D-BDF0EEB147E8.jpeg]]
---
## Topics and Notes
- Agriculture - Introduction
	- Agriculture - Related Ministries
		- [[Ministry of Agriculture & Farmers' Welfare]]
			- Department of Agriculture, Cooperation and Farmers Welfare
			- Department of Agricultural Research & Education
		- [[Ministry  for Fishries, Animal Husbandry and Dairying]]
			- Department of Fisheries
			- Department of Animal Husbandry and Dairying
- Agriculture - Inputs
	- Land
		- Land Reforms
			- Land reforms after independence
		- Land Consolidation
	- Seeds
		- Seed Village Concept
		- Seed Bank or Seed Vaults
		- Draft Seed Bill 2019
		- Green Revolution
		- Legal protections for development of plant varieties (IP, Patents etc)
		- Hybrid Seeds
		- Genetically Modified Seeds (GM Seeds)
			- Regulations - Genetic Engineering Appraisal Committee (GEAC)
			- Related ministry for approval - [[Ministry of Environment]]
			- Challenges
			- Suggestions by ES2017
	- Irrigation (Water)
		- Irrigation water productivity
		- Water related ministries
			- [[Jal Shakti Ministry]]
				- [[Department of Drinking Water and Sanitation]]  - earlier a ministry
				- [[Department of Water Resources, River Development and Ganga Rejuvenation]] - earlier a ministry
		- Water Schemes
			- National Rural Drinking Water Mission (2009)
				- Under [[Department of Drinking Water and Sanitation]]
				- Centrally Sponsored Scheme - Core Scheme
					- Not 100%
			- PM Krishi Sinchai Yojana (2015)
				-  Under[[Ministry of Agriculture & Farmers' Welfare]]
				-  4-Pillar Strategy
					1. Accelerated Irrigation Benefit Programme
					2. Watershed Development
					3. Har Khet ko Pani
					4. Per Drop More Crop
						- Fertigation
						- Mulching
						- Micro-Irrigation - Challenges
			- Namami Gange Yojana (2015)
				- Under [[Department of Water Resources, River Development and Ganga Rejuvenation]]
			- Jal Kranti Abhiyan (2015)
				- Under [[Department of Water Resources, River Development and Ganga Rejuvenation]]
			- Jal Shakti Abhiyan (2019 - July)
				- Under [[Jal Shakti Ministry]]
			- Jal Jeevan Mission (2019-August)
				- Under [[Jal Shakti Ministry]]
				- NRDWP subsumed under this scheme
			- ATAL Bhujal Yojana (ATAL JAL) (2019-December)
				- Under [[Jal Shakti Ministry]]
				- Funded by Union + World Bank (50:50)
			- DRIP (2012)
				- Dam Rehabilitation and Improvement Program
				- DHARMA web-portal to monitor safety of dams in India
			- National Hydrology Project (2016)
			- NABARD given funds for Micro-Irrigation (₹5k Crore)
		- Related Indices
			- Composite Water Management Index
				- NITI Aayog - 2019
				- Three classifications of states- Non-Himalayan, NE and Himalayan and UTs.
				- Ranks them into 9 themes and 28 indicators against the base year of 2017-18.
		- Suggestions by [[Economic Surveys]]
			- River Linking
			- Rain Water Harvesting
			- Canal Water usage - stop theft, cost based pricing
			- Watershed Management - percolation tanks
			- National-level agency to push and coordinate various suggestions.
	- Fertilizer
	- Weedicides and Pesticides
	- Organic Farming (don’t use chemicals!)
		- Paramparagat Krishi Vikas Yojana (PKVY)
		- Zero Budget Natural Farming
			- Subhash Palekar, Karnataka
	- Finance/Credit