---  
title: Terrestrial vs Jovian Planets  
source: None  
date: 2021-02-12 22:05  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/geography 
# Terrestrial vs Jovian Planets
**flashcard**{: #flashcard}{: .hash}  

-
| Terrestrial Planets                   | Jovian Planets          |
| ------------------------------------- | ----------------------- |
| Denser                                | Gaseous                 |
| Less Moons                            | More Moons              |
| Core is comparatively less dense      | Core is more dense      |
| Less gravitational pull comparatively | More gravitational pull |
| Hotter                                | Colder                  |
| More N2, CO2                          | More H2, He             |  <!--SR:!2022-03-05,60,250-->