---  
---  
# Decentralisation and Poverty Reduction

***

|             |                                            |
| ----------- | ------------------------------------------ |
| Subject     | **note**{: #note}{: .hash}  
/upsc/GS2/polity                                    |
| Paper-Topic | GS2-Topic02                                |
| Status      | **doing**{: #doing}{: .hash}  
                                     |
| Date        | 2020-11-20                                 |
| Tags        | **divisionOfPower**{: #divisionOfPower}{: .hash}  
                           |

***


- Democratic Decentralisation = greater political participation = improved quality an reach of govt. services

- Great variation - cultures, countries and regions
    *   Various roles that government play in poor and predominantly rural areas
        1.  Provisions of public goods --> education, health etc
        2.  Provisions of divisible goods--> irrigation, agricultural extensions and credit.
        3.  determination and enforcement of laws --> regulations on economics inputs aka lands, labour, and capital.
        4.  CRITICAL: recognition and protection of rights. --> organisation, association and entitlement.

***

TARGET DECK
UPSC::General Studies 2

FILE TAG
GS2::Topic02 Polity::Definitions Definitions::Polity