---  
source: Vision Monthly Magazine August 2020  
date: 2020-12-13 21:37  
aliases: ['Indo-Vietnam']  
---  
# India-Vietnam
**note**{: #note}{: .hash}  
/upsc/GS2/IR **bilateralRelations**{: #bilateralRelations}{: .hash}  
 

> GDP of VIETNAM is growing CRAZY. India can take some leanings from its growth.

## Recent Meetings **spaced**{: #spaced}{: .hash}  

1. 17th Joint Commission on Trade, Economic, Scientific and Technological Cooperation (BILATERAL)
	- Economic and Defence Engagement
		- Closer cooperation in civil nuclear energy, space, marine sciences and new technologies.
		- India -> LINE of CREDIT of US$ 500 for procurement of defence equipment from India.
		- Vietnam wants [[../Missiles#Akash]][[Missiles#Akash]]peration at **multilateral regional forums**
        - Vietnam supports India — **permanent seat at UNSC**
        - Vietnam supports India — in joining **Asia-Pacific Economic Cooperation APEC**
    - Indo-Pacific sphere
        - Indo-Pacific key notion of India’s policy to achieve shared Security, Prosperity and growth for all in the region.
        - Both agreed tos
			- Support India’s [[Indo-Pacific Oceans[[Indo-Pacific Oceans Initiative (IPOI)]]led for Vietnam’s support for **one** of the **seven central pillars**.
        	- Support [[ASEAN#Outlook on In[[ASEAN#Outlook on Indo-Pacific]]ndia-Vietnam Relationship #spaced
![[Pasted image 20201213215012.png]]

  
## Spheres of cooperation **spaced**{: #spaced}{: .hash}  

As discussed above in detail:
1. Economic and Defence Cooperation
	1. Indian support in various scientific field.
	2. Vietnam's interest in Indian Missiles.
2. Cooperation at multi-lateral regional forums
	- Vietnam supports Indian bid for
		1. Permanent Seat at UNSC
		2. Joining [[Asia-Pacific Economic Cooperation (APEC)]] **to**{: #to}{: .hash}  
-do
3. Indo-Pacific Spheres
	- agreement on 
		1. India's Indo-Pacific Initiative (IPOI)
		2. ASEAN's Outook on Indo-Pacific
	- Focus on SECURITY, PROSPERITY and GROWTH.
4. Strategic Partnership areas:
	1. Nuclear Power
	2. Enhancing regional security
	3. fighting terrorism, transnational crime and drug trafficking.

## Importance of Vietnam for India **spaced**{: #spaced}{: .hash}  
 
- [[Act East Policy]] **to**{: #to}{: .hash}  
-do
	- Vietnam core partner in ASEAN
	- Critical Partner in Look & Act East Policy
- To contain China
	- location of Vietnam crucial.
- Energy Security
	- rich hydrocarbon deposits in Vietnam
	- Vietnam wants India to explore Oil and Gas in South-China Sea.
- Trade and Economy
	-  India in top ten trading partner of Vietnam
	-  [[Mekong-Ganga Cooperation (MGC) framework.md) --> India has been taking up **Quick Impact Projects** (QIPs]] 
		-  Short gestation period
		-  direct benefits to the communities
		-  at grassroots levels



## Challenges in India-Vietnam Relations
**to**{: #to}{: .hash}  
-do