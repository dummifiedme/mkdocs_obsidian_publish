---  
title: India's Trends in Global Market  
source: None  
date: 2020-12-31  
aliases: []  
---  
Category:: [[🗃️ Economics]] , [[GS3]]

---

# India's Trends in Global Market
**flashcard**{: #flashcard}{: .hash}  

- India's Merchandise exports have witness *growth* from USD279 billion in 2016-17 to USD331 billion in 2018-19
	- [[Measures adopted by Indian Government post-2016 for promotion of merchandise exports]]
- India's share in global trade is less than 2% (1.7% in 2018)
- Net polarity in the exports 
	- 70% of the exports by 5 states - GJ, MH, TN, KN, TL