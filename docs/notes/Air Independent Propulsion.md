---  
title: Air Independent Propulsion  
source: None  
date: 2021-03-17 21:17  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Air Independent Propulsion
**flashcard**{: #flashcard}{: .hash}  

- For diesel electric [[submarines]].
	- Has to come to surface frequently.
	- Using AIP systems, the frequency can be reduced.
- Developed by Naval Materials Research Laboratory (NMRL).