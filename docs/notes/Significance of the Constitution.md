---  
---  
# Significance of the Constitution
### Significance of the Constitution
1. It gives us structure of polity and distributed or specifies the decision making power
2. The Constitution showcases our ideals and aspirations as also out conflicts. It creates and reflects an Indian identity and conveys to what it means to be Indian.
3. What's birth certificate to a child, the Constitution is to a country.
4. Constitution is a common normative home for the diverse elements at the Indian Society.
5. It is a set of basic rules for minium cooperation between various sections.
   - for example
       - Dhamma  - Ashoka's
       - Din-e-ilahi - Akbar's
6. Constitution imposes legally impossible restrictions on the state
7. A written Constitution is essential for a federal setup.