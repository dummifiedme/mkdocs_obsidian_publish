---  
title: Procedure Established by Law  
date: 2021-06-29 21:12  
alias: []  
tags: status/seed  
sr-due: 2022-01-21  
sr-interval: 8  
sr-ease: 250  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity   
# 'Procedure Established by Law'
**Definition** :: ==Limitation only on the power of the executive== (has to act withing the procedure defined in law i.e. no arbitrariness).
- Equality before law (even if you are the King)
- Principle of '*Parliamentary Sovereignty*' which means no restriction on the power of the parliament to make law.