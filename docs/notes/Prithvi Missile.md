---  
title: Prithvi Missile  
source: None  
date: 2021-02-25 13:30  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/IR
[[Missiles]] , 
---

# Prithvi Missile
**flashcard**{: #flashcard}{: .hash}  

Short-range surface to surface ballistic missile.