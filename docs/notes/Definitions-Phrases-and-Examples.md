---  
---  
# Definitions-Phrases-and-Examples

1. **Belief** 

	1. It is an internal feeling that something is true. 

	1. It is what we think about things. An individual usually tends to     internalise the beliefs of people around him or of charismatic     leaders. 

2. **Values** 

	1. Values are things which are valuable to him. They are most     important of all beliefs. Beliefs of a person can be changed easily     but values are very difficult to change. 

	1. For example, Raju may believe Americans by nature are bad,     without any prior objective facts. But if Raju sees an American     helping someone, he may change his belief system accordingly.     That means beliefs can be changed easily. Values take time to     develop, but once they are developed they are hard to change. 

3. **Values of mine** 

	1. **Patriotism:** I believe in the patriotism and the love for country     but it should not be a jingoistic feeling. Patriotism gives you a     drive to work for people, removing inequality in all spheres of     life. 

	1. **Peace:** People are fighting each other for religious, caste,     language and race reasons. Lack of peace in society brings an     ambience of fear, pain, acrimony and injustice. My duty in this     regard lies in germinating brotherhood among people. Violence     begets violence and nothing can be achieved from it. Ex: National     movement. 

	1. **Compassion:** We are all humans first. No doubt we have different     skin colours, religious preferences and political points of view, but     at the end of the day, we still need to take care of one another. 

	1. **Effort:** No matter the outcome, there is always value in the effort     when the effort is authentic and well intended. Krishna in     Bhagavadgita says one should do work diligently without any     expectation of any rewards. This reduces unnecessary     expectations and pressures. Ex: A civil servant should work as     hard as he can even though if such work is not recognised by others and may not lead to personal benefits to him. 

4. **Civil services values** 

	1. For every situation we don’t have time to test the case on ethical     theories such as utilitarianism. So, prescribed values provide time     saving shortcut in many such situations. 

	2. For example, non-partisanship is one value of civil service. So, a     civil servant need not think whether to join a political rally or not.     It is clear from the value that it is plain wrong. 

	3. They reduce moral sacrifices on one’s part. For example,     Objectivity is one of the civil services value. So, a civil servant     can easily remove any family biases in decision making without     any guilt feeling. 

	4. Values have hierarchy. For a judge, value hierarchy should be     first, Justice and then followed by mercy. 

	1. A civil servant has various discretionary powers. He must be     provided with guiding principles to prevent abuse of power. The     foundational values provide these guiding principles. 

5. **Ethics:** Ethics is a set of standards that society places on itself. They     help us decide what is right and what is wrong. So, basically they focus     on conduct or actions of individuals. Ethics are defined by society and     not individually. Also, being ethical is not same as doing whatever     society wants. In many societies, most accepted standards are ethical.     But in some societies most accepted standards may not be ethical. An     entire society can become corrupt. Ex: Nazi Germany. 

6. **Values vs ethics:** Suppose someone highly values success, we will     expect him to be goal oriented. But the method he choose to achieve     that success, either by correct or incorrect ways, is a matter of ethics.     Ex: Concepts such as competition, compromise, hard work may be     values of someone but they are not considered as ethics as they don’t     focus on action. While concepts like honesty, truthfulness, fairness are     used in decision making to decide rightness or wrongness of an action.     Thus they are considered as ethics. 

7. **Codes, principles and ideals:** A code of action will enable a person to     behave with integrity. Principles are the manifestation of values, morals     and ethics. Ex: A person who acts upon principle of transparency may     be reflecting his value of honesty. An ideal is a principle or value that one pursues as a goal, usually in the context of ethics. Ideals are one of high or noble character. 

8. **Morals:** Morals also relate to right or wrong conduct. While ethics are     provided by an external source, like societies, institutions etc., morals     are individual’s own principles regarding right and wrong. Morals     emerge from religion. Thus ethics are dependent on others for definition     while morals stem from inside. 

9. **Moral earnestness:** It means sincerity towards one’s own moral values.     If stealing is bad, a person with moral earnestness would never steal.     Ex: Not giving seat to a old man in Metro, although you are aware of     problems of old man and also you sympathise with him. Ex:     Corruption. 

10. **Ethics vs Morals:** Morals and ethics are usually congruent with each     other as an individual is part of this society only. But sometimes, morals     may come in conflict with ethics. Ex: Defence lawyer for a terrorist. 

11. **Work ethics** 

	1. It represents good attitude towards work. A person with good     work ethics will show strong commitment, sincerity, respect for     time in fulfilment of one’s duties. Work is considered not as a     burden but as an opportunity to serve and constructively     contribute to society. 
	2. As Swami Vivekananda observed, every duty is holy and devotion     to duty is the highest form of worship. PM Modi said in his     August 15 speech said that more than Karya (work), Karya     Sanskriti (work attitude) of the Government is important. 

12. **Dedication** 
	1. Dedication is the quality of being able to give one’s time and     attention to a particular activity, person or a cause. Dedication     gives strength to move on and on without any distraction. If a     person is not dedicated to his goal, he may loose sight of his goal     and may never achieve it. Dedication ensure that duty becomes an     end in itself. 

	1. Dedication is the highest form of commitment. Dedication     suggests that one is devoted even when though there is no formal     commitment. Ex: Police officers not registering the crime case if it     is not in his area. He do not want to take extra responsibility.     Although he is committed, he has no dedication towards crime     free society. 

	1. In AP, MLA slept near graveyard to chase away fear of construction workers. 

	1. Sreedharan, Metro Man, was made MD of DMRC at the age of     65. He completed the project within/before the time and within the     allocated budget. 

13. **Ethical competence** 

	1. Ethical competence is essentially the ability to implement ethics in     public life. It includes judging what constitutes ethically     appropriate conduct and ability to act ethically and communicate     the same to the public. 

	2. **Knowledge:** Ethical competence can be achieved when it is     supplemented with the legal, institutional, political, and cultural     justifications. For example, Article 14 establishes rule of law in     India. The political justification is that India is a democracy where     no citizen is above the law. 
	1. **Reasoning skills** : It is the ability to identify a situation where     there can be ethical conflicts, dilemmas, clash of values etc. For     example, a public servant should identify conflict of interest     situations. It is possible that an officer may be awarding a contract     to a relative.
	2. **Advocacy skills:** It is the ability to communicate a principled     view of the decision to media and the public at large. For example,     displacing tribals from their home forests is a major issue.     Officials must be sensitive to public sentiments and announce     such decisions with clear reasons. 

5. **Self awareness:** Officials need to develop skills in recognising the     various merits and weaknesses of their own positions, and of the     principled positions which may be taken by other officials,     individuals, interest groups, and the State, and building consensus. 

6. **Attitude and commitment:** Even officers of highest integrity     may choose to ignore ethical considerations. Ensuring consistent     ethical behaviour may require attitudinal change in the officials.     For example, a coal secretary may allocate a mining block without     dwelling much on the lives of the poor inhabitants that may be     affected. He may act solely on economic grounds. 

14. **Objectivity** 

1. Objectivity refers to taking decisions on fair basis without any     bias or external influence. It means being truthful, unbiased,     impartial and sticking to facts. Objectivity helps civil servants to     take reasoned decisions and defending them in front of others. For     example, a judicial magistrate with objectivity would go by the     merits of a robbery case rather than being influenced by the public     perception of the culprit. 

2. However objectivity should not be misconstrued as a mechanical     and rigid adherence to laws and rules. There must be an element     of fairness in the decision making. Ex: A judge imposing same     fine on a rich and poor man when both have done same crime may     be objective but not fair. Judiciary need to be fair along with being     objective or one can say that there must be enlightened     objectivity. 

15. **How to inculcate objectivity** 

1. Training. 

2. **Transparency:** After Right to information act, civil servants     today will think twice before taking discretionary decisions,     fearing that he will have to answer it if someone files an RTI. 

3. **Accountability:** Within judicial/administrative procedure, there     should be mechanism for appellate board to review decisions of 

---

 the authorities. Ex: taxation, land acquisition etc. 

4. **Critical thinking:** ASI began gold hunting in Uttar Pradesh, on     order of a union minister who believed a baba. They showed lack     of critical thinking by blindly following dictates of some person. 

5. **Information management:** If you don’t have complete     information, you can’t take objective decisions. Ex: SDGs have 17     goals and 169 targets. Previously in MDGs, we had 18 indicators,     yet we lacked proper statistical databases to compare     performance. 

16. **Subjective situations** 

1. To realise social and economic justice to downtrodden,     administrators have to consider subjective paths but their actions     should be in their jurisdiction, reasonable and have humanitarian     values. Administrator need to take decisions on the spot. To bring     innovativeness, they need certain element of subjectivity. 

2. Our constitution has also some such subjective actions. Though     Supreme court awards death sentences on objective basis,     constitution has provisions to pardon death sentences subjectively     based on good moral conduct during period of sentence. 

17. **Public trust** 

1. It is the firm belief of the people in public offices, institutions and     officials i.e. the measure of public confidence on the working of     the Government. High public trust enables a civil servant to take     bold decisions, whereas, low public trust raises question on every     activity. It can be built by consistent performance and efficient     delivery of expected services. 

2. Election Commission enjoys high trust and this has helped it to     implement 'Model Code of Conduct' even without Legislature's     backing. 

18. **Strength of character** 

1. It is a measure of how much a person can persevere in adverse     circumstances and stand against wrong actions and deeds. It is     determined by how strongly or weakly does one believes and     adheres to certain values. It stands to test in the face of hardships. 

2. This strength helps a civil servant to firmly say no to anyone's ill     demands that may go against the law or interests of general public.     Ex: During riots, passing of tenders, work during disasters and so 

---

 on. 

19. **Tolerance** 

1. Tolerance refers to a permissive attitude towards those whose     opinions, practices, religion and nationality, etc., differing from     one’s own. It is the act or capacity of enduring the diversity of     views and practices in our environment. It is based on the idea of     how to live together in peace and harmony with one another     despite dissenting view points. Tolerance upholds human rights of     dignified life and rule of law. 

2. Public officials need to be secular in outlook. To do this, value of     tolerance is necessary. Tolerance also means being open to diverse     views which is fundamental in our constitution. All India services     calls upon civil servants to serve culturally different people. A     Punjabi civil servant may find it difficult to serve in South India if     he does not have aptitude of tolerance. 

20. **How to be tolerant** 

1. Reading literature. 

2. Working and living in culturally different societies will make one     much tolerant. 

3. Interacting with different set of people in your locality, like people     from different castes, religions, etc. 

4. Writing one’s experiences daily in a diary, will help us keep in     check the violent reactions in us. I made it a practice. 

5. Try working on something where you have to be part of a team,     and communicate with each other to get the job done. 

6. A calm mind is essential for accepting diverse views. Meditation     and physical exercise is helping me to remain calm. 

21. **Selflessness** 

1. It means to put others before oneself to the extent of having little     or no concern for one's life, money, position etc. The job of civil     servant demands that public concerns be the top priority. There     might be situations where an official has to give up family time at     a stretch in order to fulfil professional responsibilities. Further,     selflessness helps in building an organization of integrity and     honesty. 

22. **Courage** 

1. Courage is the virtue that enables a person to restrain fear in the 

---

 face of danger, difficulty or doubt. As Nelson Mandela put it, "Courage is not the absence of fear, but the triumph over it". Courage is the first of human qualities because it’s the quality that guarantees the others. 

2. Courage enables people to face tough consequences for their acts.     For instance whistleblowers like Edward Snowden often pay     heavy price for disclosures. 

3. Without courage it is difficult to display qualities like leadership     which entails laying out roadmaps for the future amidst     uncertainty. For example it is courage that enabled Mahatma     Gandhi to display the virtue of non-violence against the     oppressive colonial regime. 

4. It encourages people to take firm decisions and attempt things that     they have not tried before. For instance, it takes courage to invest     in novel & seemingly impractical/commercially unviable ideas     like the SpaceX. 

5. Various personal, social and professional feats are unthinkable     without courage. Without courage, Raja Rammohan roy would not     have fought against Sati. 

6. Courage is not just dramatic physical heroism. Life provides daily     opportunities and instances of courageous acts like standing up for     injustice, supporting cleanliness in public places, standing up for     rights of minorities, women, etc. However, it must be borne in     mind that courage must stay within limits defined by reason. 

23. **Efficiency** 

1. Efficiency implies doing one’s best in one’s job, with limited     available resources and time to achieve desired objectives.     Efficiency simply does not mean mechanical productivity. For a     genuinely efficient person there is regard for the higher goals of     governance, including public welfare and he devotes himself to     the expeditious achievement of those goals. 

2. Thus, an efficient person is also an ethical person. He or she     possesses administrative morality that is essentially rooted in a     conviction in the desirability of ethical conduct. 

24. **Diligence** 

1. Diligence is the quality of showing perseverance in carrying out     the work while showing careful attention to each and every detail. 

---

 This quality is indispensable to every civil servant considering the complexity of work and great responsibility which comes with it. 

2. For example, in order to implement the anti-corruption laws in a     district administration where corruption is a way of life one needs     to be very careful and attentive with strong will to carry out the     task. 

3. An example of diligent is a worker who always stays late to get     projects done on deadline. An example of diligent is the artist who     paints every strand of hair on a portrait. 

4. But it is difficult to inculcate value of diligence since modern life     tends to value comfort more than hard work. Too many people     want to put out minimum effort for maximum pay. 

25. **How to inculcate diligence** 

1. **By role modelling:** There have been various public personalities     who showed exemplary quality of diligence in their public     conduct. Ex: Sreedharan. Such personalities should be made role     model for civil servants by making them aware of their lives. 

2. **Social recognition:** Giving recognition and rewards to individuals     who practices such quality will provide motivation to fellow civil     servants to adopt such values. 

3. **Giving adequate autonomy:** Freedom from political pressure     will provide the civil servant to actively engage in his work. 

4. **Providing adequate resources:** Diligence not only require     individual will but resource in form of information and means to     carry out the task. Provision of adequate resources will create     conducive condition for inculcation of diligence as practical value. 

26. **Non-partisanship and impartiality** 

1. Non-partisan in public administration means not to side with any     group, especially with political groups and parties. Decisions     should be based on evidence and for ensuring maximum public     welfare and not a specific entity. Advice should be without any     fear of backlash. 

2. Impartiality means that civil servants in carrying out their official     work, including functions like procurement, recruitment, delivery     of services etc, should take decisions based on merit alone. 

27. **Importance of above for civil servants** 

---

1. Impartiality enables the administrator in filling up the gaps of trust     deficit between the subjects and the Government. 

2. Impartiality provides legitimacy to the conduct of administrator     and makes it more effective. For example, in conduct of an     election process, public will measure the legitimacy of an election     on basis of actual integrity of administration. 

3. Non-partisanship strengthens the democratic procedures and     organisation’s credibility. 

4. It strongly attracts pre-requisites of noble administration like     transparency and honesty. 

28. **How to ensure non-partisanship** 

1. Maintaining contact with industrialist, builders, NGOs, politicians     etc., only at the professional level via meetings, conferences etc.     and not at the personal level. 

2. Regulate gift taking from businessmen, etc. 

3. Ensuring transparency via a website for my department where all     decisions and reasons for taking the decision will be uploaded real     time. 

4. Ensuring accountability by assigning time bound tasks to officers     and monitoring the progress. 

5. Citizens participation via 24x7 helpline and regular meetings with     civil society. 

6. Creating a citizen’s charter which will be derived from the ethics     implicit in the constitution and which will serve as guideline in     cases where the law is ambiguous. 

29. **Intuition** 

1. Intuition refers to the ability to acquire knowledge about things     without reasoning or usage of reasoning in general. It is often     conceived as a kind of inner perception. Ex: Gandhi started salt     satyagraha and Quit India movement through his intuition of     grasping the situation. 

2. It develops with age, sometimes maturity, sometimes with     experience, in some with intellect. It teaches, guides and motivates     us. We experience Deja-vu’s because our intuition might have felt     it or comprehended it much before. 

30. **Conscientiousness** 

---

1. Conscientiousness is the personality trait of being careful, or     diligent. Conscientiousness implies a desire to do a task well, and     to take obligations to others seriously. It implies a desire to do a     task well. Conscientious people are efficient and organised as     opposed to easy going and disorderly. 

31. **Prudence** 

1. Prudence is the ability to govern one’s behaviour by the use of     reason. It is often associated with wisdom, insight and knowledge.     Prudence avoids extreme actions and focuses on middle path as     suggested by Buddhism. 

2. All other virtues are regulated by it. Distinguishing when acts are     courageous, as opposed to reckless is an act of prudence. Ex:     Irwin pact, Gandhi calling off non-cooperation movement. 

32. **Temperance** 

1. It is defined as moderation or voluntary self-restraint. This     includes restraint from retaliation in the form of non-violence and     forgiveness, restraint from arrogance in the form of humility and     modesty and restraint from excessive anger or craving for     something in the form of calmness and self control. 

2. Buddhism talks about taking middle path in all actions. This is     nothing but following temperance in one’s action. 

33. **Perseverance** 

1. Steady persistence in adhering to a cause of action, a belief or a     purpose etc., in spite of difficulties or discouragement. Medicine     is a field which requires dedication and perseverance. 

2. Two greatest obstacles for people to overcome in life are failures     and fatigue. Failures may demotivate a person and might deviate     his path. Fatigue may cause boredom and may reduce     effectiveness. Thus perseverance is necessary to overcome both     fatigue and failure. Ex: Leaders of national struggle. 

34. **Activism** 

1. It signifies a bold, fearless and upright civil servant. A civil     servant must show boldness to protest against the immoral     decisions of their political masters. His obligation to the     constitution must override his loyalty to the government. 

2. Ex: Sri Lakshmi, 2g scam. 

---

35. **Honesty** 

1. There is no more fundamental ethical value than honesty. We     associate honesty with people of honour, and we admire and rely     on those who are honest. Honesty involves both communications     and conduct. 

2. Honesty precludes all acts, including half truths, out of context     statements, and even silence during times of moral crisis. 

36. **Integrity** 

1. It is a concept of consistency of thoughts, words, deeds, and     duties. Choosing the right, regardless of the consequence, is the     hallmark of integrity. Integrity is choosing your thoughts and     actions based on values rather than personal gain. 

2. People of integrity are trustworthy because they base their     decisions on moral and ethical principles, not on convenience. 

3. Integrity enabled the official to do the right and legal thing with     conviction and ensure that it sees logical end. A person without     integrity will not have the appetite for a long struggle for what on     feels is right. 

4. In this era of social media and information flow an upright official     with integrity will inspire many and there will be a popular     support to the action of such an officer. 

5. In public service it is said that one’s reputation precedes oneself.     So the quality of an official having high integrity will be well     known to those who approach him. 

6. Character and integrity are more often demonstrated by the way     we handle day to day relationship questions than by any heroic     acts of courage. 

37. **Intellectual integrity** 

1. It is defined as recognition of the need to be true to one’s own     thinking and to hold oneself to the same standards one expects     others to meet. It is to practice what one advocates to others and to     honestly admit discrepancies and inconsistencies in one’s own     thoughts and action. 

2. For example, if you succeed, you’d say you worked hard. If     someone else succeeds, you’d attribute it to his good luck. That     shows lack of intellectual integrity, because you’re not evaluating     everything on same parameter. Suppose if a husband lied to her 

---

 wife about something important, then his behaviour lacks integrity. 

38. **How to inculcate integrity** 

1. Through training. 

2. Through institutional structure such as laws, rules and regulation.     2nd ARC recommends setting up code of ethics for all     departments of the government. Ex: Speaker will monitor how     many times parliament was disrupted, a committee will monitor it     and report will be published. 

3. Select random officer and try to bribe him. This is not same as     CBI/ACB raid, they want to flush out corrupt people. But integrity     testing is done to establish honesty. 

4. If young recruit’s first posting is made under honest officer, then     he’s more like to remain honest for the rest of his life because of     mentoring by a good role model. 

39. **Fidelity:** It is defined as faithfulness to obligations and duties.     Correspondence with fact or with a given quality and adherence to truth     are vital for maintaining fidelity. A public servant is expected to be at     all times a trustworthy person in the public services. An unfaithful     public servant tarnishes the image of the entire system. 

40. **Rapprochement** : Establishing cordial relations with employees and     other people who have direct relationship with the organisation. 

41. **Equanimity** : It is a state of psychological stability and composure     which is undisturbed by experiences of good or bad, pain or pleasure, or     other phenomenon that may cause the normal people to lose the balance     of their mind. 

42. **Rationality** : It is a concept which believes in the use of reason which is     detached with passions, emotions and beliefs. If our personal beliefs or     sentiments are not in conformity with rationality, they should not     prevail over rationality. It means bringing out a practical solution to a     practical situation. 

43. **Apathy:** Lack of interest or concern. It is a state of indifference or not     showing concern, motivation, excitement, passion etc. Being indifferent     towards others problems, towards systemic lapses, towards progressive     change. 

44. **Social accountability** 

1. Social accountability is a process in which ordinary citizens or 

---

 civil society participate in extracting accountability. It does not rely on Government institutions to extract accountability of one another. Conventional methods don’t involve public in defining standards and methods of accountability. 

2. Conventional accountability mechanisms rely on government     agencies to extract accountability. Internal mechanisms like     departmental hierarchy are prone to biases, favouritism and quid     pro quo. Also, conventional methods are of post-hoc nature i.e.     they can look into the propriety of the actions of officials only     after they are done. 

45. **Excellence** 

1. An administrator would ensure the highest standards of quality in     decisions and action and would not compromise with standards     because of convenience or complacency. 

2. In a competitive international environment, an administrative     system should faithfully adhere to the requisites of total quality     management (TQM). 

46. **Fusion** 

1. An administrator would rationally bring about a fusion of     individual, organisational and social goals to help evolve unison     of ideals and imbibe in his behaviour a commitment to such a     fusion. 

2. In the situation of conflicting goals, a concern for ethics should     govern the choices made. 

47. **Responsiveness and Resilience** 

1. An administrator would respond effectively to the demands and     challenges from the external as well as internal environment. He     would adapt to environmental transformation and yet sustain the     ethical norms of conduct. 

2. In situations of deviation from the prescribed ethical norms, the     administrative system would show resilience and bounce back into     the accepted ethical mould at the earliest opportunity. 

48. **Sympathy vs empathy vs compassion** 

1. Sympathy refers to acknowledging another person’s pain and     providing comfort and assurance. 

2. Empathy is the ability to place oneself in another‘s position and 

---

 understand their feelings and experience their emotions. Unless the public officials empathize with the common man, they will not be able to understand the problems faced by him and consequently, public services will not improve. Ex: An empathetic official will ensure ramps in his/ her office premises to aid the movement of the physically disabled. 

3. Compassion refers to a step further, where a person not only feels     empathetic but also a desire to help alleviate the suffering of the     other person. Thus, the emphasis here is on action and wanting to     help. In, other word while Sympathy focuses on awareness,     empathy focuses on experience and Compassion focuses on     action. 

4. For example, if a poor person come to an administrative officer     without adequate documents for a LPG connection. In case, he     just shows his concern, it is sympathy. Further, in case he consoles     the person and tells him that he shares his agony and suffering, it     is empathy. Finally, in case he not only shows his solidarity but     also uses his discretionary powers to allocate him a connection, it     is compassion. 

5. Compassionate Kozhikode is a project by district administration     of Kozhikode to facilitate people who are willing to give to     people who are in need. 

49. **How to cultivate empathy** 

1. Art, literature, cinema can help us inculcate empathy. Ex: Satyajit     Ray’s Pather Panchali realistically portrays the poverty and rural     India. Mother Teresa’s autobiography, No Greater Love, moves     one’s heart. 

2. Encourage perspective talking, role playing games, put yourself in     the shoes of other people. 

3. Visit slums, old age homes, etc. 

4. People of all religions are given public holidays on religious     events. It should encourage them to participate in each other’s     festivals. 

5. Right to education act provides 25% reservation to children from     economically weaker section in the schools. So, rich and poor will     interact with each other and cultivate empathy for each other. 

6. IAS probationers are sent to Bharat Darshan for similar reason of 

---

 understanding the diversity of India and grow compassion towards others. 

50. **Compassion** 

1. An administrator, without violating the prescribed laws and rules,     would demonstrate compassion for the poor, the disabled and the     weak while using his discretion in making decisions. At least, he     would not grant any benefits to the stronger section of society     only because they are strong and would not deny the due     consideration to the weak, despite their weakness. 

2. Helping security guard of my hostel to fill a school application     form for his daughter. Contributing a meagre amount, from my     internship stipend, to an NGO working for children education. 

51. **National interest** 

1. A civil servant would keep in view the impact of his action on his     nation’s strength and prestige. 

2. The Japanese, Koreans, Germans and the Chinese citizens, while     performing their official roles, have at the back of their mind a     concern and respect for their nation. This automatically raises the     level of service rendered and the products delivered. 

52. **Red tapism** 

1. Red tape is excessive regulation or rigid conformity to rules that is     considered redundant or bureaucratic and hinders or prevents     action or decision making. It is applied     to governments, corporations and other large organisations. 

2. This unchecked growth may continue independently of the     organisation’s success or failure. Through bureaucratic inertia,     organisations tend to take on a life of their own beyond their     formal objectives. 

53. **Ethics audit** 

1. It refers to a neutral investigation into how well a company     conforms to the ethical standards of the industry or society     generally. 

2. An ethics audit may consider the company’s own practices, how it     redresses grievances, how it discloses its finances, what kind of     safety it accords to whistle blowers, and even the general cultural     surrounding its business dealings. Ex: Sahara group for siphoning 

---

 public money and loosing public trust, Volkswagen for violation of environment norms, etc. 

54. **Gratitude** 

1. Gratitude is a feeling of being grateful and wanting to express     thanks. It originates from the heart and then opens our eyes to the     beauties of nature. Mark Twain has rightly said I can live for two     months on a good compliment. 

2. Gratitude enable you to understand the importance of others in     your life and make you humble as your existence and success was     possible only because of them. Ex: Rahul Dravid, during his     farewell speech, thanked all the groundsmen, curators, etc., for     preparing pitches so that he could play the match. 

3. Gratitude also pushes you hard to make your contribution tangible     towards something and someone that has shaped you. Ex: Usain     Bolt does his advertisement shoots in Jamaica, so that his people     would be benefitted. 

4. Civil servants have to remember always that power and authority     that has been vested to them is for the wellbeing of the people and     they express their gratitude to the people by providing them     satisfactory service. 

55. **Magnanimity** 

1. Magnanimity is a quality of being kind, generous and forgiving.     Person filled by magnanimity do not seek revenge. Mahatma     Gandhi chose magnanimity over revenge as his guide for making     his decisions. Ex: According to christianity Jesus gave his life for     benefit of human kind. 

2. Civil servants sometimes work in very adverse and hostile     circumstances. In those critical houses, they are supposed to forget     their personal interests and issues and work towards happiness of     people. Ex: Nelson Mandela did not show any retribution against     white people when he became president. 

56. **Humility** 

1. Humility is the quality of being humble and thinking that you are     not superior to anyone. It also means not drawing attention to     yourself. 

2. For example, even though Narayan Murthy is a billionaire, he 

---

 cleans his toilet by himself. Also, being a parent can be a very humble job, wiping noses, changing diapers, and meeting a child’s every need for years. 

3. Humility asks us to acknowledge our imperfections. It requires     that we admit when we are wrong and then change course. 

4. People look towards civil servants with suspicion, because they     find most of the civil servants are full of arrogance, egoist, and     power drunk. Civil servants need to dispel this notion from the     minds of people by becoming humble. 

57. **Reliability** 

1. Reliable persons are more trusted than an unreliable person. By     making a promise to someone we are making ourself morally     accountable to them, and by not keeping up we are doing morally     incorrect thing. 

2. We should interpret your promises fairly and honestly and avoid     unwise commitments. Think about unknown or future events that     could make it difficult, undesirable or impossible. 

58. **Loyalty** 

1. Loyalty is a responsibility to promote the interests of certain     people, organisations. This duty goes beyond the normal     obligation we all share to care for others. 

2. We must rank our loyalty obligations in some rational fashion. For     example, it’s perfectly reasonable to look out for the interests of     our children over other children. 

3. Loyalty requires us to keep some information confidential. When     keeping a secret breaks the law or threatens others, however, we     may have a responsibility to blow the whistle. 

4. Employees and public servants have a duty to make all     professional decisions on merit, unimpeded by conflicting     personal interests. They owe ultimate loyalty to the public. 

59. **Respect** 

1. People are not things, and everyone has a right to be treated with     dignity. We certainly have no ethical duty to hold all people in     high esteem, but we should treat everyone with respect, regardless     of who they are and what they have done. 

2. We have a responsibility to be the best we can be in all situations, 

---

 even when dealing with unpleasant people. 

3. Respect prohibits violence, humiliation, manipulation and     exploitation. It reflects notions such as civility, courtesy, decency,     dignity, autonomy, tolerance and acceptance. 

4. A respectful person is an attentive listener, although his patience     with the boorish need not be endless. Nevertheless, the respectful     person treats others with consideration, and doesn’t resort to     intimidation, coercion or violence except in extraordinary     situations. Punishment is used in moderation and only to advance     important social goals and purposes. 

60. **Responsibility** 

1. Being responsible means being in charge of our choices and our     lives. It means being accountable for what we do and who we are.     It is a moral obligation. Ethical people show responsibility by     being accountable, pursuing excellence and exercising self     restraint. They exhibit the ability to respond to expectations. 

2. Responsible people finish what they start, overcoming rather than     surrendering to obstacles and avoid excuses. Responsible people     always look for ways to do their work better. 

3. Responsible people exercise self control, restraining passions and     appetites (such as lust, hatred, gluttony, greed and fear) for the     sake of longer term vision and better judgment. They delay     gratification if necessary and never feel its necessary to win at any     cost. 

4. An accountable person doesn’t shift blame or claim credit for the     work of others. He considers the likely consequences of his     behaviour and associations. He recognises the common complicity     in the triumph of evil when nothing is done to stop it. He leads by     example. 

61. **Fairness** 

1. Fairness implies adherence to a balanced standard of justice     without relevance to one’s own feelings or inclinations. It includes     issues of equality, impartiality, proportionality, openness and due     process. It is unfair to handle similar matters inconsistently. 

2. A fair person scrupulously employs open and impartial processes     for gathering and evaluating information necessary to make 

---

 decisions. Fair people do not wait for the truth to come to them and they seek out relevant information and conflicting perspectives before making important judgments. 

62. **Caring** 

1. If you existed alone in the universe, there would be no need for     ethics and your heart could be a cold, hard stone. Caring is the     heart of ethics, and ethical decision making. 

2. It is scarcely possible to be truly ethical and yet unconcerned with     the welfare of others. That is because ethics is ultimately about     good relations with other people. 

3. Of course, sometimes we must hurt those we truly care for, and     some decisions, while quite ethical, do cause pain. But one should     consciously cause no more harm than is reasonably necessary to     perform one’s duties. 

4. The highest form of caring is the honest expression of     benevolence, or altruism. This is not to be confused with strategic     charity. Gifts to charities to advance personal interests are a fraud.     That is, they aren’t gifts at all. They are investments or tax write     offs. 

63. **Forgiving** 

1. To err is human, to forgive is divine. Everyone makes mistakes,     commit sins at some point in their life. It’s a human nature to     make mistakes. But to forgive someone from those mistakes is     indeed very hard. It doesn’t happens naturally like making a     mistake. People are acting in a godlike (divine) way when they     forgive. One almost have to be in a real holy place in their mind to     forgive someone. 

2. It is morally correct to forgive such a person who accepts his     mistake and promises not to repeat it. It is so because someone is     showing courage and honesty by accepting his mistake with a     promise of not repeating. His thoughts are noble and gestures are     right. Forgiving him will help in making him a better human     being. 

3. Lord Rama showed no hatred against his stepmother, Kaikeyi, for     sending him to the forest for fourteen years so that her son,     Bharat, could be the king. 

4. All of us are humans. We all have our weaknesses and strength. 

---

 No one is perfect. So when someone does a wrong to us, why can’t we forgive them. Forgiving that person brings out the god like nature that lies in human. It is not very easy to do it but once we truly forgive someone for his mistake we will feel an inner peace. 

64. **Trust** 

1. Trust means that you rely on someone else to do the right thing.     You believe in the person’s integrity and strength, to the extent     that you are able to put yourself on the line at some risk to     yourself. 

2. Simply refraining from deception is not enough. Trustworthiness     is the most complicated of the six core ethical values and concerns     a variety of qualities like honesty, integrity, reliability and     loyalty. 

3. A team without trust isn’t really a team. It’s just a group of     individuals, working together, often making disappointing     progress. They may not share information, they might battle over     rights and responsibilities, and they may not cooperate with one     another. 

4. It doesn’t matter how capable or talented your people are, they     may never reach their full potential if trust isn’t present. 

5. Trust is essential to an effective team, because it provides a sense     of safety. When your team members feel safe with each other,     they feel comfortable to open up, take appropriate risks. 

6. When trust is in place, each individual in the team becomes     stronger, because he or she is part of an effective, cohesive group.     When people trust one another, the group can achieve truly     meaningful goals. 

65. **Strategies for building trust** 

1. Don't make unrealistic promises. Always fulfill the promises     made. Walk the talk once a promise is made. 

2. If you want to build trust within your team, then lead by example,     and show your people that you trust them. 

3. You cant be honest to one person and dis-honest to others.     Maintain integrity in building trust. 

4. Speak truth as a matter of habit. A single lie to a friend or loved 

---

 ones can break all trust forever. 

5. One way to build trust is to develop inter-personal relations     among colleagues. Think about creating situations that help them     share personal stories, and bond. Do this by asking sensitively     about their family, etc 

6. Don’t place blame on others. When people work together, honest     mistakes and disappointments happen, and it’s easy to blame     someone who causes these. However, when everyone starts     pointing fingers, an unpleasant atmosphere can quickly develop.     This lowers morale, undermines trust, and is ultimately     unproductive. 

66. **Anonymity** 

1. Bureaucrat is supposed to work behind the curtain and avoid     limelight. He will not get credit for the success and he will not be     blamed for the failure. It will be responsibility of the political     executive to handle all the applause and criticism. For example, in     Mundhra deal scam (1957), Chagla commission held that Minister     Krishnamachari is constitutionally responsible for the actions of     his secretary. 

2. A civil servant should avoid going to media to air his grievances     or differences of opinion. Minister must be given the power to     extra work, power to reward and punish. 

3. It allows civil servant to not fall for political compulsions and     public sentiments but be rational in his decisions and actions. He     is not worried about various pressure groups. He quietly works in     the background in public interest. It allows them to advice to     ministers freely and without fear of any adverse public or political     reactions and without fear of damage to their future careers. 

4. Anonymity has been used by civil servant to shed accountability     and refusal to be held responsible even for their malaise action.     They have derilicted duty and hid behind the cloak of ministerial     responsibility for their collusion in corruption citing anonymity.     He is liable for his actions and anonymity cannot be an excuse. In     the 2G scam case supreme court held that telecom secretary     cannot hide behind anonymity to defend his malafide inaction. 

67. **Conduct rules providing anonymity** 

---

1. He shall guard the official secrets. 

2. Shall not make any public utterance that would embarrass     relations between Union and state or between two countries. 

3. He should not criticise any policy of government. 

4. Needs government permission before publishing book or     appearing on TV. Without government permission, he must not     accept any honour, ceremony, meeting, rally held in his honour. 

5. Suppose press has made some remarks against him for his official     conduct. He cannot file defamation suit against them or make     press statements, without government permission. This ensures     discipline, decorum and moral of the services. 

68. **Arguments against anonymity** 

1. Often ministers come up with populist schemes with unattainable     targets and then blames officers for not implementing it faithfully. 

2. Ministers openly criticises bureaucrats but bureaucrats can’t     defend because norms of anonymity. Still they have to face the     people protesting against the state. 

3. Often the entire state bureaucracy is corrupt and inefficient. So,     despite using the official channels, the honest officer may not get     any justice. 

4. Norm of anonymity will stop him from approaching media. And     still if he approaches media, he will be further persecuted for     violating the service norms. 

69. **Happiness** 

1. The source of happiness is the mind that directs a person to be     happy even in circumstances that are difficult and obstacles that     may seem insurmountable to some. It is rightly said that happiness     is a state of mind. The people who manage to stay happy despite     calamity are the ones who have most of the following traits. 

2. **Contentment:** The first rule to stay happy is to be content with     whatever life gives you. One may be ambitious and put in great     efforts but satisfaction at the end of the activity must be there. It is     soothing and inspires a person to struggle. 

3. **Will power:** An indomitable will power will encourage the person     to find a way even at places where there will be none. 

4. **Positive outlook:** A person with positive outlook will always look 

---

 for constructive plans rather than destructive ones. To construct something a new is anyways an inspiring activity which gains appreciation from society and instills confidence in the person. 

5. **Passion:** To move in a direction in which one is passionate is     itself soothing. When way to destination itself becomes enjoyable,     destination is not far away. Happiness, in this case, lies not only in     destination but journey itself. 

70. **Qualitative life** 

1. The meaning of quality life varies from individual to individual.     But quality life should include the pursuit of material satisfaction,     responsibility of family, society and sharing love and spiritual     enlightenment. 

2. **Artha:** Wealth may be accumulated both ethically (businessmen)     and unethically (drug smugglers). But, unethically accumulated     wealth does not give lasting happiness. Parties to the Bofors scam     or 2G scam cannot claim to sleep peacefully at night. 

3. **Kama:** Pursuit of pleasures should not be at the expense of others.     Pleasure which stems from the world, whether it is a pleasure of     the senses or a pleasure derived from one’s position or situation in     the world, is a distraction from a higher good. For example, a civil     servant may cheat or lie in order to get quick promotions, but this     alone is unlikely to improve his career. 

4. **Dharma** : For example, financially independent adults do not only     have the responsibility of their own children, but of their parents,     the society and nation as a whole. It is said that the good of one is     contained in the good of all. By sharing happiness and love, we     not only derive personal satisfaction, but also give happiness to     others. 

5. **Moksha** : Pursuit of true knowledge is unlikely with unethical     conduct. All scriptures, teachers and saints advise to first purify     one’s conduct and character and then embark on a spiritual quest.     Selfless work, the highest ethics, is one of the virtues taught by     Gita. This alone ensures lasting happiness and thus a quality life. 

71. **Justice** 

1. Justice is fairness in protection of rights and punishment of     wrongs. Reformative justice places emphasis on reforming the 

---

 offender. It doesn’t not believe in proportionate punishments like death penalty. Retributive justice demands proportional punishment for the crimes committed. They believe criminals must pay for their crimes. Ex: Death penalty. 

2. I believe in reformative justice. To err is human and our society     should try to reform the offenders and give them a second chance     to redeem themselves. No correlation between retributive justice     and reduction in crime rate. This defeats purpose of justice. 

3. There is a difference between law and justice. The essence of law     is its force. Law is law because it carries the means to coerce or     force obedience. The power of the state is behind it. The essence     of justice is fairness. Any system of laws functions through a     hierarchy of authorities. 

4. Justice for a public servant is redressing the problems of an     aggrieved citizen in an effective, efficient and equitable way. 

72. **Ways to ensure justice** 

1. By being honest and impartial and avoiding nepotism. 

2. By being empathetic and compassionate towards each aggrieved     person and addressing their problems within a time limit. 

3. By following code of conduct and code of ethics while     discharging my duties. 

4. By being transparent and accountable to my actions. 

5. By building social capital with all stake holders to bridge the trust     deficit. 

**Vinay's lecture** 

1. **Confirmation bias** 

2. **Wishful thinking:** Unrealistic thinking. Not pragmatic. People indulge     in wishful thinking to satisfy their self-esteem. Trump wants to build a     wall between US and Mexico. 

3. **Cognition:** Mental process of acquiring knowledge through thought     experience and senses. 

4. **Pygmalion effect:** Others expectation of a target person, affects the     performance of target person. 

5. **Stereotyping:** Over-generalised belief about a particular category of 

---

 people. Ex: Men don't cry. Women don't play video games. 

6. **Fundamental attribution error:** It is the tendency people have to     overemphasise personal characteristics and ignore situational factors in     judging others' behaviour. 

7. **Red Herring:** Something misleads or distracts from something that is     relevant. Ex: Political manifestos. 

8. **Eco-chamber effect:** Your beliefs are re-inforced through repeated     sharing of same kind of information. Ex: Facebook provides you same     type of articles you like or share. This clouts you from other side of the     opinion. Ex: Radicalisation. 

9. **Dis-information** (wrong information with malafide intention), **Mis-**     **information** (Wrong information created without any malafide     intention) and **Mal-information** (Right information spread to cause     harm to a particular group). 

10. **False consensus:** Under false notion that one's morals or beliefs are     widely shared. 

11. **Bounded ethicality:** Ability to make ethical decision is limited. The     limitations arise from internal or external factors. 

12. **Conformity bias:** It refers to our tendency to take cues for proper     behaviour from the actions of others rather than exercise our own     independent judgment. Ex: Supporting Mob lynching. 

13. **Ethical fading:** Ethical fading is similar to moral disengagement.     Moral disengagement is when people restructure reality in order to     make their own actions seem less harmful than they actually are. 

14. **Diffusion of responsibility:** People not attending to road accident     victims thinking someone else would save them. 

15. **Fiduciary Duty:** Relationship between two parties that obligates one to     act solely in the interest of the other. Ex: Lawyer-Client, Accountant-     Client, etc. 

16. GroupThink 

17. **Incrementalism:** Belief in or advocacy of change by degrees;     gradualism. 

18. **Loss aversion:** Loss aversion refers to people's tendency to prefer     avoiding losses to acquiring equivalent gains. 

19. Moral absolutism 

20. Moral agent 

---

21. Moral cognition 

22. Moral equilibrium 

23. Moral muteness 

24. **Moral myopia:** Moral myopia refers to the inability to see ethical     issues clearly. 

25. Moral pluralism 

26. Moral reasoning 

27. Obedience to authority 

28. **Over-confidence bias:** The overconfidence bias is the tendency people     have to be more confident in their own abilities, such as driving,     teaching, or spelling, than is objectively reasonable. 

29. Pro-social behaviour 

30. **Rationalisation:** Finding reasons to justify your actions. Ex: My father     asked to steal. 

31. **Role morality:** It is the notion that people sometimes fail to live up to     their own ethical standards because they see themselves as playing a     certain role that excuses them from those standards. Ex: Celebrities not     using the shampoo they promote. 

32. **Self-serving bias:** The self-serving bias is people's tendency to attribute     positive events to their own character but attribute negative events to     external factors. Ex: Politicians taking credit. 

33. Social contract theory. 

34. Wheel of ignorance. 

35. Subject of moral worth. 

36. **Deontology:** Means more important than the ends. Lying is always     wrong. 

37. **Utilitarianism:** Ends are more important. Maximum good to maximum     number of people. 

38. **Applied ethics:** Applying rules of ethics to real world. 

**Examples** 

1. **Ethical actions hurting someone else** 

1. A new road is proposed, in a Tribal area, to connect the hamlet     with the town, but that will cause the tribal to lose some fertile     farmland. The road plan will bring larger benefits to the whole 

---

 community like access to hospital, schools and the market to sell their produce. So, in such a situation though the genuine interests of some are hurt by the benevolent action, it is beneficial to everyone in the long run. 

2. A new mining project is proposed in a forest area, which has some     rare endemic plants and animals. In such a situation though the     mining will have economic benefits to the community, it will lead     to habitat destruction of the rare plants and animals. 

2. **Laws and ethics** 

1. Recently proposed Transgender Persons (Protection of Rights)     Bill, 2016 gives certain discretionary power to District Magistrate     while issuing certificate of identity. A parochial mindset     bureaucrat might try to tamper with laws to act accordingly,     without violating law. 

3. **Legal vs legitimate** 

1. While the Government was in the right when it enforced the ban     on Jallikattu, the decision cannot be termed as legitimate     considering that it offended the cultural sensibilities of a people     celebrating a festival from generations, and it was taken abruptly     without consulting the people most involved in it. 

2. While the Government is legally allowed to acquire land to     construct infrastructure, the decision should be taken considering     the legitimate interests of the people whose land is to be acquired. 

4. **Campaigns of Govt based on moral principles** 

1. **Income Declaration:** Granted tax evading citizens a way out     while making an appeal for everyone to pay their rightful taxes.     Adverts run by the Govt appealed to the citizens to pay their taxes     to contribute to nation building. 

2. **LPG subsidy:** Made an appeal to economically well off citizens     to surrender their LPG subsidy. The savings were then diverted to     fund PM Ujjwala Yojana. 

3. **Good Samaritan:** To ensure immediate help for road accident     victims in the Golden Hour, the Govt declared that the bystanders     helping the victim would not be harassed by the Police and even     given a little certificate of recognition. 

4. **Reservations:** Aims to right the wrongs of our previous     generations by extending all the support possible for the     upliftment of economically and socially backward classes. 

---

5. **Rights vs Duties** 

1. A civil servant not performing his duties or obligations could     mean denial rights to 1000s of citizens. 

6. **Bio-ethics** 

1. The parents of seven year old Avinash rout were turned away by     the best hospitals in the capital and, by the time they admitted the     child to Batra Hospital, he was close to cardiovascular failure.     Devastated at having failed to protect their child, his parents killed     themselves after performing his last rites. Similar tragedies play     out every day in hospitals across the country. Patients and their     families are so inured to it that comment seems redundant. The     case of Avinash Rout, too, would have gone unnoticed but for the     suicide of his parents. 

7. **Honesty** 

1. Sir M. Visvesvrayya, then Dewan of Mysore state, used     Government vehicle while he went to tender his resignation. After     tendering his resignation, he drove back by his private vehicle. 

2. For instance, Nehru believed in being low on tips during official     tours but was a liberal tipper on personal holidays. 

8. **Intellectual honesty and freedom** 

1. Submitting to intellectual scrutiny. Anyone can question him/her     and his/her philosophy. Also, maintaining honesty while     transmitting your knowledge in the public space. Research     scientist not fudging facts. Raghuram Rajan and Vinod Rai not     succumbing to pressure. Many arguing in English on news     channels to stop english penetration. 

2. Intellectual freedom is freedom of mind and conscience. Ex:     Questioning old beliefs, ideas and to pursue knowledge. 

9. **Empathy** 

1. Alexander Fleming did not patent his penicillin drug in order to     increase its affordability. This shows compassion towards poor     and lack of greed. 

2. An engineer, under Abdul Kalam, promised to take his children to     a park and said same to Kalam for his permission. However, he     got so much involved in the work and forgot about it. Kalam sir     observed the engineer being engrossed in the work. So, he took     the engineer's children to exhibition. 

3. A farmer from Tamil Nadu protesting in Delhi attempted suicide 

---

 out of frustration as there is no response from governments to their agitation. This shows apathy from Government. 

4. Recently a techie was murdered in day light. Many people stood     their watching the act without stopping it. This shows complete     apathy. 

5. K. Jairaj, Karnataka cadre HAS officer, was to approve the     dismissal of a lady typist on the grounds of unruly behaviour by     her against her colleagues. But he found that she had been widow     and one co-worker mis-behaved her which made her lose her     temper. Later, considering her precarious financial condition and     need to educate her son, two increments were cut and she was     reinstated to service. Later, her son got a very good job in Infosys. 

10. **Integrity** 

1. Some of the civil servants suffered due to integrity are Ashok     Khemka, S. Manjunath, Durga Shakti Nagpal, etc. 

2. However, integrity brought respect for civil servants like TN     Seshan, Armstrong Pane, etc. Late officer SR Sankaran was     rightly called “an ideal people’s IAS officer”. He was responsible     for abolition of bonded labour. 

3. Gandhi mis-spelt the word ‘kettle’ when an english education     instructor asked students of Gandhi’s class to write the word.     Despite class teacher insistence, Gandhi did not correct the word     and maintained his honesty. 

4. Gandhi organised workers of Ahmadabad mill against the the     owners, who were helping Gandhi to run the Sabarmati Ashram,     without the fear that such aid may discontinue. This shows     integrity on part of Gandhi. 

5. Politicians being public representatives must practise high     standards of morality and integrity. But these politicians often get     caught in corruption cases, dowry harassment, etc. 

11. **Integrity without knowledge** 

1. Panchayati Raj Institutions (PRIs). 

2. Self Help Groups (SHGs). 

12. **Transparency** 

1. Sagayam, an IAS officer from Tamil Nadu, has disclosed his and     his family’s assets on the website. 

13. **Humility** 

1. YV reddy, previous RBI governor, used to travel in metro. 

---

2. Narayan murthy didn’t change his way of living. He cleans his     toilet by himself. 

3. One day, Nehru kept eminent statistician and Planning     Commission member PC Mahalanobis waiting beyond the     appointed time. After waiting for several minutes, the scholar     walked off, declaring that he would not wait for a prime minister     who failed to keep his time. When Nehru heard about this, he did     not get angry but instead managed his appointments better. 

4. A media person once asked former PM Indira Gandhi “What have     been your gains from the emergency?”. To which PM replied, “In     those 21 months, we comprehensively managed to alienate all     sections of Indian people.” This shows how self correction is     always a better option than self-justification. 

14. **Courage** 

1. D.Roopa whistle blower against preferential treatment meted out     to Jayalalitha. 

2. Edward Snowden. 

3. Satyendranath Dubey exposed corruption in NHAI despite death     threats. He was killed ultimately. 

4. Girish karnad vocal on many issues despite many death threats. 

5. Prakash Singh committee’s report has pointed to the fact that     many civil servants abdicated their duty and ran away, during the     recent Jat reservation stir in Haryana. 

15. **Justice** 

1. Economic injustice will lead to political injustice. Indeed,     Ambedkar saw this when he remarked that adopting the     constitution we were entering a world of contradictions, where we     try to be politically just in economically unjust situation. 

2. Women’s role in family and patriarchal values results in her     exploitation in social sphere as well. 

3. Mob lynching. 

4. USA bombing of Syria, which is leading huge casualties. 

16. **Leadership** 

1. Satish Dhawan was the chairman of ISRO during the first launch     of SLV, which was a failed mission. He took the responsibility for     failure. In the next attempt, when the launch was successful, he     gave full credit to the team that had worked for it. 

---

2. Open to change -- For example, a district collector in Manipur     used social media for crowd funding for construction of a 100 km     road. 

3. Credibility and trust of the people -- Compassionate Kozhikode     initiative to feed hungry people. 

4. Motivate and inspire sub-ordinates -- In disaster related     operations. 

17. **Tolerance** 

1. Moral policing. 

2. Attacks on valentines day, attacks on TN state people in     Karnataka show lack of tolerance. 

18. **Loyalty** 

1. A Rajput Prince was conspired to be killed. Panna who worked in     the court learnt the conspiracy. In order to save the Prince, she     replaced the Prince with her own kid. Her own kid got killed. 

19. **Prudence** 

1. Gandhi stopping non-cooperation in the wake of movement     getting out of hand with the Chauri-Chaura incident. 

2. India not attacking Pakistan militarily post Uri attack in light of     the nuclear threat can be construed as an act of prudence. 

20. **Discipline** 

1. Recent Doctor's Bandh affected so many people and denied their     freedom. Similarly many Rail rokos, Bandhs, etc deny daily     commuters their freedom. 

2. One who is uncontrolled is anything but free — Sister Nivedita. 

21. **Contentment** 

1. IAS officer need to be contended with his salary. But try to     achieve excellence. Raise the bar high. Ex: Sachin, Kohli, etc. 

22. **Self scrutiny and fault finding** 

1. Gandhi scrutinised his entire life. Ex: His visit to brothel, drinking     wine, etc. He regretted them. This made him better human being. 

23. **Purpose** 

1. Arjuna’s bow hitting right at the fish eye could not have been     possible had he not shown a sense of purpose and direction in his     goals. His courage and efforts could have failed him if he was     directionless. 

24. **Anger** 

---

1. Controlled use of anger and fault finding in specific situations is     sensible. For example, a teacher or mother can use anger as a     means to correct indiscipline of a child. This is last option when     other soft options have not worked. 

25. **Be the change you wish to see** 

1. PM Modi set example by himself cleaning the road before     promoting Swatch Bharat campaign. 

2. Japan gave up nuclear stockpiling for promoting nuclear     disarmament. USA’s double standards. 

26. **VIP culture** 

1. Ending red beacons on cars. 

2. Air travel ban on an abusive MP. 

3. Swift justice in the Chandigarh stalking case. 

4. Other issues like paying for security, standing in queues while     voting, ending mis-use of of official resources (cars, etc). 

27. **Work ethics** 

1. Many work at higher ages because of their love towards work. Ex:     Ram Jatmalani at age of 90 still practises at supreme court. 

2. Sridharan at age of 75 is still busy working for developing     metros. 

28. **Corporate ethics** 

1. Volkswagen giving wrong information regarding its pollution     emissions. 

2. Satyam fraud where the company heads fudged the facts to show     more profits, etc. 

3. Axis Bank has recently made higher pays for its top management     officials, despite troubled financial Year. The display of such     largesse shows poor corporate governance. 

4. Mehul Choksi and Vijay mallya. 

5. Johnson and Johnson facing trail over presence of asbestos in     baby powder. 

29. **Teacher** 

1. PV Sindhu, who earned silver medal for India, in Olympics this     year, could do so only because of her teacher or coach. 

2. Sachin and Ramakant Achrekar. 

30. **Attitude** 

1. The fact that attitudes are more important than abilities is best 

---

 shown by the renowned astrophysicist Stephen Hawking. Being paralysed from neck down did not stop him from using his brains to the best of his abilities to supply breakthrough theories in physics. 

2. Swapna Barman -- Born with six toes. Didn't stop her from     becoming first Indian to secure a gold medal in heptathlon at the     Asian Games in 2018. 

31. **Behavioural attitudes** 

1. Population control policy cannot be uniform in India. In certain     districts people have negative attitude towards use of     contraception. In those cases behavioural changing component of     the scheme must be the major component. In other districts     distribution of contraceptive may be the major component. 

32. **Inaction** 

1. In the 2G scam, Supreme Court did not accept the argument of     telecom secretary that he was merely following orders of minister.     Court held that his inaction to prevent public loss amounted to     criminal activity. 

33. **Good intentions producing bad results** 

1. Reservations. 

2. Power of discretion granted to public officials. 

3. AFSPA. 

4. Gun violence in USA. 

5. NDP in education leading to poor standards. 

34. **Bad actions with good intentions** 

1. Clinical trials. 

2. Banning liquor. 

35. **Technology** 

1. Recently 4 school kids used dark web to procure drugs. It just     took them 1.30 min to order the drugs which has stunned the     investigators. 

36. **Animal ethics** 

1. Jallikattu, cock fights, animal sacrifices, captivity of animals. 

37. **International funding** 

1. American funding of Pakistan under its coalition support fund,     even when funds are being diverted by Pakistan. 

38. **Rights violations** 

---

1. India has supported UN actions in Kuwait, Korean crisis, Rwanda     genocide, etc. Apart from this, India has been providing     peacekeeping forces in various African nations like Liberia,     Sudan, etc. 

2. 1971 saw blatant violations of Human Rights in East Bengal,     where India intervened actively and ultimately succeeded in     making it Bangladesh. 

39. **Instant fame** 

1. For example a local artist group in a remote village in Rajasthan     posted their songs video on youtube which became the most     viewed video of that year. 

2. How many of us remember Joginder Sharma, the cricketer who     bowled India to 2007 T20 world cup victory. 

40. **Freedom of institutions** 

1. The first president of the Sahitya Akademi was the then Prime     Minister himself. Nehru claimed, ‘As President of the Akademi, I     may tell you quite frankly I would not like the Prime Minister to     interfere with my work’. 

41. **Forgiveness** 

1. India has repeatedly forgiven Pakistan despite its state sponsored     terrorism targeting India. India tried to improve relations even     after deadly 2008 Mumbai attack. But Pakistan had viewed     forgiveness as a sign of weakness. So, India has changed it stance     now. It is giving befitting reply. 

42. **Leading by example** 

1. R. Anand kumar -- Erode collector sends his daughter to Govt     school. 

2. President Kovind -- Replaced all plastic bottles in his office by     glass bottles. 

43. **Examples** 

1. Muslim ministers resigning in Sri Lanka after terror attacks due to     intense pressure from some sections -- Hatred and anger,     Prejudices, lack of EI. 

2. Making Hindi compulsory -- Impracticality, Ignorance to other's     people opinions, Coercion. 

3. Congress MLAs defecting to TRS -- Breaking people's trust,     moral corruption, against democratic principles, bad political 

---

 attitudes. 

4. Kathua rape case -- Charge-sheet was prepared in two     months, trial lasted a year, and the verdict has been delivered     within 17 months of the occurrence -- Justice, Diligence, inspire     confidence in justice system, etc. 

5. Girish Karnad -- Courage (Vocal stand on issues despite death     threats), fearless and principled defenders of freedom of speech,     commitment (even in his frail health, he made it a point to attend     protest gatherings). 

6. TN Seshan cleaned the electoral system by effectively     implementing MCC -- Dedication towards public service, ethical     governance, moral courage, etc. 

7. Hima Das -- Success despite adversity. Positive attitude.     Determination. Strong inspiration. Donated salary to Assam after     the recent floods. 

8. Dutee Chand -- Courage. Breaking stereotypes. 

9. Armstrong Pame -- Without any help from government he     constructed 100KM road between two villages in Manipur. This     shows his dedication, determination towards nation building. It     also shows compassion, strong work ethic, outcome oriented, etc. 

10. E Shreedharan -- Constructed Delhi Metro with limited resources,     limited time and manpower. Dedication, utmost integrity, public     service, right attitude. 

11. OP chowdary -- Transformed Dantewada into a vibrant &     progressive district through unorthodox and path-breaking     initiatives. Dedication, Out of box thinking, positive attitude,     public service, etc. 

12. In 1999, Deepa Malik was diagnosed with a spinal tumour that left     her paralyzed. In 2016, Deepa Malik became the first Indian     woman to win Paralympics medal. 

13. The Padman of India -- Attitude not being affected by any external     influences; Strong moral base; Empathy and sympathy     epitomised. 

14. Saalumarada Timakka -- Padma shri, she has planted trees and     watered them in an entire 10km stretch for decades. She didn’t     have children and saw trees as her childern. Attitude, 

---

 environmental ethics. 

15. JK Rowling -- Donated much of her wealth to charity and the     social service programme of UK. When asked why she did this,     she commented that social security had sustained her during hard     times and she wanted to help someone the same way. Gratitude,     Social solidarity, Distribution of wealth, etc. 

16. Denis Mukwege -- Nobel Peace Prize Winner 2018. Fought for     victims of sexual violence in armed conflicts. Although he has     been a target of attempted murder, he never backed down. Shows     Courage, empathy, compassion and service to the society. 

17. Thailand cave rescue in 2018 -- International cooperation, use of     social media. 

18. Steve smith and Warner ball tampering -- Violation of trust, sports     ethics 

19. Abraham Lincoln -- Leadership, Humble background,     accommodating different view points, controlling emotion, out of     box thinking. 

20. Rajinikanth Mendhe -- A 29 year-old teacher in Maharashtra     travels 50 km to teach his only student in a village. 

21. Bandicoot -- Robot to eliminate manual scavenging.     Compassionate youth using technology to solve social ills. 

22. Hitler, Napoleon and emergency -- Abusing power. 

---