---  
title: Meri Saheli Initiative  
source: None  
date: 2021-02-12 21:29  
tags: status/mature  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/social-issues/women 
# Meri Saheli Initiative
**flashcard**{: #flashcard}{: .hash}  

- an initiative by Indian Railways to focus on women safety
- for helping out the women traveling alone
	- details of journey like coach and seat number are noted
	- they are provided with helpline numbers and precautionary brief.
	- then feedback after journey
- initially started in Southern zone as a pilot project