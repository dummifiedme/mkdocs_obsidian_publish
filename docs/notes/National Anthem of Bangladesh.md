---  
title: National Anthem of Bangladesh  
source: None  
date: 2021-05-20 01:55  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity
# National Anthem of Bangladesh
**flashcard**{: #flashcard}{: .hash}  

"Amar Sonar Bangla" by Rabindranath Tagore.
> Was part of a movement against the [[Partition of Bengal Province]] in 1905 as well.
<!--SR:!2021-09-06,13,250-->