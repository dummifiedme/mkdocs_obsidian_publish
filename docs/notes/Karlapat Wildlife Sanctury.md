---  
title: Karlapat Wildlife Sanctury  
source: None  
date: 2021-02-17 00:47  
tags: status/seed  
aliases: []  
---  
Category:: [[🗃️ Environment]] , [[Keywords GS3]]

---

# Karlapat Wildlife Sanctury
**flashcard**{: #flashcard}{: .hash}  

- Kalahandi, Odisha
- Recently, 6 elephants died due to [[Haemorrhage Septicaemia]]
- First notified in 1969 as sanctuary.
- Formally notified under the Wild Life Protection Act of 1972 in 1992.