---  
title: Koch Rajbongshis  
source: None  
date: 2021-02-12 21:13  
tags: status/mature  
aliases: []  
sr-due: 2022-01-25  
sr-interval: 17  
sr-ease: 270  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/history/culture #note/upsc/anthropology/peoples
# Koch Rajbongshis
- Community that traces its roots to *Kamata Kingdom (Rajvanshi)*
- Prominent in 13th Century
	- Bangladesh, West Bihar, North-East
- Currently:
	- Assam, Meghalaya, West Bengal and Bihar
		- Over 33 Lakh in WB
	- Bangladesh, Nepal and Bhutan