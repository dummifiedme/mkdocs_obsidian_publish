---  
---  
# Consanguineal Relationship
If a kin is related by blood, it is called as ==consanguineal== relationship.

##### Examples of consanguineal relationships
- Parent-children relationships
- Sibling relationships
- Adopted child (also considered biologically produced off spring; consanguineal kin)