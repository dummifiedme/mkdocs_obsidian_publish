---  
title: Borrowed Features of  Constitution  
source: None  
date: 2020-12-06 07:12  
tags: status/mature  
aliases: []  
sr-due: 2022-01-12  
sr-interval: 14  
sr-ease: 230  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity

---
**review**{: #review}{: .hash}  

# Borrowed Features of  Constitution



## Borrowed features from United Kingdom  
**flashcard**{: #flashcard}{: .hash}  

> 2/3rd of the framework of the constitution is based on [[Government of India Act 1935]].
- Nominal -> real head
- Cabinet System
- Parliamentary System
- Prime Minister
- Bicameral Legislature
- Parliamentary privileges
- Single Citizenship
- Rule of Law 
- Writs
- Speaker of LS
- Lower house more power
- Cabinet responsible to lower house
- [[Knowledge/Academics/UPSC CSE/First-Past-The-Post System]] <!--SR:!2021-12-30,1,227-->

---

## Borrowed features from USA  
**flashcard**{: #flashcard}{: .hash}  

- A part on the [[Fundamental Rights]]
- The post of the Vice President
- The process of removal of SC, HC, Judges
- The impeachment procedure of President.
- Independence of Judiciary
- The concept of [[Due Process of Law]]
- [[Judicial Review]]
	- Reading into -> expanding the amplitude of a plain **todo**{: #todo}{: .hash}  
/digdeeper ^b39530
	- read down -> reduce
- Authority of courts to review parliamentary and executive actions in terms of constitutionality and legality. <!--SR:!2021-12-30,1,227-->

---

## Borrowed features from Canada
**flashcard**{: #flashcard}{: .hash}  

- Federal setup with a strong centre
- Residuary powers with the union parliament
- Nomination of state's governors by centre
- Advisory jurisdiction of SC
<!--SR:!2022-01-14,16,230-->

---

## Borrowed features from Ireland  
**flashcard**{: #flashcard}{: .hash}  

- DPSP idea
- President message to the parliament
- Method of election of President
	- Method is also somewhat similar to the election of German President.
- Nomination of members to RS by President.
	- Senate of Ireland -> 60 members, 10 are nominated. <!--SR:!2021-12-30,1,230-->

---

## Borrowed features from South Africa
**flashcard**{: #flashcard}{: .hash}  

- Procedure of **amendment**
- Procedure to elect RS members <!--SR:!2021-12-30,1,227-->

---

## Borrowed features from Former USSR
**flashcard**{: #flashcard}{: .hash}  

- [[Fundamental Duties]]
- Idea of [[Socialist Systems|Socialism]] as part of DPSP
<!--SR:!2022-01-15,17,230-->

---

## Borrowed features from France
**flashcard**{: #flashcard}{: .hash}  

- Notion of the country being [[Republic]]
- The principles of liberty, equality and fraternity - [[French Revolution]]
<!--SR:!2022-02-27,60,250-->

---

## Borrowed features from Australia
**flashcard**{: #flashcard}{: .hash}  

- Idea of concurrent list
- Joint sitting of the two houses of parliament
- Freedom of trade and commerce throughout country <!--SR:!2021-12-30,1,230-->

---

## Borrowed features from Germany
**flashcard**{: #flashcard}{: .hash}  

- Emergency provision Weimar Constitution of Germany (1917-1919)
<!--SR:!2022-02-27,60,250-->