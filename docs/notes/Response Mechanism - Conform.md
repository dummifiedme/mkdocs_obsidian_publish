---  
title: Response Mechanism - Conform  
source: None  
date: 2021-04-17 08:24  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Response Mechanism - Conform
- majority of the animals and almost all the plants cannot maintain constant internal temperature (homeostasis)
- Conforming organisms >> [[Response Mechanism - Regulate]]
- Thermoregulation is energetically expensive for many organisms.
	- Heat Loss or Heat Gain is a function of surface area.

> Why do small animals aren't found in colder regions?
	> - Since their surface area to the volume ratio is high, they need more energy and heat than they can actually generate from that metabolism.