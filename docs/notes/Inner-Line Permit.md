---  
title: Inner-Line Permit  
source: None  
date: 2021-03-18 23:11  
tags: status/seed  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS2/polity 
# Inner-Line Permit
**flashcard**{: #flashcard}{: .hash}  

- Colonial-era concept of separating tribal areas from the plains.
	- North-East India mostly.
		- Arunachal Pradesh, Nagaland, Manipur and Mizoram
	- In Uttarakhand as well - Niti Valley and Nelong Valley
- Origin : Bengal Eastern Frontier Regulation Act (BEFR), 1873
- Recently, scrapped in Ladakh region.
- [[Citizenship Amendment Act (CAA)]] is not applicable in the ILP regions.