---  
title: Continental Drift Theory  
source: None  
date: 2021-02-12 22:19  
tags: status/draft  
aliases: []  
---  
**note**{: #note}{: .hash}  
/upsc/GS1/geography 
# Continental Drift Theory
- Theory given by :: Alfred Wagner <!--SR:!2022-03-05,60,250-->

## Forces for Continental Drift
According to Wagner:
**flashcard**{: #flashcard}{: .hash}  

- Pole Fleeing Force - moves away from poles
- Buoyant Force - floats
- Tidal Force
- Gravitational Force <!--SR:!2022-01-13,9,210-->