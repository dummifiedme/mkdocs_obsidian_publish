---  
title: Organism and its Environment  
source: NCERT XI Chemistry Chapter 13  
date: 2021-04-17 06:56  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Organism and its Environment

- Different organisms are adapted to their environment in terms of not only survivals but also reproduction
- The variations together with the temperature and precipitation account for the formation of major biomes.
	![[biome distribution wrt temp and precipitation.png]]
> Even our intestines is a unique habitat for hundreds of species of microbes.
- [[Major Abiotic Factors]]
- [[Responses to Abiotic Factors]]
- [[Adaptation]]