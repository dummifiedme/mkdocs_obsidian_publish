---  
title: Faiz Ahmed Faiz on the martyrdom of Bhagat Singh  
date: 2021-08-21 10:31  
tags: status/seed note/general/literature/shayari  
aliases: []  
---  
# Faiz Ahmed Faiz on the martyrdom of Bhagat Singh

[[Faiz on Bhagat Image]]

Jis dhaj se koi maktal mein gaya,
Wo Shaan salamat rehti hai,
Ye jaan to aani jaani hai,
Is jaan ki koi baat nahi.
— Faiz Ahmed Faiz