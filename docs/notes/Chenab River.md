---  
title: Chenab River  
source: None  
date: 2021-03-20 00:39  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Chenab River
**flashcard**{: #flashcard}{: .hash}  

- Originates at the confluence of Chandra and Bhaga rivers
	- Lahaul and Spiti District of Himachal Pradesh.
- Flows through JK, Pakistan.