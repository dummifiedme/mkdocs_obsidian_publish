---  
title: Salaries and Allowances for MPs  
source: None  
date: 2021-03-11 15:50  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Salaries and Allowances for MPs

## Salaries and Pensions for MPs
(according to constitution)
**flashcard**{: #flashcard}{: .hash}  

- ✅ ***Salaries*** - entitled to receive - as much as parliament decides.
- ❌ ***Pension*** - not mentioned in the constitution - *Parliament, however, provided it*
> Salaries, Allowances and Pension of Members of Parliament Act, 1954

## Salaries and Pensions of Speaker, Deputy Speaker of LS and Chairman, deputy Chairman of RS
(decided by/charged on)
**flashcard**{: #flashcard}{: .hash}  

Decided by Parliament - but *charged on Consolidated Fund of India* - not subjected to annual vote of Parliament.