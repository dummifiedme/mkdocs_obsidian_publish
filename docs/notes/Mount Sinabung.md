---  
title: Mount Sinabung  
source: None  
date: 2021-03-18 23:02  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Mount Sinabung
**flashcard**{: #flashcard}{: .hash}  

- Located in Karo regency, North Sumatra, Indonesia.
- A part of [[Pacific Ring of Fire]].
![[Mount Sinabung Map.png]]