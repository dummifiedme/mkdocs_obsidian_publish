---  
---  
# COVID-19 management by other federal countries
**review**{: #review}{: .hash}  
/upsc/mains
## Canada
- Emergency Act 1988 and Emergency Management Act 2007.
- Most provisions already had health acts that elaborate the measures to be employed.
- Handled at the local level with coordination from the states and centre.


## Australia
- National Health Security Act, 2007
- processes and structures are already present for pre-empt, prevention and dealing with national health emergencies.
- Death at local level by the coordination of states and centre.