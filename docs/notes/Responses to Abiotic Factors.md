---  
title: Responses to Abiotic Factors  
source: None  
date: 2021-04-17 07:47  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Responses to Abiotic Factors

- Organism try to maintain the constancy of its internal environment - ==Homeostasis==
	![[Organismic response - diagram.png]]
- Possible Mechanisms for response
	- [[Response Mechanism - Regulate]]
	- [[Response Mechanism - Conform]]
	- [[Response Mechanism - Migrate]]
	- [[Response Mechanism - Suspend]]