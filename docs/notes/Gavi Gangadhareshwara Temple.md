---  
title: Gavi Gangadhareshwara Temple  
date: 2021-06-11 12:00  
alias: []  
tags: status/seed  
---  
**upsc**{: #upsc}{: .hash}  
/prelims 
# Gavi Gangadhareshwara Temple
**flashcard**{: #flashcard}{: .hash}  

- Bengaluru, Karnataka.
- Sun rays fall directly on the Shiva linga on Makar Sankranti.
	- This year, clouds.
- Suryabhishek through horns of Nandi.
- Mostly Monolithic
- Kempegowda-I built it.
![[Gavi Gangadhareshwara Temple.png]]