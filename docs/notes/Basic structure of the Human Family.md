---  
---  
# Basic structure of the Human Family
---
title: Basic structure of the Human Family
source: [[Family]]
date:  2020-12-19 01:57
---

### Basic structure of the Human Family
tags:

- **What is the difference between a mating relationship and conjugal relationship?** **QnA**{: #QnA}{: .hash}  

    - Mating relationship is just sexual relationship
    - Conjugal relationship is institutionalized mating relationship established by Family.

> Without ==conjugal== relationship, there cannot be an establishment of a family.