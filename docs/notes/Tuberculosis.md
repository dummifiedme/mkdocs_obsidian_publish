---  
title: Tuberculosis  
source: None  
date: 2021-03-20 00:34  
tags: status/seed  
aliases: []  
---  
Category::  

---

# Tuberculosis

## India's Initiatives for TB
**flashcard**{: #flashcard}{: .hash}  

- Committed to eliminating TB in the country by 2025
	- Five years ahead of global deadline 2030
- National Strategic Plan for TB Elimination (2017-2025)
- [[Nikshay Poshan Yojana]]
- TB Harega Desh Jeetega Campaign - to expand the reach of TB care services by 2022.