---  
title: legumes  
source: None  
date: 2021-05-29 19:56  
aliases: []  
tags: status/seed  
---  
**note**{: #note}{: .hash}  
/upsc/GS3/environment
# legumes
**flashcard**{: #flashcard}{: .hash}  

A type of plant when used as dried grain is known as [[pulses]]